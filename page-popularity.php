<?php
/*
Template Name: Popularity
*/
require_once 'classes/thirdparty/Statistics.php';
 $stats = new Statistics();

$stats->getTotalEntries();
if ( get_post_status ( get_the_ID() ) == 'publish' ) {
get_header(); ?>
<div class="page-title">

         <p><?php the_title(); ?></p>
   </div><!-- End Page title -->

<div id="rw_blog_wrapper"><!--white background-->
   
   <!-- Start main content -->
   <!--<div class="container main-content clearfix">-->
    
    <!--<div class="sixteen columns">-->

    <div class="rampworld_mainbody">
    	<div class="rampworld_mainbody_content">
            <pre><?php //print_r($stats->sessions);?></pre>
            <div class="chart">
                <canvas id="popularity__chart" width="400" height="200"></canvas>
            </div>
             <div class="history_graph" style="margin: 0 auto; text-align:center">
                <canvas id="history__normal" width="400" height="400" style="display:inline-block; max-width: 90vw;margin: 0 auto;"></canvas><canvas id="history__holiday" width="400" height="400" style="display: inline-block;max-width: 90vw;margin: 0 auto;"></canvas>
            </div>
        <?php
             // Start Loop
            while (have_posts()) : the_post();
                                    
            the_content();
                                        
            endwhile;
            // End Loop
            ?>
            <div class="clearfix"></div>
    	</div>
    </div>

    
   <!--</div>--><!-- <<< End Container >>> -->

 </div><!-- end rw_blog_wrapper --> 
 <script src="http://www.rampworldcardiff.co.uk/wp-content/plugins/membership/js/node_modules/chart.js/dist/Chart.min.js"></script>
	<script>
		var entries_context = document.getElementById("history__normal").getContext('2d');
		var entrires_chart = new Chart(entries_context, {
			type: 'line',
			data: {
				labels: <?php echo $stats->total_entries__labels;?>,
				datasets: [{
					label: 'No. of Attendees',
					data: <?php echo $stats->total_entries__data;?>,
					backgroundColor: 'rgba(33,161,225, 0.2)',
					borderColor: 'rgba(33,161,225,1)',
					borderWidth: 1
				}]
			},
			options: {
                retinaScale: true,
                responsive: false,
                maintainAspectRatio: true,
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						},
                        scaleLabel: {
                            display: true,
                            labelString: 'Average number of attendees'
                        }
					}],
                    xAxes:[{
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 0,
                        display: false
                    }
                    }]
				},
				title: {
					display: true,
					text: 'Term Time Average Attendees',

				},
				tooltips: {
					callbacks: {
						title: function(tooltipItem, chart) {
							return 'Date: ' + tooltipItem[0].xLabel;
						},
						label: function(tooltipItem, chart) {
							return 'Total of '  + tooltipItem.yLabel+ ' Entrires';
						},
						labelColor: function(tooltipItem, chart) {
							return {
								backgroundColor: 'rgba(33,161,225, 0.2)',
					            borderColor: 'rgba(33,161,225,1)',
							}
						}
					}
				}
			}
		});
        
        var holiday_context = document.getElementById("history__holiday").getContext('2d');
		var holiday_chart = new Chart(holiday_context, {
			type: 'line',
			data: {
				labels: <?php echo $stats->total_holiday__labels;?>,
				datasets: [{
					label: 'No. of Attendees',
					data: <?php echo $stats->total_holiday__data;?>,
                    backgroundColor: 'rgba(33,161,225, 0.2)',
					borderColor: 'rgba(33,161,225,1)',
					borderWidth: 1
				}]
			},
			options: {
                responsive: false,
                maintainAspectRatio: true,
				scales: {
					yAxes: [{
						ticks: {
                            labelString: 'Average number of entries',
							beginAtZero:true
						}
					}],
                    xAxes:[{
                    ticks: {
                        autoSkip: true,
                        maxTicksLimit: 0,
                        display: false
                    }
                    }]
				},
				title: {
					display: true,
					text: 'Holiday Average Attendees',

				},
				tooltips: {
					callbacks: {
						title: function(tooltipItem, chart) {
							return 'Date: ' + tooltipItem[0].xLabel;
						},
						label: function(tooltipItem, chart) {
							return 'Total of '  + tooltipItem.yLabel+ ' Entrires';
						},
						labelColor: function(tooltipItem, chart) {
							return {
								backgroundColor: 'rgba(33,161,225, 0.2)',
					            borderColor: 'rgba(33,161,225,1)',
							}
						}
					}
				}
			}
		});
	</script>
    
<?php }
get_footer(); ?>