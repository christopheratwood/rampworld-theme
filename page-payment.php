<?php
/** template name: booking processing*/

//requirements
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential as OAuthTokenCredential;

//payment Requirements
use PayPal\Api\Payer as Payer;
use PayPal\Api\Details as Details;
use PayPal\Api\Amount as Amount;
use PayPal\Api\Transaction as Transaction;
use PayPal\Api\Payment as Payment;
use PayPal\Api\RedirectUrls as RedirectUrls;

use PayPal\Api\Item as Item;
use PayPal\Api\ItemList as ItemList;

use Rampworld\Booking\Modify as Booking;

session_start();
if(!isset($_SESSION['cost']['total_cost'])) {
    wp_redirect('//www.rampworldcardiff.co.uk/online-booking/book');
    die();
}

require __DIR__ .'/modules/vendor/autoload.php';

//API context
$api = new ApiContext(
    new OAuthTokenCredential('AcVKOqjrlhvyTTUUI28bDTI3Xpl4hebkY8K_OOMlGULIlL1W4ZUx7DdEQpzvTfzEWaEvDidPGZb1_fg6', 'EJPUKzfmU_zneJaRhGtGZ3PvywRwA1PJspXUogaF_74mzgJVmVkxR8e9rg3ykyJmADuisIb2f7WuD7Ih')
);
$api->setConfig(array(
    'mode'                      => 'live',
    'http.ConnectionTimeOut'    => 30,
    'log.LogEnabled'            => false,
    'log.FileName'              => 'paypal_log.txt',
    'log.LogLevel'              => 'DEBUG',
    'validation.level'          => 'log'

));

//setup

// Payer
$payer = new Payer();
$payer->setPaymentMethod('paypal');

// Details
$details = new Details();
$details->setTax('0.00')
        ->setSubtotal($_SESSION['cost']['total_cost']);

// Amount
$amount = new Amount();
$amount->setCurrency('GBP')
        ->setTotal($_SESSION['cost']['total_cost'])
        ->setDetails($details);

// Transaction
$transaction = new Transaction();
$transaction = new Transaction();
$transaction->setDescription('RampWorld Cardiff online session booking. Session times: '.$_SESSION['session']['session_start_time'].' till ' . $_SESSION['session']['session_end_time'] . '. Session Date: '.$_SESSION['session']['session_date']);
$i = 0;
$items = array();
foreach($_SESSION['participants']['members'] as $members => $val) {
    $items[$i] = new Item();
    $items[$i]->setQuantity(1)
            ->setName($val['name'])
            ->setPrice($_SESSION['cost']['session_cost'])
            ->setCurrency('GBP');
    $i++;
}
$items[$i] = new Item();
$items[$i]->setQuantity(1)
          ->setName('Booking Fee')
          ->setPrice($_SESSION['cost']['booking'])
          ->setCurrency('GBP');


$itemList = new ItemList();
$itemList->setItems($items);

$transaction->setItemList($itemList);
$transaction->setAmount($amount);

// Payment
$payment = new Payment();
$payment->setIntent('sale')
        ->setPayer($payer)
        ->setTransactions(array($transaction));

// Redirect Urls
$redirectUrls = new RedirectUrls();
$redirectUrls->setReturnUrl('https://www.rampworldcardiff.co.uk/online-booking/book?stage=4&approved=true')
             ->setCancelUrl('https://www.rampworldcardiff.co.uk/online-booking/book?stage=4&booking=cancelled');
$payment->setRedirectUrls($redirectUrls);

try {
    $payment->create($api);

    //generate hash??

    $hash = password_hash($payment->getId(), PASSWORD_BCRYPT, array('cost' => 10, 'salt' => random_bytes(22)));
    $_SESSION['transaction']['pph'] = $hash;
    $_SESSION['transaction']['ppe'] = date('Y-m-d H:i:s', strtotime('+ 10 minute'));
    $_SESSION['transaction']['approved'] = true;
    $_SESSION['error_explained'] = false;
    $_SESSION['stages']['4_complete'] = true;
    
    /* Create temp record */
    $booking = new Booking(); 
    if($booking->create(array(
      'payment_id'  => $payment->getId(),
      'hash'        => $hash,
      'email'       => $_SESSION['booking']['email'],
      'name'        => $_SESSION['booking']['name'],
      'number'      => $_SESSION['booking']['number'], 
      'session_cost'=> number_format($_SESSION['cost']['total_cost'] - $_SESSION['cost']['booking'], 2), 
      'booking_cost'=> $_SESSION['cost']['booking']
    )) === false) {
      wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book?stage=4&failure=failed_to_save&err='.$db->last_error);
      die();
    }

} catch(PayPal\Exception\PayPalConnectionException $ex) {
  echo $ex->getCode(); // Prints the Error Code
  echo $ex->getData(); // Prints the detailed error message )
  die();

} catch(Execption $e) {
  header('Location: https://www.rampworldcardiff.co.uk/online-booking/book?failure=connection_execption');
  die();
}
foreach($payment->getLinks() as $link) {
  if($link->getRel() == 'approval_url') {
    wp_redirect($link->getHref());
    die();
  }
}