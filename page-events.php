<?php
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage RampWorld Cardiff
 * @since RampWorld Cardiff 1.0
 */
use Rampworld\Event\EventFetch as EventFetch;
use Rampworld\SEO\Meta as Meta;
use Rampworld\Calendar\ClosedDates as ClosedDates;
require __DIR__ .'/modules/vendor/autoload.php';
$cdn_host = 'https://res.cloudinary.com/rampworld-cardiff/image/upload';

$closed = new ClosedDates();

$event_id = get_query_var('events');
$event = new EventFetch();
if( !empty( $event_id ) ) {
	$event->getEvent( intval($event_id) );
	if($event->validEvent) {
		$event->format();
		$image_src = substr($event->details[0]['image_portrait'], strpos( $event->details[0]['image_portrait'], '/v'));
		$sizes = array(
			'lg' 	=> '/w_360,q_90',
			'sm'	=> '/w_345,q_90',
			'md'	=> '/w_293,q_90',
			'xs'	=> '/w_346,q_90',
			'ms'	=> '/w_450,q_90'
		);
		
		Meta::description($event->details[0]['description']);
		Meta::title($event->details[0]['display_name']);
		$showJumbotron = false;
		get_header();?>

		<div class="jumbotron">
			<h1><?php echo $event->details[0]['display_name'];?></h1>
		</div>
						
		<div class="row">
			<div class="col-md-4 col-xs-6 col-ms-12">
				<div class="event-image">
					<img style="width: 100%" src="<?php echo $cdn_host.$sizes['sm'].$image_src;?>" srcset="
						<?php echo $cdn_host . $sizes['lg'] . $image_src . ' 360w,'.
							$cdn_host.$sizes['md'].$image_src. ' 293w,'.
							$cdn_host.$sizes['sm'].$image_src. ' 345w,'.
							$cdn_host.$sizes['xs'].$image_src. ' 400w,'.
							$cdn_host.$sizes['ms'].$image_src. ' 450w';?>"
						sizes="(max-width: 480px) calc(100vw - 30px), (max-width: 768px) calc(50vw - 30px), (max-width: 992px) 345px, (max-width: 1199px) 293px, (min-width: 1200px) 360px">
				</div>
			</div>
			<div class="col-md-4 col-xs-6 col-ms-12">
				<?php echo $event->details[0]['description'];?>
			</div>
			<div class="col-md-4 col-xs-6 col-ms-12">
				<section class="related-content">
					<div class="related-content-title">
						<p class="lead">Event information</p>
					</div>
					<div class="related-content-content">
						<div class="row">
							<div class="col-md-12">
								<table class="list list-with-icon">
									<tr>
										<td><i class="far fa-calendar-alt fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
										<td><span><?php echo $event->details[0]['date'];?></span></td>
									</tr>
									<tr>
										<td><i class="far fa-clock fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
										<td><span><?php echo $event->details[0]['start_time'];?> - <?php echo $event->details['end_time'];?></span></td>
									</tr>
									<tr>
										<td><i class="fas fa-ticket-alt fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
										<td><span><?php echo (($event->details[0]['online_exclusive']) ? 'Tickets for this event <strong>must</strong> purchased online.': 'You can purchase tickets for this event online and instore');?></span></td>
									</tr>
									<tr>
										<td><i class="fas fa-bicycle fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
										<td><span>This event is only available for <?php echo $event->details[0]['discipline_exclusive'];?> riders.</span></td>
									</tr>
									<?php if($event->details[0]['beginner']):?>
										<tr>
											<td><i class="fas fa-ban fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
											<td><span>This event is <strong>only</strong> for beginners.</span></td>
										</tr>
									<?php endif;?>
									<tr>
										<td><i class="fas fa-pound-sign fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
										<td><span>£<?php echo $event->details[0]['cost'];?></span></td>
									</tr>
								</table>
								<?php if($event->details[0]['pass_exclusion']):?>
									<div class="alert alert-danger"><p>If you have a <strong>week pass holder</strong>, you must purchase a ticket.  Special events are not covered within week passes.</p></div>
								<?php endif;?>
								<?php if($event->details[0]['future-event'] && intval($event->statistics['booked']) < 170):?>
									<h3>Book here</h3>
									<p>There are still tickets available for this event! You are able to use our online booking facilities to guarantee your place for this event.  There are a limited amount of tickets.</p>
									<a href="https://www.rampworldcardiff.co.uk/online-booking/book/?date=<?php echo $event->details[0]['raw_date'];?>" class="btn btn-lg btn-primary btn-block">Online booking</a>
								<?php endif;?>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		
		<?php get_footer(); ?>

	<?php } else {
		get_header(); ?>
		<?php if ( (strlen(get_post_meta(get_the_ID(), 'Page Title', true)) > 0 ||  strlen(get_post_meta(get_the_ID(), 'Page Message', true)  > 0))) :?>
        	<div class="jumbotron">
				<?php if (strlen(get_post_meta(get_the_ID(), 'Page Title', true)) > 0) :?><h1><?php echo get_post_meta(get_the_ID(), 'Page Title', true);?></h1><?php endif;?>
				<?php if (strlen(get_post_meta(get_the_ID(), 'Page Message', true)) > 0) :?><p><?php echo get_post_meta(get_the_ID(), 'Page Message', true);?></p><?php endif;?>
        	</div>
        	<?php echo $closed->get_alert();?>
      	<?php endif;?>
		<div class="c c-no-results">
			<div class="icon">
				<i class="far fa-frown fa-10x"></i>
				<h1>This event does not exist.</h1>
				<p>Looks like you couldn't find the event you were looking for? Please use our navigation to help.</p>
			</div>
		</div>

	<?php get_footer(); ?>
<?php }
} else {
	$event->getAllEvents();
	$event->format();
	$eventsHTML = '';
	
	$sizes = array(
		'lg' 	=> '/w_360,q_90',
		'sm'	=> '/w_345,q_90',
		'md'	=> '/w_293,q_90',
		'xs'	=> '/w_346,q_90',
		'ms'	=> '/w_450,q_90'
	);
	
	foreach($event->details as $evt) {
		$image_src = substr($evt['image_portrait'], strpos( $evt['image_portrait'], '/v'));
		$imageHTML = '
		<img style="width: 100%; height: auto;" src="' . $cdn_host.$sizes['sm'] . $image_src.'" srcset="' . 
			$cdn_host . $sizes['lg'] . $image_src . ' 360w,'.
			$cdn_host . $sizes['md'] . $image_src . ' 293w,'.
			$cdn_host . $sizes['sm'] . $image_src . ' 345w,'.
			$cdn_host . $sizes['xs'] . $image_src . ' 400w,'.
			$cdn_host . $sizes['ms'] . $image_src . ' 450w'.'"
			sizes="(max-width: 480px) calc(100vw - 30px), (max-width: 768px) calc(50vw - 30px), (max-width: 992px) 345px, (max-width: 1199px) 293px, (min-width: 1200px) 360px">';
		$eventsHTML .= '
			<div class="col-md-4 col-sm-6 col-xs-6 col-ms-12">
				<div class="card ">
					<div class="card-image ">
						' . $imageHTML . '
						<h2 class="card-title">' . $evt['display_name'] . '</h2>
					</div>
					<div class="card-action ">
						<table class="list list-with-icon">
							<tbody>
								<tr>
									<td><i class="far fa-calendar-alt fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
									<td><span>' . $evt['date'] . '</span></td>
								</tr>
								<tr>
									<td><i class="far fa-clock fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
									<td><span>' . $evt['start_time'] . ' - ' . $evt['end_time'] . '</span></td>
								</tr>
								<tr>
									<td><i class="fas fa-pound-sign fa-2x" style="color:#21a1e1; width: 30px;"></i></td>
									<td><span>£' . $evt['cost'] .'</span></td>
								</tr>
							</tbody>
						</table>
						<a class="btn btn-default btn-block" href="https://www.rampworldcardiff.co.uk/events/'.$evt['event_id'].'/'.str_replace(' ', '-', $evt['display_name']).'" style="margin: 0px">View event</a>
					</div>
				</div>
			</div>';
	}

	get_header();
	?>
	<?php if ( (strlen(get_post_meta(get_the_ID(), 'Page Title', true)) > 0 ||  strlen(get_post_meta(get_the_ID(), 'Page Message', true)  > 0))) :?>
		<div class="jumbotron">
			<?php if (strlen(get_post_meta(get_the_ID(), 'Page Title', true)) > 0) :?><h1><?php echo get_post_meta(get_the_ID(), 'Page Title', true);?></h1><?php endif;?>
			<?php if (strlen(get_post_meta(get_the_ID(), 'Page Message', true)) > 0) :?><p><?php echo get_post_meta(get_the_ID(), 'Page Message', true);?></p><?php endif;?>
		</div>
		<?php echo $closed->get_alert();?>
	<?php endif;?>
	<div class="row">
		<?php if(strlen($eventsHTML) == 0):?>
			<div class="c c-no-results">
				<div class="icon">
					<i class="far fa-frown fa-10x"></i>
					<h1>No events were found.</h1>
					<p>Please refine come back soon for more events!</p>
				</div>
			</div>
		<?php else:?>
			<?php echo $eventsHTML;?>
		<?php endif;?>
	</div>
	<?php get_footer(); 
}