<?php
use Rampworld\Booking\Fetch as Fetch;
use Rampworld\Form\Booking as Form;
session_start();
require __DIR__ .'/modules/vendor/autoload.php';
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @since BirdFILED 1.0
 */

if(isset($_GET['completed']) && isset($_SESSION['transaction']['pph'])) {
		$details = $_SESSION;
		$booking = new Fetch();
		$booking_ids = $booking->getIDs($_SESSION['transaction']['pph']);
		$details['booking_id'] = $booking_ids['id'];
    session_destroy();
} else {
    wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book/');
    die();
}


$form = new Form();
get_header();
?> 
</div>
<div class="status" id="booking_progress">
	<ul><li>	<a class="complete">
				<i class="registration_icon fa fa-calendar" aria-hidden="true"></i>
				<span class="content">Session Details</span>
				<span class="tablet_content">Session</span>
			</a>
		</li>

		<li>
		<a class="complete">
				<i class="registration_icon fa fa-info" aria-hidden="true"></i>
				<span class="content">Booking Details</span>
				<span class="tablet_content">Booking</span>
			</a>
		</li>
		<li>
		<a class="complete">
				<i class="registration_icon fa fa-users" aria-hidden="true"></i>
				<span class="content">Participants Details</span>
				<span class="tablet_content">Participants</span>
			</a>
		</li>
		<li>
			<a class="complete">
				<i class="registration_icon fa fa-check" aria-hidden="true"></i>
				<span class="content">Confirm</span>
				<span class="tablet_content">Confirm</span>
			</a>
		</li>
		<li>
			<a class="complete">
				<i class="registration_icon fa fa-payment" aria-hidden="true"></i>
				<span class="content">Payment</span>
				<span class="tablet_content">Payment</span></a>
		</li>
	</ul>
</div>
</div>
</div>

<div class="container center">
		<form action="#" method="post" id="booking-form" >
				<?php echo $form->getSection(5 , array(), $details);?>

		</form>

				
</div>

<?php get_footer();