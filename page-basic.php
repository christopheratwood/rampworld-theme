<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- If you delete this meta tag, Half Life 3 will never be released. -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>RampWorld Cardiff New Member</title>
<style type="text/css">
	/* ------------------------------------- 
		GLOBAL 
------------------------------------- */
@font-face {
  font-family: 'FontAwesome';
  src: url("http://www.rampworldcardiff.co.uk/wp-content/themes/birdfield/font-awesome/fontawesome-webfont.eot?v=4.1.0");
  src: url("http://www.rampworldcardiff.co.uk/wp-content/themes/birdfield/font-awesome/fontawesome-webfont.eot?#iefix&v=4.1.0") format("embedded-opentype"), url("../font-awesome/fontawesome-webfont.woff?v=4.1.0") format("woff"), url("../font-awesome/fontawesome-webfont.ttf?v=4.1.0") format("truetype"), url("font-awesome/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular") format("svg");
  font-weight: normal;
  font-style: normal; }
* { 
	margin:0;
	padding:0;
}
* { font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif; }

img { 
	max-width: 100%; 
}
.collapse {
	margin:0;
	padding:0;
}
body {
	-webkit-font-smoothing:antialiased; 
	-webkit-text-size-adjust:none; 
	width: 100%!important; 
	height: 100%;
	color: #666;
}

span.membershipNumber {
  display: block;
  margin: 20px auto;
  font-size: 64px;
  text-align: center;
}
/* ------------------------------------- 
		ELEMENTS 
------------------------------------- */
a { color: #2BA6CB;}

.btn {
	text-decoration:none;
	color: #FFF;
	background-color: #666;
	padding:10px 16px;
	font-weight:bold;
	margin-right:10px;
	text-align:center;
	cursor:pointer;
	display: inline-block;
}

p.callout {
	padding:15px;
	background-color:#ECF8FF;
	margin-bottom: 15px;
}
.callout a {
	font-weight:bold;
	color: #2BA6CB;
}

table.social {
/* 	padding:15px; */
	background-color: #ebebeb;
	
}
.social .soc-btn {
	padding: 3px 7px;
	font-size:12px;
	margin-bottom:10px;
	text-decoration:none;
	color: #FFF;font-weight:bold;
	display:block;
	text-align:center;
}
a.fb { background-color: #3B5998!important; }
a.ig { background-color: #125688!important; }

.sidebar .soc-btn { 
	display:block;
	width:100%;
}

/* ------------------------------------- 
		HEADER 
------------------------------------- */
table.head-wrap { width: 100%;}

.header.container table td.logo { padding: 15px; }
.header.container table td.label { padding: 15px; padding-left:0px;}


/* ------------------------------------- 
		BODY 
------------------------------------- */
table.body-wrap { width: 100%;}
table th {
	width: 300px;
}
table tr th p {
	font-style: normal !important;
}
table tr td p {
	font-style: italic;
}

/* ------------------------------------- 
		FOOTER 
------------------------------------- */
table.footer-wrap { width: 100%;	clear:both!important;
}
.footer-wrap .container td.content  p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}
.footer-wrap .container td.content p {
	font-size:10px;
	font-weight: bold;
	
}


/* ------------------------------------- 
		TYPOGRAPHY 
------------------------------------- */
h1,h2,h3,h4,h5,h6 {
font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;color: #21a1e1; text-align: center; color: #21a1e1;
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }

h1 { font-weight:200; font-size: 44px;}
h2 { font-weight:200; font-size: 37px;}
h3 { font-weight:500; font-size: 27px;}
h4 { font-weight:500; font-size: 23px;}
h5 { font-weight:500; font-size: 17px; text-align: center; margin: 20px auto;}
h6 { font-weight:500; font-size: 17px; color: white; }

.collapse { margin:0!important;}

p, ul { 
	margin-bottom: 10px; 
	font-weight: normal; 
	font-size:14px; 
	line-height:1.6;
}
p.lead { font-size:17px; }
p.last { margin-bottom:0px;}

ul li {
	margin-left:5px;
	list-style-position: inside;
}

/* ------------------------------------- 
		SIDEBAR 
------------------------------------- */
ul.sidebar {
	background:#ebebeb;
	display:block;
	list-style-type: none;
}
ul.sidebar li { display: block; margin:0;}
ul.sidebar li a {
	text-decoration:none;
	color: #666;
	padding:10px 16px;
/* 	font-weight:bold; */
	margin-right:10px;
/* 	text-align:center; */
	cursor:pointer;
	border-bottom: 1px solid #777777;
	border-top: 1px solid #FFFFFF;
	display:block;
	margin:0;
}
ul.sidebar li a.last { border-bottom-width:0px;}
ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}



/* --------------------------------------------------- 
		RESPONSIVENESS
		Nuke it from orbit. It's the only way to be sure. 
------------------------------------------------------ */

/* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
.container {
	display:block!important;
	max-width:600px!important;
	margin:0 auto!important; /* makes it centered */
	clear:both!important;
}

/* This should also be a block element, so that it will fill 100% of the .container */
.content {
	padding:15px;
	max-width:600px;
	margin:0 auto;
	display:block; 
}

/* Let's make sure tables in the content area are 100% wide */
.content table { width: 100%; }


/* Odds and ends */
.column {
	width: 300px;
	float:left;
}
.column tr td { padding: 15px; }
.column-wrap { 
	padding:0!important; 
	margin:0 auto; 
	max-width:600px!important;
}
.column table { width:100%;}
.social .column {
	width: 280px;
	min-width: 279px;
	float:left;
}

/* Be sure to place a .clear element after each set of columns, just to be safe */
.clear { display: block; clear: both; }


/* ------------------------------------------- 
		PHONE
		For clients that support media queries.
		Nothing fancy. 
-------------------------------------------- */
@media only screen and (max-width: 600px) {
	
	a[class="btn"] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}

	div[class="column"] { width: auto!important; float:none!important;}
	
	table.social div[class="column"] {
		width:auto!important;
	}

}
footer{
  background-color: #292c2f;
  box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.12);
  box-sizing: border-box;
  width: 100%;
  text-align: left;
  font: bold 16px sans-serif;

  padding: 55px 50px;
  margin-top: 80px;
}

footer .footer-left,
footer .footer-center,
footer .footer-right{
  display: inline-block;
  vertical-align: top;
}

/* Footer left */

footer .footer-left{
  width: 40%;
}

/* The company logo */

footer h3{
  color:  #ffffff;
  font: normal 36px 'Cookie', cursive;
  margin: 0;
}

footer h3 span{
  color:  #5383d3;
}

/* Footer links */

footer .footer-links{
  color:  #ffffff;
  margin: 20px 0 12px;
  padding: 0;
}

footer .footer-links a{
  display:inline-block;
  line-height: 1.8;
  text-decoration: none;
  color:  inherit;
   transition: color 200ms ease-in-out;
}

footer .footer-links a:hover{
  color: #21a1e1;
}

footer .footer-company-name{
  color:  #8f9296;
  font-size: 14px;
  font-weight: normal;
  margin: 0;
}

/* Footer Center */

footer .footer-center{
  width: 35%;
}
footer .footer-center p{
  display: inline-block;
  color: #ffffff;
  vertical-align: middle;
  margin:0;
}

footer .footer-center p span{
  display:inline-block;
  font-weight: normal;
  font-size:14px;
  line-height:2;
}

footer .footer-center p a{
  color:  #5383d3 !important;
  text-decoration: none;;
}


/* Footer Right */

footer .footer-right{
  width: 20%;
}

footer .footer-company-about{
  line-height: 20px;
  color:  #92999f;
  font-size: 13px;
  font-weight: normal;
  margin: 0;
}

footer .footer-company-about span{
  display: block;
  color:  #ffffff;
  font-size: 14px;
  font-weight: bold;
  margin-bottom: 20px;
}

footer .footer-icons{
  margin-top: 25px;
}

footer .footer-icons a{
  display: inline-block;
  width: 35px;
  height: 35px;
  cursor: pointer;
  background-color:  #33383b;
  border-radius: 2px;

  color: #ffffff;
  text-align: center;
  line-height: 35px !important;
  font-size: 20px  !important;
  margin-right: 3px;
  margin-bottom: 5px;
  text-decoration: none;
  vertical-align: top;
  transition: color 200ms ease-in-out;
}
footer .footer-icons .fa-facebook:before {
  content: "\f09a";
  }
footer .footer-icons .fa-instagram:before {
  content: "\f16d";
}

/* If you don't want the footer to be responsive, remove these media queries */

@media (max-width: 880px) {

  footer{
    font: bold 14px sans-serif;
  }

  footer .footer-left,
  footer .footer-center,
  footer .footer-right{
    display: block;
    width: 100%;
    margin-bottom: 40px;
    text-align: center;
  }

  footer .footer-center i{
    margin-left: 0;
  }

}
footer .footer-center p.second_address_line {
  margin-left: 68px;
  margin-top: -15px;
  display: block;
  font-size: 16px;
}
footer .footer-icons a, 
footer p.fa a,
footer p.fa {
  display: inline-block;
  font: normal normal normal 14px/1 FontAwesome;
  font-size: inherit;
  text-rendering: auto;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  color: white;

}

p.fa-map-marker:before {
  display: inline-block;
  background-color:  #33383b;
  color: #ffffff;
  font-size: 25px;
  width: 38px;
  height: 38px;
  border-radius: 50%;
  text-align: center;
  line-height: 42px;
  margin: 10px 15px;
  vertical-align: middle;
  content: "\f041";
  transition: color 200ms ease-in-out;
  
}

p.fa-envelope:before {
  display: inline-block;
  background-color:  #33383b;
  color: #ffffff;
  font-size: 25px;
  width: 38px;
  height: 38px;
  border-radius: 50%;
  text-align: center;
  line-height: 42px;
  margin: 10px 15px;
  vertical-align: middle;
  content: "\f0e0";
  font-size: 17px;
  line-height: 38px;
  transition: color 200ms ease-in-out;
}

footer .footer-link a:hover,
footer .footer-icons a:hover,
footer .footer-center p:hover:before{
  color: #21a1e1;
}
</style>
</head>
 
<body bgcolor="#FFFFFF">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#999999">
	<tr>
		<td></td>
		<td class="header container" >
				
				<div class="content">
				<table bgcolor="#999999">
					<tr>
						<td><img src="http://www.rampworldcardiff.co.uk/wp-content/uploads/2016/06/footer_logo.png" /></td>
						<td align="right"><h6 class="collapse blue">Welcome to RampWorld Cardiff!</h6></td>
					</tr>
				</table>
				</div>
				
		</td>
		<td></td>
	</tr>
</table><!-- /HEADER -->
<!-- BODY -->
<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">

			<div class="content">
			<table>
				<tr>
					<td>
						<h3>Hi, <?php echo $details['forename'] . ' ' . $details['surname'];?></h3>
						<p class="lead">Thank you for registering for the RampWorld Cardiff Membership. Below is all your details.</p>
						<p>Note: Please keep your RampWorld Cardiff Membership Number safe as you will need to provide this number when you come to RampWorld Cardiff.</p>
						<p>We recommend saving the number to your phone and/ or keeping a copy in bag.</p>
						<span class="membershipNumber">00001</span>
						<!-- Callout Panel -->
						<h5 class="">Personal Details</h5>
						<table width="100%">
							<tr>

								<th>				
									<p class="heading">Forename:</p>
								</th>
								<td>
									<p><?php echo $details['forename'];?></p>
								</td>
							</tr>
							<tr>
								<th>				
									<p class="heading">Surname:</p>
								</th>
								<td>
									<p><?php echo $details['form_surname'];?></p>
								</td>
							</tr>
							<tr>
								
								<th>				
									<p class="heading">Date of Birth:</p>
								</th>
								<td>
									<p><?php echo $details['form_dob'];?></p>
								</td>
							</tr>
							<tr>
								<th>				
									<p class="heading">Telephone Number:</p>
								</th>
								<td>
									<p><?php echo $details['form_number'];?></p>
								</td>
							</tr>
							<tr >
								
								<th>				
									<p class="heading">Email Address</p>
								</th>
								<td>
									<p><?php echo $details['form_email_address'];?></p>
								</td>
							
							</tr>
						</table>
						<h5>Address</h5>
						<table>
							<tr>
								
								<th>				
									<p class="heading">Address Line One:</p>
								</th>
								<td>
									<p><?php echo $details['address_line_one'];?></p>
								</td>
							</tr>
							<tr>
								<th>				
									<p class="heading">Address Line Two:</p>
								</th>
								<td>
									<p><?php echo $details['address_line_two'];?></p>
								</td>
							</tr>
							<tr>
								
								<th>				
									<p class="heading">City:</p>
								</th>
								<td>
									<p><?php echo $details['adress_city'];?></p>
								</td>
							</tr>
								<th>				
									<p class="heading">County:</p>
								</th>
								<td>
									<p><?php echo $details['address_city'];?></p>
								</td>
							</tr>
							<tr>
								
								<th>				
									<p class="heading">Postcode:</p>
								</th>
								<td>
									<p><?php echo $details['address_postcode'];?></p>
								</td>
							</tr>
							
						</table>
					<?php if($details['parentalVis'] == true){?>		
						<h5>Parental Consent</h5>
						<table>
							<tr>
								
								<th>				
									<p class="heading">Parental / Guardian Name:</p>
								</th>
								<td>
									<p><?php echo $details['form_consent_name'];?></p>
								</td>
							</tr>
							<tr>
								<th>				
									<p class="heading">Parental / Guardian Number:</p>
								</th>
								<td>
									<p><?php echo $details['form_consent_number'];?></p>
								</td>
							</tr>
							<tr>
								<th>				
									<p class="heading">Parental / Guardian Email:</p>
								</th>
								<td>
									<p><?php echo $details['form_consent_email'];?></p>
								</td>
							</tr>
							<tr>
								
								<th>				
									<p class="heading">Parental / Guardian Address Line One:</p>
								</th>
								<td>
									<p><?php echo $details['consent_address_line_one'];?></p>
								</td>
							</tr>
							<tr>
								<th>				
									<p class="heading">Parental / Guardian Postcode:</p>
								</th>
								<td>
									<p><?php echo $details['consent_address_postcode'];?></p>
								</td>
							</tr>

						</table>
					<?php }?>
						<h5>About Yourself</h5>
						<table>
							<tr>
								
								<th>				
									<p class="heading">Discipline (s):</p>
								</th>
								<td>
									<p><?php echo $details['sport'];?></p>
								</td>
							</tr>
							<tr>
								<th>				
									<p class="heading">Expertise:</p>
								</th>
								<td>
									<p><?php
									switch($details['expertise']) {
								      	case 0:
								      		echo 'Beginner';
								      		break;
								      	case 1:
								      		echo 'Novice';
								      		break;
								      	case 2:
								      		echo 'Experienced';
								      		break;
								      	case 3:
								      		echo 'Advanced';
								      		break;	
								      	case 4:
								      		echo 'Expert';
								      		break;
								      	case 5:
								      		echo 'Professional';
								      		break;
								      }?></p>
								</td>
							</tr>
						</table>
						<h5>Emergency Contact Details</h5>
						<table>
							
							<tr>
								<th>				
									<p class="heading">Full Name:</p>
								</th>
								<td>
									<p><?php echo $details['form_emergency1_name'];?></p>
								</td>
							</tr>
							<tr>
								
								<th>				
									<p class="heading">Number:</p>
								</th>
								<td>
									<p><?php echo $details['form_emergency1_number'];?></p>
							</tr>
							<tr>
								<th>				
									<p class="heading">Full Name:</p>
								</th>
								<td>
									<p><?php echo $details['form_emergency2_name'];?></p>
								</td>
							</tr>
							<tr>
								
								<th>				
									<p class="heading">Number:</p>
								</th>
								<td>
									<p><?php echo $details['form_emergency2_number'];?></p>
								</td>
							</tr>
						</table>
						<h5>Medical Notes</h5> 
						<p><?php echo $details['form_medical_notes'];?></p>
					
						
					</td>
				</tr>
			</table>
			</div><!-- /content -->
									
		</td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<footer class="footer-wrap">
	<div class="footer-left">

		<img src="http://www.rampworldcardiff.co.uk/wp-content/uploads/2016/06/footer_logo.png" alt="RampWorld Cardiff Footer Logo">

		<p class="footer-links">
			<a href="http://www.rampworldcardiff.co.uk">Home</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/news">News</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/park">The Park</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/about">About Us</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/session">Opening</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/membership">Membership</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/shop">The Shop</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/contact">Contact Us</a>
		</p>

		<p class="footer-company-name">RampWorld Cardiff &copy; 2016</p>
		<p class="footer-company-name">Registed Charity Number 1152842</p>
	</div>

	<div class="footer-center">

		<div>
			<p class="fa fa-map-marker">RampWorld Cardiff, Unit B, Park Ty-Glas</p>
			<p class="second_address_line"> Llanishen, Cardiff</p>
		</div>

		<div>
			<p class="fa fa-envelope email" ><a href="mailto:&#114;&#097;&#109;&#112;&#119;&#111;&#114;&#108;&#100;&#099;&#097;&#114;&#100;&#105;&#102;&#102;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;">&#114;&#097;&#109;&#112;&#119;&#111;&#114;&#108;&#100;&#099;&#097;&#114;&#100;&#105;&#102;&#102;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;</a></p>
		</div>

	</div>

	<div class="footer-right">

		<p class="footer-company-about">

			<span>About RampWorld Cardiff</span>
			RampWorld Cardiff  is run to provide an indoor recreational facility for modern wheeled sports in Cardiff, South Wales for young people with the object of improving their condition of life and in particular to provide for such persons facilities for BMX cycling, scooters, skateboarding and other such activities in a safe indoor environment.
		</p>

		<div class="footer-icons">

			<a href="http://www.facebook.com/RampWorldCardiff" class="fa-facebook" target="_blank" ></a>
			<a href="https://www.instagram.com/rampworldcardiff" class="fa fa-instagram" target="_blank" ></a>

		</div>

	</div>
</footer><!-- /FOOTER -->

</body>
</html>