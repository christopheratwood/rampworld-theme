<?php
/* 
Template Name: Archives
*/
?>
<?php get_header(); ?>
   
   <div class="page-title">
     <div class="container clearfix">
       
       <div class="sixteen columns"> 
         <h1><?php the_title(); ?></h1>
       </div>
       
     </div><!-- End Container -->
   </div><!-- End Page title -->
   
<div id="rw_blog_wrapper"><!--white background-->
 
   <!-- Start main content -->
   <div class="container main-content clearfix">
   
     <!-- Start Posts -->
     <div class="eleven columns bottom-3">
     
<?php
if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <!-- ==================== Post ==================== blog page-->
      <div class="post style-1 bottom-2">
      
      <h3 class="title bottom-1 blog_title"><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h3><!-- Title Post -->
      
      <div class="post-meta bottom-1">
        <div class="meta"><i class="icon-time"></i> <?php the_time('d M, Y') ?> </div><!-- Date -->
        <div class="meta"><i class="icon-user"></i> <?php echo the_author_meta('nickname', get_the_author_meta( 'ID' ) ); ?> </div><!-- Author -->
        <div class="meta"><i class="icon-tags"></i> <?php the_category(', '); ?> </div><!-- Category -->
        <!--<div class="meta"><i class="icon-comments"></i> <?php //comments_number() ?> </div>--><!-- Comments -->
        <div class="meta"><i class="icon-eye-open"></i> <?php countviews( get_the_ID() ); ?> </div><!-- Comments -->
      </div><!-- End post-meta -->
      
      
	<div class="post-content">
        <p><?php the_content(); ?></p>
       <!-- <a href="<?php echo get_permalink(); ?>" class="button small color">Read More</a>-->
      </div><!-- End post-content -->
      
     </div> 
     <!-- ==================== End  ==================== -->
     
<?php endwhile; else: ?>
Page Not Found
<?php endif; ?>

     
     <!-- Start Pagination -->
     <?php pagination(); ?>
     <!-- End pagination -->
     
   </div><!-- End Posts -->  
   
   
   <!-- Start Sidebar Widgets -->
   <?php get_sidebar(); ?>
   <div class="clearfix"></div>
   <!-- End Sidebar Widgets -->
    
   </div><!-- <<< End Container >>> -->
   
  </div><!-- end rw_blog_wrapper --> 
  
<?php get_footer(); ?>