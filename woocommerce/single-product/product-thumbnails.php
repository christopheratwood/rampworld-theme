<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $post, $product;
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$product_image   = wp_get_attachment_image_src( $post_thumbnail_id, $thumbnail_size )[0];

$product_image_path = substr($product_image, strpos( $product_image, '/v'));

$cdn_host = 'https://res.cloudinary.com/rampworld-cardiff/image/upload';
$sizes = array(
	'lg' 	=> '/w_146',
	'sm'	=> '/w_125',
	'md'	=> '/w_162',
	'xs'	=> '/w_117',
	'ms'	=> '/w_86'
);
if ( $attachment_ids && has_post_thumbnail() ) {
$attachment_ids = $product->get_gallery_image_ids();
$html = '<div id="product-gallery-thumbnails">
						<img class="active" data-image-path="'.$product_image_path.'" src="'.$cdn_host.$sizes['lg'].$product_image_path.'" sizes="(max-width: 86px) 112px, (max-width: 768px) 117px, (max-width: 970px) 162px, (max-width: 1199px) 125px, (min-width: 1200px) 146px" srcset="
							'.$cdn_host . $sizes['lg'] . $product_image_path . ' 146w,'.
							$cdn_host.$sizes['md'].$product_image_path. ' 125w,'.
							$cdn_host.$sizes['sm'].$product_image_path. ' 162w,'.
							$cdn_host.$sizes['xs'].$product_image_path. ' 117w,'.
							$cdn_host.$sizes['ms'].$product_image_path. ' 86w"/>';


	
 	$i = 1;
	foreach ( $attachment_ids as $attachment_id ) {
		$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
		$path = substr($full_size_image[0], strpos( $full_size_image[0], '/v'));

		$html  .= '<img class="" data-image-path="'.$path.'" src="'.$cdn_host.$sizes['thumbnail'].$path.'" sizes="(max-width: 480px) 86px, (max-width: 768px) 112px, (max-width: 970px) 117px, (max-width: 1199px) 162px, (min-width: 1200px) 146px"/>';
		$i++;
	}
	$html .= '</div>';	
}

echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );