<?php
use Rampworld\AssetFinder\SVG as SVG;

require_once __dir__.'/../../../../themes/rampworld/modules/vendor/autoload.php';
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;
$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$thumbnail_size    = apply_filters( 'woocommerce_product_thumbnails_large_size', 'full' );
$post_thumbnail_id = get_post_thumbnail_id( $post->ID );
$full_size_image   = wp_get_attachment_image_src( $post_thumbnail_id, $thumbnail_size );
$placeholder       = has_post_thumbnail() ? 'with-images' : 'without-images';
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . $placeholder,
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
) );

$product_image_path = substr($full_size_image[0], strpos( $full_size_image[0], '/v'));

$cdn_host = 'https://res.cloudinary.com/rampworld-cardiff/image/upload';
$sizes = array(
	'lg' 	=> '/w_598',
	'sm'	=> '/w_455',
	'md'	=> '/w_800',
	'xs'	=> '/w_455',
	'ms'	=> '/w_800'
);
?>
<div class="col-sm-6 col-xs-6 col-ms-12">
	<div id="product-gallery">
		<?php
		add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
		$attributes = array(
			'title'                   => get_post_field( 'post_title', $post_thumbnail_id ),
			'data-caption'            => get_post_field( 'post_excerpt', $post_thumbnail_id ),
			'data-src'                => $full_size_image[0],
			'data-large_image'        => $full_size_image[0],
			'data-large_image_width'  => $full_size_image[1],
			'data-large_image_height' => $full_size_image[2],
		);

		if($attributes['data-src'] == null) {
			$html = SVG::get('no product');
		} else {
			$html  = '<img id="primary" data-image-path="'.$product_image_path.'" src="'.$cdn_host.$sizes['small'].$product_image_path.'" srcset="
			'.$cdn_host . $sizes['lg'] . $product_image_path . ' 700w,'.
				$cdn_host.$sizes['sm'].$product_image_path. ' 550w,'.
				$cdn_host.$sizes['xs'].$product_image_path. ' 420w,'.
				$cdn_host.$sizes['ms'].$product_image_path. ' 420w" sizes="(max-width: 480px) 100vw, (max-width: 768px) 400px, (max-width: 970px) 352px, (max-width: 1199px) 460px, (min-width: 1200px) 570px">';
		}
		
		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, get_post_thumbnail_id( $post->ID ) );

		do_action( 'woocommerce_product_thumbnails' );?>
	<?php ?>
	</div>
</div>
