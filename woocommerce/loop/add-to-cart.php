<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
echo '<div class="pull-right-lg pull-right-md pull-right-xs">';
echo apply_filters( 'woocommerce_loop_add_to_cart_link',
sprintf( '<a href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s" style="margin-left: 0px">%s</a>',
	esc_url( get_permalink( $product->get_id ) ),
	esc_attr( isset( $quantity ) ? $quantity : 1 ),
	esc_attr( $product->get_id() ),
	esc_attr( $product->get_sku() ),
	esc_attr( 'btn btn-default btn-sm-block btn-ms-block' ),
	esc_html( 'View' )
),
$product );

if($product->add_to_cart_text() != 'View') {
	echo apply_filters( 'woocommerce_loop_add_to_cart_link',
		sprintf( '<a href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
			esc_url( $product->add_to_cart_url() ),
			esc_attr( isset( $quantity ) ? $quantity : 1 ),
			esc_attr( $product->get_id() ),
			esc_attr( $product->get_sku() ),
			esc_attr( isset( $class ) ? $class : 'btn btn-primary btn-sm-block product-name' ),
			esc_html( $product->add_to_cart_text() )
		),
	$product );
}
echo '</div>';