<?php
session_start();
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Exception\PayPalConnectionException;

use Rampworld\Booking\Modify as ModifyBooking;
use Rampworld\Booking\Fetch as FetchBooking;
use Rampworld\Paid\Session\Modify as Session;
use Rampworld\Paid\Pass\Modify as Pass;
use Rampworld\Email\Send as Email;
require __DIR__ .'/modules/vendor/autoload.php';



//API context
$api = new ApiContext(
  new OAuthTokenCredential('AcVKOqjrlhvyTTUUI28bDTI3Xpl4hebkY8K_OOMlGULIlL1W4ZUx7DdEQpzvTfzEWaEvDidPGZb1_fg6', 'EJPUKzfmU_zneJaRhGtGZ3PvywRwA1PJspXUogaF_74mzgJVmVkxR8e9rg3ykyJmADuisIb2f7WuD7Ih')
);
$api->setConfig(array(
  'mode'                      => 'live',
  'http.ConnectionTimeOut'    => 30,
  'log.LogEnabled'            => false,
  'log.FileName'              => 'paypal_log.txt',
  'log.LogLevel'              => 'DEBUG',
  'validation.level'          => 'log'

));

// check if approved
if(isset($_SESSION['transaction']['approved'])){

  if($_SESSION['transaction']['approved'] == true) {

    if($_SESSION['transaction']['ppe'] >= date('Y-m-d H:i:s')) {
      //get payer_ID

      //get the payment_id
      $fetchBooking = new FetchBooking();
      $booking = $fetchBooking->getIDs($_SESSION['transaction']['pph']);
      if($booking !== false) {
        $payment = Payment::get($booking['payment_id'], $api);
        
        $exec = new PaymentExecution();
        $exec->setPayerId($_SESSION['transaction']['ppp']);
        try{
          $payment->execute($exec, $api);

          //update db
          $modifyBooking = new ModifyBooking();
          if($modifyBooking->update($booking['payment_id'], array(
            'complete' => 1,
            'purchased' => date('Y-m-d H:i:s')
          ))) {
            $date = new \DateTime(date('d-m-Y', strtotime($_SESSION['session']['session_date'])));

            if($_SESSION['session']['session_type'] != 'weekpass'){
              $session = new Session();
              foreach($_SESSION['participants']['members'] as $member => $value) {
                $session->create($value['member_id'], $booking['id'], $date->format('Y-m-d'), $_SESSION['session']['session_start_time'], $_SESSION['session']['session_end_time'], $_SESSION['cost']['session_cost']);
              }
            } else {
              $pass = new Pass();
              foreach($_SESSION['participants']['members'] as $member => $value) {
                $pass->create($value['member_id'], 1, $purchased, $_SESSION['session']['session_start_time'], $_SESSION['session']['session_end_time'], 'BOOK', $pid[0]['id']);
              }
            }
          }

          $data = array(
            'booking_id'    => $booking['id'],
            'booking'       => $_SESSION['booking'],
            'participants'  => array(
              'members'     => $_SESSION['participants']['members']
            ),
            'session'       => $_SESSION['session'],
            'cost'          => $_SESSION['cost']
          );
          $data['booking']['completed_date'] = date('Y-m-d H:i:s');
          $mailer = new Email( $data );
          if( $mailer->create_booking($booking['id'] )) {

            wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book/payment/complete?completed=true&pph='.$_SESSION['transaction']['pph']);
            die();
          } else {
            wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book/payment/complete?completed=true&issue=failed_send');
            die();
          }
        } catch(PayPal\Exception\PayPalConnectionException $ex) {
          unset($_SESSION);
          wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book?stage=1&failure=already_paid');
          die();
        
        } catch( Execption $e) {
          wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book?stage=4&failure=transaction_failed');
          die();
        }
      } else {
        wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book?stage=4&failure=server_failure');
      }
   
      
    } else {
      unset($_SESSION);
      wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book?stage=4&failure=expired');
      die();
    }
  } else {
    wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book?failure=transaction_failed');
    die();
  }
}
