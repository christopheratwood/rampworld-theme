<?php

use Rampworld\Timetable\Timetable as Timetable;
use Rampworld\Timetable\NonOptionalDates as NonOptionalDates;

require_once 'modules/vendor/autoload.php';
/*
Template Name: Opening Hours
*/
	$timetable = new Timetable();	
?>
<?php get_header(); ?>

<?php echo $timetable->render();?>
<?php while (have_posts()) : the_post();
	the_content();
endwhile; ?>

<?php get_footer(); ?>