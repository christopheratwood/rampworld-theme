declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {

	const config = {
		selector: '#search'
	};
	const init = function(): void {
    addActiveClass();
        
    $( config.selector ).on('keyup', function( evt:any ) {
        evt.preventDefault();
        addActiveClass();
    });
    $( config.selector ).on('keydown', function (this:any, e: any) {

      let keycode: number = e.keyCode || e.which;
      if (keycode == 13) {
        $(this).parent().submit();
      }
    });
	};
	const addActiveClass = function(): void {

    if($( config.selector ).val().length > 0) {
      $( config.selector ).parent().parent().addClass('has_input');
    } else {
      $( config.selector ).parent().parent().removeClass('has_input');
    }
	};
	return {
		init: init
	}
})