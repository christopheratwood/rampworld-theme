declare var define: RequireDefine;
declare var $: any;

define(['jquery', 'rwcui.register.validation'], function ($: any, validation: any) {

	const init = function (): void {
		$('#form-next').on('click', function (e) {

			e.preventDefault();
			checkStage($(this).data('current'), $(this).data('next'));
		});
		$('#form-prev').on('click', function (e) {
			e.preventDefault();
			checkStage($(this).data('current'), $(this).data('next'));
		});
	};
	const checkStage = function (current: number, next: number) {
		switch (current) {

			case 1:
				validation.validate('personal', $('#form_forename'), { 'length': [1, 100], 'characters': 'letters' }, null, true, true);
				validation.validate('personal', $('#form_surname'), { 'length': [1, 100], 'characters': 'letters' }, null, true, true);
				validation.validate('personal', $('#form_email_address'), { 'length': [1, 255], 'email': true }, null, true, true);
				validation.validate('personal', $('#form_number'), { 'length': [10, 16], 'characters': 'numbers' }, null, true, true);
				validation.validate('personal', null, { 'date': true }, null, true, true);

				if (validation.calcErrors() == 0)
					$('#registration-form').attr('action', '?stage=' + next).submit();

				break;
			case 2:

				validation.validate('address', $('#form_address_line_one'), { 'length': [1, 50] }, null, true, true);

				validation.validate('address', $('#form_address_line_two'), { 'length': [0, 50] }, null, true, true);

				validation.validate('address', $('#form_address_city'), { 'length': [0, 50] }, null, true, true);

				validation.validate('address', $('#form_address_county'), { 'length': [0, 50] }, null, true, true);

				validation.validate('address', $('#form_address_postcode'), { 'postcode': true }, null, true, true);

				if (validation.calcErrors() == 0) {
					$('#registration-form').attr('action', '?stage=' + next).submit();
				}
				break;
			case 3:
				validation.validate('parental', $('#form_consent_name'), { 'length': [2, 100], 'characters': 'letters' }, null, true, true);

				validation.validate('parental', $('#form_consent_number'), { 'length': [10, 16], 'characters': 'numbers' }, null, true, true);

				validation.validate('parental', $('#form_consent_address_line_one'), { 'length': [1, 50] }, null, true, true);

				validation.validate('parental', $('#form_consent_address_postcode'), { 'postcode': true }, null, true, true);

				validation.validate('parental', $('#form_consent_email'), { 'length': [1, 255], 'email': true }, null, true, true);

				if (validation.calcErrors() == 0) {
					$('#registration-form').attr('action', '?stage=' + next).submit();
				}
				break;
			case 4:
				validation.validate('about', $('#form_sport'), { 'multiple': true, 'other': 'ifSelected', 'requires': true }, 'Sport Activities', true, true);
				if (validation.calcErrors() == 0) {

					$('#registration-form').attr('action', '?stage=' + next).submit();
				}
				break;
			case 5:

				validation.validate('emergency', $('#form_emergency1_name'), { 'characters': 'letters', 'length': [3, 100] }, 'First Emergency Name', true, true);
				validation.validate('emergency', $('#form_emergency1_number'), { 'characters': 'numbers', 'length': [10, 1000] }, 'First Emergency Number', true, true);

				validation.validate('emergency', $('#form_emergency2_name'), { 'characters': 'letters', 'length': [0, 100] }, 'Second Emergency Name', true, true);
				validation.validate('emergency', $('#form_emergency2_number'), { 'characters': 'numbers', 'length': [0, 16] }, 'Second Emergency Number', true, true);


				validation.validate('medical', $('#form_medical_notes'), { 'length': [0, 1000] }, null, true, true);

				if (validation.calcErrors() == 0) {
					$('#registration-form').attr('action', '?stage=' + next).submit();
				}
				break;
			case 6:
				validation.validate('tandcs', $('#tandc'), { 'checked': true }, 'Terms and Conditions', true, true);
				validation.validate('tandcs', $('#form_captcha'), { 'characters': 'letters', 'length': [2, 4] }, 'Security check', true, true);

				if (validation.calcErrors() == 0) {
					$('#registration-form').attr('action', '?stage=7').submit();
				}
				break;
		}
	}
	return {
	init: init,
	checkStage
	}
});