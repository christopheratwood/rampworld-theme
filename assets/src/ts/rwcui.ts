declare var $: any;
declare var require:Require;
interface root {
	live: string,
	dev: string
}

let roots: root = {
	live: '//www.rampworldcardiff.co.uk/wp-content/themes/rampworld/assets/dist/thirdparty/',
	dev: '//127.0.0.1/wp-content/themes/rampworld/assets/dist/thirdparty/'
}
const version: number = 1.6;
require.config({
	paths: {
		jquery: roots.live + 'js/jquery.min',
		bootstrap: roots.live + 'js/bootstrap.min',
		simpleselect: roots.live + 'js/simpleselect.min',
		cookieC: roots.live + 'js/cookie.min',
		moment: roots.live + 'js/moment.min',
		mmenu: roots.live + 'js/jquery.mmenu.all',
		ui: roots.live + 'js/jquery-ui.min', 
		pips: roots.live + 'js/jquery-ui-slider-pips',
		fullcalandar: roots.live + 'js/fullcalandar.min',
		fancybox: roots.live + '/js/jquery.fancybox.min',
		elevate: roots.live + '/js/jquery.elevateZoom.min'

	},
	shim: {
		bootstrap: { deps: ['jquery'] },
		simpleselect: { deps: ['jquery'] },
		impromptu: { deps: ['jquery'] },
		datatable: { deps: ['jquery', 'bootstrap'] },
		ui: { deps: ['jquery'] },
		fullcalandar: { deps: ['jquery'] },
		pips: { deps: ['ui'] },
		fancybox: { deps: ['jquery']}
	},
	waitSeconds: 200,


});
require(['jquery', 'bootstrap', 'mmenu', 'rwcui.cookie'], function ($: any, boot: any, mmenu: any, cookie: any) {
	$("#wpadminbar")
		.css("position", "fixed")
		.addClass("mm-slideout");

	let $menu = $("#menu").first().clone(),
		$button = $("#navigationToggle");

	var $selected = $menu.find("li.current-menu-item");
	var $vertical = $menu.find("li.Vertical");
	var $dividers = $menu.find("li.Divider");

	$menu.children().not("ul").remove();
	$menu.add($menu.find("ul, li"))
		.removeAttr("class")
		.removeAttr("id");

	$menu.addClass("wpmm-menu");

	$selected.addClass("Selected");
	$vertical.addClass("Vertical");
	$dividers.addClass("Divider");

	$menu.mmenu(
		{
			counters: true,
			extensions: ["shadow-page", "effect-slide-menu"],
			iconPanels: true,
			offCanvas: {
				zposition: "front"
			},
			navbar: {
				add: false
			},
			navbars: [
				{
					height: 3,
					content: [`
						<div class="wpmm-header-image">
							<!DOCTYPE svg  PUBLIC '-//W3C//DTD SVG 1.1//EN'  'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>
							<svg class="mobile-logo" preserveAspectRatio="xMinYMin meet" width="206.04" height="95" version="1.1" viewBox="-1 -1.0000052892507796 210.04121971765812 99.00052796095714" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
							<defs>
							<path id="a" d="m46.99 65c0.5 0 15.01 28.79 15.01 29.89 0 0.18-30 0.12-30 0 0-1 14.5-29.89 14.99-29.89z"/>
							<path id="k" d="m65 65h14.7c4.85 0 11.2 2.29 11.53 8.42s-6.72 10.16-7.27 10.51c-0.4 0.26 3.09 3.95 10.44 11.07h-14.7-14.7v-15-15z"/>
							<path id="f" d="m131 66h7.5 7.5v14.5 14.5h-7.5-7.5v-14.5-14.5z"/>
							<path id="d" d="m178.94 66h27.1v4.84 6.68h-2.15-3.27v3.42 4.81h-1.73-3.69v4.62 4.63h-8.13-8.13v-29z"/>
							<path id="m" d="m0 32.39h15 15v15.05 14.95l-7.76-7.24-7.76-7.24-7.24 6.86-7.24 6.95v-14.66-14.67z"/>
							<path id="e" d="m98 32.39h12.29l0.21 19.77 13.36 0.27v9.96h-25.86v-30z"/>
							<path id="g" d="m126.62 32.41c7.65-0.03 11.81-0.03 12.47 0 10.7 0.29 19.04 7.59 19.04 15.09 0 7.49-8.94 14.89-19.04 14.89h-12.47v-14.99-14.99z"/>
							<path id="n" d="m65.24 0.6c0.25 0 5.15 4.47 14.69 13.4 9.66-9.33 14.62-14 14.88-14 0.25 0 0.25 10 0 30h-29.57c-0.32-19.6-0.32-29.4 0-29.4z"/>
							<path id="h" d="m98 0h15c6.39 0 9.69 2.91 9.89 8.74 0.2 5.82-3.09 9.25-9.89 10.26v11h-15v-30z"/>
							<path id="o" d="m149 66h27.1v4.84 6.68h-2.15-3.27v3.42 4.81h-1.74-3.68v4.62 4.63h-8.13-8.13v-29z"/>
							<path id="l" d="m98 65.02c7.29-0.03 11.24-0.03 11.87 0 10.19 0.29 18.13 7.59 18.13 15.09 0 7.49-8.51 14.89-18.13 14.89h-11.87v-14.99-14.99z"/>
							<path id="p" d="m62 47.39c0 8.28-6.72 15-15 15s-15-6.72-15-15 6.72-15 15-15 15 6.72 15 15z"/>
							<path id="j" d="m65 32.39h14.7c4.85 0 11.2 2.29 11.53 8.42s-6.72 10.16-7.27 10.51c-0.4 0.25 3.09 3.94 10.44 11.07h-14.7-14.7v-15-15z"/>
							<path id="c" d="m46.99 0c0.5 0 15.01 28.79 15.01 29.89 0 0.18-30 0.12-30 0 0-1 14.5-29.89 14.99-29.89z"/>
							<path id="b" d="m0 0h14.7c4.85 0 11.2 2.29 11.53 8.42s-6.72 10.16-7.27 10.51c-0.4 0.26 3.09 3.95 10.44 11.07h-14.7-14.7v-15-15z"/>
							<path id="i" d="m30 94.98c-7.29 0.03-11.24 0.03-11.87 0-10.19-0.29-18.13-7.59-18.13-15.09 0-7.49 8.51-14.89 18.13-14.89h11.87v14.99 14.99z"/>
							</defs>
							<use fill="#ffffff" xlink:href="#a"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#a"/>
							<use fill="#ffffff" xlink:href="#k"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#k"/>
							<use fill="#ffffff" xlink:href="#f"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#f"/>
							<use fill="#ffffff" xlink:href="#d"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#d"/>
							<use fill="#ffffff" xlink:href="#m"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#m"/>
							<use fill="#ffffff" xlink:href="#e"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#e"/>
							<use fill="#ffffff" xlink:href="#g"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#g"/>
							<use fill="#ffffff" xlink:href="#n"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#n"/>
							<use fill="#ffffff" xlink:href="#h"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#h"/>
							<use fill="#ffffff" xlink:href="#o"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#o"/>
							<use fill="#ffffff" xlink:href="#l"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#l"/>
							<use fill="#ffffff" xlink:href="#p"/>
							<use fill="#ffffff" xlink:href="#j"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#j"/>
							<use fill="#ffffff" xlink:href="#c"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#c"/>
							<use fill="#ffffff" xlink:href="#b"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#b"/>
							<use fill="#ffffff" xlink:href="#i"/>
							<use fill-opacity="0" stroke="#000000" stroke-opacity="0" xlink:href="#i"/>
							</svg>
						</div>`]
				}, {
					content: ["searchfield"]
				},
				{
					"position": "bottom",
					"content": [
						"<a href='/contact'><i class=\"fa fa-envelope\" aria-hidden=\"true\"></i></a>",
						"<a href='https://www.instagram.com/rampworldcardiff'><i class=\"fab fa-instagram\" aria-hidden=\"true\"></i></a>",
						"<a href='http://www.facebook.com/RampWorldCardiff'><i class=\"fab fa-facebook\" aria-hidden=\"true\"></i></a>"
					]
				}
			],
			screenReader: true,
			keyboardNavigation: true
		}, {
			offCanvas: {
				pageSelector: "> div:not(#wpadminbar)"
			}
		}
	);

	$menu
		.find(".mm-listview")
		.find(".mm-next")
		.next()
		.filter("[href='#']")
		.prev()
		.addClass("mm-fullsubopen");

	var api = $menu.data("mmenu");

	$button
		.addClass("wpmm-button")
		.off("click")
		.on("click", function (e:any) {
			e.preventDefault();
			e.stopImmediatePropagation();
			api.open();
		});
	cookie.init();
	console.log('Core Loaded: ' + Date());
	console.log('UI version ' + version);

});

if (document.querySelector('#registration_progress') !== null) {
	require(['rwcui.register.submissions', 'rwcui.register.events'], function (submissions: any, events: any) {
		submissions.init();
		events.init();
	})
}

if (document.querySelector('.selectpicker') !== null) {
	require(['jquery', 'simpleselect'], function ($: any, search: any) {
		$('.selectpicker').simpleselect();
	})
};

if (document.querySelector('#double-label-slider') !== null) {
	require(['jquery', 'ui', 'pips'], function ($: any, ui: any, pips: any) {
		var labels = [

			"<span>Beginner</span>",
			"<span>Novice</span>",
			"<span>Experienced</span>",
			"<span>Advanced</span>",
			"<span>Expert</span>",
			"<span>Professional</span>"];


		$("#double-label-slider").slider({
			min: 0,
			max: 5,
			value: $('#expertise').val(),
			animate: 400
		}).slider('pips', {
			rest: "label",
			labels: labels

		});

	});
};


if (document.querySelector('#copyParticipant') !== null) {
	require(['rwcui.register.copy.emergency'], function (emergency: any) {

		emergency.init();
	})
}


if (document.querySelector('#copyParticipantNumber') !== null) {
	require(['rwcui.register.copy.number'], function (number: any) {

		number.init();
	})
}


if (document.querySelector('#copyParticipantEmail') !== null) {
	require(['rwcui.register.copy.email'], function (email: any) {

		email.init();
	})
}

if (document.querySelector('#copyParticipantAddress') !== null) {
	require(['rwcui.register.copy.address'], function (address: any) {

		address.init();
	})
}


/*store */
if( document.querySelector('#product-gallery') != null) {

	require(['rwcui.store.gallery'], function(gallery:any) {
		gallery.init();		

	});
}

/* Booking beginning */


if (document.querySelector('#findMember') !== null || document.querySelector('#findParticipant') !== null) {
	require(['rwcui.booking.member-lookup'], function (member: any) {
		member.init();
	});
}

if (document.querySelector('#calendar') !== null) {

	require(['rwcui.booking.session'], function (sessions: any) {
		sessions.init();
	});
}

if (document.querySelector('#booking_progress') !== null) {

	require(['rwcui.booking.submissions', 'rwcui.booking.events'], function (submissions: any, events: any) {

		submissions.init();
		events.init();
	})

}

if (document.querySelector('#session-times') !== null) {
	require(['jquery', 'datepicker'], function ($: any) {

		$('#start_time').datetimepicker({
			format: 'HH:mm'
		});
		$('#end_time').datetimepicker({
			format: 'HH:mm'
		});

	});

}



if (document.querySelector('.remove-participant-btn') !== null) {
	require(['jquery', 'bootstrap', 'rwcui.loader', 'rwcui.remove-participant'], function ($: any, bootstrap: any, loader: any, removeParticipant: any) {
		removeParticipant.init();
	});
}
if (document.querySelector('.table-sortable') !== null) {
	require(['jquery', 'datatable'], function ($: any, dt: any) {
		loadCss(roots.live + 'css/datatable.min.css');
		$('.table-sortable').DataTable({
			searching: false,
			paging: false,
			info: false,
			order: []
		});
	});
}

if (document.querySelector('#deleteMember') !== null) {
	require(['rwcui.delete-member'], function (deleteMember: any) {
		deleteMember.init();
	});
}

if (document.querySelector('#editDetails') !== null) {
	require(['jquery'], function ($: any) {
		$('#editDetails').on('click', function (this: any, e: any) {
			if ($(this).text() == 'Save changes') {
				$('#editForm').submit();
			} else {
				$(this).removeClass('btn-warning');
				$(this).addClass('btn-primary');
				$(this).text('Save changes');
				$('[readonly]').removeAttr('readonly');
				$('[disabled]').removeAttr('disabled');
				$(this).before('<a class="btn mg-10 btn-warning btn-ms-block" id="cancelEdit">Cancel</a>');
			}

		});

		$('body').on('click', '#cancelEdit', function (this: any, e: any) {

			$('#editDetails').removeClass('btn-primary');
			$('#editDetails').addClass('btn-warning');
			$('#editDetails').text('Change details');

			$('input.form-control ').attr('readonly', 'readonly');
			$('input[type="radio"].form-control ').attr('disabled', 'disabled');
			$('input[type="checkbox"].form-control ').attr('disabled', 'disabled');
			$('select ').attr('disabled', 'disabled');
			$('textarea.form-control ').attr('readonly', 'readonly');
			$(this).remove();
		});
	});
}

if (document.querySelector('#editOpening') !== null) {
	require(['rwcui.populate-checks'], function (populateChecks: any) {

		populateChecks.opening();
	});
};



if (document.querySelector('#editClosing') !== null) {
	require(['rwcui.populate-checks'], function (populateChecks: any) {
		populateChecks.closing();
	});
};


if (document.querySelector('.editNote') !== null) {
	require(['jquery'], function ($: any) {
		$('.editNote').on('click', function (this: any, e: any) {
			let note_id: string = $(this).data('note-id');
			if ($(this).text() == 'Save changes') {
				$('#editForm_' + note_id).submit();
			} else {
				$(this).removeClass('btn-warning');
				$(this).addClass('btn-primary');
				$(this).text('Save changes');
				$('#editForm_' + note_id + ' [readonly]').removeAttr('readonly');
				$('#editForm_' + note_id + ' [disabled]').removeAttr('disabled');
				$(this).before('<a class="btn btn-sm mg-10 btn-warning btn-ms-block cancelEdit" data-note-id="' + note_id + '">Cancel</a>');

			}

		});
		$('body').on('click', '.cancelEdit', function (this: any, e: any) {
			let note_id: string = $(this).data('note-id');
			$(this).remove();
			$('*[data-note-id="' + note_id + '"]').closest('.editNote').removeClass('btn-primary');
			$('*[data-note-id="' + note_id + '"]').addClass('btn-warning');
			$('*[data-note-id="' + note_id + '"]').text('Change');

			$('#editForm_' + note_id + ' input.form-control ').attr('readonly', 'readonly');
			$('#editForm_' + note_id + ' input[type="radio"].form-control ').attr('disabled', 'disabled');
			$('#editForm_' + note_id + ' input[type="checkbox"].form-control ').attr('disabled', 'disabled');
			$('#editForm_' + note_id + ' textarea.form-control ').attr('readonly', 'readonly');

		});
	});
}

if (document.querySelector('.editType') !== null) {
	require(['jquery'], function ($: any) {
		$('.editType').on('click', function (this: any, e: any) {
			let type_id: string = $(this).data('type-id');
			if ($(this).text() == 'Save changes') {
				$('#editForm_' + type_id).submit();
			} else {
				$(this).removeClass('btn-warning');
				$(this).addClass('btn-primary');
				$(this).text('Save changes');
				$('#editForm_' + type_id + ' [readonly]').removeAttr('readonly');
				$('#editForm_' + type_id + ' [disabled]').removeAttr('disabled');
				$(this).before('<a class="btn btn-sm mg-10 btn-warning btn-ms-block cancelEdit" data-type-id="' + type_id + '">Cancel</a>');

			}

		});
		$('body').on('click', '.cancelEdit', function (this: any, e: any) {
			let type_id: string = $(this).data('type-id');
			$(this).remove();
			$('*[data-type-id="' + type_id + '"]').closest('.editNote').removeClass('btn-primary');
			$('*[data-type-id="' + type_id + '"]').addClass('btn-warning');
			$('*[data-type-id="' + type_id + '"]').text('Change');

			$('#editForm_' + type_id + ' input.form-control ').attr('readonly', 'readonly');
			$('#editForm_' + type_id + ' input[type="radio"].form-control ').attr('disabled', 'disabled');
			$('#editForm_' + type_id + ' input[type="checkbox"].form-control ').attr('disabled', 'disabled');
			$('#editForm_' + type_id + ' textarea.form-control ').attr('readonly', 'readonly');

		});
	});
}


if (document.querySelector(".input-group-btn .dropdown-menu li a") !== null) {
	require(['jquery'], function ($: any) {

		$(".input-group-btn .dropdown-menu li a").click(function (this: any, e: any) {

			let selText: string = $(this).html();
			let element: string = $(this).parent().parent().siblings('button').data('element');
			let input: string = $(this).parent().parent().siblings('button').data('input');
			$(this).parents('.input-group-btn').find('.dropdown-toggle').html(selText + '  <span class="caret"></span>');
			$('#' + element).val(selText.toLowerCase());
			if (selText == 'Date of birth') {
				$('#' + input).attr('placeholder', 'DD/MM/YYYY').val();
				$('#' + input).attr('type', 'date').val('');
			} else if (selText == 'Phone number') {
				$('#' + input).attr('type', 'number').val('');
				$('#' + input).attr('placeholder', 'Please enter phone number').val();
			} else {
				$('#' + input).attr('type', 'text').val('');
				$('#' + input).attr('placeholder', 'Please enter ' + selText).val();
			}


		});
	})
}
if (document.querySelector('#removeHidden') !== null) {
	require(['jquery'], function ($: any) {
		$('#removeHidden').off().on('click', function (this: any, e: any) {
			if (document.querySelector(this).text() == 'Hide') {
				$('tr.visible').addClass('hidden');
				$('tr.visible').removeClass('visible');
			} else {
				$('tr.hidden').addClass('visible');
				$('tr.hidden').removeClass('hidden');
				$(this).text('Hide');
			}
		});
	});
}
//store//


if (document.querySelector('#product-slider') !== null) {
	require(['jquery', 'ui', 'pips'], function ($: any, ui: any, pips: any) {
		$("#product-slider")                          
        .slider({ 
			min: 0, 
            max: $('#products-max-price').val(), 
            range: true, 
			values: [0, $('#products-max-price').val()]
			
        })                
        .slider("pips", {
			rest: "label",
			step: $('#products-total').val()
        })            ;
	});
};

if (document.querySelector('#search') !== null) {
	require(['rwcui.store.search'], function (search: any) {
		search.init();
	});
}

// Account
if (document.querySelector('#form_username') !== null) {
	require(['rwcui.account'], function (account: any) {

		account.init();
	})
}
function loadCss(url:string) {
	let link: any = document.createElement("link");
	link.type = "text/css";
	link.rel = "stylesheet";
	link.href = url;
	document.getElementsByTagName("head")[0].appendChild(link);
}