declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {

	const capitalise = function (name: string): void {
		let words = $('#' + name).val().split(/[\s-]+/);
		let text = $('#' + name).val().toLowerCase();
		$('#' + name).val(text);
	};
	const formHelpContent = function (header: string, message: string): void {
		$('#form_help').html('<b style="margn-bottom:20px; display:block;">' + header + '</b><br>' + message);
	};
	const checkSideBarLocation = function (): void {

		if ($(window).width() < 768 && !$('#form_content').hasClass('fixed-top')) {
			let side_bar: any = $('#side-bar').clone();
			$('#side-bar').remove();
			$('#form_content').before(side_bar).addClass('fixed-top');
		}
		if ($(window).width() >= 768 && $('#form_content').hasClass('fixed-top')) {
			let side_bar: any = $('#side-bar').clone();
			$('#side-bar').remove();
			$('#form_content').after(side_bar).removeClass('fixed-top');
		}
	};
	const calcuateAge = function (element: any): any {

		let today = new Date();
		let birthday: any = new Date(element.split('/')[2], element.split('/')[1], element.split('/')[0]);
		let differenceInMilisecond: number = today.valueOf() - birthday.valueOf();

		let year_age: number = Math.floor(differenceInMilisecond / 31536000000);

		if (year_age < 0)
			return false
		else
			return year_age


	};
	return {
		capitalise: capitalise,
		formHelpContent: formHelpContent,
		checkSideBarLocation: checkSideBarLocation,
		calcuateAge: calcuateAge
	}
})