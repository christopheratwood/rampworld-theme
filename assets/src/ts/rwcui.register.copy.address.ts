declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {
	const init = function (): void {

		$('#copyParticipantAddress').on('click', function () {

			$('#form_consent_address_line_one').val($('#copyParticipantAddress').data('address-line-one'));
			$('#form_consent_address_postcode').val($('#copyParticipantAddress').data('address-postcode'));
		});
	}
	return {
		init: init
	}
});