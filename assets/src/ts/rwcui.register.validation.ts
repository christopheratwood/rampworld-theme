declare var define: RequireDefine;
declare var $: any;

define(['jquery', 'moment', 'rwcui.functions'], function ($: any, moment: any, functions: any) {
	let formErrors: any = {
		'personal': [],
		'address': [],
		'parental': [],
		'emergency': [],
		'about': [],
		'verifcation': [],
		'medical': [],
		'tandcs': []
	};
	let complete: any = {
		'personal': {
			'forename': false,
			'surname': false,
			'dob': false,
			'email_address': false,
			'number': false
		},
		'address': {
			'address_line_one': false,
			'address_line_two': null,
			'address_city': null,
			'address_county': null,
			'address_postcode': false
		},
		'parental': {
			'consent_name': null,
			'consent_number': null,
			'consent_address_line_one': null,
			'consent_address_postcode': null,
			'consent_email': null,
			'consent_accept': null
		},
		'about': {
			'sport': false
		},
		'emergency': {
			'emergency1_name': false,
			'emergency1_number': false,
			'emergency2_name': null,
			'emergency2_number': null,
		},
		'medical': {
			'medical_notes': null
		},
		'tandcs': {
			'tandcs': false,
			'capcha': false
		}
	};

	const init = function (): void {

	};
	const validate = function (section: string, element: any, rules: any, cusId: number, reOrderTop: boolean, next: any = null) {
		cusId = cusId || null;
		let reorder: boolean = reOrderTop || false;
		let errors: any = [];
		let id: string = '';
		let name: string = '';


		$.each(rules, function (key, value) {

			if (key != 'date')
				name = ((cusId != null) ? cusId : element.attr('id').split('form_')[1].replace(/_/g, ' ').replace(/\b\w/g, l => l.toUpperCase()))
			id = (key == 'date') ? '#dob_error' : '#' + element.attr('id').split('form_')[1] + '_error';

			if (key == 'length') {

				if (element.val().length < value[0]) {

					errors.push([element.prop('id'), name + ' is required.']);
					addError(section, name.replace('#', '') + ' must be greater than ' + value[0] + ' characters.');

				} else {
					removeError(section, name + ' must be greater than ' + value[0] + ' characters.');
				}
				if (element.val().length > value[1]) {
					errors.push([element.prop('id'), name + ' must be less than ' + value[1] + ' characters.']);
					addError(section, name + ' must be less than ' + value[1] + ' characters.');
				} else {
					removeError(section, name + ' must be less than ' + value[1] + ' characters.');
				}
			}
			if (key == 'characters') {
				if (value == 'letters') {
					if (!/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/.test(element.val())) {
						errors.push([element.prop('id'), name + ' must only contains letters and spaces.']);
						addError(section, name + ' must only contains letters and spaces.');
					} else {
						removeError(section, name + ' must only contains letters and spaces.');
					}

				} else {
					if (value == 'numbers') {
						if (!/^[0-9]*$/.test(element.val())) {
							errors.push([element.prop('id'), name + ' must only contains numbers.']);
							addError(section, name + ' must only contains numbers.');
						} else {
							removeError(section, name + ' must only contains numbers.');
						}
					}
				}
			}
			if (key == 'email') {
				if (!$.trim(element.val()).match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)) {
					errors.push([element.prop('id'), name + ' must a valid email address.']);
					addError(section, name + ' must a valid email address.');
				} else {
					removeError(section, name + ' must a valid email address.');
				}
			}
			if (key == 'postcode') {

				if (element.val().length < 6 || element.val().length > 9) {
					errors.push([element.prop('id'), name + ' must be a valid postcode']);
					addError(section, name + ' must be a valid postcode.');
				} else {
					element.val(element.val().replace(' ', ''));
					element.val(element.val().replace(' ', '').substring(0, element.val().length - 3) + " " + element.val().replace(' ', '').substring(element.val().length - 3));
					removeError(section, name + ' must be a valid postcode.');
				}
			}
			if (key == 'date') {
				if (($('#form_dob_day').val() == null || $('#form_dob_day').val().length > 2 || $('#form_dob_day').val().length == 0) || $('#form_dob_day').val() < 1 || ($('#form_dob_month').val() == null || $('#form_dob_month').val().length == 0) || ($('#form_dob_year').val() == null || $('#form_dob_year').val().length == 0)) {
					errors.push(['form_dob_day', 'Please provide a valid Date of Birth.']);
					addError(section, 'Please provide a valid Date of Birth.');
				} else {
					removeError(section, 'Please provide a valid Date of Birth.');
					let birthDate: any = moment($('#form_dob_year').val() + '-' + $('#form_dob_month').val() + '-' + $('#form_dob_day').val(), 'YYYY-MM-DD', true).isValid();

					let today: any = new Date();
					if (!birthDate) {
						errors.push(['form_dob_day', 'Please provide a valid Date of Birth.']);
						addError(section, 'Please provide a valid Date of Birth.');
					} else {
						removeError(section, 'Please provide a valid Date of Birth.');

						let age: number = functions.calcuateAge($('#form_dob_day').val() + '/' + $('#form_dob_month').val() + '/' + $('#form_dob_year').val());
						if (age > 100) {
							errors.push(['form_dob_day', 'You cant be over 100 years old.']);
							addError(section, 'You cant be over 100 years old.');
						} else {
							removeError(section, 'You cant be over 100 years old.');

						}

						if (age < 0) {
							errors.push(['form_dob_day', 'Please choose a correct Date of Birth.']);
							addError(section, 'Please choose a correct Date of Birth.');
						} else {
							removeError(section, 'Please choose a correct Date of Birth.');
						}
						if (age < 1) {
							errors.push(['form_dob_day', 'You can not be under 1 years old.']);
							addError(section, 'You can not be under 1 years old.');
						} else {
							removeError(section, 'You can not be under 1 years old.');
						}
						if (age >= 1 && age < 16) {

							if (age <= 6)
								$('#under_6_notice').addClass('block').addClass('visible');
							else
								$('#under_6_notice').removeClass('visible').removeClass('block');
						}
					}
				}


			}
			if (key == 'checked') {

				if (value == true && element.is(':checked') == false) {
					errors.push([element.prop('id'), name + ' must be accepted to continue']);
					addError(section, name + ' must be accepted to continue');
				} else {
					removeError(section, name + ' must be accepted to continue');
				}
				if (value == false && element.is(':checked') == true) {
					errors.push([element.prop('id'), name + ' must be unchecked.']);
					addError(section, name + ' must be unchecked.');
				} else {
					removeError(section, name + ' must be unchecked.');
				}
			}
			if (key == 'multiple') {

				if ($('[name="form_sport[]"]:checked').length == 0) {
					errors.push([element.prop('id'), name + ' please choose at least one sport.']);
					addError(section, name + ' please choose at least one sport.');
				} else {
					removeError(section, name + ' please choose at least one sport.');
				}


			}
			if (key == 'other') {
				let allVals: any = [];
				$('[name="form_sport[]"]:checked').each(function () {
					allVals.push($(this).val());
				});
				if ($.inArray('other', allVals) != -1 && $('#form_other_value').val().length == 0) {

					errors.push([element.prop('id'), 'Please specify your Additional sport(s).']);
					addError(section, 'Please specify your Additional sport(s).');

				} else {
					removeError(section, 'Please specify your Additional sport(s).');
				}


			}
			if (key == 'verification') {
				if (element.val().length < 1 || element.val().length > 5) {
					errors.push([element.prop('id'), 'Please complete the verifcation correctly.']);
				}
			}
			if (key == 'requires') {
				let allVals: any = [];
				$('[name="form_sport[]"]:checked').each(function () {
					allVals.push($(this).val());
				});
				if ($.inArray('other', allVals) == -1 && $('#form_other_value').val().length > 0) {

					errors.push([element.prop('id'), 'Please select the other sport checkbox.']);
					addError(section, 'Please select the other sport checkbox.');

				} else {
					removeError(section, 'Please select the other sport checkbox.');
				}
			}
		});

		if (errors.length == 0) {

			$(id).empty();
			if (next == null) {
				if (element != null) {
					$('#' + element.prop('id')).closest('.form-group').removeClass('has-error');
					$('#' + element.prop('id')).closest('.form-group').addClass('has-success');
					$('#' + element.prop('id') + '_error_icon').removeClass('visible').removeClass('block');
					$('#' + element.prop('id') + '_success_icon').addClass('block').addClass('visible');
				} else {

					$('#form_dob_day').closest('.form-group').removeClass('has-error');
					$('#form_dob_day').closest('.form-group').addClass('has-success');
					$('.form_dob_error_icon').removeClass('visible').removeClass('block');
					$('.form_dob_success_icon').addClass('block').addClass('visible');

				}
			}
			$(id).removeClass('visible');
			setTimeout(function (e) {
				$(id).removeClass('block');
			}, 1);

			complete[section][id.replace('#', '').replace('_error', '')] = true;
			let sec: boolean;
			if (section == 'address') {
				if (complete[section]['address_line_one'] == true && (complete[section]['address_two'] == null || complete[section]['address_line_two'] == true) && (complete[section]['address_city'] == null || complete[section]['address_city'] == true) && (complete[section]['address_county'] == null || complete[section]['address_county'] == true) && complete[section]['address_postcode'] == true)
					sec = true
				else
					sec = false;

			} else if (section == 'emergency') {
				if (complete[section]['emergency1_name'] == true && complete[section]['emergency1_number'] == true && (complete[section]['emergency2_name'] == null || complete[section]['emergency2_name'] == true) && (complete[section]['emergency2_number'] == null || complete[section]['emergency2_number'] == true))
					sec = true;
				else
					sec = false;
			} else {
				$.each(complete[section], function (key, value) {

					if (value == false)
						sec = false;
					else
						sec = true;
				});
			}

			if (sec) {
				$('a#' + section + '_status').removeClass('error');
				$('li a#' + section + '_status').addClass('complete');
			} else {
				$('li a#' + section + '_status').removeClass('complete');
			}
		} else {
			$('.status ul li a#' + section + '_status').removeClass('complete');
			$('.status ul li a#' + section + '_status').addClass('error');


			let total_errors: number = errors.length;
			$.each(errors, function (key, value) {
				if (value[0] == 'form_dob_day') {
					$('#form_dob_day').closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
					$('.form_dob_success_icon').removeClass('block');
					$('.form_dob_success_icon').removeClass('visible');
					$('.form_dob_error_icon').addClass('block'); $('.form_dob_error_icon').addClass('visible');
				} else {
					$('#' + value[0]).closest('.form-group').removeClass('has-success');
					$('#' + value[0]).closest('.form-group').addClass('has-error has-feedback');
					$('#' + value[0] + '_success_icon').removeClass('block');
					$('#' + value[0] + '_success_icon').removeClass('visible');
					$('#' + value[0] + '_error_icon').addClass('block');
					$('#' + value[0] + '_error_icon').addClass('visible');
					$('#' + value[0].replace('form_', '') + '_error').text(value[1]);
				}

			});


		}



	};
	const addError = function (section: string, content: string) {

		if (formErrors[section] == undefined)
			formErrors[section] = [];
		if (!formErrors[section].includes(content)) {

			formErrors[section].push(content);
		}


		renderErrorMessages();
	};
	const removeError = function (section: string, content: string) {

		if (calcErrors() == 0) {
			$('#main_page_errors').removeClass('visible block');
		} else {
			$.each(formErrors[section], function (key, value) {
				if (value == content) {
					formErrors[section].splice(key, 1);
				}
			});
			renderErrorMessages();
		}
	};
	const renderErrorMessages = function (): void {
		let total_errors = calcErrors();
		if (total_errors != 0) {
			$('#main_page_errors').empty();

			$('#main_page_errors').append('<b>Errors (' + total_errors + ')</b><br>');
			$('#main_page_errors').addClass('visible block');
			$.each(formErrors, function (key, value) {
				$.each(value, function (key2, error) {
					if ((key2 == (formErrors.length - 1)))
						$('#main_page_errors').append(error);
					else
						$('#main_page_errors').append(error + '<span style="display:block;margin-top:10px"></span>');
				});

			});
		} else {
			$('#main_page_errors').empty();
			$('#main_page_errors').removeClass('visible block');
		}
	};
	const calcErrors = function (): number {
		let total: number = 0;
		for (let section in formErrors) {
			total += formErrors[section].length;
		}

		return total;
	}
	return {
		validate: validate,
		renderErrorMessages: renderErrorMessages,
		addError: addError,
		removeError: removeError,
		calcErrors: calcErrors
	}
});