declare var define: RequireDefine;
declare var $: any;

define(['jquery', 'rwcui.functions', 'rwcui.booking.validation'], function ($: any, functions: any, validation: any) {
	const helpHeaderMessages: any = {

		address: 'This section is the participant\'s address. Only First address line and postcode is required.',
		parental: 'You are under 16 years old, therefore, your must have parent/ guardian permission.',
		about: 'This section surounds the participant\'s sporting activity and their expertise. Please only provide appropiate sports.',
		emergency: 'This will only be used in case of an accident at RampWorld Cardiff.',
		terms: 'This information can be used in if the participant sustains injury or violates our Terms and Conditions'

	};
	const init = function (): void {
		functions.checkSideBarLocation();
		$(window).on('resize', function (e) { functions.checkSideBarLocation(); })
		$('#form_booking_member_number').on('change', function (e) {
			validation.validate('booking', $(this), {
				'length': [0, 6],
				'characters': 'numbers'
			}, null, true);
		});
		$('#form_booking_name').on('change', function (e) {

			functions.capitalise('form_booking_name');
			validation.validate('booking', $(this), {
				'length': [3, 100],
				'characters': 'letters'
			}, null, true);
		});
		$('#form_booking_number').on('change', function (e) {
			validation.validate('booking', $(this), {
				'length': [10, 17],
				'characters': 'numbers'
			}, 'Phone number', true);
		});
		$('#form_booking_name').on('change', function (e) {
			functions.capitalise('form_booking_name');
			validation.validate('booking', $(this), {
				'length': [3, 100],
				'characters': 'letters'
			}, null, true);

		});
		$('#form_booking_email').on('change', function (e) {
			validation.validate('booking', $(this), {
				'length': [1, 255],
				'email': true
			}, null, true);
		});
		$('.click-loading').on('click', function () {
			$(this).html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate" id="loading"></span> Processing');
			$(this).addClass('disabled');
		});
	}
	return {
		init: init
	}
});