declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {

	const init = function (): void {

		$('#copyParticipant').on('click', function () {

			$('#form_emergency1_name').val($('#copyParticipant').data('name'));
			$('#form_emergency1_number').val($('#copyParticipant').data('number'));
		});
	}
	return {
		init: init
	}
});