declare var define: RequireDefine;
declare var $: any;

define(['jquery', 'rwcui.register.validation', 'rwcui.functions'], function ($: any, validation: any, functions: any) {
	const helpHeaderMessages: any = {

		participant_details: 'This section is information about the participant, not the parent or guardian.',
		address: 'This section is the participant\'s address. Only First address line and postcode is required.',
		parental: 'You are under 16 years old, therefore, your must have parent/ guardian permission.',
		about: 'This section surounds the participant\'s sporting activity and their expertise. Please only provide appropiate sports.',
		emergency: 'This will only be used in case of an accident at RampWorld Cardiff.',
		terms: 'This information can be used in if the participant sustains injury or violates our Terms and Conditions'

	};
	const init = function (): void {
		$(window).on('resize', function (e) { functions.checkSideBarLocation(); })
		$('#form_forename').on('change', function (e) {
			functions.capitalise('form_forename');
			validation.validate('personal', $(this), {
				'length': [1, 100],
				'characters': 'letters'
			}, null, true);
		});
		$('#form_forename').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.participant_details, 'Please provide your first name.  We will automatically format your forename.');

		});


		$('#form_surname').on('change', function (e) {
			functions.capitalise('form_surname');
			validation.validate('personal', $(this), {
				'length': [1, 100],
				'characters': 'letters'
			}, null, true);
		});
		$('#form_surname').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.participant_details, 'Please provide your second name.  We will automatically format your surname.')
		});


		$('#form_email_address').on('change', function (e) {
			validation.validate('personal', $(this), {
				'length': [1, 255],
				'email': true
			}, null, true);
		});
		$('#form_email_address').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.participant_details, 'Please provide your a valid email address.  This is used to send a copy of your membership details.')
		});

		$('#form_number').on('change', function (e) {
			validation.validate('personal', $(this), {
				'length': [10, 16],
				'characters': 'numbers'
			}, null, true);
		});
		$('#form_number').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.participant_details, 'Please provide your a valid home or mobile number.  We do not share this informtion with anyone.')
		});


		$('#form_dob_day').on('change', function (e) {
			if ($(this).val().length < 2)
				$(this).val('0' + $(this).val());

			validation.validate('personal', null, {
				'date': true
			}, null, true);

		});
		$('#form_dob_day').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.participant_details, 'Please provide the participant\'s birth date. We will do the formatting for you.')
		});


		$('#form_dob_month').on('change', function (e) {
			validation.validate('personal', null, {
				'date': true
			}, null, true);
		});

		$('#form_dob_year').on('change', function (e) {
			let year: number = parseInt(new Date().getFullYear().toString().substr(2, 2));
			if ($(this).val().length == 2) {
				if ($(this).val() > year)
					$(this).val('19' + $(this).val());
				if ($(this).val() > 0 && $(this).val() <= year)
					$(this).val('20' + $(this).val());
			} else if ($(this).val().length == 1) {
				if ($(this).val() < year)
					$(this).val('200' + $(this).val());
			}
			validation.validate('personal', null, {
				'date': true
			}, null, true);
		});
		$('#form_dob_year').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.participant_details, 'Please provide the participant\'s birth year. We will format the year for you.<br>The formats we accept are YY and YYYY.')
		});


		$('#form_address_line_one').on('change', function (e) {
			functions.capitalise('form_address_line_one');
			validation.validate('address', $(this), {
				'length': [1, 50]
			}, null, true);
		});
		$('#form_address_line_one').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.address, 'This is the first address line of the participant\'s home address.  This can be a house name and street name or house number and steet name.');
		});


		$('#form_address_line_two').on('change', function (e) {
			functions.capitalise('form_address_line_two');
			validation.validate('address', $(this), {
				'length': [0, 50]
			}, null, true);
		});
		$('#form_address_line_two').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.address, 'This is the second address line of the participant\'s home address.  This is not required.');
		});


		$('#form_address_city').on('change', function (e) {
			functions.capitalise('form_address_city');
			validation.validate('address', $(this), {
				'length': [0, 50]
			}, null, true);
		});
		$('#form_address_city').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.address, 'This is the city of the participant\'s home address.  This is not required.');
		});


		$('#form_address_county').on('change', function (e) {
			functions.functions.capitalise('form_address_county');
			validation.validate('address', $(this), {
				'length': [0, 50]
			}, null, true);
		});
		$('#form_address_county').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.address, 'This is the county of the participant\'s home address.  This is not required.');
		});


		$('#form_address_postcode').on('change', function (e) {

			validation.validate('address', $(this), {
				'postcode': true
			}, null, true);
		});
		$('#form_address_postcode').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.address, 'This is the postcoe of the participant\'s home address.');
		});


		//perental


		$('#form_consent_name').on('change', function (e) {
			functions.capitalise('form_consent_name');
			validation.validate('parental', $(this), {
				'length': [2, 100],
				'characters': 'letters'
			}, null, true);
		});
		$('#form_consent_name').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.parental, 'This is the forename and surname of the partent/ guardian.');
		});


		$('#form_consent_number').on('change', function (e) {
			validation.validate('parental', $(this), {
				'length': [10, 16],
				'characters': 'numbers'
			}, null, true);

		});
		$('#form_consent_number').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.parental, 'This is the telephone/ mobile of the partent/ guardian.');
		});


		$('#form_consent_address_line_one').on('change', function (e) {
			validation.validate('parental', $(this), {
				'length': [1, 50]
			}, null, true);

		});
		$('#form_consent_address_line_one').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.parental, 'This is the first address line  of the partent/ guardian\'s home address.  This can be a house name and street name or house number and steet name.');
		});
		$('#form_consent_address_postcode').on('change', function (e) {
			functions.capitalise('form_consent_address_postcode');
			validation.validate('parental', $(this), {
				'postcode': true
			}, null, true);

		});
		$('#form_consent_address_postcode').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.parental, 'This is the postcode of the partent/ guardian\'s home address.');
		});

		$('#form_consent_email').on('change', function (e) {

			validation.validate('parental', $(this), {
				'length': [1, 255],
				'email': true
			}, null, true);

		});
		$('#form_consent_email').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.parental, 'This is the email address of the parent/ guardian.  We will send a copy of the membersship details to this address.');
		});

		$(':checkbox').on('change', function () {
			if ($(this).val() == 'other')
				$('#form_sport_container').toggleClass('hidden');



		});

		$('#form_other_value').on('change', function (e) {
			functions.capitalise('form_other_value');
			validation.validate('about', $(this), {
				'length': [0, 20],
				'requires': 'other'
			}, 'Sport', true);
		});


		//emergency details
		$('#form_emergency1_name').on('change', function (e) {
			functions.capitalise('form_emergency1_name');
			validation.validate('emergency', $(this), {
				'characters': 'letters',
				'length': [3, 100]
			}, 'First Emergency Name', true);
		});
		$('#form_emergency1_name').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.emergency, 'This is the full name of the first emergency contact');
		});


		$('#form_emergency1_number').on('change', function (e) {
			validation.validate('emergency', $(this), {
				'characters': 'numbers',
				'length': [10, 16]
			}, 'First Emergency Number', true);
		});
		$('#form_emergency1_number').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.emergency, 'This is the number of the first emergency contact');
		});


		$('#form_emergency2_name').on('change', function (e) {
			functions.capitalise('form_emergency2_name');
			validation.validate('emergency', $(this), {
				'characters': 'letters',
				'length': [0, 100]
			}, 'Second Emergency Name', true);
		});
		$('#form_emergency2_name').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.emergency, 'This is the full name of the second emergency contact. This is not required.');
		});



		$('#form_emergency2_number').on('change', function (e) {
			validation.validate('emergency', $(this), {
				'characters': 'numbers',
				'length': [0, 16]
			}, 'Second Emergency Number', true);
		});
		$('#form_emergency2_number').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.emergency, 'This is the number of the second emergency contact. This is not required.');
		});


		$('#form_medical_notes').on('change', function (e) {
			validation.validate('medical', $(this), {
				'length': [0, 1000]
			}, null, true);
		});
		$('#form_medical_notes').on('focusin', function (e) {
			functions.formHelpContent(helpHeaderMessages.emergency, 'This is the full name of the second emergency contact. This is not required.');
		});

		$('#tandc').on('click', function () {
			validation.validate('tandcs', $(this), {
				'checked': true
			}, 'Terms and Conditions', true);
		});
		$('#form_captcha').on('change', function (e) {
			validation.validate('tandcs', $(this), {
				'characters': 'letters',
				'length': [2, 4]
			}, 'Security check', true);
		});
	};
	return {
		init: init
	}
});