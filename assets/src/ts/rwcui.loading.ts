declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {

	const add = function (element: any, message: string, colour: string): void {
		element.attr('style', 'min-height: 250px; position:relative;');
		element.append('<div id="loader" class="' + colour + '"><div class="loading-circle ' + colour + '"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div><p class="lead" id="message">' + message + '</p><div id="loader- bg"></div>');
		setTimeout(function () {
			element.children('#loader').addClass('loaded');
			element.children('#message').addClass('loaded');
		}, 10)

	};
	const destroy = function (element: any): void {
		$(element).children('#loader-bg').remove();
		$(element).children('#message').removeClass('loaded');
		setTimeout(function () {
			$(element).children('#message').remove();
			$(element).children('#loader').removeClass('loaded');
			setTimeout(function () {
				$(element).children('#loader').remove();
			}, 150);
		}, 100);
	}
	return {
		destroy: destroy,
		add: add
	}
});