declare var define: RequireDefine;
declare var $: any;

define(['jquery'], function ($: any) {

	const clear = function (name: string) {

		$('#form_' + name).closest('.form-group').addClass('has-success').removeClass('has-error');
		$('#form_' + name + '_success_icon').addClass('block').addClass('visible');
		$('#form_' + name + '_error_icon').removeClass('block visible');
		$('#' + name + '_error').text('');
	};
	const add = function (name: string, message: string) {
		$('#form_' + name).closest('.form-group').removeClass('has-success').addClass('has-error has-feedback');
		$('#form_' + name + '_success_icon').removeClass('block').removeClass('visible');
		$('#form_' + name + '_error_icon').addClass('block visible');
		$('#' + name + '_error').text(message);
	};
	const reset = function (name: string) {
		$('#form_' + name).closest('.form-group').removeClass('has-error has-success has-feedback')
		$('#form_' + name + '_success_icon').removeClass('block visible');
		$('#form_' + name + '_error_icon').removeClass('block visible');
		$('#' + name + '_error').text('');
	}
	return {
		clear: clear,
		add: add,
		reset: reset
	}
});