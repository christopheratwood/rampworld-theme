'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const cleanCSS = require('gulp-clean-css');
const gzip = require('gulp-gzip');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const typescript = require('gulp-typescript');
const replace = require('gulp-replace');

const input = {
  rwcui: './src/sass/rwcui.sass',
  ts: './src/ts/**/*.ts',
  sass: './src/sass/**/*.sass'
}
const output = {
  css: './dist/css/',
  js: './dist/js',
  docs: './dist/docs'
}

const sassOptions = {
  errLogToConsole: true,
  outputStyle: 'expanded'
}

const autoprefixerOptions = {
  browsers: ['last 2 versions', '> 5%', 'Firefox ESR']
};


gulp.task('sass', ['clean-css'], function () {
  return gulp.src(input.rwcui)
    .pipe(sass(sassOptions).on('error', sass.logError))
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(gulp.dest(output.css))
    .pipe(cleanCSS({
      compatibility: 'ie8',
      debug: true
    }, function (details) {
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(rename({
      extname: '.min.css'
    }))
    .pipe(gulp.dest(output.css))
});
/* Typescript */
gulp.task('typescript', ['clean-js'], function () {
  return gulp.src(input.ts)
    .pipe(typescript({
      target: "es5",
      noImplicitUseStrict: false,
      sourceMap: false,
      declaration: false,
      module: "amd",
      removeComments: true
    }))
    .pipe(replace(/\\t|\\n/g, ''))
    .pipe(uglify())
    .pipe(gulp.dest(output.js))
});

gulp.task('watch-sass', function () {
  return gulp
    .watch(input.sass, ['sass'])
    .on('change', function (event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});

gulp.task('watch-ts', function () {
  return gulp
    .watch(input.ts, ["typescript"])
    .on('change', function (event) {
      console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
    });
});


gulp.task('clean-css', function () {
  gulp.src('./dist/css')
    .pipe(clean());
});

gulp.task('clean-js', function () {
  gulp.src('./dist/js')
    .pipe(clean());
});

gulp.task('default', [ 'sass', 'typescript'], function () {});