<?php

$verifyEmail = '';
$verifyEmail .='<!DOCTYPE html>
<html lang="en" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
	<title>Verify your account</title>
	
	
</head>
<body style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; color: #666; margin: 0; padding: 0;">
<style type="text/css">
@font-face { font-family: \'FontAwesome\' !important; src: url("http://www.rampworldcardiff.co.uk/wp-content/themes/birdfield/font-awesome/fontawesome-webfont.eot?#iefix=4.1.0") format("embedded-opentype"), url("../font-awesome/fontawesome-webfont.woff?v=4.1.0") format("woff"), url("../font-awesome/fontawesome-webfont.ttf?v=4.1.0") format("truetype"), url("font-awesome/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular") format("svg") !important; font-weight: normal !important; font-style: normal !important; }
footer .footer-links a:hover { color: #21a1e1 !important; }
footer .footer-icons .fa-facebook:before { content: "\f09a" !important; }
footer .footer-icons .fa-instagram:before { content: "\f16d" !important; }
p.fa-map-marker:before { display: inline-block !important; background-color: #33383b !important; color: #ffffff !important; font-size: 25px !important; width: 38px !important; height: 38px !important; border-radius: 50% !important; text-align: center !important; line-height: 42px !important; margin: 10px 15px !important; vertical-align: middle !important; content: "\f041" !important; transition: color 200ms ease-in-out !important; }
p.fa-envelope:before { display: inline-block !important; background-color: #33383b !important; color: #ffffff !important; width: 38px !important; height: 38px !important; border-radius: 50% !important; text-align: center !important; margin: 10px 15px !important; vertical-align: middle !important; content: "\f0e0" !important; font-size: 17px !important; line-height: 38px !important; transition: color 200ms ease-in-out !important; }
footer .footer-link a:hover { color: #21a1e1 !important; }
footer .footer-icons a:hover { color: #21a1e1 !important; }
footer .footer-center p:hover:before { color: #21a1e1 !important; }
></style>
	<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; text-align: center; margin: 0px auto; max-width: 90%; padding: 0;" align="center">
		<h1 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 200; font-size: 44px; margin: 0 0 15px; padding: 0;" align="center">Hi ' . $this->_data['consent_name'] . ', you\'re almost done!</h1>
		<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">As a parent / Guardian of ' . $this->_data['forename'] . ' ' .$this->_data['surname'] . ' , you must verify that you your understand the terms and conitions and give your permission for the participent to attend RampWorld Cardiff.  You can do this by clicking the button below or by following the link.</p>
		</div>
		<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; text-align: center; max-width: 90%; margin: 20px auto; padding: 0;" align="center">
			<a href="http://www.rampworldcardiff.co.uk/membership/verify?consent_reference=' . $this->_data['consent_verification'].'&amp;email_address='. $this->_data['consent_email'].'" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: #555; text-align: center; height: 38px; font-size: 11px; font-weight: 600; line-height: 38px; letter-spacing: .1rem; text-transform: uppercase; text-decoration: none; white-space: nowrap; border-radius: 4px; cursor: pointer; box-sizing: border-box; display: block; width: 250px; background-color: transparent; margin: 20px auto; padding: 0 30px; border: 1px solid #bbb;">Verify your Consent</a>
			<a href="http://www.rampworldcardiff.co.uk/membership/verify?consent_reference=' . $this->_data['consent_verification'].'&amp;email_address='. $this->_data['consent_email'].'" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: #2BA6CB; text-align: center; font-size: 12px; margin: 0; padding: 0;">http:/www.rampworldcardiff.co.uk/membership/verify?consent_reference=' . $this->_data['consent_verification'].'&amp;email_address='. $this->_data['consent_email'].'</a>
		</div>
	</div>
	
	<footer style="box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.12); box-sizing: border-box; width: 100%; text-align: left; font-style: normal; font-variant: normal; font-weight: bold; font-size: 14px; line-height: normal; font-family: sans-serif; background-color: #292c2f; margin: 80px 0 0; padding: 55px 50px;">
	<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

		<img src="http://wwww.rampworldcardiff.co.uk/wp-content/uploads/2016/06/footer_logo.png" alt="RampWorld Cardiff Footer Logo" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;">

		
		<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #ffffff; margin: 20px 0 12px; padding: 0;">
			<a href="http://wwww.rampworldcardiff.co.uk" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Home</a>
			&bull;
			<a href="http://wwww.rampworldcardiff.co.uk/news" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">News</a>
			&bull;
			<a href="http://wwww.rampworldcardiff.co.uk/park" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">The Park</a>
			&bull;
			<a href="http://wwww.rampworldcardiff.co.uk/about" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">About Us</a>
			&bull;
			<a href="http://wwww.rampworldcardiff.co.uk/session" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Opening</a>
			&bull;
			<a href="http://wwww.rampworldcardiff.co.uk/membership" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Membership</a>
			&bull;
			<a href="http://wwww.rampworldcardiff.co.uk/shop" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">The Shop</a>
			&bull;
			<a href="http://wwww.rampworldcardiff.co.uk/contact" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Contact Us</a>
		</p>
		

		
		<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #8f9296; margin: 0; padding: 0;">RampWorld Cardiff © 2016</p>
		<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #8f9296; margin: 0; padding: 0;">Registed Charity Number 1152842</p>
		

		
	</div>
	
	<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

		<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
			<p style="display: inline-block; color: white; vertical-align: middle; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: 1; font-family: FontAwesome; margin: 0; padding: 0;">RampWorld Cardiff, Unit B, Park Ty-Glas  Llanishen, Cardiff</p>
		</div>

		<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
			<p style="display: inline-block; color: white; vertical-align: middle; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: 1; font-family: FontAwesome; margin: 0; padding: 0;"><a href="mailto:rampworldcardiff@gmail.com" style="color: #5383d3 !important; text-decoration: none; display: inline-block; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: 1; font-family: FontAwesome; margin: 0; padding: 0;">rampworldcardiff@gmail.com</a></p>
		</div>

	</div>
	
	
	
	<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

		<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 13px; line-height: 20px; color: #92999f; margin: 0; padding: 0;">

			<span style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; color: #ffffff; font-size: 14px; font-weight: bold; margin: 0 0 20px; padding: 0;">About RampWorld Cardiff</span>
			RampWorld Cardiff  is run to provide an indoor recreational facility for modern wheeled sports in Cardiff, South Wales for young people with the object of improving their condition of life and in particular to provide for such persons facilities for BMX cycling, scooters, skateboarding and other such activities in a safe indoor environment.
		</p>
			
		
		<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 25px 0 0; padding: 0;">

			<a href="http://www.facebook.com/RampWorldCardiff" target="_blank" style="color: white; display: inline-block; width: 35px; height: 35px; cursor: pointer; border-radius: 2px; text-align: center; text-decoration: none; vertical-align: top; transition: color 200ms ease-in-out; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 20px; line-height: 35px; font-family: FontAwesome; background-color: #33383b; margin: 0 3px 5px 0; padding: 0;"></a>
			<a href="https://www.instagram.com/rampworldcardiff" target="_blank" style="color: white; display: inline-block; width: 35px; height: 35px; cursor: pointer; border-radius: 2px; text-align: center; text-decoration: none; vertical-align: top; transition: color 200ms ease-in-out; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 20px; line-height: 35px; font-family: FontAwesome; background-color: #33383b; margin: 0 3px 5px 0; padding: 0;"></a>

		</div>
		
		
	</div>
	
	
</footer>
</body>
</html>';?>
