<?php
$email = '';
$email .='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
<head>
<!-- If you delete this meta tag, Half Life 3 will never be released. -->
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>RampWorld Cardiff New Member</title>

</head>
 
<body bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; color: #666; margin: 0; padding: 0;"><style type="text/css">
@font-face { font-family: "FontAwesome" !important; src: url("http://www.rampworldcardiff.co.uk/wp-content/themes/birdfield/font-awesome/fontawesome-webfont.eot?#iefix=4.1.0") format("embedded-opentype"), url("../font-awesome/fontawesome-webfont.woff?v=4.1.0") format("woff"), url("../font-awesome/fontawesome-webfont.ttf?v=4.1.0") format("truetype"), url("font-awesome/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular") format("svg") !important; font-weight: normal !important; font-style: normal !important; }
footer .footer-links a:hover { color: #21a1e1 !important; }
footer .footer-icons .fa-facebook:before { content: "\f09a" !important; }
footer .footer-icons .fa-instagram:before { content: "\f16d" !important; }
p.fa-map-marker:before { display: inline-block !important; background-color: #33383b !important; color: #ffffff !important; font-size: 25px !important; width: 38px !important; height: 38px !important; border-radius: 50% !important; text-align: center !important; line-height: 42px !important; margin: 10px 15px !important; vertical-align: middle !important; content: "\f041" !important; transition: color 200ms ease-in-out !important; }
p.fa-envelope:before { display: inline-block !important; background-color: #33383b !important; color: #ffffff !important; width: 38px !important; height: 38px !important; border-radius: 50% !important; text-align: center !important; margin: 10px 15px !important; vertical-align: middle !important; content: "\f0e0" !important; font-size: 17px !important; line-height: 38px !important; transition: color 200ms ease-in-out !important; }
footer .footer-link a:hover { color: #21a1e1 !important; }
footer .footer-icons a:hover { color: #21a1e1 !important; }
footer .footer-center p:hover:before { color: #21a1e1 !important; }
&gt;</style>

<!-- HEADER -->
<table class="head-wrap" bgcolor="#999999" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
	<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
		<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
		<td class="header container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">
				
				<div class="content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;">
				<table bgcolor="#999999" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
					<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
						<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="http://www.rampworldcardiff.co.uk/wp-content/uploads/2016/06/footer_logo.png" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td>
						<td align="right" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
						<h6 class="collapse blue" style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: white; font-weight: 500; font-size: 17px; margin: 0; padding: 0;" align="center">Welcome to RampWorld Cardiff!</h6></td>
					</tr>
				</table>
				</div>
				
		</td>
		<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
	</tr>
</table><!-- /HEADER -->
<!-- BODY -->
<table class="body-wrap" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
	<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
		<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
		<td class="container" bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">

			<div class="content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;">
			<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
				<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
					<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
						<h3 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 27px; margin: 0 0 15px; padding: 0;" align="center">Hi, '. $this->_data['forename'] . ' ' . $this->_data['surname'].'</h3>
						<p class="lead" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 17px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Thank you for registering for the RampWorld Cardiff Membership. Below are your details.</p>
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Note: Please keep your RampWorld Cardiff Membership Number safe as you will need to provide this number when you come to RampWorld Cardiff.</p>
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">We recommend saving the number to your phone and/ or keeping a copy in your bag.</p>
						<h5 class="" style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 17px; margin: 20px auto; padding: 0;" align="center">RampWorld Cardiff Membership Number</h5>
						<span class="membershipNumber" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; font-size: 64px; text-align: center; margin: 20px auto; padding: 0;">'.str_pad($memberID, 6, '0', STR_PAD_LEFT).'</span>
						<!-- Callout Panel -->
						<h5 class="" style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 17px; margin: 0px auto; padding: 0;" align="center">Personal Details</h5>
						<table width="100%" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">

								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Forename:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['forename'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Surname:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['surname'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Gender</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">';
										$gender = array_keys(array('Male' => 'm', 'Female' => 'f', 'Other' => 'o', 'Please Select' => null), $this->_data['gender']);
										$email .= $gender[0];
									
								    $email .= '</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Date of Birth:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['dob'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Telephone Number:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['number'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Email Address</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['email'].'</p>
								</td>
							
							</tr>
						</table>
						<h5 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 17px; margin: 20px auto; padding: 0;" align="center">Address</h5>
						<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Address Line One:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['address_line_one'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Address Line Two:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['address_line_two'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">City:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['address_city'].'</p>
								</td>
							</tr>
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">County:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['address_county'].'</p>
								</td>
							
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Postcode:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['address_postcode'].'</p>
								</td>
							</tr>
							
						</table>';

					if( !empty( $this->_data['consent_name'] ) == true ){	
						$email .= '<h5 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 17px; margin: 20px auto; padding: 0;" align="center">Parental Consent</h5>
						<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Parental / Guardian Name:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['consent_name'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Parental / Guardian Number:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['consent_number'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Parental / Guardian Email:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['consent_email'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Parental / Guardian Address Line One:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['consent_address_line_one'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Parental / Guardian Postcode:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['consent_address_postcode'].'</p>
								</td>
							</tr>

						</table>';
					}
					$email .='<h5 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 17px; margin: 20px auto; padding: 0;" align="center">About Yourself</h5>
						<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Discipline (s):</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['discipline'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Expertise:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">';
									switch($this->_data['expertise']) {
								      	case 0:
								      		$email .= 'Beginner';
								      		break;
								      	case 1:
								      		$email .= 'Novice';
								      		break;
								      	case 2:
								      		$email .= 'Experienced';
								      		break;
								      	case 3:
								      		$email .= 'Advanced';
								      		break;	
								      	case 4:
								      		$email .= 'Expert';
								      		break;
								      	case 5:
								      		$email .= 'Professional';
								      		break;
								      	default:
								      		$email .= 'Beginner';
								      		break;
								      }
								    $email .= '</p>
								</td>
							</tr>
						</table>
						<h5 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 17px; margin: 20px auto; padding: 0;" align="center">Emergency Contact Details</h5>
						<table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
							
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Full Name:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['emergency_name_1'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Number:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['emergency_number_1'].'</p>
							</td></tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Full Name:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['emergency_name_2'].'</p>
								</td>
							</tr>
							<tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
								
								<th style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-wdith: 300px; width: 50%; margin: 0; padding: 0;">				
									<p class="heading" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: normal !important; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Number:</p>
								</th>
								<td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
									<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['emergency_number_2'].'</p>
								</td>
							</tr>
						</table>
						<h5 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 17px; margin: 20px auto; padding: 0;" align="center">Medical Notes</h5> 
						<p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">'. $this->_data['medical_notes'].'</p>
					
						
					</td>
				</tr>
			</table>
			</div><!-- /content -->
									
		</td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<footer class="footer-wrap" style="box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.12); box-sizing: border-box; width:  100%; text-align: left; font-style: normal; font-variant: normal; font-weight: bold; font-size: 14px; line-height: normal; font-family: sans-serif; background-color: #292c2f; margin: 80px 0 0; padding: 55px 50px;">
	<div class="footer-left" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

		<img src="http://www.rampworldcardiff.co.uk/wp-content/uploads/2016/06/footer_logo.png" alt="RampWorld Cardiff Footer Logo" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" />

		<p class="footer-links" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #ffffff; margin: 20px 0 12px; padding: 0;">
			<a href="http://www.rampworldcardiff.co.uk" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Home</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/news" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">News</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/park" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">The Park</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/about" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">About Us</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/session" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Opening</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/membership" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Membership</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/shop" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">The Shop</a>
			&bull;
			<a href="http://www.rampworldcardiff.co.uk/contact" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Contact Us</a>
		</p>

		<p class="footer-company-name" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #8f9296; margin: 0; padding: 0;">RampWorld Cardiff © 2016</p>
		<p class="footer-company-name" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #8f9296; margin: 0; padding: 0;">Registed Charity Number 1152842</p>
	</div>

	<div class="footer-center" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

		<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
			<p class="fa fa-map-marker" style="display: inline-block; color: white; vertical-align: middle; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: 1; font-family: FontAwesome; margin: 0; padding: 0;">RampWorld Cardiff, Unit B, Park Ty-Glas</p>
			<p class="second_address_line" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 16px; line-height: 1.6; display: block; color: #ffffff; vertical-align: middle; margin: -15px 0 0 68px; padding: 0;"> Llanishen, Cardiff</p>
		</div>

		<div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
			<p class="fa fa-envelope email" style="display: inline-block; color: white; vertical-align: middle; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: 1; font-family: FontAwesome; margin: 0; padding: 0;"><a href="mailto:rampworldcardiff@gmail.com" style="color: #5383d3 !important; text-decoration: none; display: inline-block; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: 1; font-family: FontAwesome; margin: 0; padding: 0;">rampworldcardiff@gmail.com</a></p>
		</div>

	</div>

	<div class="footer-right" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

		<p class="footer-company-about" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 13px; line-height: 20px; color: #92999f; margin: 0; padding: 0;">

			<span style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; color: #ffffff; font-size: 14px; font-weight: bold; margin: 0 0 20px; padding: 0;">About RampWorld Cardiff</span>
			RampWorld Cardiff  is run to provide an indoor recreational facility for modern wheeled sports in Cardiff, South Wales for young people with the object of improving their condition of life and in particular to provide for such persons facilities for BMX cycling, scooters, skateboarding and other such activities in a safe indoor environment.
		</p>

		

	</div>
</footer><!-- /FOOTER -->

</body>
</html>';