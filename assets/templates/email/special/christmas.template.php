<?php
$email = '<!DOCTYPE html>';
$email .= '<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>A special thank you from RampWorld Cardiff!</title>
</head>
<body bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; color: #666; margin: 0; padding: 0;"><style type="text/css">
@font-face { font-family: "FontAwesome" !important; src: url("https://www.rampworldcardiff.co.uk/wp-content/themes/rampworld/assets/dist/font-awesome/fontawesome-webfont.eot?#iefix=4.1.0") format("embedded-opentype"), url("https://www.rampworldcardiff.co.uk/wp-content/themes/rampworld/assets/dist/font-awesome/fontawesome-webfont.woff?v=4.1.0") format("woff"), url("https://www.rampworldcardiff.co.uk/wp-content/themes/rampworld/assets/dist/font-awesome/fontawesome-webfont.ttf?v=4.1.0") format("truetype"), url("kttps://www.rampworldcardiff.co.uk/wp-content/themes/rampworld/assets/dist/font-awesome/fontawesome-webfont.svg?v=4.1.0#fontawesomeregular") format("svg") !important; font-weight: normal !important; font-style: normal !important; }
footer .footer-links a:hover { color: #21a1e1 !important; }
footer .footer-icons .fa-facebook:before { content: "\f09a" !important; }
footer .footer-icons .fa-instagram:before { content: "\f16d" !important; }
p.fa-map-marker:before { display: inline-block !important; background-color: #33383b !important; color: #ffffff !important; font-size: 25px !important; width: 38px !important; height: 38px !important; border-radius: 50% !important; text-align: center !important; line-height: 42px !important; margin: 10px 15px !important; vertical-align: middle !important; content: "\f041" !important; transition: color 200ms ease-in-out !important; }
p.fa-envelope:before { display: inline-block !important; background-color: #33383b !important; color: #ffffff !important; width: 38px !important; height: 38px !important; border-radius: 50% !important; text-align: center !important; margin: 10px 15px !important; vertical-align: middle !important; content: "\f0e0" !important; font-size: 17px !important; line-height: 38px !important; transition: color 200ms ease-in-out !important; }
footer .footer-link a:hover { color: #21a1e1 !important; }
footer .footer-icons a:hover { color: #21a1e1 !important; }
footer .footer-center p:hover:before { color: #21a1e1 !important; }

</style>
  <table class="head-wrap" bgcolor="#999999" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
    <tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
      <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
      <td class="header container" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 1000px !important; clear: both !important; margin: 0 auto; padding: 0;">
          
          <div class="content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 1000px; display: block; margin: 0 auto; padding: 15px;">
          <table bgcolor="#999999" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
            <tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
              <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="https://www.rampworldcardiff.co.uk/wp-content/uploads/2016/06/footer_logo.png" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td>
              <td align="right" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
              <h6 class="collapse blue" style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: white; font-weight: 500; font-size: 17px; margin: 0; padding: 0;" align="center">A special thank you from RampWorld Cardiff!</h6></td>
            </tr>
          </table>
          </div>
          
      </td>
      <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
    </tr>
  </table><!-- /HEADER -->
  <table class="body-wrap" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
    <tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
      <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
      <td class="container" bgcolor="#FFFFFF" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block !important; max-width: 1000px !important; clear: both !important; margin: 0 auto; padding: 0;">

        <div class="content" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 1000px; display: block; margin: 0; padding: 15px;">
        <table style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
          <tr style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
            <td style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
              <h3 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: left; color: #21a1e1; font-weight: 500; font-size: 27px; margin: 0 0 15px; padding: 0;" align="center">Dear '.$forename.' '.$surname.',</h3>
              <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 17px; line-height: 1.6; margin: 0 0 10px; padding: 0; text-align: left;">We would like to personally thank you for visiting RampWorld Cardiff this year.  As we are a local charity, people like you are the key to our success and from everyone at RampWorld Cardiff, we would like to wish you a Merry Christmas and a Happy New Year.</p>
              <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 17px; line-height: 1.6; margin: 0 0 10px; padding: 0; text-align: left;">You are one the top 300 members to attend RampWorld Cardiff this year and below are some other nerdy statistics you may like to share with your friends!</p>
              <h3 style="font-family: \'HelveticaNeue-Light\', \'Helvetica Neue Light\', \'Helvetica Neue\', Helvetica, Arial, \'Lucida Grande\', sans-serif; line-height: 1.1; text-align: center; color: #21a1e1; font-weight: 500; font-size: 27px; margin: 0 0 15px; padding: 0;" align="center">Your badges</h3>
              <div style="text-align: center; margin-bottom: 60px;">
                <div style="display:inline-block;vertical-align: top;">
                  <div style="width: 175px;height: 225px;background-image: url(https://www.rampworldcardiff.co.uk/wp-content/uploads/2017/06/badge.png);background-size: 100% 100%; position: relative;"><div style="height: 50px"></div><span style="font-size: 42px;display: block; color: white; text-align: center">'.$rank.'</span><span style="font-size: 16px;display: block;color: white;
                  bottom: 0;">Rank</span></div>
                </div>
                <div style="display:inline-block;vertical-align: top;">
                <div style="width: 175px;height: 225px;background-image: url(https://www.rampworldcardiff.co.uk/wp-content/uploads/2017/06/badge.png);background-size: 100% 100%; position: relative;"><div style="height: 50px"></div><span style="font-size: 42px;display: block;color: white;">'.$visits.'</span><span style="font-size: 16px;display: block; right: 0;color: white;">Visits</span></div>
              </div>
              <div style="display:inline-block;vertical-align: top;">
                <div style="width: 175px;height: 225px;background-image: url(https://www.rampworldcardiff.co.uk/wp-content/uploads/2017/06/badge.png);background-size: 100% 100%; position: relative;"><div style="height:48px"></div><span style="font-size: 33px;display: block;color: white;">'.$hours.'</span><span style="font-size: 16px;display: block;color: white;">Hours<br>spent</span></div>
              </div>
               
              </div>
              <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0; text-align: left;">Thank you for all your support during 2017 and hope to see you in the New Year.</p>
              <a href="https://www.rampworldcardiff.co.uk/opening-hours" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0; color: #21a1e1">Click here to see our opening times over the Christmas and New Year period </a>
    
              <p style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-style: italic; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 20px 0 10px; padding: 0; text-align: left;">RampWorld Cardiff</p>
              <span style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; font-size: 10px; text-align: center; padding: 0; text-align:left;">If the name attached this email is not yourself, please ignore this email.</span>
            </td>
          </tr>
        </table>
        </div><!-- /content -->
                    
      </td>
    </tr>
  </table><!-- /BODY -->

  <!-- FOOTER -->
  <footer class="footer-wrap" style="box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.12); box-sizing: border-box; width:  100%; text-align: left; font-style: normal; font-variant: normal; font-weight: bold; font-size: 14px; line-height: normal; font-family: sans-serif; background-color: #292c2f; margin: 80px 0 0; padding: 55px 50px;">
    <div class="footer-left" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

      <img src="https://www.rampworldcardiff.co.uk/wp-content/uploads/2016/06/footer_logo.png" alt="RampWorld Cardiff Footer Logo" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" />

      <p class="footer-links" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #ffffff; margin: 20px 0 12px; padding: 0;">
        <a href="https://www.rampworldcardiff.co.uk" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Home</a>
        &bull;
        <a href="https://www.rampworldcardiff.co.uk/news" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">News</a>
        &bull;
        <a href="https://www.rampworldcardiff.co.uk/park" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">The Park</a>
        &bull;
        <a href="https://www.rampworldcardiff.co.uk/about" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">About Us</a>
        &bull;
        <a href="https://www.rampworldcardiff.co.uk/session" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Opening</a>
        &bull;
        <a href="https://www.rampworldcardiff.co.uk/membership" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Membership</a>
        &bull;
        <a href="https://www.rampworldcardiff.co.uk/online-booking" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Online booking</a>
        &bull;
        <a href="https://www.rampworldcardiff.co.uk/rampworld-shop" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">The Shop</a>
        &bull;
        <a href="https://www.rampworldcardiff.co.uk/contact" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; color: inherit; display: inline-block; line-height: 1.8; text-decoration: none; transition: color 200ms ease-in-out; margin: 0; padding: 0;">Contact Us</a>
      </p>

      <p class="footer-company-name" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #8f9296; margin: 0; padding: 0;">RampWorld Cardiff © 2017</p>
      <p class="footer-company-name" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; color: #8f9296; margin: 0; padding: 0;">Registed Charity Number 1152842</p>
    </div>

    <div class="footer-center" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

      <div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
      <svg xmlns="https://www.w3.org/2000/svg" style="width: 20px" xmlns:xlink="https://www.w3.org/1999/xlink" version="1.1" preserveAspectRatio="xMidYMid meet" viewBox="135.43617021276594 137.69680851063833 39.2659574468085 65.09512171746218" width="35.27" height="61.1"><defs><path d="M155.52 138.77L156.23 138.86L156.93 138.98L157.62 139.13L158.31 139.32L158.98 139.54L159.64 139.79L160.29 140.07L160.93 140.39L161.56 140.73L162.17 141.1L162.77 141.49L163.36 141.92L163.93 142.37L164.48 142.84L165.02 143.35L165.54 143.87L166.05 144.42L166.54 144.99L167.01 145.59L167.46 146.2L167.89 146.84L168.3 147.5L168.69 148.17L169.06 148.87L169.41 149.58L169.73 150.31L170.04 151.06L170.32 151.83L170.57 152.6L170.8 153.4L171.01 154.21L171.19 155.03L171.34 155.86L171.47 156.71L171.57 157.56L171.64 158.43L171.69 159.31L171.7 160.19L171.69 161.08L171.64 161.96L171.57 162.83L171.47 163.7L171.34 164.56L171.19 165.42L171.01 166.28L170.8 167.13L170.57 167.99L170.32 168.84L170.04 169.7L169.73 170.56L169.41 171.42L169.06 172.29L168.69 173.17L168.3 174.05L167.89 174.94L167.46 175.83L167.01 176.74L166.54 177.66L166.05 178.59L165.54 179.53L165.02 180.48L164.48 181.45L163.93 182.44L163.36 183.44L162.77 184.46L162.17 185.5L161.56 186.56L160.93 187.64L160.29 188.74L159.64 189.86L158.98 191.01L158.31 192.18L157.62 193.38L156.93 194.6L156.23 195.86L155.52 197.14L154.8 198.45L154.07 199.79L153.34 198.45L152.62 197.14L151.91 195.86L151.21 194.6L150.52 193.38L149.83 192.18L149.16 191.01L148.5 189.86L147.84 188.74L147.21 187.64L146.58 186.56L145.97 185.5L145.37 184.46L144.78 183.44L144.21 182.44L143.66 181.45L143.12 180.48L142.59 179.53L142.09 178.59L141.6 177.66L141.13 176.74L140.68 175.83L140.25 174.94L139.84 174.05L139.45 173.17L139.08 172.29L138.73 171.42L138.4 170.56L138.1 169.7L137.82 168.84L137.57 167.99L137.34 167.13L137.13 166.28L136.95 165.42L136.79 164.56L136.67 163.7L136.57 162.83L136.49 161.96L136.45 161.08L136.44 160.19L136.45 159.31L136.49 158.43L136.57 157.56L136.67 156.71L136.79 155.86L136.95 155.03L137.13 154.21L137.34 153.4L137.57 152.6L137.82 151.83L138.1 151.06L138.4 150.31L138.73 149.58L139.08 148.87L139.45 148.17L139.84 147.5L140.25 146.84L140.68 146.2L141.13 145.59L141.6 144.99L142.09 144.42L142.59 143.87L143.12 143.35L143.66 142.84L144.21 142.37L144.78 141.92L145.37 141.49L145.97 141.1L146.58 140.73L147.21 140.39L147.84 140.07L148.5 139.79L149.16 139.54L149.83 139.32L150.52 139.13L151.21 138.98L151.91 138.86L152.62 138.77L153.34 138.71L154.07 138.7L154.8 138.71L155.52 138.77ZM153.38 148.91L153.05 148.95L152.71 149.01L152.39 149.09L152.06 149.18L151.74 149.28L151.43 149.4L151.12 149.53L150.82 149.68L150.52 149.84L150.23 150.02L149.95 150.2L149.67 150.4L149.4 150.62L149.14 150.84L148.88 151.08L148.63 151.33L148.39 151.59L148.16 151.86L147.94 152.14L147.73 152.44L147.52 152.74L147.33 153.05L147.14 153.37L146.97 153.7L146.8 154.04L146.65 154.38L146.51 154.74L146.37 155.1L146.25 155.47L146.14 155.84L146.04 156.23L145.96 156.61L145.89 157.01L145.83 157.41L145.78 157.82L145.74 158.23L145.72 158.64L145.72 159.06L145.72 159.48L145.74 159.9L145.78 160.31L145.83 160.71L145.89 161.11L145.96 161.51L146.04 161.9L146.14 162.28L146.25 162.66L146.37 163.03L146.51 163.39L146.65 163.74L146.8 164.09L146.97 164.43L147.14 164.75L147.33 165.08L147.52 165.39L147.73 165.69L147.94 165.98L148.16 166.26L148.39 166.53L148.63 166.79L148.88 167.04L149.14 167.28L149.4 167.51L149.67 167.72L149.95 167.92L150.23 168.11L150.52 168.28L150.82 168.44L151.12 168.59L151.43 168.73L151.74 168.84L152.06 168.95L152.39 169.04L152.71 169.11L153.05 169.17L153.38 169.21L153.72 169.24L154.07 169.24L154.41 169.24L154.75 169.21L155.09 169.17L155.42 169.11L155.75 169.04L156.08 168.95L156.4 168.84L156.71 168.73L157.02 168.59L157.32 168.44L157.62 168.28L157.91 168.11L158.19 167.92L158.47 167.72L158.74 167.51L159 167.28L159.26 167.04L159.5 166.79L159.74 166.53L159.98 166.26L160.2 165.98L160.41 165.69L160.62 165.39L160.81 165.08L161 164.75L161.17 164.43L161.34 164.09L161.49 163.74L161.63 163.39L161.77 163.03L161.89 162.66L162 162.28L162.09 161.9L162.18 161.51L162.25 161.11L162.31 160.71L162.36 160.31L162.39 159.9L162.41 159.48L162.42 159.06L162.41 158.64L162.39 158.23L162.36 157.82L162.31 157.41L162.25 157.01L162.18 156.61L162.09 156.23L162 155.84L161.89 155.47L161.77 155.1L161.63 154.74L161.49 154.38L161.34 154.04L161.17 153.7L161 153.37L160.81 153.05L160.62 152.74L160.41 152.44L160.2 152.14L159.98 151.86L159.74 151.59L159.5 151.33L159.26 151.08L159 150.84L158.74 150.62L158.47 150.4L158.19 150.2L157.91 150.02L157.62 149.84L157.32 149.68L157.02 149.53L156.71 149.4L156.4 149.28L156.08 149.18L155.75 149.09L155.42 149.01L155.09 148.95L154.75 148.91L154.41 148.89L154.07 148.88L153.72 148.89L153.38 148.91Z" id="c37TQceQUQ"/></defs><g><g><use xlink:href="#c37TQceQUQ" opacity="1" fill="#fff" fill-opacity="1"/></g></g></svg>
      <p style="display: inline-block; color: white; vertical-align: top; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 16px; line-height: 1; margin: 17px 0 0 0; padding: 0; ">RampWorld Cardiff, Unit B, Park Ty-Glas <br>Llanishen, Cardiff</p>
      </div>

      <div style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
        <svg xmlns="http://www.w3.org/2000/svg" style="width: 20px" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" preserveAspectRatio="xMidYMid meet" viewBox="162.56382978723403 257.96276595744683 44.69148936170211 36.55319148936172" width="40.69" height="32.55"><defs><path d="M163.56 291.52L163.56 258.96L204.26 258.96L204.26 291.52L163.56 291.52ZM183.65 280.66L167.38 267.3L167.38 287.18L200.44 287.18L200.44 266.79L183.7 280.54L183.65 280.66ZM183.6 274.36L197.06 263.3L170.14 263.3L183.6 274.36Z" id="duZwz7E5m"/></defs><g><g><use xlink:href="#duZwz7E5m" opacity="1" fill="#fff" fill-opacity="1"/></g></g></svg>
        <p style="display: inline-block; color: white; vertical-align: top; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 16px; line-height: 1; margin:7px 0 0 0; padding: 0;"><a href="mailto:rampworldcardiff@gmail.com" style="color: #5383d3 !important; text-decoration: none; display: inline-block; text-rendering: auto; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; font-style: normal; font-variant: normal; font-weight: normal; font-size: 14px; line-height: 1; font-family: FontAwesome; margin: 0; padding: 0;">rampworldcardiff@gmail.com</a></p>
      </div>

    </div>

    <div class="footer-right" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; vertical-align: top; width: 100%; text-align: center; margin: 0 0 40px; padding: 0;" align="center">

      <p class="footer-company-about" style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; font-weight: normal; font-size: 13px; line-height: 20px; color: #92999f; margin: 0; padding: 0;">

        <span style="font-family: \'Helvetica Neue\', \'Helvetica\', Helvetica, Arial, sans-serif; display: block; color: #ffffff; font-size: 14px; font-weight: bold; margin: 0 0 20px; padding: 0;">About RampWorld Cardiff</span>
        RampWorld Cardiff  is run to provide an indoor recreational facility for modern wheeled sports in Cardiff, South Wales for young people with the object of improving their condition of life and in particular to provide for such persons facilities for BMX cycling, scooters, skateboarding and other such activities in a safe indoor environment.
      </p>

      

    </div>
  </footer><!-- /FOOTER -->
</body>
</html>';
echo $email;