<?php
use Rampworld\AssetFinder\SVG as SVG;
require_once __dir__.'/../../../../modules/vendor/autoload.php';
?>
</div>
<footer>
	<div class="footer-left">
        <?php echo SVG::get('rampworld logo');?>

		<!-- Footer Navigation start -->
		<p class="footer-links">
			<a href="<?php echo base_url;?>">Home</a>
			&bull;
			<a href="<?php echo base_url;?>news">News</a>
			&bull;
			<a href="<?php echo base_url;?>opening-hours">Opening</a>
			&bull;
			<a href="<?php echo base_url;?>membership">Membership</a>
            &bull;
            <a href="<?php echo base_url;?>online-booking">Online booking</a>
			&bull;
			<a href="<?php echo base_url;?>rampworld-shop">The Shop</a>
            &bull;
			<a href="<?php echo base_url;?>events">Events</a>
			&bull;
			<a href="<?php echo base_url;?>contact">Contact Us</a>
		</p>
		<!-- Footer Navigation end -->

		<!-- Footer Website meta start -->
		<p class="footer-company-name">RampWorld Cardiff &copy; <?php echo date("Y"); ?></p>
		<p class="footer-company-name">Registed Charity Number 1152842</p>
		<?php if (!is_user_logged_in()) {?>
        <a class="footer-company-name" href="/wp-admin">Login</a>
        <?php } else {?>
        <a class="footer-company-name" href="<?php echo wp_logout_url( get_permalink() ); ?>">Logout</a>
        <?php }?>

        <!-- Footer Website meta end -->
    </div>
    <!-- Footer addresses start -->
    <div class="footer-center">

        <div>
            <p class="fas fa-map-marker-alt"><a style="color: white !important;vertical-align: inherit;" href="//www.rampworldcardiff.co.uk/contact">RampWorld Cardiff,<br>Unit B,<br>Park Ty-Glas,<br>Llanishen,<br>Cardiff,<br>CF14 5DU</a></p>
        </div>
        <div>
            <p class="fas fa-envelope" ><a href="mailto:&#114;&#097;&#109;&#112;&#119;&#111;&#114;&#108;&#100;&#099;&#097;&#114;&#100;&#105;&#102;&#102;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;">&#114;&#097;&#109;&#112;&#119;&#111;&#114;&#108;&#100;&#099;&#097;&#114;&#100;&#105;&#102;&#102;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;</a></p>
        </div>

    </div>
    <!-- Footer addresses end -->
    
    <!-- Footer company info start -->
    <div class="footer-right">

        <p class="footer-company-about">

            <span>About RampWorld Cardiff</span>
            RampWorld Cardiff is run to provide an indoor recreational facility for modern wheeled sports in Cardiff, South Wales for young people with the object of improving their condition of life and in particular to provide facilities for BMX cycling, scooters, skateboarding and other such activities in a safe indoor environment.
        </p>
            
        <!-- Footer social media start -->
        <div class="footer-icons">

            <a href="https://www.facebook.com/RampWorldCardiff" class="fab fa-facebook-f" target="_blank" ></a>
            <a href="https://www.instagram.com/rampworldcardiff" class="fab fa-instagram" target="_blank" ></a>

        </div>
        <!-- Footer social media end -->
        
    </div>
    
    <!-- Footer company info end -->
</footer>
</div>
<!-- FOOTER start -->

    <?php wp_footer(); ?>
    <script data-main="<?php echo base_url;?>wp-content/themes/rampworld/assets/dist/js/rwcui" src="<?php echo base_url;?>wp-content/themes/rampworld/assets/dist/thirdparty/js/require.min.js"></script>
</body>
</html>