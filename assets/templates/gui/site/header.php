<?php
use Rampworld\AssetFinder\SVG as SVG;
require_once __dir__.'/../../../../modules/vendor/autoload.php';
define("base_url", 'https://' . $_SERVER['SERVER_NAME'] . '/');

?>
<header>
  <div class="container-fluid clearfix">
    <ul id="navigationToggle" class="burger"><li></li><li></li> <li></li></ul>
    <div class="clearfix headerlogo">
      <a href="<?php echo base_url;?>">
        <?php echo SVG::get("Rampworld logo");?>
      </a>
    </div>
    <div class="clearfix nav-menu">
      <div class="header-navigation" >
        <nav id="menu" class="navigation" role="navigation">
          <ul id="nav">
            <li class="menu-item"><a href="<?php echo base_url;?>">Home</a></li>
            <li class="menu-item"><a href="<?php echo base_url;?>park/">Park</a></li>
            <li class="menu-item"><a href="<?php echo base_url;?>opening-hours/">Opening Hours &amp; Prices</a></li>
            <li class="menu-item"><a href="<?php echo base_url;?>private-hire/">Private Bookings/Parties</a></li>
            <li class="menu-item"><a href="<?php echo base_url;?>online-booking/">Online booking</a></li>
            <li class="menu-item"><a href="<?php echo base_url;?>membership/">Membership</a></li>
            <li class="menu-item"><a href="<?php echo base_url;?>shop/">Shop</a></li>
            <li class="menu-item"><a href="<?php echo base_url;?>events/">Events</a></li>
            <li class="menu-item"><a href="<?php echo base_url;?>contact/">Contact</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</header>