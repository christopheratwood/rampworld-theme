<nav class="store">
	<ul class="store_nav left">
		<li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
		<li class="">
			<a href="#">BMX <b class="caret bmx"></b></a>
			<div class="dd col-xs-12">
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Components</h3>
					<ul>
						<li><a href="#">Bars</a></li>
						<li><a href="#">Frame</a></li>
						<li><a href="#">Forks</a></li>
						<li><a href="#">Forks</a></li>	
						<li><a href="#">Grips</a></li>
						<li><a href="#">Headsets</a></li>
						<li><a href="#">Wheels</a></li>
					</ul>
				</div>
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Completes</h3>
					<ul>
						<li><a href="#">Bars</a></li>
						<li><a href="#">Frame</a></li>
						<li><a href="#">Decks</a></li>
						<li><a href="#">Forks</a></li>	
						<li><a href="#">Grips</a></li>
						<li><a href="#">Grip tapes</a></li>
						<li><a href="#">Headsets</a></li>
						<li><a href="#">Wheels</a></li>
					</ul>
				</div>
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Drive Chain</h3>
					<ul>
						<li><a href="#">Chains</a></li>
						<li><a href="#">Cranks</a></li>
						<li><a href="#">Pedals</a></li>
						<li><a href="#">Sprockets</a></li>
					</ul>
					 <h3>Brakes</h3>
					<ul>
						<li><a href="#">Cables</a></li>
						<li><a href="#">Calipers</a></li>
						<li><a href="#">Gyros</a></li>
						<li><a href="#">Levers</a></li>
						<li><a href="#">Pads</a></li>
					</ul>
				</div>
				<div class="hidden col-sm-visible col-sm-12 col-xs-visible col-xs-12 col-ms-visible col-ms-12"></div>
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Wheels</h3>
					<a class="heading">Complete</a>
					<ul>
						<li><a href="#">Front Wheels</a></li>
						<li><a href="#">Rear Wheels</a></li>
					</ul>
					<a class="heading">Hubs</a>
					<ul>

						<li><a href="">Front Hubs</a></li>
						<li><a href="">Rear Hubs</a></li>
					</ul>
					<a class="heading">Components</a>
					<ul>
						<li><a href="">Hub Guards</a></li>
						<li><a href="">Inner Tubes</a></li>
						<li><a href="">Rims</a></li>
						<li><a href="">Spokes</a></li>
						<li><a href="">Tyres</a></li>
					</ul>
				</div>
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Seating</h3>
					<ul>
						<li><a href="#">Seats</a></li>
						<li><a href="#">Seat Posts Scooters</a></li>
					</ul>
					<h3>Grinding</h3>
					<ul>
						<li><a href="#">Pegs</a></li>
						<li><a href="#">Wax</a></li>
					</ul>
				</div>
			</div>
		
		</li>
		<li class="">
			<a href="#">Scooters <b class="caret"></b></a>
			<div class="dd col-xs-12">
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Components</h3>
					<ul>
						<li><a href="#">Bars</a></li>
						<li><a href="#">Brakes</a></li>
						<li><a href="#">Decks</a></li>
						<li><a href="#">Forks</a></li>	
						<li><a href="#">Grips</a></li>
						<li><a href="#">Grip tapes</a></li>
						<li><a href="#">Headsets</a></li>
						<li><a href="#">Wheels</a></li>
					</ul>
				</div>
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Axels and Bolts</h3>
					<ul>
					   <li><a href="#">Scooter Axels</a></li>
						<li><a href="#">Clamp Bolts</a></li>
						<li><a href="#">Break Bolts</a></li>
						
					</ul>
				   
				</div>
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Completes</h3>
					<ul>
						<li><a href="#">Complete Scooters</a></li>
						<li><a href="#">Custom Scooters</a></li>
					   
						
					</ul>
				   
				</div>
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6">
					<h3>Miscellaneous</h3>
					<ul>
						<li><a href="#">Compression Kits</a></li>
						<li><a href="#">Wheel Bearings</a></li>
						 <li><a href="#">Pegs</a></li>
					</ul>
				</div>
				
			</div>
		</li>
		 <li class="">
			<a href="#">MTB <b class="caret"></b></a>
		</li>
		<li class="">
			<a href="#">Protection <b class="caret"></b></a>
			<div class="dd col-xs-12">
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6 w50">
					<h3>Junior/ Youth</h3>
					<ul>
						<li><a href="">Elbow Pads</a></li>
						<li><a href="">Gloves</a></li>
						<li><a href="">Gum Sheilds</a></li>
						<li><a href="">Knee Pads</a></li>
						<li><a href="">Pads Sets</a></li>
						<li><a href="">Shin Pads</a></li>

					</ul>
				</div>
				<div class="store-menu-dropdown col-md-3 col-sm-4 col-xs-6 col-ms-6 w50">
					<h3>Adult</h3>
					<ul>
						<li><a href="">Ankle Supports</a></li>
						<li><a href="">Elbow Pads</a></li>
						<li><a href="">Gloves</a></li>
						<li><a href="">Gum Sheilds</a></li>
						<li><a href="">Knee Pads</a></li>
						<li><a href="">Pads Sets</a></li>
						<li><a href="">Shin Pads</a></li>
						<li><a href="">Wrist Guards</a></li>
					   
						
					</ul>
				   
				</div>
				
			</div>
		</li>
	</ul>
	<ul class="store_nav right">
	

		<li class="box">
			
			<div class="search_c <?php echo ((isset($_GET['global_search_query'])) ? 'has_input' : '');?>">
				<span class="icon"><i class="glyphicon glyphicon-search"></i></span>
				<form action="/shop/search" method="GET"><input type="search" id="search" name="search_query" placeholder="Search..." value="<?php echo ((isset($_GET['global_search_query'])) ?$_GET['global_search_query']: '');?>"/></form>
			</div>
			
		</li>
	</ul>
</nav>