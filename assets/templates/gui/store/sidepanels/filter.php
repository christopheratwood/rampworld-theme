<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$cat = get_queried_object();
$args =  new stdClass();
$args->categories = array(
    'hierarchical' => 1,
    'show_option_none' => '',
    'hide_empty' => 0,
    'product_cat' => $cat->term->id
    
);
$args->products = array(
    'parent' => $cat->slug,
    'post_type' => 'product',
);

$selectedItems = new stdClass();

$subcats = get_categories( $args->categories );
$loop = new WP_Query( $args->products );



foreach( $subcats as $subcat ) {
    if( !in_array( $subcat->name, $selectedItems->categories ) ) {
        $selectedItems->categories[] = $subcat->name ;
    }
}
if( $loop->have_posts() ) {
    $selectedItems->price->max = 0.00;
    $selectedItems->products->count = 0;
    while ( $loop->have_posts() ) : $loop->the_post();
        global $product; 
        $colour = get_post_meta($product->id, 'colour', true);

        if( isset( $selectedItems->price->max ) && $selectedItems->price->max < $product->price) {
            $selectedItems->price->max = $product->price ;
        }
        
        $brands = wp_get_object_terms( $product->get_id(), 'pwb-brand' );
        foreach( $brands as $brand ) {
            if( !in_array( $brand->name, $selectedItems->brands ) ) {
                $selectedItems->brands[] = $brand->name ;
            }
        }
        $selectedItems->products->count++;
    endwhile;

}?>
<style>
    #steps-fivepercent-slider .ui-slider-tip {
        visibility: visible;
        opacity: 1;
        top: -30px;
    }
</style>
<script>

</script>
<h3>Filter</h3>

<div class="search-filtering">
    <div class="search-filtering-price">
        <p class="search-filtering-title">Filter by price (£)</p>
        <div id="product-slider"></div>
    </div>
    <div class="search-filtering-brands">
        <p class="search-filtering-title">Filter by price (£)</p>
        <div id="product-slider"></div>
    </div>
    <div class="search-filtering-variables">
        <input type="hidden" id="products-max-price" value="<?php echo $selectedItems->price->max;?>">
        <input type="hidden" id="products-total" value="<?php echo $selectedItems->products->count;?>">
    </div>
</div>
<?php
echo '<pre>';
print_r($selectedItems);
echo '</pre>';