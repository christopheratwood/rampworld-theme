<?php
    spl_autoload_register(function($class_name){

        include    ABSPATH.'wp-content/plugins/RWCommerce/classes/'.$class_name.'.php';
    });
    // since wordpress 4.5.3
    $setting = new Setting();
    $category = new Category();
    //$category = new Category();
    if($setting->get('display_popuarity_on_home') == 1) {
        $categories = $category->getSidePanel(true);
        
    } else {
        $categories = $category->getAll();
    }
?>

<h3>Popular Categories</h3>
<div class="list-group" id="sidepanel">

    <a class="list-group-item active" data-toggle="collapse" data-target="#scooter" data-parent="#accordion">Scooter</a>
    <div id="scooter" class="collapse in">
    <?php foreach($categories['scooter'] as $category):?>
        <a href="category.php?parent=scooter&amp;name=<?php echo str_replace(' ', '-', $category['display_name']);?>/" class="list-group-item"><?php echo ucwords($category['display_name']);?></a>
    <?php endforeach;?>
    </div>
    
    <a class="list-group-item active" data-toggle="collapse" data-target="#bmx" data-parent="#accordion">BMX</a>
    <div id="bmx" class="collapse">
    <?php foreach($categories['bmx'] as $category):?>
        <a href="/store/bmx/<?php echo str_replace(' ', '-', $category['display_name']);?>/" class="list-group-item"><?php echo ucwords($category['display_name']);?></a>
    <?php endforeach;?>
    </div>
    <a class="list-group-item active" data-toggle="collapse" data-target="#bmx" data-parent="#accordion">BMX</a>
    <div id="bmx" class="collapse">
    <?php foreach($categories['bmx'] as $category):?>
        <a href="/store/category/<?php echo $category['category_id'];?>/" class="list-group-item"><?php echo ucwords($category['display_name']);?></a>
    <?php endforeach;?>
    </div>
    
</div>

