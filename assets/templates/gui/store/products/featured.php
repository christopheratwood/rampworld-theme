<?php

$meta_query  = WC()->query->get_meta_query();
$tax_query   = WC()->query->get_tax_query();
$tax_query[] = array(
    'taxonomy' => 'product_visibility',
    'field'    => 'name',
    'terms'    => 'featured',
    'operator' => 'IN',
);
 
$args = array(
    'post_type'           => 'product',
    'post_status'         => 'publish',
    'posts_per_page'      => 10,
    'meta_query'          => $meta_query,
    'tax_query'           => $tax_query,
);
 
$featured_query = new WP_Query( $args );
     
if ($featured_query->have_posts()) {?>
    <div class="homepage-group homepage-group-featured">
        <h3>Featured Products</h3>
        <div class="g g-featured-products">
    <?php while ($featured_query->have_posts()) : 
     
        $featured_query->the_post();
         
        $product = get_product( $featured_query->post->ID );?>

        <div class="g g-item-product">
            <div class="g g-element-product-image">
                <?php echo woocommerce_get_product_thumbnail(); ?>
                <?php if( !empty( $product->sale_price ) ):?>
                    <span class="onsale">Sale!</span>
                <?php endif;?>
            </div>
            <div class="g g-element-product-name">
                <a href="<?php the_permalink(); ?>">
                    <h4><?php the_title(); ?></h4>
                </a>
            </div>
            <?php if( !empty( $product->sale_price ) ):?>
                <div class="g g-element-product-price">
                    <p class="text-danger"><del><strong>RRP: </strong>£<?php echo $product->regular_price; ?></del></p>
                    <p class="text-success"><strong>Now: </strong>£<?php echo $product->price; ?></p>
                </div>
            <?php else:?>
                <div class="g g-element-product-price">
                    £<?php echo $product->price; ?>
                </div>
            <?php endif;?>
            <div class="g g-element-product-view">
                <a href="<?php the_permalink(); ?>" class="btn btn-default">View</a>
            </div>
            
        </div>
    <?php endwhile;?>
        </div>
    </div>
<?php }?>