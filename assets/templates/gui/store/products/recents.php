<?php
    // Get WooCommerce Global
    global $woocommerce;

    // Get recently viewed product cookies data
    $viewed_products = ! empty( $_COOKIE['woocommerce_recently_viewed'] ) ? (array) explode( '|', $_COOKIE['woocommerce_recently_viewed'] ) : array();
    $viewed_products = array_filter( array_map( 'absint', $viewed_products ) );


    if ( empty( $viewed_products ) )
		echo __( 'You have not viewed any product yet!', 'rc_wc_rvp' );

	// Create the object
	ob_start();

    // Get products per page
	if( !isset( $per_page ) ? $number = 5 : $number = $per_page )

	// Create query arguments array
    $query_args = array(
    				'posts_per_page' => $number, 
    				'no_found_rows'  => 1, 
    				'post_status'    => 'publish', 
    				'post_type'      => 'product', 
    				'post__in'       => $viewed_products, 
    				'orderby'        => 'rand'
    				);

	// Add meta_query to query args
	$query_args['meta_query'] = array();

    // Check products stock status
    $query_args['meta_query'][] = $woocommerce->query->stock_status_meta_query();

	// Create a new query
	$r = new WP_Query($query_args);

	// If query return results
	if ( $r->have_posts() ) {?>

		<div class="homepage-group homepage-group-sale">
            <h3>Products you have recently viewed</h3>
            <div class="g g-recent-products">

                <?php while ( $r->have_posts()) {
                    $r->the_post();
                    global $product;?>

                <div class="g g-item-product">
                    <div class="g g-element-product-image">
                        <?php echo woocommerce_get_product_thumbnail();?>
                        <?php if( !empty( $product->sale_price ) ):?>
                            <span class="onsale">Sale!</span>
                        <?php endif;?>
                    </div>
                    <div class="g g-element-product-name">
                        <a href="<?php the_permalink(); ?>">
                            <h4><?php the_title(); ?></h4>
                        </a>
                    </div>
                    <?php if( !empty( $product->sale_price ) ):?>
                        <div class="g g-element-product-price">
                            <p class="text-danger"><del><strong>RRP: </strong>£<?php echo $product->regular_price; ?></del></p>
                            <p class="text-success"><strong>Now: </strong>£<?php echo $product->price; ?></p>
                        </div>
                    <?php else:?>
                        <div class="g g-element-product-price">
                            £<?php echo $product->price; ?>
                        </div>
                    <?php endif;?>
                    <div class="g g-element-product-view">
                        <a href="<?php the_permalink(); ?>" class="btn btn-default">View</a>
                    </div>
                </div>
                <?php } ?>

            </div>
        </div>

	<?php } ?>