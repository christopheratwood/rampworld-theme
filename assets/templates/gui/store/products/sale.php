<?php

$query_args = array(
    'posts_per_page'    => 8,
    'no_found_rows'     => 1,
    'post_status'       => 'publish',
    'post_type'         => 'product',
    'meta_query'        => WC()->query->get_meta_query(),
    'post__in'          => array_merge( array( 0 ), wc_get_product_ids_on_sale() )
);
$products = new WP_Query( $query_args );

     
if ($products->have_posts()) {?>

<div class="homepage-group homepage-group-sale">
    <h3>On sale items</h3>
    <div class="g g-sale-products">
        <?php while ($products->have_posts()) : 
        
            $products->the_post();
            
            $product = get_product( $products->post->ID );
            //print_r($product);?>

            <div class="g g-item-product">
                <div class="g g-element-product-image">
                    <?php echo woocommerce_get_product_thumbnail(); ?>
                    <span class="onsale">Sale!</span>
                </div>
                <div class="g g-element-product-name">
                    <a href="<?php the_permalink(); ?>">
                        <h4><?php the_title(); ?></h4>
                    </a>
                </div>
                <div class="g g-element-product-price">
                    <p class="text-danger"><del><strong>RRP: </strong>£<?php echo $product->regular_price; ?></del></p>
                    <p class="text-success"><strong>Now: </strong>£<?php echo $product->price; ?></p>
                </div>
                <div class="g g-element-product-view">
                    <a href="<?php the_permalink(); ?>" class="btn btn-default">View</a>
                </div>
            </div>
        <?php endwhile;?>
    </div>
</div>
<?php } ?>