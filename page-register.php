<?php
session_start();
require __DIR__ .'/modules/vendor/autoload.php';

use Rampworld\Member\Modify as MemberModify;
use Rampworld\Email\Send as Email;
use Rampworld\Form\Registration as Form;

/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @since BirdFILED 1.0
 * Template Name: Registration
 */

if (isset($_GET['clear'])) {
    session_unset();
    session_destroy();
    session_write_close();
    session_regenerate_id(true);
    wp_redirect('//www.rampworldcardiff.co.uk/membership/registration');
}
//form control
$stage = (isset($_GET['stage']) ? $_GET['stage'] : 1);
if (!isset($_SESSION['largest_stage'])) {
    $_SESSION['largest_stage'] = 1;
}


$complete = array();
$regErrors = array();

if (isset($_POST['stage'])) {
  $pass = array("failed" => 0);

  switch ($_GET['stage'] -1) {
    case 1:
      /*
       * Forename
       * Surname
       * Gender
       * DOB
       * Email
       * Number
       */

      //forename validation
      if (isset($_POST['forename']) && strlen($_POST['forename']) == 0) {
        $regErrors['forename'] = 'Forename is required.';
        $pass['failed']++;
      } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['forename'])) {
        $regErrors['forename'] = 'Forename must only contain characters.';
        $pass['failed']++;
      } elseif (strlen($_POST['forename']) < 2) {
        $regErrors['forename'] = 'Forename must be more 2 characters.';
        $pass['failed']++;
      } elseif (strlen($_POST['forename']) > 100) {
        $regErrors['forename'] = 'Forename must be under 100 characters.';
        $pass['failed']++;
      }
          //surname validation
      if (empty($_POST['form_surname'])) {
        $regErrors['surname'] = 'Surname is required.';
        $pass['failed']++;
      } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['form_surname'])) {
        $regErrors['surname'] = 'Surname must only contain characters.';
        $pass['failed']++;
      } elseif (strlen($_POST['form_surname']) < 2) {
        $regErrors['surname'] = 'Surname must be more 2 characters.';
        $pass['failed']++;
      } elseif (strlen($_POST['form_surname']) > 100) {
        $regErrors['surname'] = 'Surname must be under 100 characters.';
        $pass['failed']++;
      }

      //Gender
      if (empty($_POST['form_gender'] )) {
        $regErrors['gender'] = 'Your gender is required.';
        $pass['failed']++;
      } elseif (in_array( $_POST['form_gender'], array('m','f', 'o') ) != true) {
        $regErrors['gender'] = 'Please provide a valid gender.';
        $pass['failed']++;
      }
      $today = new \DateTime(date('d-m-Y H:i:s'));
      $dob = \DateTime::createFromFormat('d-m-Y', $_POST['form_dob_day'] .'-'. $_POST['form_dob_month'] .'-'. $_POST['form_dob_year']);
  
      $errors = \DateTime::getLastErrors();
      if (!empty($errors['warning_count'])) {
        $regErrors['dob'] = 'Please provide a valid Date of Birth.';
        $pass['failed']++;
      } else {
        if (is_bool($dob)) {
          $regErrors['dob'] = 'Please provide a valid Date of Birth.';
          $pass['failed']++;
        } else {
          $intval = $today->diff($dob);

          if (intval($intval->y) < 2) {
            $regErrors['dob'] = 'Your must be at least 2 years old.';
            $pass['failed']++;
          } elseif (intval($intval->y) >100) {
            $regErrors['dob'] = 'Your must be under 100 years old.';
            $pass['failed']++;
          }
        }
      }
      

      //email address
      if (empty($_POST['form_email_address'])) {
        $regErrors['email'] = 'Email address is required.';
        $pass['failed']++;
      } elseif (!filter_var($_POST['form_email_address'], FILTER_VALIDATE_EMAIL)) {
        $regErrors['email'] = 'Email address must be valid. We will use this to confirm your membership.';
        $pass['failed']++;
      } elseif (strlen($_POST['form_email_address']) > 255) {
        $regErrors['email'] = 'Email Address must number 255 characters.';
        $pass['failed']++;
      }
      //telephone
      if (empty($_POST['form_number'])) {
        $regErrors['number'] = 'Your mobile/ telephone number is required.';
        $pass['failed']++;
      } elseif (!is_numeric($_POST['form_number'])) {
        $regErrors['number'] ='Your number must consist of only digits.';
        $pass['failed']++;
      } elseif (strlen($_POST['form_number']) < 7 || strlen($_POST['form_number']) > 16) {
        $regErrors['number'] = 'Please enter a valid telephone number.';
        $pass['failed']++;
      }
      if (count($regErrors) != 0) {
        $stage = 1;
        $_SESSION['stage_1_complete'] = false;
      } else {
        $_SESSION['stage_1_complete'] = true;
        $_SESSION['largest_stage'] = 2;
      }
    if (!empty($_POST['forename']) && !empty($_POST['form_surname']) && !empty($_POST['form_email_address']) && !empty($_POST['form_number']) && !empty($_POST['form_dob_day']) && !empty($_POST['form_dob_year'])) {
        $_SESSION['forename'] = ucwords(strtolower(filter_var($_POST['forename'], FILTER_SANITIZE_STRING)));
        $_SESSION['form_surname'] = ucwords(strtolower(filter_var($_POST['form_surname'], FILTER_SANITIZE_STRING)));
        $_SESSION['form_email_address'] = strtolower( $_POST['form_email_address']);
        $_SESSION['form_number'] = $_POST['form_number'];
        $_SESSION['form_gender'] = $_POST['form_gender'];

        $_SESSION['form_dob_day'] = $_POST['form_dob_day'];
        $_SESSION['form_dob_month'] = $_POST['form_dob_month'];
        $_SESSION['form_dob_year'] = $_POST['form_dob_year'];
        $_SESSION['age']=$intval->y;
      }
      break;
    case 2:
      /*
        * Address line 1
        * Address line 2
        * City
        * County
        * Postcode
        */
      if (empty($_POST['address_line_one'])) {
        $regErrors['address_line_one'] = 'Please enter the first line of your address';
        $pass['failed']++;
      } elseif (strlen($_POST['address_line_one']) > 50) {
        $regErrors['address_line_one'] = 'First address line must be under 50 characters.';
        $pass['failed']++;
      }
      if (!empty($_POST['address_line_two']) && strlen($_POST['address_line_two']) > 50) {
        $regErrors['address_line_two'] = 'Second address line must be under 50 characters.';
        $pass['failed']++;
      }
      if (!empty($_POST['address_city']) && strlen($_POST['address_city']) > 50) {
        $regErrors['address_city'] = 'City line must be under 50 characters.';
        $pass['failed']++;
      }
      if (!empty($_SESSION['address_county']) && strlen($_POST['address_county']) > 50) {
        $regErrors['address_county'] = 'County line must be under 50 characters.';
        $pass['failed']++;
      }
      if (empty($_POST['address_postcode'])) {
        $regErrors['postcode'] = 'Please enter your postcode.';
        $pass['failed']++;
      } elseif (strlen($_POST['address_postcode']) < 5 || strlen($_POST['address_postcode']) > 10) {
        $regErrors['postcode'] = 'Please enter a valid postcode.';
        $pass['failed']++;
      }
      if (count($regErrors) != 0) {
        $stage = 2;
        $_SESSION['stage_2_complete'] = false;
      } else {
        if ($_SESSION['age'] >= 16) {
          $_SESSION['stage_3_complete'] = true;
          $stage = 4;
          $_SESSION['largest_stage'] = 4;
          wp_redirect('//www.rampworldcardiff.co.uk/membership/registration/?stage=4');
        }
        $_SESSION['stage_2_complete'] = true;
        $_SESSION['largest_stage'] = 3;
      }

      $_SESSION['address_line_one'] = ucfirst(strtolower($_POST['address_line_one']));
      $_SESSION['address_line_two'] = ucfirst(strtolower($_POST['address_line_two']));
      $_SESSION['address_city'] = ucfirst(strtolower($_POST['address_city']));
      $_SESSION['address_county'] = ucfirst(strtolower($_POST['address_county']));
      $_SESSION['address_postcode'] = strtoupper($_POST['address_postcode']);
      break;
    case 3:
      /*
        * Consent name
        * Consent number
        * Consent address line one
        * Consent address post code
        * Consent email
        */
      if (empty($_POST['form_consent_name'])) {
        $regErrors['form_consent_name'] = 'Please provide parent / guardian\'s name.';
        $pass['failed']++;
      } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['form_consent_name'])) {
        $regErrors['form_consent_name'] = 'Surname must only contain characters.';
        $pass['failed']++;
      } elseif (strlen($_POST['form_consent_name']) > 100) {
        $regErrors['form_consent_name'] = 'Surname must number 100 characters.';
        $pass['failed']++;
      }
      //parent number
      if (empty($_POST['form_consent_number'])) {
        $regErrors['form_consent_number'] = 'Please provide parent / guardian\'s first address line.';
        $pass['failed']++;
      } elseif (!is_numeric($_POST['form_consent_number'])) {
        $regErrors['form_consent_number'] ='Your number must consist of only digits.';
        $pass['failed']++;
      } elseif (strlen($_POST['form_consent_number']) < 7 || strlen($_POST['form_consent_number']) > 16) {
        $regErrors['consent_number'] = 'Please enter a valid telephone number.';
        $pass['failed']++;
      }
      
      //parent address
      if (empty($_POST['consent_address_line_one'])) {
        $regErrors['consent_address_line_one'] = 'Please provide parent / guardian\'s name.';
        $pass['failed']++;
      } elseif (strlen($_POST['consent_address_line_one']) > 50) {
        $regErrors['consent_address_line_one'] = 'Address line must be under 50 characters long.';
        $pass['failed']++;
      }

      //parent postcode
      if (empty($_POST['consent_address_postcode'])) {
        $regErrors['consent_address_postcode'] = 'Please enter your postcode.';
        $pass['failed']++;
      } elseif (strlen($_POST['consent_address_postcode']) < 5 || strlen($_POST['consent_address_postcode']) > 10) {
        $regErrors['consent_address_postcode'] = 'Please enter a valid postcode.';
        $pass['failed']++;
      }
      //parental email address
      if (empty($_POST['form_consent_email'])) {
        $regErrors['form_consent_email'] = 'Email address is required.';
        $pass['failed']++;
      } elseif (!filter_var($_POST['form_consent_email'], FILTER_VALIDATE_EMAIL)) {
        $regErrors['form_consent_email'] = 'Email address must be valid. We will use this to confirm your membership.';
        $pass['failed']++;
      } elseif (strlen($_POST['form_consent_email']) > 255) {
        $regErrors['form_consent_email'] = 'Email Address must number 255 characters.';
        $pass['failed']++;
      }
      if (!isset($_POST['member_id'])) {
        if (!isset($_POST['consentAccept']) || $_POST['consentAccept'] != 'true') {
          $regErrors['consentAccept'] = 'You must accept the terms and conditions.';
          $pass['failed']++;
        }
      }
      
      if (count($regErrors) != 0) {
        $stage = 3;
        $_SESSION['stage_3_complete'] = false;
      } else {
        $_SESSION['stage_3_complete'] = true;
        $_SESSION['largest_stage'] = 4;
      }
      

      $_SESSION['form_consent_name'] = ucwords(strtolower($_POST['form_consent_name']));
      $_SESSION['form_consent_number'] = $_POST['form_consent_number'];
      $_SESSION['consent_address_line_one'] = ucfirst($_POST['consent_address_line_one']);
      $_SESSION['consent_address_postcode'] = strtoupper($_POST['consent_address_postcode']);
      $_SESSION['form_consent_email'] = $_POST['form_consent_email'];
      break;

    case 4:
      /*
       * discipline
       * expertise
       */
      if (!isset($_POST['form_sport'])) {
        $regErrors['discipline'] = "Please pick your discipline(s).";
        $pass['failed']++;
      } else {
        if ((in_array( 'other', $_POST['form_sport']) !== false) && empty($_POST['sport_other_val'])) {
          $regErrors['discipline'] = "Please state your other discipline(Comma separated value).";
          $pass['failed']++;
        }
      }
      if (!isset($_POST['expertise']) || ($_POST['expertise'] < 0 || $_POST['expertise'] > 5)) {
        $regErrors['expertise'] = "Please click a valid option.";
        $pass['failed']++;
      }

      if (count($regErrors) != 0) {
        $stage = 4;
        $_SESSION['stage_4_complete'] = false;
      } else {
        $_SESSION['stage_4_complete'] = true;
        $_SESSION['largest_stage'] = 5;
        $stage = 5;
      }
      $_SESSION['expertise'] = $_POST['expertise'];
      $_SESSION['form_sport'] = implode(',', $_POST['form_sport']);
      $_SESSION['sport_other_val']  = ucwords(strtolower($_POST['sport_other_val']));
      
      break;
    case 5:
        /*
         * emergnecy name 1
         * emergnecy number 1
         * emergnecy name 2
         * emergnecy number 2
         * Medical
         */
      //ermengcy name 1
      if (empty($_POST['form_emergency1_name'])) {
        $regErrors['emergency1_name'] = "Please provide a emergency contact name.";
        $pass['failed']++;
      } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['form_emergency1_name'])) {
        $regErrors['emergency1_name'] = "Emergency name must only contain characters.";
        $pass['failed']++;
      } elseif (strlen($_POST['form_emergency1_name']) > 100) {
        $regErrors['emergency1_name'] = 'Name must number 100 characters.';
        $pass['failed']++;
      }
      //emergency number 1
      if (empty($_POST['form_emergency1_number'])) {
        $regErrors['emergency1_number'] = 'A emergency number is required.';
        $pass['failed']++;
      } elseif (!is_numeric($_POST['form_emergency1_number'])) {
        $regErrors['emergency1_number'] ='The emergency number must only consist of digits.';
        $pass['failed']++;
      } elseif (strlen($_POST['form_emergency1_number']) < 7 || strlen($_POST['form_emergency1_number']) > 16) {
        $regErrors['emergency1_number'] = 'Please enter a valid emergency number.';
        $pass['failed']++;
      }

        //emergency name 2
      if (!empty($_POST['form_emergency2_name']) && !empty($_POST['form_emergency2_number'])) {
        if (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['form_emergency2_name'])) {
          $regErrors['emergency2_name'] = "Emergency name must only contain characters. (a-z A-Z)";
          $pass['failed']++;
        } elseif (strlen($_POST['form_emergency2_name']) > 100) {
          $regErrors['emergency2_name'] = 'Name must number 100 characters.';
          $pass['failed']++;
        }
        if (!is_numeric($_POST['form_emergency2_number'])) {
          $regErrors['emergency2_number'] ='The emergency number must only consist of digits.';
          $pass['failed']++;
        } elseif (strlen($_POST['form_emergency2_number']) < 7 || strlen($_POST['form_emergency2_number']) > 16) {
          $regErrors['emergency2_number'] = 'Please enter a valid emergency number.';
          $pass['failed']++;
        }
      } elseif (!empty($_POST['form_emergency2_name']) && empty( $_POST['form_emergency2_number'] )) {
        $regErrors['form_emergency2_name'] = 'Please enter a valid emergency number.';
        $pass['failed']++;
      } elseif (empty($_POST['form_emergency2_name']) && !empty( $_POST['form_emergency2_number'] )) {
        $regErrors['form_emergency2_name'] = 'Please enter a valid emergency contact name.';
        $pass['failed']++;
      }
      if (count($regErrors) != 0) {
        $stage = 5;
        $_SESSION['stage_5_complete'] = false;
      } else {
        $_SESSION['stage_5_complete'] = true;
        $_SESSION['largest_stage'] = 6;
      }
      $_SESSION['form_emergency1_name'] = ucwords(strtolower($_POST['form_emergency1_name']));
      $_SESSION['form_emergency1_number'] = $_POST['form_emergency1_number'];
      $_SESSION['form_emergency2_name']  = ucwords(strtolower($_POST['form_emergency2_name']));
      $_SESSION['form_emergency2_number']  = $_POST['form_emergency2_number'];
      $_SESSION['form_medical_notes']  = ucfirst(strtolower($_POST['form_medical_notes']));
      break;
    case 6:
      if (!isset($_SESSION['forename']) && strlen($_SESSION['forename']) < 2) {
        wp_redirect('//www.rampworldcardiff.co.uk/membership/registration?stage=1');
      } else {
        $code = (isset($_SESSION['ver'])) ? $_SESSION['ver']:'';

        if (!isset($_POST['tandc'])) {
          $regErrors['tandc'] = 'Please accept the Terms and Conditions.';
          $pass['failed']++;
        } elseif (!isset($_POST['captcha']) || $code != strtolower(str_replace(' ', '', $_POST['captcha']))) {
          $regErrors['captcha'] = 'Verfication was incorrect. Please try again.';
          $pass['failed']++;
        }
    
        if (count($regErrors) != 0) {
          $stage = 6;
          $_SESSION['stage_6_complete'] = false;
        } else {
          $dob =  \DateTime::createFromFormat('d-m-Y', $_SESSION['form_dob_day'] .'-'. $_SESSION['form_dob_month'] .'-'. $_SESSION['form_dob_year']);
          $_SESSION['tandc'] = $_POST['tandc'];
          $data_tmp = array(
          'forename'                      => $_SESSION['forename'],
          'surname'                       => $_SESSION['form_surname'],
          'dob'                               => $_SESSION['form_dob_year'] .'-'. $_SESSION['form_dob_month'] .'-'.$_SESSION['form_dob_day'],
          'gender'                            => $_SESSION['form_gender'],
          'email'                             => $_SESSION['form_email_address'],
          'number'                            => $_SESSION['form_number'],
          'address_line_one'      => ucwords($_SESSION['address_line_one']),
          'address_line_two'      => ucwords($_SESSION['address_line_two']),
          'address_city'              => ucwords($_SESSION['address_city']),
          'address_county'            => ucwords($_SESSION['address_county']),
          'address_postcode'      => strtoupper($_SESSION['address_postcode']),
          'discipline'                    => str_replace('other', $_SESSION['sport_other_val'], $_SESSION['form_sport']),
          'expertise'                     => $_SESSION['expertise'],
          'medical_notes'             => $_SESSION['form_medical_notes'],
          'consent_name'              => ( isset( $_SESSION['form_consent_name'])) ? $_SESSION['form_consent_name']: '',
          'consent_number'            => ( isset( $_SESSION['form_consent_name'])) ? $_SESSION['form_consent_number']: '',
          'consent_address_line_one'      => ( isset( $_SESSION['form_consent_name'])) ? $_SESSION['consent_address_line_one']: '',
          'consent_address_postcode'      => ( isset( $_SESSION['form_consent_name'])) ? $_SESSION['consent_address_postcode']: '',
          'consent_email'             => ( isset( $_SESSION['form_consent_name']) ) ? $_SESSION['form_consent_email']: '',
          'emergency_name_1'      => $_SESSION['form_emergency1_name'],
          'emergency_number_1'    =>  $_SESSION['form_emergency1_number'] ,
          'emergency_name_2'      => (isset($_SESSION['form_emergency2_name']) ) ? ucwords($_SESSION['form_emergency2_name']): '',
          'emergency_number_2'    => (isset($_SESSION['form_emergency2_number']) ) ? $_SESSION['form_emergency2_number']: ''
          );
          $_SESSION['stage_6_complete'] = true;
          $_SESSION['largest_stage'] = 7;

          $membership = new MemberModify();
          $member_id = $membership->create( $data_tmp );
          
          
          if (is_int($member_id)) {

            $mailer = new Email( $data_tmp );
            
            if ($mailer->create_welcome( $member_id )) {
              session_unset();
              session_destroy();
              session_write_close();
              unset($_POST);
              $complete = array(
              'name'          => $data_tmp['forename']. ' '. $data_tmp['surname'],
              'mid'           => $member_id,
              'emails'        => array(
                'participant' => $data_tmp['email'],
                'consent'     => $data_tmp['consent_email']
                )
              );
              
            } else {
              session_unset();
              session_destroy();
              session_write_close();
              unset($_POST);
              $complete = array(
                'errorID'       => 2,
                'name'          => $data_tmp['forename']. ' '. $data_tmp['surname'],
                'mid'           => $member_id,
                'emails'        => array(
                  'participant' => $data_tmp['email'],
                  'consent'     => $data_tmp['consent_email']
                )
              );
            }
          } else {
            session_unset();
            session_destroy();
            session_write_close();
            unset($_POST);
            $complete = array(
            'errorID' => 1,
            'error'         => 'Could not create membership',
            
            );
          }
        }
      }
      break;
  }
}
if (empty($_SESSION) && ($_GET['stage'] != 7 && empty($complete))) {
  wp_redirect('//www.rampworldcardiff.co.uk/membership/registration?stage=1');
  session_unset();
  session_destroy();
  session_write_close();
} elseif (isset($_GET['stage']) && isset($_SESSION['largest_stage'])  && $_SESSION['largest_stage'] < $_GET['stage'] && count($regErrors) == 0) {
  wp_redirect('//www.rampworldcardiff.co.uk/membership/registration?stage='.$_SESSION['largest_stage']);
}

$form = new Form();
get_header();
?> 
</div>
<div class="status" id="registration_progress">
  <ul>
    <li>
      <a href="?stage=1" id="personal_status" <?php echo (isset($_SESSION['stage_1_complete']) ? (($_SESSION['stage_1_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
        <i class="registration_icon fa fa-user" aria-hidden="true"></i>
        <span class="content">Particpant Details</span>
        <span class="tablet_content">Particpant</span>
      </a>
    </li>
    <li>
      <a <?php echo (isset($_SESSION['stage_2_complete']) && isset($_GET['stage'])) ? 'href="?stage=2&next='.$_GET['stage'].'"' : '';?> id="address_status" <?php echo (isset($_SESSION['stage_2_complete']) ? (($_SESSION['stage_2_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
        <i class="registration_icon fa fa-home" aria-hidden="true"></i>
        <span class="content">Address</span>
        <span class="tablet_content">Address</span>
      </a>
    </li>
    <li>
      <a <?php echo (isset($_SESSION['stage_3_complete']) && isset($_SESSION['form_consent_name']) && isset($_GET['stage'])) ? 'href="?stage=3&next='.$_GET['stage'].'"' : '';?> id="parental_status" <?php echo (isset($_SESSION['stage_3_complete']) ? (($_SESSION['stage_3_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
        <i class="registration_icon fa fa-users" aria-hidden="true"></i>
        <span class="content">Parental Details</span>
        <span class="tablet_content">Parental</span>
      </a>
    </li>
    <li>
      <a <?php echo (isset($_SESSION['stage_4_complete']) && isset($_GET['stage'])) ? 'href="?stage=4&next='.$_GET['stage'] .'"': '';?> id="about_status" <?php echo (isset($_SESSION['stage_4_complete']) ? (($_SESSION['stage_4_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
        <i class="registration_icon fa fa-bicycle" aria-hidden="true"></i>
        <span class="content">About You</span>
        <span class="tablet_content">About</span>
      </a>
    </li>
    <li>
      <a <?php echo (isset($_SESSION['stage_5_complete']) && isset($_GET['stage'])) ? 'href="?stage=5&next='.$_GET['stage'] .'"': '';?> id="emergency_status" <?php echo (isset($_SESSION['stage_5_complete']) ? (($_SESSION['stage_5_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
          <i class="registration_icon fa fa-mobile" aria-hidden="true"></i>
          <span class="content">Emergency Information</span>
          <span class="tablet_content">Emergency</span>
      </a>
    </li>
    <li>
      <a <?php echo (isset($_SESSION['stage_5_complete']) && isset($_GET['stage'])) ? 'href="?stage=5&next='.$_GET['stage'].'"' : '';?> id="medical_status" <?php echo (isset($_SESSION['stage_5_complete']) ? (($_SESSION['stage_5_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
        <i class="registration_icon fa fa-health" aria-hidden="true"></i>
        <span class="content">Medical Information</span>
        <span class="tablet_content">Medical</span>
      </a>
    </li>
    <li>
      <a <?php echo (isset($_SESSION['stage_6_complete']) && isset($_GET['stage'])) ? 'href="?stage=6&next='.$_GET['stage'] .'"' : '';?> id="tandcs_status" <?php echo (isset($_SESSION['stage_6_complete']) ? (($_SESSION['stage_6_complete'] == 'true') ? 'class="complete"': (($_SESSION['stage_6_complete'] == 'false')? 'class="error"' : '')): '');?>>
        <i class="registration_icon fa fa-list" aria-hidden="true"></i>
        <span class="content">Terms and Conditions</span>
        <span class="tablet_content">T&C</span>
      </a>
    </li>
  </ul>
</div>


<div class="container center">
  <form action="#" method="post" id="registration-form" >
    <?php if ($stage == 3 && $_SESSION['age'] >= 16) {
      echo $form->getSection($stage + 1, $regErrors, $complete);
    } else {
      echo $form->getSection($stage, $regErrors, $complete);
    }?>
  </form>
</div>


<?php get_footer(); ?>
<noscript>
    <style>
        body{ width:100%; height: 100%; overflow:hidden; }
    </style>
    <div class="overlay"></div>

    <div class="container">
        <div class="error">
            <div class="l la"></div>
            <div class="l ra"></div>
        </div>
        <div class="body">
            <h1>You must have JavaScript enabled.</h1>
            <p class="medium">Please enable your JavaScript and refresh this page.</p>
            <p>This setting can be found in your browser's settings page. For more help, search 'How to turn on JavaScript' into your favorite search engine.</p>
        </div>
        <div class="footer">
            <p class="small">RampWorld Cardiff 2017 &copy;</p>
        </div>
    </div>

</noscript>