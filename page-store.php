<?php
if (session_status() == PHP_SESSION_NONE) {
	session_start();
}
spl_autoload_register(function ($class_name) {
    include  ABSPATH.'wp-content/plugins/RWCommerce/classes/'.$class_name.'.php';
});

$products = new Product();
$featured = $products->getFeatured();
$_SESSION['breadcrumbs']= array(array('name' => 'Store Homepage', 'url' => 'http://127.0.0.1/store/') , array('name' => 'Store Homepage', 'url' => 'http://127.0.0.1/store/'), array('name' => 'Store Homepage', 'url' => 'http://127.0.0.1/store/'));
get_header();

?>


<div id="rw_blog_wrapper">

<div class="col-md-9 col-sm-9 col-xs-12">

    
    <div class="jumbotron">
        <h1>Featured Products</h1> 
        <p>Here is a special collection of products that we think you might like</p> 
    </div>
    <div class="featured_products">
    <?php
        $i = 0;
    foreach ($featured as $item) :?>
            <div class="col-md-4 col-sm-6 col-xs-6 ">
                <div class="thumbnail">
                    <div class="product_image_container">
                        <?php if (getimagesize('http://127.0.0.1/wp-content/uploads/store/product/'.$item['image_src']) !== false) :?>
                            <img class="group list-group-image img-responsive img-rounded" src="http://127.0.0.1/wp-content/uploads/store/product/<?php echo $item['image_src'];?>" id="img_<?php echo $i;?>" data-zoom-image="http://127.0.0.1/wp-content/uploads/store/product/<?php echo $item['image_src'];?>">
                        <?php else :?>
                            <img class="group list-group-image img-responsive img-rounded" src="http://127.0.0.1/wp-content/uploads/store/product/noProduct.png" img_<?php echo $i;?>>
                        <?php endif;?>

                        <?php if ($item['sale_cost'] != null) :?>
                            <span class="sale_percentage">Sale <?php echo intval($item['sale_discount']);?>%</span>
                            <span class="product_cost">
                                <span class="old">
                                    Old £<?php echo number_format($item['cost'], 2);?>
                                </span>
                                <span class="new">
                                    Now £<?php echo number_format($item['sale_cost'], 2);?>
                                </span>
                            </span>

                        <?php else :?>
                            <span class="product_cost">

                                <span class="new">
                                    Now £<?php echo number_format($item['cost'], 2);?>
                                </span>
                            </span>
                        <?php endif;?>
                    </div>  
                    
                   
                     <div class="caption">
                        <h4 class="group inner list-group-item-heading">
                        <?php echo $item['display_name'];?></h4>
                        <?php foreach (explode(', ', $item['colours']) as $colour) :?>
                            <span class="product-colour <?php echo $colour;?>" data-toggle="tooltip" title="<?php echo ucwords($colour);?>"></span>
                        <?php endforeach;?>
                     </div>
                     <div class="row">
                        <div class="col-xs-12">
                            <a class="btn btn-default" href="<?php echo( isset($_SERVER['HTTPS']) ? "https://" : "http://").$_SERVER['HTTP_HOST'];?>/store/category/<?php echo $item['category_id'];?>/product/<?php echo $item['id'];?>/">View</a>
                            <a class="btn btn-success" >Add to cart</a>
                        </div>
                    </div>
                </div>
               

            </div>
    <?php $i++;
    endforeach;?>
    </div>
</div>
</div>
</div>
<script>
    <?php for ($y = 0; $y < $i; $y++) :?> 
        $("#img_<?php echo $y;?>").elevateZoom({zoomWindowFadeIn: 500, zoomWindowFadeOut: 500,zoomWindowPosition: 6, lensFadeIn: 500, lensFadeOut: 500 });
    <?php endfor; ?>
</script>
<?php get_footer();