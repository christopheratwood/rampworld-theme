<?php
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @since BirdFILED 1.0
 */
 
if( !isset($_GET['mid']) ){
    wp_redirect('http://www.rampworldcardiff.co.uk/membership');
} else {
    $key = hash('sha256', mid_key);
	$iv = substr(hash('sha256', mid_sec_key), 0, 16);
    $mid = openssl_decrypt(base64_decode($_GET['mid']), 'AES-256-CBC', $key, 0, $iv);

    get_header();
    ?>
    <div class="page-title">

        <p>Welcome <?php echo ucwords(str_replace('_', ' ', $_GET['forename']));?> to RampWorld Cardiff</p>

    </div><!-- End Page title -->

    <div id="rw_blog_wrapper">
        <div class="container center">
            
            <h1>You have successfully finished the registration process.</h1>
            <p class="medium">Below is your membership number. Please make sure you have this save this number.  We have sent you an email with all your membership details.</p>
            
            <p class="medium"><span class="bold"><?php echo ucwords(str_replace('_', ' ', $_GET['forename']));?></span> <?php echo ucwords(str_replace('_', ' ', $_GET['surname']));?></p>
            <span class="membershipNumber"><?php echo $mid;?></span>
                <p class="medium emailHighlight error">NOTE: Please check your junk folder and allow up to 1 hour for the email to arrive. Staff member will check all information upon arrival. Thank you.</p>
            <a href="http://www.rampworldcardiff.co.uk/membership" class="button">Register Here</a>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
            <script type="text/javascript">
                var idleTime =  0;
                var idelIncrement = setInterval( TimerIncre, 60000);

                $(this).mousemove( function( e ) {
                    
                    idleTime = 0;
                });
                $(this).keypress( function( e ) {
                    
                    idleTime = 0;
                });

                function TimerIncre() {
                
                    idleTime += 1;
                    if( idleTime > 4 ){
                        window.location = 'http://www.rampworldcardiff.co.uk/membership/registration?pre_req=destroy';
                    }
                }
            </script>
        </div>
    </div>
    <?php
    get_footer();
}