<?php
$category_id = intval(get_query_var('cid'));
$category_name = str_replace('+', ' ', get_query_var('cname'));
$product_id = str_replace('+', ' ', get_query_var('pid'));
$product_name = str_replace('+', ' ', get_query_var('pname'));

spl_autoload_register(function($class_name){
    include  ABSPATH.'wp-content/plugins/RWCommerce/classes/'.$class_name.'.php';
});
$setting = new Setting();
$products = new Product();
$category = new Category();
$stock = new Stock();

if($setting->get('show_stock_level') == 1) {
    $level = $stock->getStockLevel($product_id);
} else {

}

$product = $products->getProductView($product_id);
$categories = $category->getCategories($category_id);


if($product != false)
    if($category_name != false )
        $breadcrumbs = array(array('name' => 'Store Homepage', 'url' => 'http://127.0.0.1/store/') , array('name' => $category_name, 'url' => 'http://127.0.0.1/store/category/'.str_replace('', '+', $category_name)), array('name' => $product_name, 'url' => 'http://127.0.0.1/store/category/'.str_replace('', '+', $category_name).'/product/'.str_replace(' ', '+',$product_name)));
    else
        $breadcrumbs = array(array('name' => 'Store Homepage', 'url' => 'http://127.0.0.1/store/') , array('name' => 'Error', 'url' => ''), array('name' => ucwords($product_name), 'url' => 'http://127.0.0.1/store/category/'.str_replace('', '+', $category_name).'/product/'.str_replace(' ', '+',$product_name)));
else
     if($category_name != false )
         $breadcrumbs = array(array('name' => 'Store Homepage', 'url' => 'http://127.0.0.1/store/') , array('name' => $category_name, 'url' => 'http://127.0.0.1/store/category/'.$category_name), array('name' => 'Error', 'url' => ''));
    else
        $breadcrumbs = array(array('name' => 'Store Homepage', 'url' => 'http://127.0.0.1/store/') , array('name' => 'Error', 'url' => ''), array('name' => 'Error', 'url' => ''));
   

get_header();

require_once 'templates/gui/shop/page_navigation.php';
?>


    <div id="rw_blog_wrapper">
    <div class="row">
        <pre>
            <?php print_r($product);?>
        </pre>
    </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if( $product == false):?>
                    <div class="jumbotron small">
                        <h1>Opps, That product doesn't exist:(</h1>  
                        <p>Strange, That product doesn't exists in our system. Please contact customer support if you continue to have this issue.</p> 
                    </div>
                <?php else:?>
                    <div class="col-md-6 col-sm-6 col-xs-12 product_image_container" >
                        <div id='carousel-custom' class='carousel slide' data-ride='carousel'>
                            <div class='carousel-outer'>
                                <!-- me art lab slider -->
                                <div class='carousel-inner '>
                                    <?php
                                        if(empty($product['images'])):?>
                                            <div class=' active'>
                                                <img src='http://127.0.0.1/wp-content/uploads/store/product/noProduct.png' alt='No Image'  data-zoom-image="http://127.0.0.1/wp-content/uploads/store/product/noProduct.png"/>
                                            </div>
                                        <?php else:?>
                                            
                                            <?php $i = 0;foreach($product['images'] as $image):?>
                                            
                                                <?php if(getimagesize('http://127.0.0.1/wp-content/uploads/store/product/'.$image['src']) !== false):?>
                                                    <div class="item <?php echo ($i ==0)?'active':'';?>">
                                                    <img src="http://127.0.0.1/wp-content/uploads/store/product/<?php echo $image['src'];?>" alt="<?php echo ucwords($product['details']['display_name']);?> Product Image  - <?php echo $i + 1;?>" id="img_<?php echo $i;?>">
                                                    </div>
                                                <?php else:?>
                                                    <div class="item <?php echo ($i ==0)?'active':'';?>">
                                                        <img src='http://127.0.0.1/wp-content/uploads/store/product/noProduct.png' alt="<?php echo ucwords($product['details']['display_name']);?> No Image">  
                                                    
                                                    </div>
                                                <?php endif;?>
                                            <?php $i++; endforeach;?>
                                        <?php endif;?>
                                    <script>
                                        $("#zoom_05").elevateZoom({ zoomType    : "inner", cursor: "crosshair" });
                                    </script>
                                </div>
                                <!-- sag sol -->
                                <a class='left carousel-control' href='#carousel-custom' data-slide='prev'>
                                    <span class='glyphicon glyphicon-chevron-left'></span>
                                </a>
                                <a class='right carousel-control' href='#carousel-custom' data-slide='next'>
                                    <span class='glyphicon glyphicon-chevron-right'></span>
                                </a>
                            </div>
                            <h4>Other Images</h4>
                            <!-- thumb -->
                            <ol class='carousel-indicators mCustomScrollbar meartlab'>
                                <?php
                                    if(empty($product['images'])):?>
                                        <li data-target='#carousel-custom' data-slide-to='0' class="active">
                                            <img src='http://127.0.0.1/wp-content/uploads/store/product/noProduct.png' alt='No Image'/>
                                        </li>
                                    <?php else:?>
                                        
                                        <?php $i = 0;foreach($product['images'] as $image):?>
                                            
                                            <?php if(getimagesize('http://127.0.0.1/wp-content/uploads/store/product/'.$image['src']) !== false):?>
                                                <li data-target="#carousel-custom" data-slide-to="<?php echo $i;?>" class="<?php echo ($i ==0)?'active':'';?>">
                                                    <img src="http://127.0.0.1/wp-content/uploads/store/product/<?php echo $image['src'];?>" alt="<?php echo ucwords($product['details']['display_name']);?> Product Image  - <?php echo $i + 1;?>" >
                                                </li>
                                            <?php else:?>
                                                <div class="item <?php echo ($i ==0)?'active':'';?>">
                                                    <img src='http://127.0.0.1/wp-content/uploads/store/product/noProduct.png' alt="<?php echo ucwords($product['details']['display_name']);?> No Image">  
                                                
                                                </div>
                                            <?php endif;?>
                                        <?php $i++; endforeach;?>
                                    <?php endif;?>
                            </ol>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                    
                        <?php if(@getimagesize('http://127.0.0.1/wp-content/uploads/store/brands/'.$product['details']['logo']) !== false):?>
                            
                            <div class="pull-right">
                                <a href="http://127.0.0.1/store/category/brand/<?php echo $product['details']['brand_name'];?>" ><img src="http://127.0.0.1/wp-content/uploads/store/brands/<?php echo $product['details']['logo'];?>" alt="" class="brand-logo" data-toggle="tooltip" title="View more <?php echo ucwords($product['details']['brand_name']);?> products"></a>
                            </div>
                        <?php else :?>
                            <div class="pull-right">
                                <a href="http://127.0.0.1/store/category/brand/<?php echo $product['details']['brand_name'];?>" data-toggle="tooltip" title="View more <?php echo ucwords($product['details']['brand_name']);?> products" data-title><p class="lead"><?php echo ucwords($product['details']['brand_name']);?></p></a>

                            </div>
                        <?php endif;?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12" id="">
                        <div id="product_details">
                            <p class="lead"><?php echo ucwords($product['details']['display_name']);?></p>
                            <ol class="breadcrumb clear">
                                
                                <?php for($i = count($categories) - 1; $i >= 0; $i--):?>

                                    <li><a href="http://127.0.0.1/store/category/<?php echo str_replace(' ', '+', $categories[$i]['name']);?>/<?php echo $categories[$i]['category_id'];?>/"><?php echo ucwords($categories[$i]['name']);?></a></li>
                                    
                                <?php endfor;?>

                            </ol>
                            <div class="col-sm-7">
                                <h3>Product Description</h3>
                                <p style="max-height:150px"><?php echo $product['details']['description'];?></p>
                            
                                <div class="row">
                                    <?php if(count($product['variants'][0]) > 1):?>
                                        <h3>Colours</h3>
                                        <select name="product_var" id="product_var">
                                            <?php foreach($product['variants'] as $var):?>
                                                <option value="<?php echo $var['id'];?>"><?php echo ucwords($var['colours']);?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php endif;?>

                                </div>
                                <div class="row">
                                    <?php if(count($product['variants'][0]['sizes']) > 0):?>
                                        <h3>Sizing</h3>
                                        <select name="size" id="size">
                                            <?php foreach($product['variants'][0]['sizes'] as $size):?>
                                                <option value="<?php echo $size['barcode'];?>"><?php echo ucwords($size['display_name']);?> - <?php echo ucwords($size['start_size']).$size['unit'] .' - ' . ucwords($size['end_size']). $size['unit'];?></option>
                                            <?php endforeach;?>
                                        </select>
                                        <?php endif;?>
                                    <h3>Colours</h3>
                                </div>
                                

                            </div>
                            <?php if($product['details']['sale_cost'] != null):?>
                                <div class="col-sm-5 txt-center">
                                    <span class="sale-icon">On Sale!</span>
                                    <div class="product_view_cost">
                                        <span class="old">Was £<?php echo number_format($product['details']['cost'], 2);?></span>
                                        <span class="new">Now £<?php echo number_format($product['details']['sale_cost'], 2);?>                                    </span>
                                        <small class="percentage">Saving <?php echo intval($product['details']['sale_discount']);?>%</small>
                                        <h4 class="text-right">Sale Ends</h4>
                                        <h5 id="time_remaining"> </h5>
                                    </div>
                                </div>
                                <script>
                                    $("#time_remaining").countdown('<?php echo $product["details"]["sale_end"];?>', function(event) {
                                        $(this).text(
                                        event.strftime('%Dd %Hh %Mm %Ss')
                                        );
                                    });
                                </script>
                            <?php else:?>
                                <div class="col-sm-3 txt-center">
                                    <div class="product_view_cost">
                                        <span class="new">£<?php echo number_format($product['details']['cost'], 2);?> </span>
                                    </div>
                                </div>
                        <?php endif;?>
                        </div>

                    </div>

                <?php endif;?>
            
            </div>
        </div>
       
    </div>
  <script src="http://127.0.0.1/wp-content/themes/birdfield/js/store/product_selector.js" type="text/javascript"></script>
<script>

    var selection = new Selection();
    $('#product_var').on('change', function(e) {
        selection.renderOpts($(this).val());
    })
    <?php for($y = 0; $y < $i; $y++):?> 
        $("#img_<?php echo $y;?>").elevateZoom({zoomWindowFadeIn: 500, zoomWindowFadeOut: 500,zoomWindowPosition: 3, lensFadeIn: 500, lensFadeOut: 500 });
    <?php endfor; ?>
</script>
<?php get_footer();