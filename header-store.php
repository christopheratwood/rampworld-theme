<?php 
use Rampworld\Calendar\ClosedDates as ClosedDates;
use Rampworld\Component\StoreComponent as StoreComponent;
use Rampworld\Component\SiteComponent as SiteComponent;

require_once 'modules/vendor/autoload.php';
$closed = new ClosedDates();
?>

<!DOCTYPE htm <?php language_attributes(); ?>l>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

  <!-- Basic Page Needs -->
  <meta charset="utf-8">
  <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php wp_head(); ?>
  <link rel="stylesheet" href="//127.0.0.1/wp-content/themes/rampworld/assets/dist/css/rwcui.css">
</head>
<body <?php body_class( ); ?>>
  <div class="boxed">
    <?php SiteComponent::header();?>
    <?php StoreComponent::header();?>
    <div class="container">  