<?php
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @since BirdFILED 1.0
 * Template Name: RampWorld New  Homepage 
 */

get_header(); ?>



<?php while (have_posts()) : the_post();
the_content();
endwhile; ?>
</div>

<?php get_footer(); ?>
