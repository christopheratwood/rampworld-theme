<?php
/*
 *Template Name: Booking Template
 */
//form control

session_start();
require __DIR__ .'/modules/vendor/autoload.php';

use Rampworld\Booking\Init as Booking;
use Rampworld\Form\Booking as Form;
//require __DIR__ .'/classes/thirdparty/Booking.php';

$booking = new Booking();
//check if dates and availablilty it okay
if(isset($_SESSION['session']['session_start_time']))
	$booking->checkAvailable($_SESSION['session']['session_date'], $_SESSION['session']['session_id'], $_SESSION['session']['session_start_time'], $_SESSION['session']['session_end_time']);

if(isset($_SESSION['start_time']) && date('Y-m-d H:i:s', strtotime('+30 minutes', strtotime($_SESSION['start_time']))) <= date('Y-m-d H:i:s') ) {
	session_destroy();
	wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book/');
	
} else if(!isset($_SESSION['start_time'])){
	$_SESSION['start_time'] = date('Y-m-d H:i:s');
}

//devopment
if(isset($_GET['ACTION']) && $_GET['ACTION'] = 'clear_session'){
	session_destroy();
	wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book/');
}

$stage = (isset($_GET['next']))? $_GET['next'] : (isset($_GET['stage']) ? $_GET['stage'] : 1);

if(!isset($_SESSION['largest_stage']))
	$_SESSION['largest_stage'] = 1;

$formErrors = [];
$complete = [];

if(isset($_GET['approved']) && $_GET['approved'] == true) {
	$_SESSION['transaction']['ppp'] = $_GET['PayerID'];
}

if(isset($_POST['stage'])) {
	$pass = array("failed" => 0);
	
	switch($_POST['stage']){

		case 1:
			$_SESSION['stages']['1_complete'] = true;
			$_SESSION['largest_stage'] = 2;
			$stage = isset($_GET['next']) ? $_GET['next'] : 2;
			
			$_SESSION['session']['session_start_time'] = filter_var($_POST['session_start'], FILTER_SANITIZE_STRING);
			$_SESSION['session']['session_end_time'] = filter_var($_POST['session_end'], FILTER_SANITIZE_STRING);

			$_SESSION['session']['session_date'] = filter_var($_POST['session_date'], FILTER_SANITIZE_STRING);
			$_SESSION['session']['session_type'] = filter_var($_POST['session_type'], FILTER_SANITIZE_STRING);
			$_SESSION['session']['session_id'] = filter_var($_POST['session_id'], FILTER_SANITIZE_STRING);
			$_SESSION['session']['session_name'] = filter_var($_POST['session_name'], FILTER_SANITIZE_STRING);
			$_SESSION['session']['session_beginner'] = filter_var($_POST['session_beginner'], FILTER_SANITIZE_STRING);
			$_SESSION['session']['session_disciplines'] = filter_var($_POST['session_disciplines'], FILTER_SANITIZE_STRING);
			$_SESSION['session']['session_pass_exclusion'] = filter_var($_POST['session_pass_exclusion'], FILTER_SANITIZE_STRING);

			//cost
			if(!isset($_SESSION['cost']['total_cost'])) {

				$_SESSION['cost']['session_cost'] = number_format(filter_var($_POST['session_cost'], FILTER_SANITIZE_STRING), 2);
				$_SESSION['cost']['total_cost'] = number_format(0.5, 2);
				$_SESSION['cost']['booking'] = 0.50;
			} else {
				$_SESSION['cost']['session_cost'] = number_format(filter_var($_POST['session_cost'], FILTER_SANITIZE_STRING), 2);
				if(isset($_SESSION['participants']['total']))
					$_SESSION['cost']['total_cost'] = number_format($_SESSION['participants']['total'] * $_SESSION['cost']['session_cost'] +0.5, 2);
			}
			
			break;
		case 2:
			//Booking name
			if(!isset($_POST['account_id'])) {
				
				if(isset($_POST['form_booking_name']) && strlen($_POST['form_booking_name']) == 0) {
					$formErrors['booking_name_error'] = 'Booking name is required.';
					$pass['failed']++;
				} else if(!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['form_booking_name'])) {
					$formErrors['booking_name_error'] = 'Booking name must only contain characters.';
					$pass['failed']++;
				} else if(strlen($_POST['form_booking_name']) < 2) {
					$formErrors['booking_name_error'] = 'Booking name must be more 2 characters.';
					$pass['failed']++;
				} else if(strlen($_POST['form_booking_name']) > 100) {
					$formErrors['booking_name_error'] = 'Booking name must be under 100 characters.';
					$pass['failed']++;
				}
				//email address
				if(empty($_POST['form_booking_email'])) {
					$formErrors['booking_email_error'] = 'Email address is required.';
					$pass['failed']++;
				} else if(!filter_var($_POST['form_booking_email'], FILTER_VALIDATE_EMAIL)) {
					$formErrors['booking_email_error'] = 'Email address must be valid. We will use this to confirm your membership.';
					$pass['failed']++;
				} else if(strlen($_POST['form_booking_email']) > 255) {
					$formErrors['booking_email_error'] = 'Email Address must number 255 characters.';
					$pass['failed']++;
				}
				//telephone
				if(empty($_POST['form_booking_number'])) {
					$formErrors['booking_number_error'] = 'Your mobile/ telephone number is required.';
					$pass['failed']++;
				} else if(!is_numeric($_POST['form_booking_number'])) {
					$formErrors['booking_number_error'] ='Your number must consist of only digits.';
					$pass['failed']++;
				} else if(strlen($_POST['form_booking_number']) < 7 || strlen($_POST['form_booking_number']) > 16) {
					$formErrors['booking_number_error'] = 'Please enter a valid telephone number.';
					$pass['failed']++;
				}
			}


			if(count($formErrors) == 0){
				$_SESSION['stages']['2_complete'] = true;
				$_SESSION['largest_stage'] = 3;
				$stage = isset($_GET['next']) ? $_GET['next'] : 3;
				
			} else {
				$_SESSION['stages']['2_complete'] = false;
				$stage = 2;
			}

			$_SESSION['booking']['member_number'] = filter_var($_POST['form_booking_member_number'], FILTER_SANITIZE_STRING);
			$_SESSION['booking']['name'] = ucwords(strtolower(filter_var($_POST['form_booking_name'], FILTER_SANITIZE_STRING)));
			$_SESSION['booking']['number'] = filter_var($_POST['form_booking_number'], FILTER_SANITIZE_STRING);
			$_SESSION['booking']['email'] = filter_var($_POST['form_booking_email'], FILTER_SANITIZE_STRING);

			
			
			break;
		case 3:
			if(isset($_SESSION['participants']['total']) && $_SESSION['participants']['total'] > 0) {
				echo 'as';
				$_SESSION['stages']['3_complete'] = true;
				$_SESSION['largest_stage'] = 4;
				$stage = 4;
			} else {
				echo 'as';
				$_SESSION['stages']['3_complete'] = false;
				$stage = 3;
			}
			break;
	}
	
	wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book/?stage='.$stage);
}
if(isset($_GET['stage']) && isset($_SESSION['largest_stage'])  && $_GET['stage'] > $_SESSION['largest_stage'] ) {
	wp_redirect('https://www.rampworldcardiff.co.uk/online-booking/book?stage='.$_SESSION['largest_stage']);
}

$form = new Form();
get_header();
?> 
</div>
<div class="status" id="booking_progress">
	<ul><li>	<a <?php echo (isset($_SESSION['stages']['1_complete']) && isset($_GET['stage'])) ? 'href="?stage=1&next='.$_GET['stage'] .'"' : '';?> id="personal_status" <?php echo (isset($_SESSION['stages']['1_complete']) ? (($_SESSION['stages']['1_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
				<i class="registration_icon fa fa-calendar-alt" aria-hidden="true"></i>
				<span class="content">Session Details</span>
				<span class="tablet_content">Session</span>
			</a>
		</li>

		<li>
			<a <?php echo (isset($_SESSION['stages']['2_complete']) && isset($_GET['stage'])) ? 'href="?stage=2&next='.$_GET['stage'].'"' : '';?> id="address_status" <?php echo (isset($_SESSION['stages']['2_complete']) ? (($_SESSION['stages']['2_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
				<i class="registration_icon fas fa-info" aria-hidden="true"></i>
				<span class="content">Booking Details</span>
				<span class="tablet_content">Booking</span>
			</a>
		</li>
		<li>
			<a <?php echo (isset($_SESSION['stages']['3_complete']) && isset($_GET['stage'])) ? 'href="?stage=3&next='.$_GET['stage'].'"' : '';?> id="parental_status" <?php echo (isset($_SESSION['stages']['3_complete']) ? (($_SESSION['stages']['3_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
				<i class="registration_icon fa fa-users" aria-hidden="true"></i>
				<span class="content">Participants Details</span>
				<span class="tablet_content">Participants</span>
			</a>
		</li>
		<li>
			<a <?php echo (isset($_SESSION['stages']['4_complete']) && isset($_GET['stage'])) ? 'href="?stage=4&next='.$_GET['stage'] .'"': '';?> id="about_status" <?php echo (isset($_SESSION['stages']['4_complete']) ? (($_SESSION['stages']['4_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
				<i class="registration_icon fa fa-check" aria-hidden="true"></i>
				<span class="content">Confirm</span>
				<span class="tablet_content">Confirm</span>
			</a>
		</li>
		<li>
			<a <?php echo (isset($_SESSION['stages']['5_complete']) && isset($_GET['stage'])) ? 'href="?stage=5&next='.$_GET['stage'] .'"': '';?> id="emergency_status" <?php echo (isset($_SESSION['stages']['5_complete']) ? (($_SESSION['stages']['5_complete'] == 'true') ? 'class="complete"': 'class="error"'): '');?>>
				<i class="registration_icon fab fa-paypal" aria-hidden="true"></i>
				<span class="content">Payment</span>
				<span class="tablet_content">Payment</span></a>
		</li>
	</ul>
</div>
</div>
</div>

<div class="container center">
		<form action="#" method="post" id="booking-form" >
			<input type="hidden" name="stage" value="<?php echo $stage;?>">
			<?php echo $form->getSection($stage , $formErrors, $complete);?>

		</form>

				
</div>
<?php

get_footer();?>