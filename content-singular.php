<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @since BirdFILED 1.09
 */
?>

	<?php the_content(); ?>
	<?php wp_link_pages( array(
		'before'		=> '<div class="page-links">' . __( 'Pages:', 'birdfield' ),
		'after'			=> '</div>',
		'link_before'	=> '<span> < ',
		'link_after'	=> '</span>'
		) ); ?>

<?php if( is_single() ): // Only Display Excerpts for Single ?>
	<div class="meta">
		<div class="readmore">
			<a href="<?php the_permalink(); ?>" class="button">Read More</a>
		</div>
		<div class="category">
			<p class="fa fa-tag"><?php echo get_the_category()[0]->name;?></p>
		</div><div class="postDate">
			<p class="fa fa-clock"><?php echo get_post_time('dS M Y' );?></span>
			
		</div><div class="author">
			<p class="fa fa-user"><?php the_author(); ?></p>
		</div>
	</div>
	
	</footer>
<?php endif; ?>

