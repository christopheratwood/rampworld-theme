<?php
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @si
 */
get_header();



?>
<div id="pageTitle">
	<p>News</p>
</div>

<div class="main">
	<?php query_posts( 'cat=11' ); ?>
	<?php $id=0; if ( have_posts() ) : while ( have_posts() ) : the_post(); $id++;?>
	 <div class="news-item" data-squence="<?php echo $id;?>"><?php the_post_thumbnail();?>

		<span class="title"><?php the_title(); ?></span>
		<small><?php the_content(); ?></small>
	   <a href="" class="read-more"></a>
	</div>
	<?php endwhile;endif ?>
	<?php wp_reset_query(); ?>s
</div>
<div class="left">
	
</div>
<div class="other_news">
	
</div>
<?php
get_footer();