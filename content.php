<?php
/**
 * The default template for displaying content
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @since BirdFILED 1.0
 */
?>

<li id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php the_post_thumbnail( 'thumbnail' ); ?>
	<div class="entry-content"><?php the_excerpt(); ?></div>
	</a>
</li>
