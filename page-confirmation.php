<?php
/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @si
 */



if( !isset( $_SESSION['form_complete'] ) && $_SESSION['form_complete'] != true ) {
	header( 'Location: ../registration' );
	die();
} else  {
	$details = $_SESSION;

}
get_header();

if( isset( $_POST['complete'] ) ) {
	$gender = array_keys(array('m' => 'Male', 'f' => 'Female', 'o' => 'Other'), $_SESSION['form_gender']);
	if( isset($_SESSION['member_id'])) {
		$dob =  DateTime::createFromFormat('d-F-Y',$_SESSION['form_dob_day'] .'-'. $_SESSION['form_dob_month'] .'-'. $_SESSION['form_dob_year']);
		
		$data = array(
			'forename' 			=> ucfirst($_SESSION['forename']),
			'surname' 			=> ucfirst($_SESSION['form_surname']),
			'dob' 				=> $dob->format('d-m-Y'),
			'gender'			=> $gender[0],
			'email' 			=> $_SESSION['form_email_address'],
			'number' 			=> $_SESSION['form_number'],
			'address_line_one'	=> $_SESSION['address_line_one'],
			'address_line_two'	=> $_SESSION['address_line_two'],
			'address_city' 		=> $_SESSION['address_city'],
			'address_county' 	=> $_SESSION['address_county'],
			'address_postcode' 	=> strtoupper($_SESSION['address_postcode']),
			'discipline' 		=> $_SESSION['sport'] . $_SESSION['sport_other_val'],
			'expertise' 		=> $_SESSION['expertise'],
			'medical_notes' 	=> $_SESSION['form_medical_notes'],
			'consent_name' 				=> ucwords($_SESSION['form_consent_name']),
			'consent_number' 			=> $_SESSION['form_consent_number'],
			'consent_address_line_one'	=> $_SESSION['consent_address_line_one'],
			'consent_address_postcode' 	=> strtoupper($_SESSION['consent_address_postcode']),
			'consent_email' 		=> $_SESSION['form_consent_email'],
			'emergency_name_1' 		=> ucwords($_SESSION['form_emergency1_name']),
			'emergency_number_1' 	=> $_SESSION['form_emergency1_number'],
			'emergency_name_2' 		=> ucwords($_SESSION['form_emergency2_name']),
			'emergency_number_2' 	=> $_SESSION['form_emergency2_number'],
			'emergency_id_1' 		=> $_SESSION['emergency_1_id'], 
			'emergency_id_2' 		=> $_SESSION['emergency_2_id'],
			'consent_id' 			=> $_SESSION['consent_id']);

		require_once 'classes/thirdparty/Membership.php';
		$membership = new Membership();
		$update = $membership->update_member($_SESSION['member_id'], $data );
		
		if( $update === true) {
			$memid = $_SESSION['member_id'];
			session_unset();
			session_destroy();
			session_write_close();
			
		
			if( is_user_logged_in() ) {
				get_header();?>
				<div class="page-title">

					<p>Success!</p>

				</div><!-- End Page title -->

				<div id="rw_blog_wrapper">
					<div class="container center">
						<h1>Membership has been successfully updated.</h1>
						<span class="membershipNumber"><?php echo $memid ;?></span>
						<a href="<?php echo esc_url(site_url( '/wp-admin/admin.php?page=membership%2Fmembership.php%2FChange-details' ));?>" class="button block">Go Back</a>
					</div>
					
				</div>	
			<?php

			get_footer();
			
				
			} else {
				wp_redirect('/');
			}
		} else {
			get_header();
		?>
		<div class="page-title">

			<p>Something went wrong:(</p>

		</div><!-- End Page title -->

		<div id="rw_blog_wrapper">
			<div class="container center">
				<h1>Sorry, an error occurred.</h1>
				<p class="medium">Details could not be updated. Please try again.</p>
				<small><?php echo $update;?></small>
			</div>
		</div>	
			<?php

			get_footer();
		}



	} else {
		$dob =  DateTime::createFromFormat('d-F-Y',$_SESSION['form_dob_day'] .'-'. $_SESSION['form_dob_month'] .'-'. $_SESSION['form_dob_year']);

		$data_tmp = array(
			'forename' 			=> ucfirst($_SESSION['forename']),
			'surname' 			=> ucfirst($_SESSION['form_surname']),
			'dob' 				=> $dob->format('d-m-Y'),
			'gender'			=> $gender[0],
			'email' 			=> $_SESSION['form_email_address'],
			'number' 			=> $_SESSION['form_number'],
			'address_line_one'	=> ucwords($_SESSION['address_line_one']),
			'address_line_two'	=> ucwords($_SESSION['address_line_two']),
			'address_city' 		=> ucwords($_SESSION['address_city']),
			'address_county' 	=> ucwords($_SESSION['address_county']),
			'address_postcode' 	=> strtoupper($_SESSION['address_postcode']),
			'discipline' 		=> $_SESSION['sport'],
			'expertise' 		=> $_SESSION['expertise'],
			'medical_notes' 	=> $_SESSION['form_medical_notes'], 
			'consent_name' 				=> ( isset( $_SESSION['parentalVis']) && ( $_SESSION['parentalVis'] ) == true ) ? ucwords($_SESSION['form_consent_name']): '',
			'consent_number' 			=> ( isset( $_SESSION['parentalVis']) && ( $_SESSION['parentalVis'] ) == true ) ? ucwords($_SESSION['form_consent_number']): '',
			'consent_address_line_one'	=> ( isset( $_SESSION['parentalVis']) && ( $_SESSION['parentalVis'] ) == true ) ? ucwords($_SESSION['consent_address_line_one']): '',
			'consent_address_postcode' 	=> ( isset( $_SESSION['parentalVis']) && ( $_SESSION['parentalVis'] ) == true ) ? strtoupper($_SESSION['consent_address_postcode']): '',
			'consent_email' 			=> ( isset( $_SESSION['parentalVis']) && ( $_SESSION['parentalVis'] ) == true ) ? $_SESSION['form_consent_email']: '',
			'emergency_name_1' => ucwords($_SESSION['form_emergency1_name']),
			'emergency_number_1' =>  $_SESSION['form_emergency1_number'] ,
			'emergency_name_2' => (isset($_SESSION['form_emergency2_name']) ) ? ucwords($_SESSION['form_emergency2_name']): '',
			'emergency_number_2' => (isset($_SESSION['form_emergency2_number']) ) ? $_SESSION['form_emergency2_number']: ''
		);

		require_once 'classes/thirdparty/Membership.php';

		$membership = new Membership();
		$member_id = $membership->create( $data_tmp );
			
			
		if( is_int($member_id) ) {

			require_once 'classes/thirdparty/Mailer.php';
			$mailer = new Mailer( $data_tmp );
				
			if($mailer->create_welcome( $member_id ) ) {

				if( $mailer->send() == true ) {
				
					
					session_unset();
					session_destroy();
					session_write_close();
					
					get_header();
					?>
					<div class="page-title">

						<p>Welcome <?php echo $data_tmp['forename'];?> to RampWorld Cardiff</p>

					</div><!-- End Page title -->

					<div id="rw_blog_wrapper">
						<div class="container center">
							
							<h1>You have successfully finished the registration process.</h1>
							<p class="medium">Below is your membership number. Please make sure you have this save this number.  We have sent you an email with all your membership details.</p>
							
							<p class="medium"><span class="bold"><?php echo $data_tmp['forename'];?></span> <?php echo $data_tmp['surnamee'];?></p>
							<span class="membershipNumber"><?php echo $member_id;?></span>
								<p class="medium emailHighlight error">NOTE: Please check your junk folder and allow up to 1 hour for the email to arrive. Staff member will check all information upon arrival. Thank you.</p>
							<a href="http://www.rampworldcardiff.co.uk/membership" class="button">Register Here</a>
							<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
							<script type="text/javascript">
								var idleTime =  0;
								var idelIncrement = setInterval( TimerIncre, 60000);

								$(this).mousemove( function( e ) {
									
									idleTime = 0;
								});
								$(this).keypress( function( e ) {
									
									idleTime = 0;
								});

								function TimerIncre() {
								
									idleTime += 1;
									if( idleTime > 4 ){
										window.location = 'http://www.rampworldcardiff.co.uk/membership/registration?pre_req=destroy';
									}
								}
							</script>
						</div>
					</div>
					<?php
					get_footer();
				} else {

					get_header();
					?>
					<div class="page-title">
		
						<p>Something went wrong:(</p>

					</div><!-- End Page title -->

					<div id="rw_blog_wrapper">
						<div class="container center">
							<p class="medium">Please go back and check that you have filled the form in correctly</p>
							<small><?php echo $mailer->errorMessage();?></small>
						</div>
					</div>
					<?php
					get_footer();

				}

			} else {

					get_header();
					?>
					<div class="page-title">
		
						<p>Something went wrong:(</p>

					</div><!-- End Page title -->

					<div id="rw_blog_wrapper">
						<div class="container center">
							<p class="medium">Please go back and check that you have filled the form in correctly</p>
							<small><?php echo $mailer->errorMessage();?></small>
						</div>
					</div>
					<?php
					get_footer();
			}
		} 
	}
	
} else {
	$dob_preview =  DateTime::createFromFormat('d-F-Y',$_SESSION['form_dob_day'] .'-'. $_SESSION['form_dob_month'] .'-'. $_SESSION['form_dob_year']);
	get_header();
?>
<div class="page-title">
    
	<p><?php the_title(); ?></p>

</div><!-- End Page title -->

<div id="rw_blog_wrapper">
	<div class="container center">
		
		<div class="registration-form">
			<h1>You're almost done!</h1>
			<h2>Please confirm your details.</h2>
			<div class="section">
				<p class="medium">Participant Personal Details</p>
				<?php echo '<table class="clean minimised">
					<thead>
						<th>Forename</th>
						<th>Surname</th>

					</thead>
					<tbody>
						<tr>
					      <td data-label="Forename">'.$_SESSION['forename'].'</td>
					      <td data-label="Surname">'.$_SESSION['form_surname'].'</td>
					    </tr>
					</tbody>
				</table>
				<table class="clean minimised">
					<thead>
						<th>Gender</th>
						<th>Date of Birth</th>
					</thead>
					<tbody>
					    <tr>
					      <td data-label="Gender">'.$_SESSION['form_gender'].'</td>
					      <td data-label="Date of Birth">'.$dob_preview->format('d-m-Y').'</td>
					    </tr>
					    
					</tbody>
				</table>
				<table class="clean minimised">
					<thead>
						<th>Email Address</th>
						<th>Number</th>

					</thead>
					<tbody>
						<tr>
					      <td data-label="Email Address">'.$_SESSION['form_email_address'].'</td>
					      <td data-label="Number">'.$_SESSION['form_number'].'</td>
					    </tr>
					    
					</tbody>
				</table>
			</div>';
			if($_SESSION['parentalVis'] == true) {
				echo '<div class="section">
				<p class="medium">Participant Personal Details</p>
					<table class="clean minimised">
						<thead>
							<th>Parent / Guardian Name</th>
							<th>Parent / Guardian Number</th>
							<th>Parent / Guardian Address</th>
							<th>Parent / Guardian Postcode</th>
							<th>Parent / Guardian Email Address</th>

						</thead>
						<tbody>
							<tr>
						      <td data-label="Parent / Guardian Name">'.$_SESSION['form_consent_name'].'</td>
						      <td data-label="Parent / Guardian Number">'.$_SESSION['form_consent_number'].'</td>
						      <td data-label="Parent / Guardian Address">'.$_SESSION['consent_address_line_one'].'</td>
						      <td data-label="Parent / Guardian Postcode">'.$_SESSION['consent_address_postcode'].'</td>
						      <td data-label="Parent / Guardian Email Address">'.$_SESSION['form_consent_email'].'</td>

						    </tr>
						    
						</tbody>
					</table>
				</div>';

		}

		echo '<div class="section">
				<p class="medium">Your Address</p>
				<table class="clean minimised">
					<thead>
						<th>First Line</th>
						<th>Second Line</th>
						<th>Third Line</th>
						<th>County</th>
						<th>Postcode</th>

					</thead>
					<tbody>
						<tr>
					      <td data-label="First Line">'. $_SESSION['address_line_one'].'</td>
					      <td data-label="Second Line">'. $_SESSION['address_line_two'] .'</td>
					      <td data-label="Third Line">'. $_SESSION['address_city'] .'</td>
					      <td data-label="County">'. $_SESSION['address_county'] .'</td>
					      <td data-label="Postcode">'. $_SESSION['address_postcode'] .'</td>
					    </tr>
					    
					</tbody>
				</table>
			</div>
			<div class="section">
				<p class="medium">About Yourself</p>
				<table class="clean minimised">
					<thead>
						<th>Sport Activities</th>
						<th>Expertise</th>


					</thead>
					<tbody>
						<tr>
					      <td data-label="Sport Activities">'.$_SESSION['sport'].'</td>
					      <td data-label="Expertise">';

					      switch($_SESSION['expertise']) {
					      	case 0:
					      		echo 'Beginner';
					      		break;
					      	case 1:
					      		echo 'Novice';
					      		break;
					      	case 2:
					      		echo 'Experienced';
					      		break;
					      	case 3:
					      		echo 'Advanced';
					      		break;	
					      	case 4:
					      		echo 'Expert';
					      		break;
					      	case 5:
					      		echo 'Professional';
					      		break;
					      }

					      echo '</td>
					     
					    </tr>
					    
					</tbody>
				</table>
			</div>
			<div class="section">
				<p class="medium">Emergency Contact details</p>
				<table class="clean minimised">
					<thead>
						<th>First Emergency Contact Name</th>
						<th>First Emergency Contact Number</th>


					</thead>
					<tbody>
						<tr>
					      <td data-label="First Emergency Contact Name">'.$_SESSION['form_emergency1_name'].'</td>
					      <td data-label="First Emergency Contact Number">'.$_SESSION['form_emergency1_number'].'</td>
					     
					    </tr>
					    
					</tbody>
				</table >
				<table  class="clean minimised">
					<thead>
						<th>Second Emergency Contact Name</th>
						<th>Second Emergency Contact Number</th>


					</thead>
					<tbody>
						<tr>
					      <td data-label="Second Emergency Contact Name">'.$_SESSION['form_emergency2_name'].'</td>
					      <td data-label="Second Emergency Contact Number">'.$_SESSION['form_emergency2_number'].'</td>
					     
					    </tr>
					    
					</tbody>
				</table>
				<table class="clean minimised">
					<thead>
						<th>Medical Notes</th>


					</thead>
					<tbody>
						<tr>
					      
					      <td data-label="Medical Notes">'.$_SESSION['form_medical_notes'].'</td>
					     
					    </tr>
					    
					</tbody>
				</table>';
				?>
			</div>

			<div class="section">
				<p class="medium">Terms and Conditions</p>
				
				<div class="terms-conditions">
					<div class="section">
						<p class="medium">Acknowledgment of Risk</p>
						<ul>
							<li>I am aware of the risks involved in using RampWorld Cardiff and I have sought medical advice for any ailments/medical conditions which may put me at risk whilst using the park.</li>
							<li>I accept the risks involved in using RampWorld Cardiff and for my own safety I agree to follow all instructions given to me by any member of staff.</li>
							<li>I accept that these activities are dangerous and can result in death and/or injury.</li>
							<li>I am responsible for my own actions and/or involvement.</li>
						</ul>
					</div>
					<div class="section">
						<p class="medium">Acknowledgment of Safety</p>
						<ul>
							<li>It is recommended that ALL participants wear satisfactory safety equipment including helmets, wrist and elbow guards and knee pads.</li>
							<li>It is compulsory for participants under 16 to wear helmets.</li>
							<li>Anyone who is visibly intoxicated or under the influence of drugs shall not be permitted to participate.</li>
							<li>Alcohol is not permitted on RampWorld Cardiff premises.</li>
							<li>Smoking is not permitted within RampWorld Cardiff or within 20 meters of the doors</li>
							<li>No food or drink to be taken into ramp area.</li>
						</ul>

					</div>
					<div class="section">
						<p class="medium">Acknowledgment of Suitability</p>
						<ul>
							<li>I am fit to participate.</li>
							<li>I have declared any existing injuries and medical conditions to RampWorld Cardiff.</li>
							<li>I give permission for medical assistance to be administered in the case of an accident or an emergency.</li>
						</ul>
						
					</div>
					<div class="section">
						<p class="medium">Safety Equipment opt out</p>
						<ul>
							<li>If you are aged 16 years or over and wish not to wear safety equipment. RampWorld Cardiff have informed me of the risks of participating in extreme sports without safety equipment and I understand that I am participating at my own risk.</li>
							<li>All Scooter riders <span class="underline italic bold">must</span> wear a helmet.</li>
							
						</ul>
						
					</div>
					
					
					<div class="section">
						<p class="medium">Terms and Conditions</p>
						<ol>
							<li>In the absence of negligence or fraudulent misrepresentation committed by RampWorld Cardiff, their employees, officers, officials or agents, these Terms hereby exclude to the fullest extent permitted by law all responsibility and liability for the death, personal injury or illness of any person whilst present on the site.</li>
							<li>Neither RampWorld Cardiff or its employees or agents shall be responsible for any loss, theft or damage to valuables or any personal property that belongs to any person whilst on RampWorld Cardiff premises.</li>
							<li>RampWorld Cardiff reserves the right to refuse admission to any person on any grounds. Anyone who is visibly intoxicated or under the influence of drugs shall not be permitted to take part or on the premises.</li>
							<li>Minimum standards of conduct apply including, but not limited to, controlling your speed, not behaving in a way that endangers or alarms others, using the equipment safely and respecting all signs.</li>
							<li>Any abusive, dangerous, rude or anti-social behavior will not be tolerated and may lead to the member/participant(s) being removed from the park and receiving a ban.</li>
							
							<li>RampWorld Cardiff reserves the right to require you to leave the premises if you are deemed by a RampWorld Cardiff employee or officer not to be meeting reasonable standards of behavior. In such cases, no refund will be given and a note placed on the members file.</li>
							<li>No graffiti will be tolerated anywhere in the park.</li>
							<li>Children under the age of 12 must be accompanied by a responsible adult.</li>
							<li>Buggies/pushchairs must not be taken into the park area under any circumstances.</li>
							<li>In the interest of safety, beginners and those new to ramp riding should take time to familiarise themselves with the etiquette of the park before venturing into the main skating area.</li>
							<li>We do not advise beginners and young children ride this park at peak times. Please see our website for information on coaching or contact a member of staff for advice.</li>
							<li>Under 16s MUST have consent of a parent or guardian. No entry to the park if a consent form has not been completed. There are no exceptions to this rule.</li>
							<li>If you observe a hazard or damage or are injured whilst using RampWorld Cardiff facilities you must bring this to the attention of an employee immediately. An accident report form must be completed and any injury to the participant should be notified and recorded therein, signed by the injured person or their representative and witness statements taken.</li>
							<li>By signing and agreeing to our terms and conditions, you are declaring you are fit to participate and have declared any existing injuries and/or medical conditions.</li>
							<li>All participants will adhere to safety rules in place and understand the risks involved (serious injury/death). By participating you agree to accept responsibility for your own actions and involvement.</li>
							<li>Due to the proliferation of camera phones and the culture of filming edits in extreme sports please expect you image to be captured on film in the skatepark.  The only way to guarantee avoiding being filmed is not to enter.</li>
							<!---li>RampWorld Cardiff will monitor and this footage and  copyright is the property of RampWorld Cardiff solely. Users are advised this footage will be used for promotional and training purposes only.</li>-->							<li>School attendees should be aware that we will contact the appropriate authorities if we suspect anyone to be at RampWorld Cardiff at a time we believe they should be in school.</li>
							<li>Payment for use of RampWorld Cardiff will be made prior to participation. Anyone found to be in breach of this will be subject to having their membership removed.</li>
							<li>No refunds will be made for “no show”, late entry (other than that which has been caused by RampWorld Cardiff) or for leaving a session before the end. Exceptional circumstances may apply and will be at management discretion.</li>
							<li>Any deception on the part of users of RampWorld Cardiff will result in membership review or withdrawn, this may include multiple people using one membership, fraudulent entry to the park, forgery of documents etc.</li>
							<li>RampWorld Cardiff reserves the right to change its policies and terms and conditions from time to time. Please read and observe these policies and terms and conditions displayed in reception of RampWorld Cardiff before you use the park in case changes have been made.</li>
							<li>RampWorld Cardiff operates a no smoking policy.</li>
						</ol>
						
					</div>
					<div class="section">
						<p class="medium">Disclaimer</p>
						<ul>
							<li>The risk of injury from the activities involved in RampWorld Cardiff is significant, including the potential for permanent disability and death, and while particular rules, equipment and personal discipline may reduce this risk, the risk of serious injury does exist. All participants knowingly and freely assume such risks, both known and unknown, and assume full responsibility for their participation and for having and using the appropriate safety equipment. We do not accept any responsibility for other participant’s failure to comply with these rules other than due to our negligence.</li>
						
							
						</ul>
					</div>
					<div class="section">
						<p class="medium">Drugs and Alcohol Declaration</p>
						<ul>
							<li>RampWorld Cardiff operates a zero tolerance Drugs & Alcohol Policy. Any person whether a participant or a spectator is found to be taking or dealing drugs or consuming alcohol will receive a life ban and possible further action. Anyone deemed intoxicated will not be allowed to enter the park.</li>
						
							
						</ul>

					</div>

				</div>
				<p class="medium">By clicking 'Become a member', you are agreeing the terms and conditions and all information provided is correct.</p>
				
				<form action="#" method="post">
					<input type="hidden" name="complete" value="true">
					<a href="../registration?mode=change" class="button">Change your Details</a>
					<?php if( isset($_SESSION['member_id']) ){?>
						<input type="submit" class="button" value="Update Details">
					<?php } else {?>
						<input type="submit" class="button" value="Become a member">

					<?php }?>
				</form>
			

					
			</div>
		</div>
	</div>
</div>

<?php
}
get_footer();?>
<noscript>
	<style>
		body{ width:100%; height: 100%; overflow:hidden; }
	</style>
	<div class="overlay"></div>

	<div class="container">
		<div class="error">
			<div class="l la"></div>
			<div class="l ra"></div>
		</div>
		<div class="body">
			<h1>You must have JavaScript enabled.</h1>
			<p class="medium">Please enable your JavaScript and refresh this page.</p>
			<p class="medium">This setting can be found in your browser's settings page. For more help, search 'How to turn on JavaScript' into your favorite search engine.</p>
		</div>
		<div class="footer">
			<p class="small">RampWorld Cardiff 2016 &copy;</p>
		</div>
	</div>

</noscript>
