<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @since BirdFILED 1.0
 */
get_header(); ?>


<div class="jumbotron">
	<h1>404 - Page not found</h1>
	<p>Gosh! Looks like you couldn't find what you were looking for? Please use our navigation to help.<br>Sorry for an inconvenience, RWC Team</p>
</div>
	
</div>

<?php get_footer(); ?>
