<?php /* Template Name: Custom Rampworld Page */ ?>
<?php get_header(); ?>
   
   <div class="page-title">

         <p><?php the_title(); ?></p>
   </div><!-- End Page title -->

<div id="rw_blog_wrapper"><!--white background-->
   
   <!-- Start main content -->
   <!--<div class="container main-content clearfix">-->
    
    <!--<div class="sixteen columns">-->
    <div class="rampworld_mainbody">
    	<div class="rampworld_mainbody_content">
			<?php
            // Start Loop
            while (have_posts()) : the_post();
                                    
            the_content();
                                        
            endwhile;
            // End Loop
            ?>
            <div class="clearfix"></div>
    	</div>
    </div>
    
   <!--</div>--><!-- <<< End Container >>> -->

 </div><!-- end rw_blog_wrapper --> 
    
<?php get_footer(); ?>