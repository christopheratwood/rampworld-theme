<?php
/**
* Template Name: 
 */


require 'classes/thirdparty/Booking.php';
$booking = new Booking();

$nonDates = $booking->getNonOperationalDates();
$started = false;
if($booking->started) {
  $started = true;
  $booking->getSessionData();
}

if(isset($_GET['approved']) && $_GET['approved'] == true) {
  $_SESSION['transaction']['ppp'] = $_GET['PayerID'];
  if($_SESSION['transaction']['ppe'] < date('Y-m-d H:i:s')) {
    session_destroy();
    wp_redirect('http://www.rampworldcardiff.co.uk/online-booking/booking?timeout');
    die();
  }
}
require_once 'templates/gui/booking_header.php';
?>
<section class="top_overview">
  <div class="container__total <?php echo (isset($_SESSION['pph']))? 'minimum':'';?>">
    <p>Total: <span class="bold">£</span><span id="total" class="bold"><?php echo ($started && $booking->data['cost']['total_session_cost'] > 0 && $booking->data['progress']['stage'] >= 1) ? number_format($booking->data['cost']['total_cost'], 2) : '0.00';?></span></p>
    <?php if(isset($_SESSION['transaction']['pph'])){?>
        <p style="text-align: right;" data-end-time="<?php echo $_SESSION['transaction']['ppe'];?>" id="clock"><span class="bold" id="counter"></span> Remaining</p>
    <?php }?>
  </div><div class="container__middle">
      <p>Selected Date: </p><input type="text" name="start_date" id="start_date" data-large-mode="true" data-large-default="true" data-theme="rampword_custom" data-format="d F Y" data-default-date="<?php echo ($started == true)? $booking->data['session']['date']:'';?>" class="datetime_input" >
      
    </div>
 
 

</section>

<main>
  <?php if(isset($_GET['issue'])):?>
  <div class="alert alert-danger visible block">
    <?php if($_GET['issue'] == 'timeout'):?>
      <span>You have been timedout. This is done for security reasons. Please complete the form again. Sorry for any inconvenience caused.</span>
    <?php else:?>
     <span>Sorry! We encountered an error whilst processing your payment. Please try again. If this continues to occure, please contact us via Email.  Sorry for any inconvenience caused. <a href="mailto:&#114;&#097;&#109;&#112;&#119;&#111;&#114;&#108;&#100;&#099;&#097;&#114;&#100;&#105;&#102;&#102;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;">&#114;&#097;&#109;&#112;&#119;&#111;&#114;&#108;&#100;&#099;&#097;&#114;&#100;&#105;&#102;&#102;&#064;&#103;&#109;&#097;&#105;&#108;&#046;&#099;&#111;&#109;</a></span>
    <?php endif;?>
  </div>
  <?php endif;?>
  <div id="stage_1" class="stage">
    <div class="title" data-stage="1">
      <h2 id="step_name" class="booking">Sessions</h2>
      <span class="overview" id="stage_1_overview"><?php if($booking->started && $booking->data['progress']['stage'] >= 1) {echo $booking->data['session']['start_time'].' - ' . $booking->data['session']['end_time'];}?></span>
    </div>
    <div class="info visible <?php if($started && $booking->data['progress']['stage'] > 1):?>visible complete<?php endif;?>" id="stage_1_info">

      <div class="alert alert-danger <?php echo (isset($formErrors['stage_1_errors'])) ? 'visible block': '';?>" id="stage_1_error">
        <?php echo (isset($formErrors['stage_1_errors'])) ? $formErrors['stage_1_errors']: '';?>
      </div>
      <p class="medium">Please select your desired session(s).</p>
      <div id="sessions">Loading</div>
      
    </div>
    </div>
  <div id="stage_2" class="stage <?php echo (isset($_SESSION['stage']) && $_SESSION['stage'] == 2)? 'visible':'';?> <?php echo (isset($_SESSION['stage']) && intval($_SESSION['stage']) > 2)? 'complete':'';?>">
    <div class="title" data-stage="2">
      <h2 id="step_name" class="booking">Booking Details</h2>
      <span class="overview" id="stage_2_overview"><?php if($booking->started && isset($booking->data['booking']['email']) ) {echo $booking->data['booking']['email'];}?></span>
    </div>
    <div class="info <?php if($started && $booking->data['progress']['stage'] == 2):?> visible<?php elseif($started && $booking->data['progress']['stage'] > 2):?>visible complete<?php endif;?>" id="stage_2_info">
      <p class="medium">Are you already a member? To save time, enter your membership number otherwise fill in the email address, name, and number.</p>
      <small class="center">We will use this information to send a receipt via email. </small>

    
      <form action="#" method="post">
        <div class="group">
          <div class="item">
            
            <h3 class="booking">Non-existing Members</h3>
            <div class="item">
              <input type="email" name="input_email" id="input_email" class="rwc_new" value="<?php echo ($started && !isset($booking->data['booking']['member_id']) && isset($booking->data['booking']['email'])) ? $booking->data['booking']['email']: '';?>">
              <label>Email Address</label>
            </div>
            <div class="item">
              <input type="text" name="input_name" id="input_name" class="rwc_new" value="<?php echo ($started && !isset($booking->data['booking']['member_id']) && isset($booking->data['booking']['name'])) ? $booking->data['booking']['name']: '';?>">
              <label>Full Name</label>
            </div>
            <div class="item">
              <input type="number" name="input_number" id="input_number" class="rwc_new" value="<?php echo ($started && !isset($booking->data['booking']['member_id']) && isset($booking->data['booking']['number'])) ? $booking->data['booking']['number']: '';?>">
              <label>Mobile/ Telephone Number</label>
            </div>
            </div><div class="item">
              <h3 class="booking">Members</h3>
              <div class="item">
                
                <input type="number" name="input_membership_number" id="input_mid" class="rwc_new" value="<?php echo ($started && isset($booking->data['booking']['member_id'])) ? $booking->data['booking']['member_id']: '';?>">
                 <label>Membership Number</label>
              </div>
            </div>
          </div>
          <div class="group">
            <div class="alert alert-danger <?php echo (isset($formErrors['stage_booking_details_error'])) ? 'visible block': '';?>" id="stage_2_error">
              <?php echo (isset($formErrors['stage_booking_details_error'])) ? $formErrors['stage_booking_details_error']: '';?>
            </div>
          </div>
          <div class="results " id="members"></div>
        </article>
        <div class="group" id="stage_2_btns">
          <a class="btn btn-default" id="goBack" data-prev="1">Back</a>
          <input type="submit" class="btn btn-success" id="stage_2_submit" value="Next">
        </div>
      </form>
    </div>
  </div>
  <div id="stage_3" class="stage">
    <div class="title" data-stage="3">
      <h2 id="step_name" class="booking">Members</h2>
      <span class="overview" id="stage_3_overview"><?php if($started && $booking->data['progress']['stage'] >= 3) {echo $booking->data['participants']['total'] . ' participants';}?></span>
    </div>
    <div class="info <?php if($started && $booking->data['progress']['stage'] == 3):?> visible<?php elseif($started && $booking->data['progress']['stage'] > 3):?>visible complete<?php endif;?>" id="stage_3_info">
      <h2>Please add all participants.</h2>

      <form action="#" method="post">
        <div class="group">
          <div class="item">
            <h3 class="booking">Membership Number</h3>
            <input type="number" name="input_membership_number" id="add_member_input" class="rwc_new" placeholder="Please enter an membership number.">
          </div>
        </div>
        <input type="submit" class="btn btn-success" id="stage_3_add" value="Add">
      </form>
      <div class="group">
        <div class="alert alert-danger <?php echo (isset($formErrors['stage_3_errors'])) ? 'visible block': '';?>" id="stage_3_error">
          <?php echo (isset($formErrors['stage_3_errors'])) ? $formErrors['stage_3_errors']: '';?>
        </div>
      </div>
      <div class="group" style="padding: 5px;">
          <h2>Selected Member<span id="pular"></span><span id="totalParticipants"></span></h2>
          <small>To remove a member,  please click their name.</small>
          <div id="participants">
            <?php if($started && $booking->data['progress']['stage'] >= 3){
              foreach($booking->data['participants']['members'] as $member => $val):?>
                <div class="item"><div class="participant_list" data-id="<?php echo $val['member_id'];?>"><i class="fa member fa-<?php echo $val['gender'];?>"></i><p class="medium" style="font-size:1.2vmax;"><?php echo $val['name'];?>, Aged <?php echo $val['age'];?></div></div>
              <?php endforeach;
              }?>
          </div>
      </div>
      <div class="group" id="stage_3_btns">
        <a class="btn btn-default" id="goBack" data-prev="2">Back</a>
        <?php if($started && isset($booking->data['participants']) && $booking->data['participants']['total'] > 0):?>
          <input type="submit" value="Next" id="stage_3_complete" class="btn btn-success">
        <?php endif;?>
      </div>
    </div>
  </div>
  <div id="stage_4" class="stage">
    <div class="title" date-stage="4">
      <h2 id="step_name" class="booking">Confirmation</h2>
      <span class="overview" id="stage_4_overview"></span>
    </div>
    <div class="info <?php if($started && $booking->data['progress']['stage'] == 4):?> visible<?php elseif($started && $booking->data['progress']['stage'] > 4):?>visible complete<?php endif;?>" id="stage_4_info">
      <h2>Are these details correct?</h2>
      <small>Have you provided the correct information? Please remember check that you have provided </small>
      <div class="group" style="padding: 5px;">
        <div class="item" style="width: 50%;margin-right:none;margin-right:0 !important;">
          <h3>Booking Details</h3>
          <table id="details_confirmation" class="rwc_table">
            <?php if($started && $booking->data['progress']['stage'] >= 4):?>
              <thead><tr><th> </th><th></th></tr></thead>
              <tbody>
                <?php if($started && $booking->data['progress']['stage'] >= 4 && isset($booking->data['booking']['member_id'])):?>
                <tr>
                  <th>Membership Number:</th>
                  <td><?php echo $booking->data['booking']['member_id'];?></td>
                </tr>
                <?php endif;?>
                <tr>
                  <th>Booking Email:</th>
                  <td><?php echo $booking->data['booking']['email'];?></td>
                </tr>
                <tr>
                  <th>Name:</th>
                  <td><?php echo $booking->data['booking']['name'];?></td>
                </tr>
                <tr>
                  <th>Number:</th>
                  <td><?php echo $booking->data['booking']['number'];?></td>
                </tr>
              </tbody>
            <?php endif;?>
          </table>
          <small>Please ensure that the session's details are correct. These times cannot be changed once purchased.</small> 
          <h3>Session Details</h3>
          <table id="sesions_details_confirmation" class="rwc_table">
            <?php if($started && $booking->data['progress']['stage'] >= 4):?>
              <tbody>
                <tr>
                  <th>Date:</th>
                  <td><?php echo $booking->data['session']['date'];?></td>
                </tr>
                <tr>
                  <th>Session Start Time:</th>
                  <td><?php echo $booking->data['session']['start_time'];?></td>
                </tr>
                <tr>
                  <th>Session End Time:</th>
                  <td><?php echo $booking->data['session']['end_time'];?></td>
                </tr>
                <tr>
                  <th>Price per Participant:</th>
                  <td>£<?php echo $booking->data['cost']['session_cost'];?></td>
                </tr>
              </tbody>
            <?php endif;?>
          </table>
           <small>Please ensure that the session's details are correct. The session times cannot be changed once purchased.</small> 
        </div><div class="item" style="width: 50%;margin-right:0 !important;">
          <h3>Participant(s) Details</h3>
         
          <table id="participants_confirmation" class="rwc_table">
            <?php if($started && $booking->data['progress']['stage'] >= 4){
              echo '<thead><tr><th>Membership Number</th><th>Member\'s Name</th></tr></thead><tbody>';
              foreach($booking->data['participants']['members'] as $member => $value){
                echo '<tr><td>'. $value['member_id'].'</td><td>'.$value['name'].'</td></tr>';
              }
            }?>
          </table>
           <small>Please ensure that the participant's member number is correct.  The purchase will automatically be assigned to the member number.</small>
           <div id="totals">
             <h3>Price Summary</h3>
             <table class="rwc_table confirmation">
               <tr><th>Session Costs:</th><td><span id="session_total">£<?php if($started && $booking->data['progress']['stage'] >= 4){echo $booking->data['cost']['total_session_cost'];}?></span></td></tr>
               <tr><th>Booking Charge:</th><td><span id="booking_charge">£<?php if($started && $booking->data['progress']['stage'] >= 4){echo $booking->data['cost']['booking_cost'];}?></span></td></tr>
               <tr><th>Total:</th><td class="double_top_border"><span id="total_charge">£<?php if($started && $booking->data['progress']['stage'] >= 4){echo $booking->data['cost']['total_cost'];}?></span></td></tr>
             </table>
             <h3>Checkout</h3>
              <?php if(isset($_GET['approved']) && $_GET['approved'] == true):?>
                <small>Is everything correct? Please click Buy</small>
                <a href="/pay" class="paypal">Buy</a>
                <a href="http://www.rampworldcardiff.co.uk/online-booking/booking/" class="btn btn-default">No, Go back</a>
              <?php else:?>
                <small>Is everything correct? Please checkout out securly with PayPal.  You will be redirected to paypal to complete the transaction.</small>
                <a href="http://www.rampworldcardiff.co.uk/online-booking/booking/payment"><img src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/blue-rect-paypalcheckout-60px.png" alt="PayPal Checkout" style="margin: 8px auto;"></a>
                <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppppcmcvdam.png" alt="Credit Card Badges">

              <?php endif;?>
             
           </div>

        </div>
        <div class="alert alert-danger <?php echo (isset($formErrors['stage_4_errors'])) ? 'visible block': '';?>" id="stage_4_error">
          <?php echo (isset($formErrors['stage_4_errors'])) ? $formErrors['stage_4_errors']: '';?>
        </div>
      </div>
      </div>
  </div>
</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript" src="http://www.rampworldcardiff.co.uk/wp-content/themes/simple/js/datedropper.min.js"></script>
<script type="text/javascript" src="http://www.rampworldcardiff.co.uk/wp-content/themes/simple/js/input.js"></script>
<script type="text/javascript" src="http://www.rampworldcardiff.co.uk/wp-content/themes/simple/js/booking.js"></script>
<script>
    $('#start_date').dateDropper();
      
    $(document).ready(function() {
      var booking = new Booking();
      booking.date = $('#start_date').val();

      if($('#clock').length >0) {
        updateClock(new Date( $('#clock').data('end-time')));
      }

      if($('#stage_1_overview').length != 0)
          booking.renderSessions();
      else 
          booking.renderSessions();


      $('#start_date').change(function(e) {
          booking.date =$(this).val();
          booking.renderSessions(booking.date);
          if(booking.stage_id > 1) {
              booking.showError('#stage_1_error', 'You have changed the date, please reselect the session you desire.');
              $('#stage_1_info').removeClass('complete');
          }
      });

      $('body').on('click', '.overlay-warning-session', function(e) {
          e.preventDefault();
          $('.sessionPreview').removeClass('selected');

          booking.stage1($(this).parent().parent());
          $(this).remove();
          $(this).children('.sessionPreview').addClass('selected');

      });
      $('body').on('click', '.selectSession', function(e) {
          e.preventDefault();
          $('.sessionPreview').removeClass('selected');
          
          if($(this).data('beginner-only') == true) {
            $(this).addClass('blur');
            $(this).children('.sessionPreview').append('<div class="overlay-warning-session"><span><b>Warning!</b></span><span>This session IS ONLY for beginners and novices. You will NOT be allowed to attend this session if you are not a beginner.</div>');
            $(this).children('.sessionPreview').addClass('warning_selected');
          } else {
            $(this).children('.sessionPreview').addClass('selected');
            booking.stage1($(this));
          }

      });

      //Stage 2
      $('body').on('click', '#stage_2_submit', function(e) {
          e.preventDefault();
          booking.stage2($('#input_email').val(),$('#input_name').val(),$('#input_number').val(), $('#input_mid').val());
      });
      $('body').on('click','#stage_2_fresh', function(e) {
          e.preventDefault();
          booking.removeSession('participants', ['member_id', booking.memid]);

          $('.results').empty();
          $(this).remove();
          $('#input_mid').removeAttr('disabled');
          $('#input_mid').focus();
          $('#stage_2_complete').remove();
          $('#stage_2_submit').show();
          $('#stage_2_btns').html('<a class="btn btn-default" id="goBack" data-prev="1">Back</a><input type="submit" class="btn btn-success" id="stage_2_submit" value="Next">');
      });
      $('body').on('click','#stage_2_complete', function(e) {
          e.preventDefault();
          booking.confirmBookingMember(); 
      });


      //stage 3
      $('#stage_3_add').on('click', function(e) {
          e.preventDefault();
          booking.addParticipant($('#add_member_input').val());
          $('#add_member_input').empty();
      });
      $('body').on('click', '.participant_list', function(e) {
          e.preventDefault();
          booking.removeParticipant($(this), $(this).data('id'));
      });
      $('body').on('click','#stage_3_complete', function(e) {
          e.preventDefault();
          booking.confirmation();
      });



      // stage 4
      $('.title').on('click', function(e) {
          e.preventDefault();
          var stage = $(this).data('stage');
          if( $('#stage_'+ stage +'_info').hasClass('complete')) {
              $('#stage_'+ stage +'_info').removeClass('complete');
              $('#stage_'+ stage +'_info').addClass('open');
          } else if( $('#stage_'+ stage +'_info').hasClass('open')) {
              $('#stage_'+ stage +'_info').addClass('complete');
              $('#stage_'+ stage +'_info').removeClass('open');
          }
      });

      $('body').on('click', '#goBack', function(e) {
          e.preventDefault();
          $('#stage_'+$(this).data('prev')+'_info').removeClass('complete');
      });
      var ppe = $('')

      
      var overview_fixed = false;
      var session_fixed = false;
      var booking_fixed = false;
      var participants_fixed = false;
      var confirmation_fixed = false;


      var admin = ($('#wpadminbar').length > 0) ? true : false;
      var overview = $('.top_overview');
      var session = $('#stage_1');
      var book = $('#stage_2');
      var participant = $('#stage_3');
      var confirmation = $('#stage_4');
      
      if (overview_fixed == false && $(window).scrollTop() > 200) {
          overview_fixed = !false;
          fixed('.top_overview', admin);
          $('main').removeClass('fixed');
      }

      var lst = 0;
      $('body').bind('touchmove', function (e) {
          if (overview_fixed == false && $(window).scrollTop() > 200) {
              overview_fixed = !false;
              fixed('.top_overview', admin)
              fixed('main', admin)
          } else if ($(window).scrollTop() < 200) {
              overview_fixed = false;
              overview.removeClass('fixed');
              $('main').removeClass('fixed');
          }
          if (session_fixed == false && $(window).scrollTop() > 240) {
              session_fixed = !false;
              session.addClass('fixed');
          } else if ($(window).scrollTop() < 240) {
              session_fixed = false;
              session.removeClass('fixed');
          }
          if (session_fixed == false && $(window).scrollTop() > 280) {
              session_fixed = !false;
              session.addClass('fixed');
          } else if ($(window).scrollTop() < 280) {
              session_fixed = false;
              session.removeClass('fixed');
          }
          if(session_fixed && lst < $(window).scrollTop()) {
              e.preventDefault();
          }
          lst = $(window).scrollTop();

      });

      
      $(window).scroll(function (e) {

          
          if (overview_fixed == false && $(window).scrollTop() > 200) {
              overview_fixed = !false;
              fixed('.top_overview', admin);
              fixed('main', admin);

          } else if ($(window).scrollTop() < 200) {
              overview_fixed = false;
              overview.removeClass('fixed');
              $('main').removeClass('fixed');
          }
        
        if (session_fixed == false && $(window).scrollTop() > 235) {
              session_fixed = !false;
              session.addClass('fixed');
          } else if ($(window).scrollTop() < 235) {
              session_fixed = false;
              session.removeClass('fixed');
          }
          
          if($('#stage_1_info').hasClass('complete') && (booking_fixed == false && $(window).scrollTop() > 255)) {
              booking_fixed = !false;
              book.addClass('fixed');
          } else if (!$('#stage_1_info').hasClass('complete') || $(window).scrollTop() < 255) {
              booking_fixed = false;
              book.removeClass('fixed');
          }
          if($('#stage_2_info').hasClass('complete') && (participants_fixed == false && $(window).scrollTop() > 275)) {
              participants_fixed = !false;
              participant.addClass('fixed');
          } else if (!$('#stage_2_info').hasClass('complete') || $(window).scrollTop() < 275) {
              participants_fixed = false;
              participant.removeClass('fixed');
          }
          if($('#stage_3_info').hasClass('complete') && (confirmation_fixed == false && $(window).scrollTop() > 295)) {
              confirmation_fixed = !false;
              confirmation.addClass('fixed');
          } else if (!$('#stage_3_info').hasClass('complete') || $(window).scrollTop() < 295) {
              confirmation_fixed = false;
              confirmation.removeClass('fixed');
          }

          
      });

  });
  function fixed(element, admin) {
      $(element).addClass('fixed');
      if (admin)
          $(element).addClass('admin');
  }
  function updateClock(end_time) {
    var vis = false;
    var now = new Date();
    var dif = end_time.getTime() - now.getTime();
    console.log(dif);
    if( dif <= 0 ) {
      $('#counter').empty();
      vis = false;
    } else {
      var seconds = Math.floor(dif / 1000);
      var minutes = Math.floor(seconds / 60);

      minutes %= 60;
      seconds %= 60;
      $('#counter').text(((minutes < 10)?("0" + minutes):parseInt(minutes)) + ":" + ((seconds < 10)?("0" + seconds): parseInt(seconds)));
    }
    setInterval(function() {
        var now = new Date();
        var dif = end_time.getTime() - now.getTime();
        console.log(dif);
        if( dif <= 0 ) {
          if(vis == false){
             $('#counter').text('00:00');
             vis = true;
          } else {
              $('#counter').empty();
              vis = false;
          }

        } else {
          var seconds = Math.floor(dif / 1000);
          var minutes = Math.floor(seconds / 60);

          minutes %= 60;
          seconds %= 60;
          $('#counter').text(((minutes < 10)?("0" + minutes):minutes) + ":" + ((seconds < 10)?("0" + seconds): seconds));
        }

        

    }, 1000);
  }
</script>

<?php get_footer();?>