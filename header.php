<?php 
use Rampworld\Calendar\ClosedDates as ClosedDates;
use Rampworld\Component\SiteComponent as SiteComponent;
use Rampworld\SEO\Meta as Meta;
require_once 'modules/vendor/autoload.php';
$closed = new ClosedDates();
$showJumbotron = (strpos($_SERVER['REQUEST_URI'], 'events') !== false) ? false: true;
?>

<!DOCTYPE html <?php language_attributes(); ?>>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

  <!-- Basic Page Needs -->
  <meta charset="utf-8">
  <title><?php echo ((strlen(Meta::$title) > 0) ? Meta::$title .' - ' . get_bloginfo('name'): wp_title()); ?></title>
  <?php echo Meta::$meta;?>
  
  <!-- Mobile Specific Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="canonical" href="https://www.rampworldcardiff.co.uk/" />
  <meta property="og:locale" content="en_GB" />
  <meta property="og:type" content="website" />
  <meta property="og:url" content="https://<?php echo $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>">

  <?php wp_head(); ?>
  <link rel="stylesheet" href="https://www.rampworldcardiff.co.uk/wp-content/themes/rampworld/assets/dist/css/rwcui.min.css">
</head>
<body <?php body_class( ); ?>>
  <div class="boxed">
    <?php SiteComponent::header();?>
    <div class="container">
      <?php if ( $showJumbotron === true && (strlen(get_post_meta(get_the_ID(), 'Page Title', true)) > 0 ||  strlen(get_post_meta(get_the_ID(), 'Page Message', true)  > 0))) :?>
        <div class="jumbotron">
          <?php if (strlen(get_post_meta(get_the_ID(), 'Page Title', true)) > 0) :?><h1><?php echo get_post_meta(get_the_ID(), 'Page Title', true);?></h1><?php endif;?>
          <?php if (strlen(get_post_meta(get_the_ID(), 'Page Message', true)) > 0) :?><p><?php echo get_post_meta(get_the_ID(), 'Page Message', true);?></p><?php endif;?>
        </div>
        <?php echo $closed->get_alert();?>
      <?php endif;?>