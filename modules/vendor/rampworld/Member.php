<?php
date_default_timezone_set('Europe/London');
class Member {
	public $numOfResults ;
	public $consentIsPresent,
			$count;
	private $_db;
	private $_member_details;
	private $_results;
	private $_database_prefix = "rwc_";

	public function __construct() {
	
		global $wpdb;
		if(!isset($wpdb))
		{
		    //the '../' is the number of folders to go up from the current file to the root-map.
		    require_once('../../../../../wp-config.php');
		    require_once('../../../../../wp-includes/wp-db.php');
		}
		$this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
	}

	public function getMembersAsJSON($fc1q, $fc2q, $and, $dif,$fc1d, $fc2d) {
		
			$this->_member_details = $this->_db->get_results('SELECT 
									mem.`member_id`, 
									mem.`forename`, 
									mem.`surname`, 
									mem.`dob`, 
									mem.`discipline`, 
									mem.`medical_notes`, 
									mem.`expertise`, 
									mem.`additional_verification`, 
									mem.`staff_verification`, 
									settings.`value`, 
									CASE WHEN paid.`start_date` <= CURDATE() 
									AND paid.`end_date` >= CURDATE() 
									AND paid.`premature` IS NULL THEN type.`display_name` ELSE 0 END AS has_membership, 
									CASE WHEN entries.`entry_date` = CURRENT_DATE() 
									AND entries.`premature_exit` IS NULL AND entries.`exit_time` >= CURRENT_TIME() - INTERVAL 30 minute  THEN 1 ELSE 0 END AS alreadyOnPremises 
								FROM 
									`rwc_dashboard_settings` as settings, 
									`rwc_members` as mem 
									LEFT JOIN `rwc_paid_membership` as paid ON mem.`member_id` = paid.`member_id` 
									AND paid.`premature` IS NULL 
									AND paid.`end_date` >= CURDATE() 
									AND paid.`start_date` <= CURDATE() 
									LEFT JOIN `rwc_members_entries` as entries ON mem.`member_id` = entries.`member_id` 
									AND entries.`entry_date` = CURRENT_DATE() 
									AND entries.`premature_exit` IS NULL
								    AND entries.`exit_time` >= CURRENT_TIME() - INTERVAL 30 minute
								    LEFT JOIN rwc_membership_types as type ON paid.`membership_type` = type.`membership_type_id`
								WHERE 
									settings.`cmd_name` = \'staff_verification\' and '.$fc1q. $and .$fc2q.'
								GROUP BY mem.`member_id`
								ORDER BY 
									mem.`member_id` ASC, '.$fc1d. $dif . $fc2d , ARRAY_A);
		
		
		$this->count = count($this->_member_details);
		if(count($this->_member_details) > 0){
			$this->_addNotes();

		
			return json_encode($this->_member_details);
		} else {
			return json_encode(array());
		}
	}
	

}