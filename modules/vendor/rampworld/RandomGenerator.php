<?php

class RandomGenerator {
	function __construct( ) {}
	public function generate( $length = 32,  $format = '') { 



		switch ( $format) {
			case 'numeric':

				$salt = '0123456789';
				break;
			case 'alpha_lower':

				$salt = 'abcdefghijklmnopqrstuvwxyz';
				break;
			case 'alpha_upper':

				$salt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				break;
			default:

				$salt = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_0123456789';
				break;

			
		}

		
		$random = '';
		$i = 0;
		while ($i < $length) { // Loop until you have met the length
			$num = rand() % strlen($salt);
			$tmp = substr($salt, $num, 1);
			$random = $random . $tmp;
			$i++;
		}
		return $random;
	}
}