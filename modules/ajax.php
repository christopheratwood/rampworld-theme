<?php
require_once 'vendor/autoload.php';
use Rampworld\Session\Fetch as Session;
use Rampworld\Calendar\Booking as Calendar;
use Rampworld\Member\Fetch as Membership;
session_start();


if(isset($_POST['action']) && $_POST['action'] == 'get_sessions') {

	$sesh = new Session();

	echo  $sesh->get_sessions($_POST['date']);
	exit();
}
if(isset($_POST['action']) && $_POST['action'] == 'get_prices') {

	$sesh = new Session();

	echo $sesh->get_prices();
	exit();
}
if(isset($_POST['action']) && $_POST['action'] == 'session_add') {
	if(isset($_POST['values'])) {
		$sessions = $_POST['values'];
		$section = $_POST['section'];
		if( is_array($sessions) ) {
			for( $i = 0; $i < count($sessions); $i++){
				if($section != null)
					$_SESSION[$section][$sessions[$i][0]] = $sessions[$i][1];
				else
					$_SESSION[$sessions[$i][0]] = $sessions[$i][1];
			}
		} else {
			if($section != null){
				if(!isset($_SESSION[$section])) 
					$_SESSION[$section] = array();
				$_SESSION[$section][$sessions[0]] = $sessions[1];
			} else {
				$_SESSION[$sessions[0]] = $sessions[1];
			}
		}
		print_r($_SESSION);
		exit();
		
	}
	echo false;
	exit();
}
if(isset($_POST['action']) && $_POST['action'] == 'session_remove') {
	if(isset($_POST['condition'])) {
		$condition = $_POST['condition'];
		$section = $_POST['section'];
		if($section == null) {
			foreach($_SESSION as $sess => $val) {
				if($val[$condition[0]] == $condition[1]) {
					unset($_SESSION[$sess]);
					echo json_encode(true);
					exit();
				}
			}
		} else {
			foreach($_SESSION[$section] as $sess => $val) {
				print_r($val);
				if($val[$condition[0]] == $condition[1]) {
					unset($_SESSION[$section][$sess]);
					print_r($sess);
					echo json_encode(true);
					exit();
				}
			}
		}
		
	}
	echo false;
	exit();
}
if(isset($_POST['action']) && $_POST['action'] == 'check_booking_member') {
	if(isset($_POST['mid'])) {
		$mid = intval($_POST['mid']);

		$member = new Membership();
		$check = $member->checkBookingMember($mid);

		if($check != false){
			if(isset($_SESSION['booking']['member_id']) && $_SESSION['booking']['member_id'] == $mid){
				echo json_encode(null);
				exit();
			}
		} else {
			echo json_encode(false);
			exit();
		}			

		echo json_encode($check);
		exit();
	}
}

if(isset($_POST['action']) && $_POST['action'] == 'check_participants') {
	if(isset($_POST['mid'])) {
		$found = false;
		$mid = intval($_POST['mid']);

		$member = new Membership();
		$check = $member->checkBookingMember($mid);

		if($check) {
			if(isset($_SESSION['participants']['members'])) {
				foreach($_SESSION['participants']['members'] as $member){
					if($member['member_id'] == $mid) {
						$found = true;
					}
				}
				if($found == true) {
					echo json_encode(true);
					exit();
				} else {
					echo json_encode($check);
					exit();
				}
			}
		} else {
			echo json_encode(false);
			exit();
		}

		echo json_encode($check);
		exit();
	}
}
if(isset($_POST['action']) && $_POST['action'] == 'add_participant') {
	if(isset($_POST['mid'])) {
		$mid = intval($_POST['mid']);

		$member = new Membership();
		$check = $member->checkBookingMember($mid);
		if($check) {

			if( $member->alreadyBooked($mid)){
				if(!isset($_SESSION['participants'])) {
					$_SESSION['participants'] = array('total'  => 0, 'members'=> array());
				} else {
					if(!empty($_SESSION['participants']['members'])) {
						foreach($_SESSION['participants']['members'] as $part => $val) {
							if($val['member_id'] == $mid){
								echo json_encode(null);
								exit();
							}
						}
					}
				}

				
				$_SESSION['participants']['members'][] = array(
					'member_id' => $mid,
					'name'		=> ucwords(strtolower($check[0]['forename']. ' ' .$check[0]['surname'])));

				$_SESSION['participants']['total']++;


				$_SESSION['cost']['total_cost'] = number_format($_SESSION['cost']['total_cost'] + $_SESSION['cost']['session_cost'], 2);
				
				echo json_encode(array('member_id' => $check[0]['member_id'],'name' => ucwords(strtolower($check[0]['forename']) . ' ' . strtolower($check[0]['surname'])), 'cost'=> $_SESSION['cost']['session_cost'], 'total_cost' => $_SESSION['cost']['total_cost']));
				exit();
			} else {
				echo json_encode('alreadyBooked');
			}
		} else {
			echo json_encode(false);
			exit();
		}
			
	} else {
		echo json_encode(false);
		exit();
	}	

	exit();
	
}
if(isset($_POST['action']) && $_POST['action'] == 'remove_participant') {
	if(isset($_POST['mid'])) {
		$mid = intval($_POST['mid']);
		if(count($_SESSION['participants']['members']) > 0) {
			foreach($_SESSION['participants']['members'] as $participant => $val) {
				if($val['member_id'] == $mid){
					unset($_SESSION['participants']['members'][$participant]);
					$_SESSION['participants']['total']--;
					$_SESSION['cost']['total_cost'] = number_format($_SESSION['cost']['total_cost'] - $_SESSION['cost']['session_cost'], 2);
					echo json_encode(array('total_cost' => $_SESSION['cost']['total_cost']));
					exit();
				}
			}
			echo json_encode(false);
			exit();
		} else {
			echo json_encode(false);
			exit();
		}
		
		echo json_encode(false);
		exit();
	}
}
if(isset($_POST['action']) && $_POST['action'] == 'get_session_data') {
	if(empty($_SESSION)) {
		echo json_encode(false);
		exit();
	} else {
		echo json_encode($_SESSION);
		exit();
	}
}

if(isset($_POST['action']) && $_POST['action'] == 'get_variant_details') {
	require  ABSPATH.'wp-content/plugins/RWCommerce/classes/ProductVariant.php';
	$pv = new ProductVariant();
	echo json_encode($pv->getDetails($_POST['pv']));
}

//calendar evetns

if(isset($_POST['action']) && $_POST['action'] == 'getCalendarEvents') {

	$calendar = new Calendar();

	$events = array();
	$interval = DateInterval::createFromDateString('+1 day');
	$start = new DateTime(date('Y-m-d',strtotime($_POST['start'])));
	$end = new DateTime( date('Y-m-d',strtotime($_POST['end'])));

	$duration = new DatePeriod($start, $interval, $end);


	foreach($duration as $day) {
		$events = array_merge( $events, $calendar->get_sessions($day));
	}

	echo json_encode($events);

}
if(isset($_POST['action']) && $_POST['action'] == 'getCalendarEventsPrivate') {

	$calendar = new Calendar();

	$events = array();
	$interval = DateInterval::createFromDateString('+1 day');
	$start = new DateTime(date('Y-m-d',strtotime($_POST['start'])));
	$end = new DateTime( date('Y-m-d',strtotime($_POST['end'])));

	$duration = new DatePeriod($start, $interval, $end);


	foreach($duration as $day) {
		$events = array_merge( $events, $calendar->get_private_hires($day));
	}

	echo json_encode($events);

}
if(isset($_POST['action']) && $_POST['action'] == 'getTodaysSessions') {

	$session = new Session();

	$events = array();
	
	$date= new DateTime(date('Y-m-d',strtotime($_POST['date'])));

	$events = $session->get_sessions($date);
	

	echo json_encode($events);

}
if(isset($_POST['action']) && $_POST['action'] == 'get_total_participants') {
	
	echo (!isset($_SESSION['participants']['total'])) ? json_encode(0) : json_encode($_SESSION['participants']['total']);
	exit();
	
}
/* 
if(isset($_POST['action']) && $_POST['action'] == 'verify_account') {

	$account = new Account();
	$valid = $account->check($_POST['username'], $_POST['password']);
	if($valid === false) {
		echo json_encode(array(
			"errorCode" => 1,
			"errorMessage" => 'incorrect details'));
		exit();
	} elseif( is_array($valid)) {
		echo json_encode($valid);
		exit();
	} else {
		echo json_encode($account->account);
		exit();
	}
}

if(isset($_POST['action']) && $_POST['action'] == 'logout') {

	$account = new Account();
	echo json_encode($account->logout($_POST['account_id']));
	exit();
} */