<?php namespace Rampworld\Component;

date_default_timezone_set('Europe/London');

class StoreComponent {

  public static function header() {
    return require_once __dir__.'/../../../../assets/templates/gui/store/header.php';
  }

  public static function sidebar() {
    return require_once __dir__.'/../../../../assets/templates/gui/store/sidepanels/homepage.php';
  }

  public static function featuredProducts() {
    return require_once __dir__.'/../../../../assets/templates/gui/store/products/featured.php';
  }

  public static function saleProducts() {
    return require_once __dir__.'/../../../../assets/templates/gui/store/products/sale.php';
  }

  public static function recentViewed() {
    return require_once __dir__.'/../../../../assets/templates/gui/store/products/recents.php';
  }
  public static function filter() {
    return require_once __dir__.'/../../../../assets/templates/gui/store/sidepanels/filter.php';
  }
}