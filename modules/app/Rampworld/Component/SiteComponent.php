<?php namespace Rampworld\Component;

date_default_timezone_set('Europe/London');

class SiteComponent {

  public static function header() {
    return require_once __dir__.'/../../../../assets/templates/gui/site/header.php';
  }
  public static function footer() {
    return require_once __dir__.'/../../../../assets/templates/gui/site/footer.php';
  }
}