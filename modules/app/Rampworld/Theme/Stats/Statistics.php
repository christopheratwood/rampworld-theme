<?php
namespace Rampworld\Theme\Stats;

use Rampworld\Shared\DB\Connection as Connection;

class Statistics {
	public $is_open;
	private $_db;
	public $total_holiday__data;
	public $total_holiday__labels;
	public $total_entries__data;
	public $total_entries__labels;
	public $total_weeks;
	public $holiday_total_weeks;
	public $names;
	public $sessions = array(0 => array(),1 => array(),2 => array(),3 => array(),4 => array(),5 => array(),6=> array());
	public $holiday_sessions = array(0 => array(),1 => array(),2 => array(),3 => array(),4 => array(),5 => array(),6=> array());
	public $data;
	private $holiday_data;
	public $todays_sessions;

	public function __construct() {
		require_once __DIR__.'/../../Helpers/Connection.php';
		$this->_db  = Connection::connect();
	}

	public function getTotalEntries() {
		$datas 			= array();
		$labels 		= array();
		$holiday_datas 	= array();
		$holiday_labels = array();
		$days 			= array(1 => 'Monday', 2=> 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday');

		$sesh = $this->_db->get_results('SELECT day, start_time, end_time FROM rwc_sessions WHERE private IS NULL order by day asc,end_time asc', ARRAY_A);
		$hol_sesh = $this->_db->get_results('SELECT day, start_time, end_time FROM rwc_holidays_sessions WHERE holiday_id = 1 AND private IS NULL order by day asc,end_time asc', ARRAY_A);
		for($i = 0; $i < count($sesh); $i++) {
			$this->sessions[$sesh[$i]['day']][$sesh[$i]['end_time']] = array('start_time' => $sesh[$i]['start_time'], 'total' => 0);
		}
		for($i = 0; $i < count($hol_sesh); $i++) {
			$this->holiday_sessions[$hol_sesh[$i]['day']][$hol_sesh[$i]['end_time']] = array('start_time' => $hol_sesh[$i]['start_time'], 'total' => 0);
		}
		$this->total_weeks = $this->_db->get_results('SELECT dayofweek(a.entry_date)  day, count(distinct a.entry_date) total FROM `rwc_members_entries` a LEFT JOIN rwc_holidays h on a.entry_date >= h.start_date and a.entry_date <= h.end_date WHERE a.entry_date >= "2017-01-04" AND h.start_date IS NULL GROUP BY day', ARRAY_A);
		$this->holiday_total_weeks = $this->_db->get_results('SELECT dayofweek(a.entry_date)  day, count(distinct a.entry_date) total FROM `rwc_members_entries` a LEFT JOIN rwc_holidays h on a.entry_date >= h.start_date and a.entry_date <= h.end_date WHERE a.entry_date >= "2017-01-04" AND h.start_date IS NOT NULL GROUP BY day', ARRAY_A);
		
		$this->data = $this->_db->get_results('SELECT dayofweek(entry_date) -1 AS day, exit_time, date_format(a.entry_time, "%H:%i") as entry_time, date_format(a.exit_time, "%H:%i") as exit_time FROM rwc_members_entries a LEFT JOIN rwc_sessions s on a.exit_time = s.end_time AND DAYOFWEEK(a.entry_date) -1 = s.day LEFT JOIN rwc_holidays h on a.entry_date >= h.start_date and a.entry_date <= h.end_date WHERE a.entry_date >= "2017-01-04" AND h.start_date IS NULL AND s.private IS NULL order by day asc, exit_time asc', ARRAY_A);
		$this->holiday_data = $this->_db->get_results('SELECT dayofweek(entry_date) -1 AS day, exit_time, date_format(a.entry_time, "%H:%i") as entry_time, date_format(a.exit_time, "%H:%i") as exit_time FROM rwc_members_entries a LEFT JOIN rwc_sessions s on a.exit_time = s.end_time AND DAYOFWEEK(a.entry_date) -1 = s.day LEFT JOIN rwc_holidays h on a.entry_date >= h.start_date and a.entry_date <= h.end_date WHERE a.entry_date >= "2017-01-04" AND h.start_date IS NOT NULL AND s.private IS NULL order by day asc, exit_time asc', ARRAY_A);


		for($i =0; $i < count($this->data); $i++) {
			
			$time 	= date('H:i', strtotime($this->data[$i]['entry_time'] . '+30 minutes' ));
			$exit 	= ($this->data[$i]['exit_time'] == '00:00')? '24:00' : $this->data[$i]['exit_time'];
			if( isset( $this->sessions[$this->data[$i]['day']][$exit]) ) {
				
				if($time < $this->sessions[$this->data[$i]['day']][$exit]['start_time']) {
					foreach($this->sessions[$this->data[$i]['day']] as $key => $value){
						if( $time < $this->sessions[$this->data[$i]['day']][$key]['start_time'])
							$this->sessions[$this->data[$i]['day']][$key]['total']++;
					}
				} else {
					$this->sessions[$this->data[$i]['day']][$exit]['total']++;
				}
			}
		}
		for($i =0; $i < count($this->holiday_data); $i++) {
			
			$time 	= date('H:i', strtotime($this->holiday_data[$i]['entry_time'] . '+30 minutes' ));
			$exit 	= ($this->holiday_data[$i]['exit_time'] == '00:00')? '24:00' : $this->holiday_data[$i]['exit_time'];
			
			if( isset( $this->holiday_sessions[$this->holiday_data[$i]['day']][$exit]) ) {
				
				if($time < $this->holiday_sessions[$this->holiday_data[$i]['day']][$exit]['start_time']) {
					foreach($this->holiday_sessions[$this->holiday_data[$i]['day']] as $key => $value){
						if( $time < $this->holiday_sessions[$this->holiday_data[$i]['day']][$key]['start_time'])
							$this->holiday_sessions[$this->holiday_data[$i]['day']][$key]['total']++;
					}
				} else {
					$this->holiday_sessions[$this->holiday_data[$i]['day']][$exit]['total']++;
				}
			}
		}
		$tmp = $this->sessions[0];
		unset($this->sessions[0]);
		$this->sessions[7] = $tmp;

		$tmp_total = $this->total_weeks[0];
		unset($this->total_weeks[0]);
		$this->total_weeks[7] = $tmp_total;

		$tmp_hol = $this->holiday_sessions[0];
		unset($this->holiday_sessions[0]);
		$this->holiday_sessions[7] = $tmp_hol;
		
		$tmp_total_holiday = $this->holiday_total_weeks[0];
		unset($this->holiday_total_weeks[0]);
		$this->holiday_total_weeks[7] = $tmp_total_holiday;

		foreach($this->sessions as $day => $value) {
			$noOfWeeks = $this->total_weeks[$day]['total'];
			foreach($value as $exit_time => $data) {
				array_push($labels, $days[$day] . ': '.
				$data['start_time']. ' - '. $exit_time );
				array_push($datas, intval($data['total'] / $noOfWeeks));
			}
			
		}
		foreach($this->holiday_sessions as $day => $value) {
			$noOfWeeks = $this->holiday_total_weeks[$day]['total'];
			foreach($value as $exit_time => $data) {
				array_push($holiday_labels, $days[$day] . ': '.
				$data['start_time']. ' - '. $exit_time );
				array_push($holiday_datas, intval($data['total'] / $noOfWeeks));
			}
			
		}
		$this->total_entries__data = json_encode($datas);
		$this->total_entries__labels = json_encode($labels);
		$this->total_holiday__data = json_encode($holiday_datas);
		$this->total_holiday__labels = json_encode($holiday_labels);
	}
	public function isHoliday() {
		$t = $this->_db->get_var('Select holiday_id from `rwc_holidays` WHERE `start_date` <= CURDATE() AND `end_date` >= CURDATE()', ARRAY_A);
		print_r($t);
		if(!empty($t))
			return $t[0]['holiday_id'];
		else 
			return false;
	}
	public function isOpen() {
		$id = $this->isHoliday();
		if($id != false) {
			$t = $this->_db->get_results('SELECT 1 from rwc_holidays_sessions WHERE day = dayofweek(CURDATE())  -1 AND end_time > date_format(NOW(), "%H:%i") AND holiday_id = '.$id, ARRAY_A);
		} else {
			$r = $this->_db->get_results('SELECT 1 from rwc_sessions WHERE day = dayofweek(CURDATE())  -1 AND end_time > date_format(NOW(), "%H:%i")', ARRAY_A);
		}
		
		return (!empty($t))? true: false;
	}
	public function getTotalMembersOnPremises() {
		$numOfEntries = $this->_db->get_results( 'SELECT count(*) as total FROM `rwc_members_entries` WHERE`rwc_members_entries`.`entry_date` = CURRENT_DATE AND `rwc_members_entries`.`exit_time` >= NOW() + INTERVAL 10 MINUTE AND premature_exit IS NULL', ARRAY_A );
		return $numOfEntries[0]['total'];
	}
}