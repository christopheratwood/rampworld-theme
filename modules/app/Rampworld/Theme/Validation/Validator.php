<?php


/**
 * 
 */
namespace Rampworld\Theme\Validation;
class Validator {
    public $age;
    public $has_error;
    public $error_list = array();



    public function __construct($items, $allowed_null) {
        foreach($items as $item => $value) {
           
            if(!in_array($value['section'], $this->error_list)) {
                $this->error_list[$value['section']] = array();
                
            }
            if(isset($value['secondary_value']))
                $this->check(ucwords(str_replace('_', ' ', $item)), $value['section'], $value['value'], $value['requirements'], $value['secondary_value']);
            else
                $this->check(ucwords(str_replace('_', ' ', $item)), $value['section'], $value['value'], $value['requirements']);
            
        }
        var_dump($this->error_list);
    }
    public function addChecks($items) {
        foreach($items as $item => $value) {
           
            if(!in_array($value['section'], $this->error_list)) {
                $this->error_list[$value['section']] = array();
                
            }
            if(isset($value['secondary_value']))
                $this->check(ucwords(str_replace('_', ' ', $item)), $value['section'], $value['value'], $value['requirements'], $value['secondary_value']);
            else
                $this->check(ucwords(str_replace('_', ' ', $item)), $value['section'], $value['value'], $value['requirements']);
            
        }
        var_dump($this->error_list);
    }
    public function check($name, $section, $value, $r, $s_value = null) {
        foreach($r as $type => $rules) {
            switch($type) {
                case 'length':

                    if (strlen($value) < $rules[0]) {
                        $this->error_list[$section][] = $name . ' must be greater than ' . $rules[0] . ' characters.';
                    } 
                    if (strlen($value) > $rules[1]) {
                        $this->error_list[$section][] = $name . ' must be less than ' . $rules[1] . ' characters.';
                    } 
                    break;
                case 'characters':
                
                    if($rules == 'letters') {
                        
                       $re = '/^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.\'-]+$/';

                        if(!preg_match($re, $value) && strlen($value) > 0 ){
                             $this->error_list[$section][] = $name . ' must only contains letters and spaces.';
                        }
                        
                    } else if( $rules == 'numbers') {
                        $re = '/^[\d]+$/';
                        if(!preg_match($re, $value) && strlen($value) > 0){
                             $this->error_list[$section][] = $name . ' must only contains numbers.';
                        } 
                    }
                    break;
                case 'email':
                    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                        $this->error_list[$section][] = $name . ' must a valid email address.';
                    } else {
                        $index = array_search($name + ' must a valid email address',  $this->error_list[$section]);
                        if($index != false)
                            unset($this->error_list[$section][$index]);
                    }
                    break;
                case 'postcode':
                    $tmp = str_replace(' ','', $value);
                    if(strlen($tmp) < 6 || strlen($tmp) > 9) {
                        $this->error_list[$section][] = $name . ' must a valid postcode.';
                    } else {
                        $index = array_search($name + ' must a valid postcode',  $this->error_list[$section]);
                        if($index != false)
                            unset($this->error_list[$section][$index]);
                    }
                    break;
                case 'dob':

                    $dob = DateTime::createFromFormat('d/m/Y',$value);
                    $today = new DateTime(date('d-m-Y'));
                    $errors = DateTime::getLastErrors();
                    if (!empty($errors['warning_count'])) {
                        $this->error_list[$section][] ='Please provide a valid Date of Birth.';

                    } else {
                        if(is_bool($dob)) {
                            $this->error_list[$section][] ='Please provide a valid Date of Birth.';
                        } else {
                            $this->age = $today->diff($dob);
                            $this->age = $this->age->y;
                            
                            if(intval($this->age) < 2) {
                               $this->error_list[$section][] = 'Your must be at least 2 years old.';
                                $pass['failed']++;
                            } else if(intval($this->age) >100) {
                                $this->error_list[$section][] = 'Your must be under 100 years old.';
                                $pass['failed']++;
                                
                            }
                        }
                    }


                    break;
                case 'allowed':
                    foreach($r as $item => $val) {
                        if(!in_array($val, $r )) {
                            $this->error_list[$section][] =$name.' must have atleast one selected.';
                        }
                        if($val == 'other' && strlen($s_value) < 1){
                            $this->error_list[$section][] ='Please provided a sporting activity.';
                        }
                    }
                    break;
                case 'checked':
                    if($value != true) {
                        $this->error_list[$section][] =$name.' must be selected.';

                    } else {
                        $index = array_search($name.' must be selected.',  $this->error_list[$section]);
                        if($index != false)
                            unset($this->error_list[$section][$index]);
                    }
                    break;
            }
        }
    }

}
