<?php namespace Rampworld\Pagination;

use Rampworld\DB\Connection as Connection;
use Rampworld\Booking\Fetch as FetchBooking;
use Rampworld\Entries\FetchEntries as FetchEntries;
use Rampworld\Session\Fetch as FetchSessions;

class Create {

  public static $body = '';
  public static $breadcrumb = '';

	public static function booking($search_field, $search_value , $start, $limit, $page) {
    $booking = new FetchBooking();

    $start_no = ( $page - 1 ) * $limit;
    $booking->getAll( $search_field, $search_value , $start_no, $limit);

    if(count($booking->bookings) > 0){
      self::$body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th>#</th><th>Booking Name</th><th>Booking Email</th><th>Session Date</th><th>Session Times</th><th>Purchased Date</th><th>Modify</th></tr></thead><tbody>';
			for($i = 0; $i < count($booking->bookings); $i++) {

				self::$body .= '<tr><td>'.$booking->bookings[$i]['id'].'</td><td>'.$booking->bookings[$i]['name'].'</td><td>' . $booking->bookings[$i]['email']  . '</td><td>'.$booking->bookings[$i]['session_date'].'</td><td>'.$booking->bookings[$i]['times'].'</td><td>'.$booking->bookings[$i]['purchased'].'</td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fsingle&bid='.$booking->bookings[$i]['id'].'" class="btn btn-default" style=" margin: 0px;">View</a></td></tr>';
			}
			self::$body .= '</tbody></table></div>';

			self::$breadcrumb .= '<div class="pagination-container">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $booking->count)? ($booking->count): intval($start_no + $limit)).' of '.$booking->count.' bookings</p><ul class="pagination">';
			$num_of_pages = ceil($booking->count/$limit);

			self::$breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.($page-1) .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').' aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

			if($num_of_pages > 11) {
				if($page > 6) {
					for($i = $page - 5; $i <= $page + 5; $i++) {
						self::$breadcrumb  .= '<li '.(($i == $page)? 'class="active"': '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.$i .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').' >'.$i.'</a></li>';
					}
				} else {
					for($i = 1; $i <= 10; $i++) {
						self::$breadcrumb  .= '<li '.(($i == $page)? 'class="active"': '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.$i .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').'>'.$i.'</a></li>';
					}
				}
			} else {
				for($i = 1; $i <= $num_of_pages; $i++) {
					self::$breadcrumb  .= '<li '.(($i == $page)? 'class="active"': '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.$i .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').' >'.$i.'</a></li>';
				}
			}
			self::$breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fbookings%2Fview%2Fall&p='.($page + 1) .(($search_field != null)? '&search_field='.str_replace(' ', '+', $search_field).'': '').(($search_value != null)? '&search_value='.str_replace(' ', '+', $search_value).'': '').(($limit != null)? '&limit='.$limit.'"': ''): '').' aria-label="">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>';
			self::$breadcrumb .= '</ul></div>';

		} else {
			self::$body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No bookings</span><small>Please refine your search</small></div></div>';
		}
  }

  public function entries($forename, $surname, $membership_id, $sessionTime, $end_dt, $start, $limit, $page) {
    
    $entries = new FetchEntries();
    $sessions = new FetchSessions();


    $sessions->getTodaysSessions();

    $start_no = ( $start - 1 ) * $limit;

    $entries->getAll($forename, $surname, $membership_id, $sessionTime, $end_dt, intval($start_no), intval($limit));
    if($sessionTime == null) {
      $session_time = new DateTime();
    } else {
      $session_time = DateTime::createFromFormat('Y-m-d H:i', date('Y-m-d H:i', strtotime(explode('T', $sessionTime)[0]. ' '. explode('T', $sessionTime)[1])));
    }
    
    $can_delete = $session_time->getTimestamp() >= strtotime('now');
    $can_modify = $session_time->format('Y-m-d') == date('Y-m-d');

    $expertise = array(0 => 'Beginner', 1=> 'Novice', 2 => 'Experienced', 3 => 'Advanced' , 4 => 'Expert', 5 => 'Professional');

    if(count($entries->entries) > 0){
      $today = new DateTime();
      $this->body = '<div class="table-responsive"><table class="table table-striped table-hover table-sortable"><thead><tr><th >#</th><th>Forename</th><th>Surname</th><th>Date</th><th>Time in</th><th>Session end</th><th>Age</th><th>View member</th><th>Change entry</th><th>Remove entry</th></tr></thead><tbody>';

      for($i = 0; $i < count($entries->entries); $i++) {


        $dob = DateTime::createFromFormat('Y-m-d', $entries->entries[$i]['dob']);
        $errors = DateTime::getLastErrors();
        if (!empty($errors['warning_count'])) {
          $dob = 'Error.';
        } else {
          $dob = $today->diff($dob);
          $dob = $dob->y;
        }
        $this->body .= '<tr '.(($entries->entries[$i]['paidMembership'] != "0")? 'class="info"': '').'><td>'.$entries->entries[$i]['member_id'].'</td><td>'. ucwords(strtolower($entries->entries[$i]['forename'])).'</td><td>' . ucwords(strtolower($entries->entries[$i]['surname']))  . '</td><td>'.$entries->entries[$i]['entry_date'].'</td><td>'.$entries->entries[$i]['entry_time'].'</td><td>'.$entries->entries[$i]['exit_time'].'</td><td>'.$dob.'</td><td><a href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fmembers%2Fview&member='.$entries->entries[$i]['member_id'].'" class="btn btn-default" style=" margin: 0px;">View Member</a></td>
        
        <td>'.(( $can_modify && count($sessions->sessionTimes) >= 1)? '<a data-member-id="'.$entries->entries[$i]['member_id'].'" data-forename="'.$entries->entries[$i]['forename'].'" data-surname="'.$entries->entries[$i]['surname'].'" data-age="'.$dob.'" data-has-membership="'.(($entries->entries[$i]['paidMembership'] != "0") ? $entries->entries[$i]['paidMembership']: 'None').'" data-expertise="'.$expertise[$entries->entries[$i]['expertise']].'" data-current-end="'.$entries->entries[$i]['exit_time'].'" data-session-id="'.$entries->entries[$i]['entry_id'].'" data-current-times="'.$entries->entries[$i]['entry_time'].','.$entries->entries[$i]['exit_time'].'"class="btn btn-default change-session-btn" style=" margin: 0px;">Change session</a>':'<a class="btn btn-default disabled" style=" margin: 0px;">Cannot change session</a>').'</td><td>'.(($can_delete)? '<a data-entry-id="'.$entries->entries[$i]['entry_id'].'" data-member-id="'.$entries->entries[$i]['member_id'].'"  data-member="'.ucwords(strtolower($entries->entries[$i]['forename'])).' '.ucwords(strtolower($entries->entries[$i]['surname'])).'" class="btn btn-danger remove-participant-btn" style=" margin: 0px;">Delete entry</a>' : '<a class="btn btn-danger disabled" style=" margin: 0px;">Unable to delete</a' ).'</td></tr>';
      }
      $this->body .= '</tbody></table></div>';
      $this->breadcrumb = '<div class="pagination-container"><p style="margin-bottom: 0;">'. ($start_no + 1) . ' - ' . ((intval($start_no + $limit) > $entries->count)? ($entries->count): intval($start_no + $limit)).' of '.$entries->count.' entries</small><ul class="pagination">';
      $num_of_pages = ceil($entries->count/$limit);

      
      $this->breadcrumb  .= '<li '.(($page == 1)? 'class="disabled"' : '').'><a '.(($page != 1)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.($page - 1) . (($limit != null)? '&limit='.$limit: '').(($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"':''). (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt: '') : '').'" aria-label="Previous"><i class="glyphicon glyphicon-chevron-left"></i> Previous</a></li>';

      if($num_of_pages > 11) {
        if($page > 6) {
          for($i = $page - 5; $i <= $page + 5; $i++) {
            $this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: '').(($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"': ''). (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt:''): '').'">'.$i.'</a></li>';
          }
        } else {
          for($i = 1; $i <= 10; $i++) {
            $this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: '').(($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"': ''). (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt: ''): '').'">'.$i.'</a></li>';
          }
        }
      } else {
        for($i = 1; $i <= $num_of_pages; $i++) {
          $this->breadcrumb .= '<li '.(($i == $page)? 'class="active"': '').'><a '. (($i != $start)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.$i .(($limit != null)? '&limit='.$limit: ''). (($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"': ''). (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt:''): '').'">'.$i.'</a></li>';
        }
      }
      $this->breadcrumb  .= '<li '.(($page == $num_of_pages)? 'class="disabled"' : '').'><a '.(($page != $num_of_pages)? 'href="'.host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fentries%2Fview%2Fall&p='.($page + 1) . (($limit != null)? '&limit='.$limit: '').(($forename != null)? '&forename='.str_replace(' ', '+', $forename).'': '').(($surname != null)? '&surname='.str_replace(' ', '+', $surname).'': '').(($membership_id != null)? '&membership_id='.$membership_id.'"':'').  (($sessionTime != null)? '&start_dt='.$sessionTime.'&end_dt='.$end_dt:'') : '').'" aria-label="Previous">Next <i class="glyphicon glyphicon-chevron-right"></i></a></li>'; 
      $this->breadcrumb .= '</ul></div>';


      $this->breadcrumb .= '</ul></div>';
    } else {
      $this->body = '<div class="no-results"><div class="icon"><i>i</i></div><div class="message"><span class="message">No entries</span><small>Please refine your search</small></div></div>';
    }
    

  }
}
