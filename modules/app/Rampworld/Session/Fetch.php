<?php
namespace Rampworld\Session;

use Rampworld\DB\Connection as Connection;
use Rampworld\Event\EventFetch as EventFetch;

if (session_status() == PHP_SESSION_NONE) {
	session_start();
}


date_default_timezone_set('Europe/London');

class Fetch extends EventFetch {

	public $sessions,
					$sessionTimes = array();

	protected $_nonOperationalDates,
						$_event,
						$_db;

	public function __construct() {

		$this->_db  = Connection::connect();
		$this->_event = new EventFetch();
	}
	protected function _setNonOperationalDates() {

		$this->_nonOperationalDates = $this->_db->get_results('SELECT date_format(`date`, "%Y/%m/%d") as date FROM rwc_closed_dates WHERE `date` >= CURDATE()', ARRAY_A);
	}

	public function get_sessions($date,$html = null, $private = null) {

		$curdate = (isset($_SESSION['session']['session_date']) && date('Y-m-d', strtotime($_SESSION['session']['session_date'])) == $date->format('Y-m-d'));
		
		$this->_setNonOperationalDates();
		$pricing = json_decode($this->get_prices(), true);
		
		//Get amount of week pass members
		$week_pass_amount = intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_paid_membership WHERE %s BETWEEN start_date AND end_date AND `premature` IS NULL AND refunded IS NULL', array($date->format("Y-m-d"))),ARRAY_A)[0]['total']);

		for($i = 0; $i < count($this->_nonOperationalDates); $i++) {
			if($this->_nonOperationalDates[$i]['date'] == $date->format('Y/m/d')){
				return json_encode(false);
				exit();
			}
		}
	
		$date = $date;
		$day = $date->format("w");
		
		$not_string = '';
		$events = $this->has_event($date);

		for($i = 0; $i < count($events); $i++) {
			
			if($events[$i]['online_exclusive']) {
				$not_string = 'AND (`start_time` < \''. $events[$i]['start_time'].'\' OR `start_time` >= \''. $events[$i]['end_time'].'\')' ;
			}

			if($date >=date('Y-m-d')) {
				$events[$i]['totalBooked'] = intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_prepaid_sessions WHERE `session_date` = %s AND session_start_time <= %s AND session_end_time >= %s', array($date->format('Y-m-d'),  $events[$i]['start_time'], $events[$i]['end_time'])) ,ARRAY_A)[0]['total']);
			
				if($date == date('Y-m-d')) {
					$events[$i]['totalBooked'] += intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_members_entries WHERE `entry_date` = %d AND exit_time  >= %s', array($events[$i]['start_time'], $events[$i]['end_time'])), ARRAY_A)[0]['total']);
				}
			}
		}
		$isHoliday = $this->_db->get_results($this->_db->prepare("SELECT holiday_id FROM rwc_holidays WHERE start_date <= %s AND end_date >= %s LIMIT 1",array($date->format('Y-m-d'), $date->format('Y-m-d'))), ARRAY_A);
		
		if($private == null){

				if(isset($isHoliday[0]['holiday_id']) ) {

					$this->sessions = $this->_db->get_results($this->_db->prepare("SELECT holiday_session_id, display_name, start_time, end_time, private, beginner, consecutive FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d AND private IS NULL ".$not_string." ORDER BY start_time ASC",array($isHoliday[0]['holiday_id'], $day)), ARRAY_A);
					
				} else {
					
					$this->sessions = $this->_db->get_results( $this->_db->prepare("SELECT session_id, display_name, start_time, end_time, private, beginner, consecutive FROM rwc_sessions WHERE day = %d AND private IS NULL ".$not_string."  ORDER BY start_time ASC",array($day)), ARRAY_A);
				}
		} else {

			if(isset($isHoliday[0]['holiday_id']) ) {

				$this->sessions = $this->_db->get_results($this->_db->prepare("SELECT id, display_name, end_time,  start_time,  private, beginner, consecutive FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d ".$not_string." ORDER BY start_time ASC",array($isHoliday[0]['holiday_id'], $day)), ARRAY_A);
			} else {

				$this->sessions = $this->_db->get_results( $this->_db->prepare("SELECT session_id, display_name, start_time, end_time, private, beginner, consecutive FROM rwc_sessions WHERE day = %d AND private IS NULL ".$not_string."  ORDER BY start_time ASC",array($day)), ARRAY_A);
			}
		}
		//get counts
		for($i = count($this->sessions) -1; $i >= 0; $i--) {

			$this->sessions[$i]['totalBooked'] = intval($this->_db->get_results('SELECT count(member_id) as total from rwc_prepaid_sessions WHERE `session_date` = \''.$date->format('Y-m-d').'\' AND session_start_time <= \''.$this->sessions[$i]['start_time'].'\' AND session_end_time >= \''.$this->sessions[$i]['end_time'].'\'',ARRAY_A)[0]['total']);
			
			if($date == date('Y-m-d')) {

				$this->sessions[$i]['totalBooked'] += intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_members_entries WHERE `entry_date` = CURDATE() AND exit_time  >= %s', array($this->sessions[$i]['end_time'])), ARRAY_A)[0]['total']);
			}
			
			$this->sessions[$i]['totalBooked'] += $week_pass_amount;
		}
		$this->sessions = array_merge($events, $this->sessions);

		for($i = count($this->sessions) -1;$i >= 0; $i--) {

			if($this->sessions[$i]['beginner'] == 1 || (round(abs(strtotime($this->sessions[$i]['end_time']) - strtotime($this->sessions[$i]['start_time'])) /60 /60) <= 2)) {
				
				$this->sessions[$i]['cost'] = $pricing['b'];
			} else if( isset ($this->sessions[$i]['event_id']) ) {
				$this->sessions[$i]['cost'] = $this->sessions[$i]['cost'];
			} else {
				$this->sessions[$i]['cost'] = $pricing[1];
			}
			if(isset($_SESSION['session']['session_type'])){
				
				switch($_SESSION['session']['session_type']) {
				
					case 'session':
						
						$this->sessions[$i]['selected'] =  ($curdate && isset($this->sessions[$i]['session_id']) && $_SESSION['session']['session_id'] == $this->sessions[$i]['session_id'] ) ? '1': null;
						break;
					case 'holiday':
					
						$this->sessions[$i]['selected'] = ($curdate && isset($this->sessions[$i]['holiday_session_id']) && $_SESSION['session']['session_id'] == $this->sessions[$i]['holiday_session_id'] ) ? '1': null;
						break;
					case 'event':
					
						$this->sessions[$i]['selected'] = ($curdate && isset($this->sessions[$i]['event_id']) && $_SESSION['session']['session_id'] == $this->sessions[$i]['event_id'] ) ? '1': null;
						break;
				}
			}

			$this->sessions[$i]['isConsecutive'] = null;

			if ($this->sessions[$i]['consecutive'] == "1") {
				
				$start_time = $this->sessions[$i]['start_time'];
				$j = 1;

				foreach ($this->sessions as $sesh) {

					$total = $sesh['totalBooked'];
					if ($sesh['start_time'] > $start_time && $sesh['consecutive'] == "1") {

						$j++;
						//$total += $sesh['totalBooked'];
						$selected = ($curdate && $_SESSION['session']['session_start_time'] == $start_time && $_SESSION['session']['session_end_time'] == $sesh['end_time'] ) ? '1': null;
						
						$this->sessions[]= array('id' => 0, 'display_name' => $this->sessions[$i]['display_name'], 'start_time' => $start_time, 'end_time' => $sesh['end_time'], 'private' => null, 'beginner' => null, 'consecutive'=> $j, 'totalBooked' => $total, 'cost' => $pricing[$j], 'selected' => $selected, 'isConsecutive' => 1 );
					}
				}
			}
			//echo round(abs(strtotime($this->sessions[$i]['end_time']) - strtotime($this->sessions[$i]['start_time'])) / 60 /60). '<br>';
		}

		uasort($this->sessions, function($a,$b){
			$c = $a['start_time'] - $b['start_time'];
			$c .= $a['end_time'] - $b['end_time'];
			return $c;
		});
		//echo '<pre>';print_r($this->sessions);echo '</pre>';
		//$return  = $this->array_preserve_js_order($this->sessions);
			//week pass

		$time = \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime($date->format('Y-m-d'))));
		$week = $time->format('W');
		$year =  $time->format('Y');

		$timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
		$timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
		$monday = date( 'Y-m-d', $timestamp_for_monday );

		if($date->format('w') == "0") {
			$sunday = 	$sunday = \DateTime::createFromFormat('Y-m-d', date('Y-m-d',  strtotime($date->format('Y-m-d'))));
		} else {
			$sunday = \DateTime::createFromFormat('Y-m-d', date('Y-m-d', strtotime('sunday this week', strtotime($date->format('Y-m-d')))));
		}

		if(isset($this->sessions[0])) {
			switch(key($this->sessions[0])){
				case 'event_id':
					$type = 'event';
					break;
				case 'session_id':
					$type = 'session';
					break;
				case 'holiday_session_id':
					$type = 'holiday';
					break;
				
			}
		}
		$cost = ($type == 'holiday') ? '30.00' : '20.00';

		$week_pass = intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_prepaid_sessions WHERE `session_date` = %s AND refunded IS NULL', array($date->format("Y-m-d"))),ARRAY_A)[0]['total']);
		
		$this->sessions[] = array(
			'weekpass_id' => 1,
			'display_name'	=> ($type == 'holiday') ? 'Holiday week pass': 'Week pass',
			'start_time'		=> $monday,
			'end_time'			=> $sunday->format('Y-m-d'),
			'private'				=> null,
			'beginner'			=> null,
			'consecutive'		=> null,
			'totalBooked'		=> intval($week_pass_amount + $week_pass),
			'cost'					=> $cost,
			'selected'			=> (isset($_SESSION['session']['session_start_time']) && $_SESSION['session']['session_start_time'] == $monday && $_SESSION['session']['session_end_time'] == $sunday->format('Y-m-d')) ? 1 : null,
			'isConsecutive'	=> null
		);
	

		return json_encode($this->array_preserve_js_order($this->sessions));

		
			
	

	}
	public function array_preserve_js_order(array $data) {
			return array_map(
					function($key, $value) {
							if (is_array($value)) {
									$value = $this->array_preserve_js_order($value);
							}
							return array($key, $value);
					},
					array_keys($data),
					array_values($data)
			);
	}
	public function todaySessions($tomorrow = null) {

		$today =  new DateTime();
		if($tomorrow == true) {
			$today =  new DateTime(date('Y-m-d', strtotime('+1 day')));
		}


		if($this->_db->get_results($this->_db->prepare('SELECT count(1) FROM rwc_closed_dates WHERE `date` = %s', array($today->format('Y-m-d'))), ARRAY_A)[0]['count(1)'] == 1){
			return json_encode(false);
			exit();
		} else {
			$day = $today->format("w");
			$isHoliday = $this->_db->get_results($this->_db->prepare("SELECT holiday_id FROM rwc_holidays WHERE start_date <= %s AND end_date >= %s LIMIT 1",array($today->format('Y-m-d'), $today->format('Y-m-d'))), ARRAY_A);

			
			if(count($isHoliday) > 0) {
				$this->sessions = $this->_db->get_results($this->_db->prepare("SELECT `display_name` as title, concat('".$today->format('Y-m-d')." ', end_time) as end, concat('".$today->format('Y-m-d')." ', start_time) as start, private, beginner FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d ".$not_string." ORDER BY start_time ASC",array($isHoliday[0]['holiday_id'], $day )), ARRAY_A);
			} else {
				$this->sessions = $this->_db->get_results( $this->_db->prepare("SELECT `display_name` as title, concat('".$date->format('Y-m-d')." ', end_time) as end, concat('".$date->format('Y-m-d')." ', start_time) as start, private, beginner FROM rwc_sessions WHERE day = %d ".$not_string."  ORDER BY start_time ASC",array($day)), ARRAY_A);
			}
	
			for($i = 0; $i < count($this->sessions); $i++) {
				$this->sessions[$i]['totalBooked'] = intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_prepaid_sessions WHERE `session_date` = %s AND session_start_time <= %s AND session_end_time >= %s', array($today->format('Y-m-d'), $this->sessions[$i]['start'], $this->sessions[$i]['end'])),ARRAY_A)[0]['total']);
				//var_dump($this->sessions[$i]);
				if($tomorrow == null) {
						$this->sessions[$i]['totalBooked'] += intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_members_entries WHERE `entry_date` = CURDATE() AND entry_time >= %s AND  exit_time = %s ', array( $this->sessions[$i]['start'], $this->sessions[$i]['end'])), ARRAY_A)[0]['total']);
						if($i != count($this->sessions) - 1)
							$this->sessions[$i]['totalBooked'] = intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_members_entries WHERE `entry_date` = %s AND entry_time >= %s AND entry_time <= %s AND  exit_time > %s  ', array($today->format('Y-m-d'), $this->sessions[$i]['start'], $this->sessions[$i]['end'], $this->sessions[$i]['start'])), ARRAY_A)[0]['total']);

				}
			}
			
		}

		return json_encode($this->sessions);

	}
	public function get_availability() {
		
	}
	public function get_prices() {
		$arr = array();
		$d = $this->_db->get_results("SELECT * FROM rwc_pricing", ARRAY_A);

		foreach ($d as $key => $value) {
			$arr[$value['num_of_sessions']] = $value['cost'];
		}
		return json_encode($arr);
	}

	public function getTodaysSessions() {

		$date = date('w');
		$holiday = $this->_db->get_results('SELECT holiday_id FROM rwc_holidays WHERE CURDATE() between start_date AND end_date AND visible = 1', ARRAY_A);
		if(!empty($holiday )) {
			$this->sessionTimes =  $this->_db->get_results($this->_db->prepare('SELECT end_time, display_name from rwc_holidays_sessions WHERE holiday_id = %d AND day = %d AND end_time >= %s ORDER BY end_time ASC', array($holiday[0]['holiday_id'], $date, date('H:i'))), ARRAY_A);
		} else {
			$this->sessionTimes =  $this->_db->get_results($this->_db->prepare('SELECT end_time, display_name from rwc_sessions WHERE day = %d AND end_time >= %s ORDER BY end_time ASC', array( $date, date('H:i'))), ARRAY_A);
		}
	}
}