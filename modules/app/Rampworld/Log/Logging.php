<?php
namespace Rampworld\Log;


class Logging{
	private $_db;

  public function __construct() {

    require_once __DIR__.'/../../Helpers/Connection.php';
    $this->_db  = $db;
  }
  public function addAccountLog($account_id, $type) {

    $this->_db->insert( 'rwc_accounts_activity', array(
      'account_id'    => $account_id,
      'type'          => $type,
      'page'          => $_SERVER['REQUEST_URI'],
      'IP_address'    => $this->getIPAddress()
    ), array(
      '%d', '%d', '%s', '%s'
    ));
  }
  protected function getIPAddress (){
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'unknown';
    return $ipaddress;
  }
}