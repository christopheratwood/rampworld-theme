<?php
namespace Rampworld\Entries;

use Rampworld\DB\Connection as Connection;

class Fetch {
	private $_db;
	function __construct( ) {

		$this->_db  = Connection::connect();
  }
  
  public function getAll($forename = null, $surname = null, $membership_id = null, $start_dt = null, $end_dt = null, $start_no = null, $limit = 9999) {
    $remap = array('forename'=> '`mem`.forename','surname'=> '`mem`.surname','membership_id'=> '`mem`.member_id','phone number'=> '`mem`.number','address line 1'=> '`mem`.address_line_one','postcode'=> '`mem`.postcode', 'booking name' => '`book`.name', 'booking number' => '`book`.id', 'booking email' => '`book`.email' );
    $joins = '';
    $query = '';
    $included_joins = array();
    if($start_dh != null) {
      $start_dt = ($start_dt != null) ? str_replace('%3A', ':', $start_dt) : null;
      $end_dt = ($end_dt != null) ? str_replace('%3A', ':', $end_dt) : null;
      $query .= 'TIMESTAMP(ent.`entry_date`, ent.`exit_time`) BETWEEN TIMESTAMP(%s, %s) AND  TIMESTAMP(%s, %s)';
      $bindings[] = '';
    }
    if($forename != null) {
      $query='';
    }
    //ent.`entry_date` = CURRENT_DATE
    /* AND ent.`exit_time` >= NOW() - INTERVAL 10 MINUTE
    AND ent.`premature_exit` IS NULL 
  GROUP BY ent.`entry_id`
 */
    $this->entries  = $this->_db->get_results($this->_db->prepare('SELECT ent.`entry_id`, ent.`entry_time`, DATE_FORMAT(ent.`entry_date`, "%%d-%%m-%%Y") AS entry_date, DATE_FORMAT(ent.`exit_time`, "%%H:%%i") as exit_time, mem.`member_id`, mem.`forename`, mem.`surname`, mem.`number`, mem.`dob`,mem.`expertise`, ent.`premature_exit` as premature,
      CASE WHEN `rwc_paid_membership`.`start_date` <= CURRENT_DATE() 
        AND pass.`end_date` >= CURRENT_DATE()
        AND pass.`premature` IS NULL
        THEN pass.`display_name` ELSE 0 END AS paidMembership 
      FROM rwc_members_entries as ent
      LEFT JOIN rwc_members as mem ON ent.`member_id` = mem.`member_id`
      LEFT JOIN `rwc_paid_membership` as pass ON mem.`member_id` = pass.`member_id`
        AND pass.`start_date` <= CURRENT_DATE()
        AND pass.`end_date` >= CURRENT_DATE()
        AND pass.`premature` IS NULL
      LEFT JOIN `rwc_membership_types` as pass ON pass.`membership_type` = type.`membership_type_id`
      WHERE ent.premature_exit IS NULL
        '.$query.'
      ORDER BY `entry_date` ASC, `exit_time` ASC, `entry_time` ASC LIMIT %d, %d', $bindings), ARRAY_A);

  }

	
}