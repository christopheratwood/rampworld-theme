<?php namespace Rampworld\Validation;


class Check {
  public static $errors = array();
  public static $passed = true;

  public static function validate($form, array $data) {
    switch ($form) {
      case 'member':
        //personal details
        if (isset($_POST['forename']) && strlen($_POST['forename']) == 0) {
          self::$errors['forename'] = 'Forename is required.';
          self::$passed = false;
        } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['forename'])) {
          self::$errors['forename'] = 'Forename must only contain characters.';
          self::$passed = false;
        } elseif (strlen($_POST['forename']) < 2) {
          self::$errors['forename'] = 'Forename must be more 2 characters.';
          self::$passed = false;
        } elseif (strlen($_POST['forename']) > 100) {
          self::$errors['forename'] = 'Forename must be under 100 characters.';
          self::$passed = false;
        }
            //surname validation
        if (empty($_POST['surname'])) {
          self::$errors['surname'] = 'Surname is required.';
          self::$passed = false;
        } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['surname'])) {
          self::$errors['surname'] = 'Surname must only contain characters.';
          self::$passed = false;
        } elseif (strlen($_POST['surname']) < 2) {
          self::$errors['surname'] = 'Surname must be more 2 characters.';
          self::$passed = false;
        } elseif (strlen($_POST['surname']) > 100) {
          self::$errors['surname'] = 'Surname must be under 100 characters.';
          self::$passed = false;
        }

        //Gender
        if (empty($_POST['gender'] )) {
          self::$errors['gender'] = 'Your gender is required.';
          self::$passed = false;
        } elseif (in_array( $_POST['gender'], array('m','f', 'o') ) != true) {
          self::$errors['gender'] = 'Please provide a valid gender.';
          self::$passed = false;
        }
        $today = new DateTime(date('d-m-Y H:i:s'));
        $dob = DateTime::createFromFormat('Y-m-d', $_POST['dob']);
    
        $errors = DateTime::getLastErrors();
        if (!empty($errors['warning_count'])) {
          self::$errors['dob'] = 'Please provide a valid Date of Birth.';
          self::$passed = false;
        } else {
          if (is_bool($dob)) {
            self::$errors['dob'] = 'Please provide a valid Date of Birth.';
            self::$passed = false;
          } else {
            $intval = $today->diff($dob);

            if (intval($intval->y) < 2) {
              self::$errors['dob'] = 'Your must be at least 2 years old.';
              self::$passed = false;
            } elseif (intval($intval->y) >100) {
              self::$errors['dob'] = 'Your must be under 100 years old.';
              self::$passed = false;
            }
          }
        }
        $_POST['age'] =intval($intval->y);

        //email address
        if (empty($_POST['email'])) {
          self::$errors['email'] = 'Email address is required.';
          self::$passed = false;
        } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
          self::$errors['email'] = 'Email address must be valid. We will use this to confirm your membership.';
          self::$passed = false;
        } elseif (strlen($_POST['email']) > 255) {
          self::$errors['email'] = 'Email Address must number 255 characters.';
          self::$passed = false;
        }
        //telephone
        if (empty($_POST['number'])) {
          self::$errors['number'] = 'Your mobile/ telephone number is required.';
          self::$passed = false;
        } elseif (!is_numeric($_POST['number'])) {
          self::$errors['number'] ='Your number must consist of only digits.';
          self::$passed = false;
        } elseif (strlen($_POST['number']) < 7 || strlen($_POST['number']) > 16) {
          self::$errors['number'] = 'Please enter a valid telephone number.';
          self::$passed = false;
        }

        if (empty($_POST['address_line_one'])) {
          self::$errors['address_line_one'] = 'Please enter the first line of your address';
          self::$passed = false;
        } elseif (strlen($_POST['address_line_one']) > 50) {
          self::$errors['address_line_one'] = 'First address line must be under 50 characters.';
          self::$passed = false;
        }
        if (!empty($_POST['address_line_two']) && strlen($_POST['address_line_two']) > 50) {
          self::$errors['address_line_two'] = 'Second address line must be under 50 characters.';
          self::$passed = false;
        }
        if (!empty($_POST['address_city']) && strlen($_POST['address_city']) > 50) {
          self::$errors['address_city'] = 'City line must be under 50 characters.';
          self::$passed = false;
        }
        if (!empty($_SESSION['address_county']) && strlen($_POST['address_county']) > 50) {
          self::$errors['address_county'] = 'County line must be under 50 characters.';
          self::$passed = false;
        }
        if (empty($_POST['address_postcode'])) {
          self::$errors['address_postcode'] = 'Please enter your postcode.';
          self::$passed = false;
        } elseif (strlen($_POST['address_postcode']) < 5 || strlen($_POST['address_postcode']) > 10) {
          self::$errors['address_postcode'] = 'Please enter a valid postcode.';
          self::$passed = false;
        } else {
          $_POST['address_postcode'] = strtoupper(wordwrap(str_replace(' ', '', $_POST['address_postcode']), strlen(str_replace(' ', '', $_POST['address_postcode']))-3, ' ', true));
        }
        if (intval($intval->y) < 16) {
          if (empty($_POST['consent_name'])) {
            self::$errors['consent_name'] = 'Please provide parent / guardian\'s name.';
            self::$passed = false;
          } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['consent_name'])) {
            self::$errors['consent_name'] = 'Surname must only contain characters.';
            self::$passed = false;
          } elseif (strlen($_POST['consent_name']) > 100) {
            self::$errors['consent_name'] = 'Surname must number 100 characters.';
            self::$passed = false;
          }
          //parent number
          if (empty($_POST['consent_number'])) {
            self::$errors['consent_number'] = 'Please provide parent / guardian\'s first address line.';
            self::$passed = false;
          } elseif (!is_numeric($_POST['consent_number'])) {
            self::$errors['consent_number'] ='Your number must consist of only digits.';
            self::$passed = false;
          } elseif (strlen($_POST['consent_number']) < 7 || strlen($_POST['consent_number']) > 16) {
            self::$errors['consent_number'] = 'Please enter a valid telephone number.';
            self::$passed = false;
          }
          
          //parent address
          if (empty($_POST['consent_address_one'])) {
            self::$errors['consent_address_one'] = 'Please provide parent / guardian\'s name.';
            self::$passed = false;
          } elseif (strlen($_POST['consent_address_one']) > 50) {
            self::$errors['consent_address_one'] = 'Address line must be under 50 characters long.';
            self::$passed = false;
          }

          //parent postcode
          if (empty($_POST['consent_postcode'])) {
            self::$errors['consent_postcode'] = 'Please enter your postcode.';
            self::$passed = false;
          } elseif (strlen($_POST['consent_postcode']) < 5 || strlen($_POST['consent_postcode']) > 10) {
            self::$errors['consent_postcode'] = 'Please enter a valid postcode.';
            self::$passed = false;
          } else {
            $_POST['consent_postcode'] = strtoupper(wordwrap(str_replace(' ', '', $_POST['consent_postcode']), strlen(str_replace(' ', '', $_POST['consent_postcode']))-3, ' ', true));
          }
          //parental email address
          if (empty($_POST['consent_email'])) {
            self::$errors['consent_email'] = 'Email address is required.';
            self::$passed = false;
          } elseif (!filter_var($_POST['consent_email'], FILTER_VALIDATE_EMAIL)) {
            self::$errors['consent_email'] = 'Email address must be valid. We will use this to confirm your membership.';
            self::$passed = false;
          } elseif (strlen($_POST['consent_email']) > 255) {
            self::$errors['consent_email'] = 'Email Address must number 255 characters.';
            self::$passed = false;
          }
        }
        if (!isset($_POST['discipline'])) {
          self::$errors['discipline'] = "Please pick your discipline(s).";
          self::$passed = false;
        }
        if (!isset($_POST['expertise']) || ($_POST['expertise'] < 0 || $_POST['expertise'] > 5)) {
          self::$errors['expertise'] = "Please click a valid option.";
          self::$passed = false;
        }
        if (empty($_POST['emergency_name_one'])) {
          self::$errors['emergency_name_one'] = "Please provide a emergency contact name.";
          self::$passed = false;
        } elseif (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['emergency_name_one'])) {
          self::$errors['emergency_name_one'] = "Emergency name must only contain characters.";
          self::$passed = false;
        } elseif (strlen($_POST['emergency_name_one']) > 100) {
          self::$errors['emergency_name_one'] = 'Name must number 100 characters.';
          self::$passed = false;
        }
        //emergency number 1
        if (empty($_POST['emergency_number_one'])) {
          self::$errors['emergency_number_one'] = 'A emergency number is required.';
          self::$passed = false;
        } elseif (!is_numeric($_POST['emergency_number_one'])) {
          self::$errors['emergency_number_one'] ='The emergency number must only consist of digits.';
          self::$passed = false;
        } elseif (strlen($_POST['emergency_number_one']) < 7 || strlen($_POST['emergency_number_one']) > 16) {
          self::$errors['emergency_number_one'] = 'Please enter a valid emergency number.';
          self::$passed = false;
        }

        //emergency name 2
        if (!empty($_POST['emergency_name_two']) && !empty($_POST['emergency_number_two'])) {
          if (!preg_match('/^[a-zA-Z-,á,é,í,ó,ú,â,ê,ē,ô,ã,õ,ç,Á,É,Í,Ó,Ú,Â,Ê,Ô,Ã,Õ,Ç,ü,ñ,Ü,Ñ,\s]*$/', $_POST['emergency_name_two'])) {
            self::$errors['emergency_name_two'] = "Emergency name must only contain characters. (a-z A-Z)";
            self::$passed = false;
          } elseif (strlen($_POST['emergency_name_two']) > 100) {
            self::$errors['emergency_name_two'] = 'Name must number 100 characters.';
            self::$passed = false;
          }
          if (!is_numeric($_POST['emergency_number_two'])) {
            self::$errors['emergency_number_two'] ='The emergency number must only consist of digits.';
            self::$passed = false;
          } elseif (strlen($_POST['emergency_number_two']) < 7 || strlen($_POST['emergency_number_two']) > 16) {
            self::$errors['emergency_number_two'] = 'Please enter a valid emergency number.';
            self::$passed = false;
          }
        } elseif (!empty($_POST['emergency_name_two']) && empty( $_POST['emergency_number_two'] )) {
          self::$errors['emergency2_number'] = 'Please enter a valid emergency number.';
          self::$passed = false;
        } elseif (empty($_POST['emergency_name_two']) && !empty( $_POST['emergency_number_two'] )) {
          self::$errors['emergency_name_two'] = 'Please enter a valid emergency contact name.';
          self::$passed = false;
        }
  
        break;

      case 'holiday':
        if (isset($_POST['display_name']) && strlen($_POST['display_name']) == 0) {
          self::$errors['display_name'] = 'Display name is required.';
          self::$passed = false;
        } elseif (strlen($_POST['display_name']) < 2) {
          self::$errors['display_name'] = 'Display name must be more 2 characters.';
          self::$passed = false;
        } elseif (strlen($_POST['display_name']) > 100) {
          self::$errors['display_name'] = 'Display name must be under 100 characters.';
          self::$passed = false;
        }
        if ((isset($_POST['start_date']) && strlen($_POST['start_date']) == 0 ) || (isset($_POST['end_date']) && strlen($_POST['end_date']) == 0 )) {
          self::$errors['start_date'] = 'Start date and end date is required.';
          self::$passed = false;
        } else {
          $t = strtotime('now');
          $s = strtotime($_POST['start_date']);
          $e = strtotime($_POST['end_date']);
          if ($s < $t || $e < $t) {
            self::$errors['start_date'] = 'Holidays cannot be in the past.';
            self::$passed = false;
          } elseif ($s > $e) {
            self::$errors['start_date'] = 'Start date must be before end date.';
            self::$passed = false;
          }
        }

        if (!isset($_POST['visible'])) {
          self::$errors['visible'] = 'Publish status is required.';
          self::$passed = false;
        }

        if (!isset($_POST['update_counter'])) {
          if (!isset($_POST['inherit'])) {
            self::$errors['inherit'] = 'Default times is required.';
            self::$passed = false;
          }
        }
        break;
      case 'session':
        if (isset($_POST['display_name']) && strlen($_POST['display_name']) == 0) {
          self::$errors['display_name'] = 'Display name is required.';
          self::$passed = false;
        } elseif (strlen($_POST['display_name']) < 2) {
          self::$errors['display_name'] = 'Display name must be more 2 characters.';
          self::$passed = false;
        } elseif (strlen($_POST['display_name']) > 50) {
          self::$errors['display_name'] = 'Display name must be under 50 characters.';
          self::$passed = false;
        }
        if (isset($_POST['day']) && $_POST['day'] == 'null') {
          self::$errors['day'] = 'Day is required.';
          self::$passed = false;
        }
        if (isset($_POST['start_time']) && strlen($_POST['start_time']) == 0) {
          self::$errors['start_time'] = 'Start time is required.';
          self::$passed = false;
        }
        if (isset($_POST['end_time']) && strlen($_POST['end_time']) == 0) {
          self::$errors['end_time'] = 'End time is required.';
          self::$passed = false;
        }
        if ((isset($_POST['start_time']) && strlen($_POST['start_time']) != 0) && (isset($_POST['end_time']) && strlen($_POST['end_time']) != 0)) {
          $s = strtotime($_POST['start_time']);
          $e = strtotime($_POST['end_time']);

          if ($_POST['end_time'] != '00:00' && $s > $e) {
            self::$errors['start_time'] = 'Start time must be before end time.';
            self::$errors['end_time'] = 'End time is before start time.';
            self::$passed = false;
          } elseif ($s == $e) {
            self::$errors['start_time'] = 'Start time cannot be the same as end time.';
            self::$errors['end_time'] = 'End time cannot be the same as start time.';
            self::$passed = false;
          }
        }

        if (!isset($_POST['beginner'])) {
          self::$errors['beginner'] = 'Beginner status is required.';
          self::$passed = false;
        }
        if (!isset($_POST['consecutive'])) {
          self::$errors['consecutive'] = 'Consecutive status is required.';
          self::$passed = false;
        }
        if (!isset($_POST['private'])) {
          self::$errors['private'] = 'Private status is required.';
          self::$passed = false;
        }

        break;
      case 'note':
        if (empty($_POST['type'] )) {
            self::$errors['type'] = 'The type is required.';
            self::$passed = false;
        } elseif (in_array( $_POST['type'], array('1','2', '3', '4') ) != true) {
            self::$errors['type'] = 'Please provide a valid type.';
            self::$passed = false;
        }

        if (isset($_POST['start_date']) && strlen($_POST['start_date']) == 0) {
          self::$errors['start_date'] = 'Start time is required.';
          self::$passed = false;
        }
        if (isset($_POST['end_date']) && strlen($_POST['end_date']) == 0) {
          self::$errors['end_date'] = 'End time is required.';
          self::$passed = false;
        }
        if ((isset($_POST['start_date']) && strlen($_POST['start_date']) != 0) && (isset($_POST['end_date']) && strlen($_POST['end_date']) != 0)) {
          $s = strtotime($_POST['start_date']);
          $e = strtotime($_POST['end_date']);

          if ($_POST['end_date'] != '00:00' && $s > $e) {
            self::$errors['start_date'] = 'Start time must be before end time.';
            self::$errors['end_date'] = 'End time is before start time.';
            self::$passed = false;
          } elseif ($s == $e) {
            self::$errors['start_date'] = 'Start time cannot be the same as end time.';
            self::$errors['end_date'] = 'End time cannot be the same as start time.';
            self::$passed = false;
          }
        }

        if (!isset($_POST['comments'])) {
          self::$errors['comments'] = 'Comment is required.';
          self::$passed = false;
        }
        break;
      case 'membership':
        if (empty($_POST['type'] )) {
            self::$errors['type'] = 'The type is required.';
            self::$passed = false;
        }

        if (isset($_POST['start_date']) && strlen($_POST['start_date']) == 0) {
            self::$errors['start_date'] = 'Start time is required.';
            self::$passed = false;
        }
        if (isset($_POST['end_date']) && strlen($_POST['end_date']) == 0) {
            self::$errors['end_date'] = 'End time is required.';
            self::$passed = false;
        }
        if ((isset($_POST['start_date']) && strlen($_POST['start_date']) != 0) && (isset($_POST['end_date']) && strlen($_POST['end_date']) != 0)) {
          $s = strtotime($_POST['start_date']);
          $e = strtotime($_POST['end_date']);

          if ($_POST['end_date'] != '00:00' && $s > $e) {
            self::$errors['start_date'] = 'Start time must be before end time.';
            self::$errors['end_date'] = 'End time is before start time.';
            self::$passed = false;
          } elseif ($s == $e) {
            self::$errors['start_date'] = 'Start time cannot be the same as end time.';
            self::$errors['end_date'] = 'End time cannot be the same as start time.';
            self::$passed = false;
          }
        }

        break;
      case 'event':
        if (empty($_POST['display_name'])) {
          self::$errors['display_name'] = 'Please enter a display name.';
          self::$passed = false;
        } elseif (strlen($_POST['display_name']) > 75) {
          self::$errors['display_name'] = 'Display name must be under 75 characters.';
          self::$passed = false;
        }

        if (isset($_POST['date']) && strlen($_POST['date']) == 0) {
          self::$errors['date'] = 'Event date is required.';
          self::$passed = false;
        }
        if (isset($_POST['start_time']) && strlen($_POST['start_time']) == 0) {
          self::$errors['start_time'] = 'Start time is required.';
          self::$passed = false;
        }
        if (isset($_POST['end_time']) && strlen($_POST['end_time']) == 0) {
            self::$errors['end_time'] = 'End time is required.';
            self::$passed = false;
        }
        
        if ((isset($_POST['start_time']) && strlen($_POST['start_time']) != 0) && (isset($_POST['end_time']) && strlen($_POST['end_time']) != 0)) {
          $s = strtotime($_POST['start_time']);
          $e = strtotime($_POST['end_time']);

          if ($_POST['end_time'] != '00:00' && $s > $e) {
            self::$errors['start_time'] = 'Start time must be before end time.';
            self::$errors['end_time'] = 'End time is before start time.';
            self::$passed = false;
          } elseif ($s == $e) {
            self::$errors['start_time'] = 'Start time cannot be the same as end time.';
            self::$errors['end_time'] = 'End time cannot be the same as start time.';
            self::$passed = false;
          }
        }

        if (isset($_POST['price']) && strlen($_POST['price']) == 0) {
          self::$errors['price'] = 'Event price is required.';
          self::$passed = false;
        }

        if (isset($_POST['description']) && strlen($_POST['description']) == 0) {
          self::$errors['description'] = 'Event description is required.';
          self::$passed = false;
        }

        if (!isset($_POST['beginner'])) {
          self::$errors['beginner'] = 'Beginner status is required.';
          self::$passed = false;
        }

        if (!isset($_POST['online'])) {
          self::$errors['online'] = 'Online status is required.';
          self::$passed = false;
        }

        if (!isset($_POST['pass'])) {
          self::$errors['pass'] = 'Pass status is required.';
          self::$passed = false;
        }

        if (!isset($_POST['discipline'])) {
          self::$errors['discipline'] = "Please pick the event discipline(s).";
          self::$passed = false;
        }

        if (isset($_POST['image_portrait']) && strlen($_POST['image_portrait']) == 0) {
          self::$errors['image_portrait'] = 'Event overview image is required.';
          self::$passed = false;
        }

        if (isset($_POST['image_landscape']) && strlen($_POST['image_landscape']) == 0) {
          self::$errors['image_landscape'] = 'Event booking image is required.';
          self::$passed = false;
        }
        break;
    }
  }
}