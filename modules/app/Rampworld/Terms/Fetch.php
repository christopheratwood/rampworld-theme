<?php
namespace Rampworld\Terms;

use Rampworld\DB\Connection as Connection;

class Fetch {
	private $_db;
		
	public function __construct() {

		$this->_db  = Connection::connect();
	}
	public function get($version = false){

		if(!$version){
			$tandc =  $this->_db->get_results($this->_db->prepare('SELECT version_number, content FROM rwc_terms_and_conditions WHERE active = %d AND publish_date <= NOW() AND (deactivation_date >= now() OR deactivation_date IS NULL)LIMIT 1', array(1)), ARRAY_A);

			if(count($tandc) < 1) {
				$tandc =  $this->_db->get_results($this->_db->prepare('SELECT version_number, content FROM rwc_terms_and_conditions WHERE tandc_id = %d LIMIT 1', array(1)), ARRAY_A);
			}
		} else {
			$tandc =  $this->_db->get_results($this->_db->prepare('SELECT version_number, content FROM rwc_terms_and_conditions WHERE version_number = %s LIMIT 1', array(1)), ARRAY_A);

			if(count($tandc) < 1) {
				$tandc =  $this->_db->get_results($this->_db->prepare('SELECT version_number, content FROM rwc_terms_and_conditions WHERE tandc_id = %d LIMIT 1', array(1)), ARRAY_A);
			}
		}
		return $tandc[0];
	}
	
}