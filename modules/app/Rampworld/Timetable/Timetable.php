<?php
namespace Rampworld\Timetable;

use Rampworld\DB\Connection as Connection;

class Timetable {
  private $_db;
  private $holiday_sessions = array();
  private $normal_sessions = array();
  
  public $timetable ='<div class="row">';
  private $ordrer = array();
  public $holiday = array();
  public $holiday_priority = true;
  public $has_holiday = false;
  private $_days = array(1=>'Monday', 2=>'Tuesdays', 3=>'Wednesday', 4=>'Thursday', 5=>'Friday', 6=>'Saturday',7=> 'Sunday');
  
		
	public function __construct() {

    $this->_db  = Connection::connect();
    
    $this->_setHolidays();
    
    if( $this->holiday_priority ) {
      $this->_formatTimetable( $this->holiday_sessions, true);
      $this->_formatTimetable( $this->normal_sessions);
      
    } else {
      $this->_formatTimetable( $this->normal_sessions);
      $this->_formatTimetable( $this->holiday_sessions, true);

    }
    $this->timetable .= '</div>';
  }
  public function render() {
    return $this->timetable;
  }
	private function _formatTimetable($arr, $isholiday = false){

    for($i = 0; $i < count($arr); $i++) {
      $this->timetable .= '
        <div class="col-md-6 col-sm-12">
          <div class="printcontainer" id="printTimes">
            <div class="print-title big" style="position: relative">
              '.(( $isholiday ) ?
              '<p>'. $this->holiday[0]['display_name'].'</p>
              <p>From '.$this->holiday[0]['start_date'].' to '.$this->holiday[0]['end_date'].'</p>'
              : '<p style="line-height: 80px">Cardiff School Term Time Hours</p>').'
            </div>
            <div class="content">
              <table class="clean session_times" id="pricing" >
                <thead>
                  <tr>
                    <th>Day</th>
                    <th>Session Time</th>
                    <th>Information</th>
                  </tr>
                </thead>
                <tbody>';
                  foreach(array_reverse($arr[$i]) as $session) {
                    $this->timetable .= $session;
                  }
                $this->timetable .= '</tbody>
              </table>
            </div>
            <div class="footer print-title">
              <p>www.rampworldcardiff.co.uk</p>
            </div>
            <p class="medium">Other sports are welcome during the priority sessions.</p>
          </div>
        </div>';
    }
	}
  private function _setHolidays() {
    $this->holiday = $this->_db->get_results('SELECT holiday_id, date_format(start_date, "%d/%m/%Y") as start_date, date_format(end_date, "%d/%m/%Y") as end_date, display_name FROM rwc_holidays WHERE end_date >= CURDATE() AND visible = 1 ORDER BY start_date LIMIT 1', ARRAY_A);

    if(count($this->holiday) > 0 ) {
      $this->has_holiday = true;
      $start_date =  \DateTime::createFromFormat('d/m/Y', $this->holiday[0]['start_date'] );
      $current_date = new \DateTime();
      if( $start_date > $current_date) {
        $this->holiday_priority = false;
        $this->_setSessions();
        $this->_setSessions(true, $this->holiday[0]['holiday_id']);
        
      } else {
        $this->holiday_priority = true;
        $this->_setSessions(true, $this->holiday[0]['holiday_id']);
        $this->_setSessions();
      }
      
    } else {
      $this->_setSessions();      
    }
  }
  private function _setSessions($isHoliday = false, $holidayId = null) {
    $formattedTimes = array();
    $sessionTimes = array(0=> '' ,1=> '',2=> '',3=> '',4=> '',5=> '',6=> '');
    if( !$isHoliday ) {
      //get sessions
      $sessions =  $this->_db->get_results("SELECT * FROM `rwc_sessions` ORDER BY day ASC, start_time ASC", ARRAY_A);
    } else {
      $sessions =  $this->_db->get_results($this->_db->prepare("SELECT * FROM `rwc_holidays_sessions` WHERE holiday_id = %d ORDER BY day ASC, start_time ASC", array($holidayId)), ARRAY_A);
    }

    //loop through settings
    $i = 0;
     
    $counts = array();
    foreach ($sessions as $key=>$subarr) {
      if($subarr['day'] == 0) {
        if (isset($counts['Sunday'])) {
            $counts['Sunday']++;
          } else {
            $counts['Sunday'] = 1;
          }
      } else {
        // Add to the current group count if it exists
        if (isset($counts[$this->_days[$subarr['day']]])) {
          $counts[$this->_days[$subarr['day']]]++;
        } else {
          $counts[$this->_days[$subarr['day']]] = 1;
        }
      }
      // Or the ternary one-liner version
      // instead of the preceding if/else block
      //$counts[$this->_days[$subarr['day']]] = isset($counts[$subarr['day']]) ? $counts[$subarr['day']]++ : 1;
    }
  
    foreach ($sessions as $session) {
  
      $sessionTimes[$session['day']][] = array('start_time' => $session['start_time'],'end_time' => $session['end_time'], 'display_name'=>$session['display_name']);
    }
    
    $tmp = $sessionTimes[0];
    unset($sessionTimes[0]);
    $sessionTimes[7] = $tmp;

    $indexs_to_skip = array();
    for($i = count($sessionTimes) ; $i > 0; $i--) {

      if(!in_array($i, $indexs_to_skip)) {
        $indexs_to_skip[] =$i;
        if(isset($sessionTimes[$i -1]) && ($sessionTimes[$i] === $sessionTimes[$i - 1])) {
  
          if(isset($sessionTimes[$i - 2])  && ($sessionTimes[$i] === $sessionTimes[$i - 2])) {
            $indexs_to_skip[] = $i-1;
            $indexs_to_skip[] = $i-2;
            
            $index = 0;
            if(empty($formattedTimes))
              $key_index = 0;
            else
              $key_index = key(array_slice( $formattedTimes, -1, 1, TRUE )) + 1;
            
            if(is_array($sessionTimes[$i])) {
              foreach ($sessionTimes[$i] as $key => $value) {
                if($index == $counts[$this->_days[$i]] -1) {
                  if($index == 0) {
                    $formattedTimes[$key_index] = '<tr class="printmode lastrecord"><td>
                        '.$this->_days[$i-2].', '.$this->_days[$i-1].', '.$this->_days[$i].'<br><small>Session Times.</small>
                      </td>
                      <td data-label="Display Name">
                        <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                      </td>
                      <td>
                        <p class="medium">'.$value['display_name'].'</p>
                      </td>
                      </tr>';
                  } else {
                    $formattedTimes[$key_index] .= '<tr class="printmode lastrecord"><td></td>
                      <td data-label="Display Name">
                        <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                      </td>
                      <td>
                        <p class="medium">'.$value['display_name'].'</p>
                      </td>
                      </tr>';
                  }
                } else {
                  if($index == 0) {
                    $formattedTimes[$key_index] = '<tr><td>
                        '.$this->_days[$i-2].', '.$this->_days[$i-1].', '.$this->_days[$i].'<br><small>Session Times.</small>
                      </td>
                      <td data-label="Display Name">
                        <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                      </td>
                      <td>
                        <p class="medium">'.$value['display_name'].'</p>
                      </td>
                      </tr>';
                  } else {
                    $formattedTimes[$key_index] .= '<tr><td></td>
                      <td data-label="Display Name">
                        <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
                      </td>
                      <td>
                        <p class="medium">'.$value['display_name'].'</p>
                      </td>
                      </tr>';
                  }
                }
                $index++;
              }
            }
          } else {
            //TWO MATCHES
            $indexs_to_skip[] = $i-1;
            $index = 0;
            if(empty($formattedTimes))
              $key_index = 0;
            else
              $key_index = key(array_slice( $formattedTimes, -1, 1, TRUE )) + 1;
            
            if(is_array($sessionTimes[$i])) {
              foreach ($sessionTimes[$i] as $key => $value) {
  
              if($index == $counts[$this->_days[$i]] -1) {
                if($index == 0) {
                  $formattedTimes[$key_index] = '<tr class="printmode lastrecord"><td>
                      '.$this->_days[$i-1].', '.$this->_days[$i].'<br><small>Session Times.</small>
                    </td>
                    <td data-label="Display Name">
                      <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                    </td>
                    <td>
                      <p class="medium">'.$value['display_name'].'</p>
                    </td>
                    </tr>';
                } else {
                  $formattedTimes[$key_index] .= '<tr class="printmode lastrecord"><td></td>
                    <td data-label="Display Name">
                      <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                    </td>
                    <td>
                      <p class="medium">'.$value['display_name'].'</p>
                    </td>
                    </tr>';
                }
              } else {
                if($index == 0) {
                  $formattedTimes[$key_index] = '<tr><td>
                      '.$this->_days[$i-1].', '.$this->_days[$i].'<br><small>Session Times.</small>
                    </td>
                    <td data-label="Display Name">
                      <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                    </td>
                    <td>
                      <p class="medium">'.$value['display_name'].'</p>
                    </td>
                    </tr>';
                } else {
                  $formattedTimes[$key_index] .= '<tr><td></td>
                    <td data-label="Display Name">
                      <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                    </td>
                    <td>
                      <p class="medium">'.$value['display_name'].'</p>
                    </td>
                    </tr>';
                }
              }
              $index++;
              }
            }
            
          }
        } else {
          $index = 0;
          if(empty($formattedTimes))
            $key_index = 0;
          else
            $key_index = key(array_slice( $formattedTimes, -1, 1, TRUE )) + 1;
          
          if(is_array($sessionTimes[$i])) {
            foreach ($sessionTimes[$i] as $key => $value) {
  
              if($index == $counts[$this->_days[$i]] -1) {
                if($index == 0) {
                  $formattedTimes[$key_index] = '<tr class="printmode lastrecord"><td>
                      '.$this->_days[$i].'<br><small>Session Times.</small>
                    </td>
                    <td data-label="Display Name">
                      <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                    </td>
                    <td>
                      <p class="medium">'.$value['display_name'].'</p>
                    </td>
                    </tr>';
                } else {
                  $formattedTimes[$key_index] .= '<tr class="printmode lastrecord"><td></td>
                    <td data-label="Display Name">
                      <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                    </td>
                    <td>
                      <p class="medium">'.$value['display_name'].'</p>
                    </td>
                    </tr>';
                }
              } else {
                if($index == 0) {
                  $formattedTimes[$key_index] = '<tr><td>
                      '.$this->_days[$i].'<br><small>Session Times.</small>
                    </td>
                    <td data-label="Display Name">
                      <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                    </td>
                    <td>
                      <p class="medium">'.$value['display_name'].'</p>
                    </td>
                    </tr>';
                } else {
                  $formattedTimes[$key_index] .= '<tr><td></td>
                    <td data-label="Display Name">
                      <p class="medium">'.$value['start_time'].' - '.$value['end_time'].'</p>
  
                    </td>
                    <td>
                      <p class="medium">'.$value['display_name'].'</p>
                    </td>
                    </tr>';
                }
              }
              $index++;
            }
          }
        }
      }
    }

    if( !$isHoliday ) {
      $this->normal_sessions[] = $formattedTimes;
    } else {
      $this->holiday_sessions[] = $formattedTimes;
    }
  }
}