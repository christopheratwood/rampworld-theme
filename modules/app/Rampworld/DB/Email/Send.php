<?php

namespace Rampworld\Email;

require_once __dir__ . '/../../../vendor/autoload.php';
use Mailgun\Mailgun;

class Send
{
    private $_data,
            $_mg;
    
    
    function __construct($data){
        $this->set_data( $data );
        $this->_mg = Mailgun::create(MailgunKey);
    }
    
    public function create_welcome($memberID) {
       
        require_once __dir__.'/../../../../assets/templates/email/member/new.template.php';

        if($this->_data['consent_email']  == '') {
            $this->_mg->messages()->send('www.rampworldcardiff.co.uk', [
                'from'          => 'RampWorld Cardiff - Members <members@rampworldcardiff.co.uk>',
                'to'            => $this->_data['email'],
                'subject'       => 'Welcome! Your membership number is '.$memberID,
                'html'          => $email
            ]);
        } else {
            $this->_mg->messages()->send('www.rampworldcardiff.co.uk', [
                'from'          => 'RampWorld Cardiff - Members <members@rampworldcardiff.co.uk>',
                'to'            => $this->_data['email'],
                'subject'       => 'Welcome! Your membership number is '.$memberID,
                'html'          => $email,
                'cc'            => $this->_data['consent_email']
            ]);
        }
          return true;
    }   
    public function create_member_update($memberID) {
       
        require_once __dir__.'/../../../../assets/templates/email/member/update.template.php';

        $this->_mg->messages()->send('www.rampworldcardiff.co.uk', [
            'from'          => 'RampWorld Cardiff - Members <members@rampworldcardiff.co.uk>',
            'to'            => $this->_data['email'],
            'subject'       => 'Your membership number is '.$memberID, 
            'html'          => $email,
            'cc'            => $this->_data['consent_email']
          ]);
          return true;
    }   
    public function create_member_reissue($memberID) {
       
        require_once __dir__.'/../../../../assets/templates/email/member/reissue.template.php';

        $this->_mg->messages()->send('www.rampworldcardiff.co.uk', [
            'from'          => 'RampWorld Cardiff - Members <members@rampworldcardiff.co.uk>',
            'to'            => $this->_data['email'],
            'subject'       => 'Your membership number is '.$memberID,
            'html'          => $email,
            'cc'            => $this->_data['consent_email']
          ]);
          return true;
    }   
    public function create_booking() {

        require_once __dir__.'/../../../../assets/templates/email/booking/new.template.php';
        
        $this->_mg->messages()->send('www.rampworldcardiff.co.uk', [
            'from'          => 'RampWorld Cardiff - Bookings <bookings@rampworldcardiff.co.uk>',
            'to'            => $this->_data['booking']['email'],
            'subject'       => 'Your booking number is '.$this->_data['booking_id'],
            'html'          => $email
            ]);
        return true;
    }
    public function create_christmas_message($forename, $surname, $email_address, $rank, $visits, $hours) {
        
         require_once __dir__.'/../../../../assets/templates/email/special/christmas.template.php';
 
         $this->_mg->messages()->send('www.rampworldcardiff.co.uk', [
             'from'          => 'RampWorld Cardiff <members@rampworldcardiff.co.uk>',
             'to'            => $email_address,
             'subject'       => 'A special thank from RampWorld Cardiff',
             'html'          => $email
           ]);
           return true;
     } 
    public function set_data($data)
    {
        $this->_data = $data;
    }
}
