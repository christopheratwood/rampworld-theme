<?php namespace Rampworld\SEO;

Class Meta {
	public static $meta = '';
	public static $title = '';

	public static function add($type, $content, $property = true) {
		if($property){
			self::$meta .= '<meta property="'.$type.'" content="'.strip_tags($content).'">';
		} else {
			self::$meta .= '<meta name="'.$type.'" content="'.strip_tags($content).'">';
		}
	}
	public static function title($title) {
		self::add('twitter:title', $title, true);
		self::$title = strip_tags($title);
	}
	public static function description($content) {
		self::add('description', $content, false);
		self::add('og:description', $content);
		self::add('twitter:description', $content, true);
	}
}
