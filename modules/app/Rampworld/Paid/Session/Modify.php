<?php namespace Rampworld\Paid\Session;

use Rampworld\DB\Connection as Connection;

class Modify {
	private $_db;
	function __construct( ) {

		$this->_db  = Connection::connect();
  }
  public function create($member_id, $transaction_id, $session_date, $start_time, $end_time, $cost) {
    
    if($this->_db->insert(
      'rwc_prepaid_sessions',
      array(
        'member_id'           => $member_id,
        'transaction_id'      => $transaction_id,
        'session_date'        => $session_date,
        'session_start_time'  => $start_time,
        'session_end_time'    => $end_time,
        'cost'                => $cost
      ),
      array(
        '%d', '%d', '%s', '%s', '%s', '%d'
      )
    ) !== false) {
      return true;
    } else {
      return false;
    }

  }
}
