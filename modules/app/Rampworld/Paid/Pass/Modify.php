<?php namespace Rampworld\Paid\Pass;

use Rampworld\DB\Connection as Connection;

class Modify {
	private $_db;
	function __construct( ) {

		$this->_db  = Connection::connect();
  }
  public function create($member_id, $membership_type,$purchased_date, $start_date, $end_date, $staff_name, $transaction_id) {
    if($this->_db->insert(
      'rwc_paid_membership',
      array(
        'member_id'       => $member_id,
        'membership_type' => $membership_type,
        'date'            => $purchased_date,
        'start_date'      => $start_date,
        'end_date'        => $end_date,
        'staff_init'      => $staff_name,
        'transaction_id'  => $transaction_id
      ),
      array(
        '%d', '%d', '%s', '%s', '%s', '%s', '%d'
      )
    ) !== false) {
      return true;
    } else {
      return false;
    }

  }
}
