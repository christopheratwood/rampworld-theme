<?php namespace Rampworld\URL;


class URLStore {
	public static function getRewrites() {

    // add tags with `_` prefix to avoid screwing up query
    add_rewrite_tag( '%_parent%', '([a-zA-Z\d\-_+]+)' );
    add_rewrite_tag( '%_subcategory%', '([a-zA-Z\d\-_+]+)' );
    add_rewrite_tag( '%_category%', '([a-zA-Z\d\-_+]+)' );

    // create URL rewrite
    add_rewrite_rule( '^store/([a-zA-Z\d\-_+]+)/([a-zA-Z\d\-_+]+)/([a-zA-Z\d\-_+]+)?', 'page-category.php?_parent=$matches[1]&_subcategory=$matches[2]&_category=$matches[3]', 'top' );

    // required once after rules added/changed
    self::flush();
    self::debug();

  }

  private static function flush() {
    flush_rewrite_rules( true );
  }
  private static function debug(){
    global $wp_query;
    
      echo "<pre>";
      print_r($wp_query->query_vars);
      echo "</pre>";
  }
}