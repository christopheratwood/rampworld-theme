<?php
namespace Rampworld\Form;

use Rampworld\Terms\Fetch as TermsAndConditions;
use Rampworld\Event\EventFetch as EventFetch;

class Booking {
	private $_form;
	public function __construct() {
	
	}
    protected function _getHeader($title, $info = null) {
        return '<section ><div class="content" ><h2>'.ucwords($title).'</h2>';
    }
	public function getSection($stage, $formErrors, $completeDetails = null) {

        switch($stage) {

            case 1:
                if(isset($_GET['date'])) {
                    $date = new \DateTime(date('d-m-Y', strtotime($_GET['date'])));
                } else {
                    $date = (isset($_SESSION['session']['session_date']) && strlen($_SESSION['session']['session_date']) > 0)? new \DateTime(date('d-m-Y', strtotime($_SESSION['session']['session_date']))) : null;
                }
                $content =  $this->_getHeader('Session Dates and times',''). '
                    <div class="col-xs-12">
                        <div id="calendar" '.(($date != null)? 'data-default-date="'.$date->format('Y-m-d').'"': '').'></div>
    
                        
                    </div>
                </div>
                </section>';
                if(isset($_GET['failure']) && !isset($_SESSION['error_explained']) && $_SESSION['error_explained'] == false){
                    $content .= '<div id="errorDialog" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Transaction has already been processed.</h4>
                            </div>
                            <div class="modal-body">';
                            if($_GET['failure'] == 'already_purchased') {
                                $content .= '<p>The transaction has already been processed. You will receive an email confirming all ypour booking details.  All members will have their session associate with their account.  If you do not receive an email, please contact us at <a href="mailto:rampworldcardiff@gmail.com">rampworldcardiff@gmail.com</a></p>';
                            } else {
                                $content .= '<p>Unforuniatly, the transaction was not successful. Please try again. Sorry for any incovenience. If this conintue to occur, please contact us at <a href="mailto:rampworldcardiff@gmail.com">rampworldcardiff@gmail.com</a></p>';
                            }
                    $content .= '</div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </div>

                        </div>
                        </div><script>$(function () { $("#errorDialog").modal("toggle") });</script>';
                    $_SESSION['error_explained'] = true;
                }
                return $content;
                break;
            case 2:
                $content =$this->_getHeader('Booking Information',''). '
                    <div class="col-xs-12">
                        <div class="col-md-8 col-sm-6" id="form_content">
                            <div class="row">
                                <div class="col-md-12 ">
                                    <p class="alert alert-info">If the participant you are booking for is not currently a member or you do not know their membership number(s) then follow guidance here. <a href="//www.rampworldcardiff.co.uk/guidance/online-booking/" target="_blank" class="btn btn-default">View Guidance here.</a></p>
                                    <div class="col-xs-12 ">'.
                                        $this->getHtml('text', 12, 'Booking Name',1,'form_booking_name', 'form_booking_name', 'booking_name_error',1,'capitalize' , 'booking', 'name').
                                        $this->getHtml('number', 12, 'Booking Phone Number',1,'form_booking_number', 'form_booking_number', 'booking_number_error',1, null, 'booking', 'number' ).
                                        $this->getHtml('email', 12, 'Booking Email',1,'form_booking_email', 'form_booking_email', 'booking_email_error',1, null, 'booking', 'email' ). '
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6" id="side-bar">
                            '.$this->sideBar().'
                            
                            <div class="alert alert-warning alert-dismissable "><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All fields marked with a <span class="required"></span> are required.</div>

                          <div class="alert alert-danger reg '. ((!empty($formErrors) & isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                                if(isset($formErrors)){
                                
                                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').'</span>)</b>';
                                    foreach ($formErrors as $key =>$err) {
                                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                                    }	
                                }
                            $content .= '</div>

                        </div>
                           
                            
                           
                            <span class="alert alert-danger reg '. ((!empty($formErrors) & isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                                if(isset($formErrors)){
                                
                                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').')</b>';
                                    foreach ($formErrors as $key =>$err) {
                                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                                    }	
                                }
                            $content .= '</span>

                        </div>
                    </div>
                    <div class="col-xs-12 text-right ">
                        <input type="hidden" name="stage" value="2">
                        <a href="//www.rampworldcardiff.co.uk/online-booking/book/?stage=1" class="btn btn-default">Back</a>
                        <a data-current="2" data-next="3" class="btn btn-primary" id="form-next" >Next</a>
                    </div>
                </div>
                </section>';
                return $content;
                break;
            case 3:

                $content =$this->_getHeader('Participants',''). '

                        <div class="col-md-8 col-sm-6" id="form_content">
                          <div class="row">
                            <div class="col-md-12 ">
                                <p class="lead">Members</p>
                                <div class="alert alert-info"><p>If you have not registered with us, you can easily do by following the link below. If you cannot remember your membership number please follow our guidance in what to do next. </p><a class="btn btn-primary" href="//www.rampworldcardiff.co.uk/membership/">Register here</a><a class="btn btn-primary" href="//www.rampworldcardiff.co.uk/guidance/online-booking/">Online booking guidance</a>
                                </div>
                            </div>
                            <div class="form-group col-md-12 col-sm-12 col-xs-12 col-ms-12" id="participants">
                                    ';
                                    if(isset($_SESSION['participants']['members'])){
                                       foreach($_SESSION['participants']['members'] as $member){
                                            $content .= '<div class="col-md-6 col-xs-12"><p class="lead float-left">'.ucwords($member['name']).'</p><a id="removeParticipant" data-member-id="'.$member['member_id'].'" class="remove-btn"><span>remove</span><div class="icons"><i class="fa fa-times-circle"></i><i class="fa fa-check"></i></div></a></div>';
                                        }
                                    }
                                $content .= '</div>
                                    
                            <div class="col-xs-12">
                                <p class="lead">Add participants</p>
                                <p class="alert alert-info">If you are already a member, please enter your membership number(s) below and we will do the rest!</p>
                            </div>
                            <div class="form-group col-xs-12 col-ms-12">
                                <label class="col-md-4 col-sm-12 control-label reg required" for="form_participant_member_number">Membership Number </label>
                                <div class="col-md-8 col-sm-12">
                                    <input type="number" class="rwc_new col-sm-12 find-member-input" name="form_participant_member_number" id="form_participant_member_number" tabindex="1" >
                                    <span class="glyphicon glyphicon-remove form-control-feedback reg find-member" id="form_participant_member_number_error_icon"></span>
                                    <span class="glyphicon glyphicon-ok form-control-feedback reg find-member" id="form_participant_member_number_success_icon"></span>
                                    <a id="findParticipant" class="btn btn-primary find-member-btn">Find Member</a>
                                    <div class="row">
                                        <span class="error col-xs-12" id="participant_member_number_error"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="results col-xs-12" id="members"></div>
                        </div>
    
                            </div>
                        </div>
                        
                        <div class="col-md-4 col-sm-6" id="side-bar">
                            '.$this->sideBar().'
                            
                            <div class="alert alert-warning alert-dismissable "><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All fields marked with a <span class="required"></span> are required.</div>

                            <div class="alert alert-info" id="form_help">Please add each member for the </div><div class="alert alert-danger reg '. ((!empty($formErrors) & isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                                if(isset($formErrors)){
                                
                                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').'</span>)</b>';
                                    foreach ($formErrors as $key =>$err) {
                                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                                    }	
                                }
                            $content .= '</div>
                        </div>
å
                    <div class="col-xs-12 text-right ">
                        <input type="hidden" name="stage" value="3">
                        <a data-current="3" data-next="2" class="btn btn-default" id="form-prev">Back</a>
                        <a data-current="3" data-next="4" class="btn btn-primary" id="form-next" >Next</a>
                    </div>
                </div>
                </section>';
                return $content;
                break;
            case 4:
                
                $terms = new TermsAndConditions();
                $tandcContent = $terms->get();
                $content = '';
                if(isset($_GET['failed'])) {
                    $content .= '<div class="alert alert-danger" style="margin-top: 20px;"><strong>Opps,</strong> there was an error. You was not charged for this transaction. Please try again. If you still encountering issues, please contact us via email. <a class="btn btn-default" href="mailto:rampworldcardiff@gmail.com">Email us</a></div>';
                }
                $content .=$this->_getHeader('Please check your details',''). '
                    <div class="row">
                        <div class="col-md-8 col-sm-6" id="form_content">
                            <p class="lead">Session Information</p>';
                            
                if($_SESSION['session']['session_type'] != 'weekpass') {
                     $content .= '<div class="hidden-ms"><div class="table-responsive"><table class="table ">
                                        <thead><tr><th>Name</th><th>Start Date</th><th>End Date</th></tr></thead>
                                        <tbody><tr><td>'.$_SESSION['session']['session_name'].'</td><td>'.$_SESSION['session']['session_date'].'</td><td>'.$_SESSION['session']['session_start_time'].' - '.$_SESSION['session']['session_end_time'].'</td></tr></tbody>
                                    </table></div></div>
                                <div class="visible-ms">
                                    <table class="table ">
                                <tbody><tr><th>Name</th><td>'.$_SESSION['session']['session_name'].'</td></tr><tr><th>Start Date</th><td>'.$_SESSION['session']['session_date'].'</td></tr><tr><th>End Date</th><td>'.$_SESSION['session']['session_start_time'].' - '.$_SESSION['session']['session_end_time'].'</td></tr></tbody></table>';
                     
                $content .= '</div>';
                } else {
                    
                    $end = \DateTime::createFromFormat('Y-m-d', date('Y-m-d',strtotime($_SESSION['session']['session_end_time'])))->format('l jS F Y');
                    $content .= '<div class="hidden-ms"><div class="table-responsive"><table class="table ">
                                        <thead><tr><th>Name</th><th>Start Date</th><th>End Date</th></tr></thead>
                                        <tbody><tr><td>'.$_SESSION['session']['session_name'].'</td><td>'.\DateTime::createFromFormat('Y-m-d', date('Y-m-d',strtotime($_SESSION['session']['session_date'])))->format('l jS F Y').'</td><td>'. $end .'</td></tr></tbody>
                                    </table>
                                </div></div>
                                <div class="visible-ms">
                                    <table class="table ">
                                    <tbody><tr><th>Name</th><td>'.$_SESSION['session']['session_name'].'</td></tr><tr><th>Start Date</th><td>'.\DateTime::createFromFormat('Y-m-d', date('Y-m-d',strtotime($_SESSION['session']['session_date'])))->format('l jS F Y').'</td></tr><tr><th>End Date</th><td>'. $end .'</td></tr></tbody></table></div>
                                    <div class="alert alert-danger">
                                        The week only runs until the last day of the week. This pass will end on the <strong>' . $end . '</strong>. This pass <strong>does not</strong> entitle non-beginners to attend beginner sessions
                                    </div>';
                }
                if( isset( $_SESSION['session'] ) && isset( $_SESSION['session']['session_pass_exclusion'] ) && $_SESSION['session']['session_pass_exclusion'] == "1" && isset( $_SESSION['session']['session_disciplines']) ) {
                    $content .= '<div class="alert alert-danger"><p>This event is only for <strong>'.$_SESSION['session']['session_disciplines'].'</strong>. No other disciplines will be allowed to attend.</p></div>';
                }
                $content .= '<div class="alert alert-warning">
                                        <strong>Incorrect?</strong> You can change session information here. <a href="//www.rampworldcardiff.co.uk/online-booking/book?stage=1" class="btn btn-default">Change</a>
                                    </div>
                                    <p class="lead">Booking Information</p>
                                    <div class="hidden-ms">
                                        <div class="table-responsive">
                                            <table class="table ">
                                                <thead><tr><th>Booking Name</th><th>Booking Number</th><tr></tr></thead>
                                                <tbody><tr><td>'.$_SESSION['booking']['name'].'</td><td>'.$_SESSION['booking']['number'].'</td></tr><tr><th colspan="2">Booking Email</th></tr><tr><td>'.$_SESSION['booking']['email'].'</td></tr></tbody>
                                            </table>
                                        
                                        </div>
                                    </div>
                                    <div class="visible-ms">
                                        <table class="table ">
                                            <tbody><tr><th>Booking Name</th><td>'.$_SESSION['booking']['name'].'</td></tr><tr><th>Booking Number</th><td>'.$_SESSION['booking']['number'].'</td></tr><tr><th>Booking Email</th><td>'.$_SESSION['booking']['email'].'</td></tr></tbody>
                                        </table>
                                    </div> 
                                    <div class="alert alert-warning">
                                        <strong>Incorrect?</strong> You can change booking details here. <a href="//www.rampworldcardiff.co.uk/online-booking/book?stage=2" class="btn btn-default">Change</a>
                                    </div>
                                    <p class="lead">Participants</p>
                                    <div class="hidden-ms">
                                        <div class="table-responsive">
                                            <table class="table ">
                                                <thead><tr><th>Member Number</th><th>Member Name</th><tr></tr></thead>
                                                <tbody>';
                                                    foreach($_SESSION['participants']['members'] as $member){
                                                        $content .= '<tr><td>'.$member['member_id'].'</td><td>'.ucwords($member['name']).'</td></tr>';
                                                    }
                                $content .= '   </tbdoy>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="visible-ms">
                                        <table class="table">
                                            <thead><tr><th>Number</th><th>Member Name</th><tr></tr></thead>
                                            <tbody>';
                                                foreach($_SESSION['participants']['members'] as $member){
                                                    $content .= '<tr><td>'.$member['member_id'].'</td><td>'.ucwords($member['name']).'</td></tr>';
                                                }
                            $content .= '   </tbody>
                                        </table>
                                    </div>
                                    <div class="alert alert-warning">
                                        <strong>Incorrect?</strong> You can add and remove participants here. <a href="//www.rampworldcardiff.co.uk/online-booking/book?stage=3" class="btn btn-default">Change</a>
                                    </div>
                                    <p class="lead">Terms and Conditions</p>
                                    <div class="terms-conditions-container">
                                <div class="terms-conditions">
                                    '.$tandcContent['content'].'
                                </div>
                                <div id="versionNumber">Version Number: '.$tandcContent['version_number'].'</div>
                            </div>
                        </div>
                        
                        <div class="col-md-4 col-sm-6" >
                           <p class="lead">Checkout</p>
                            <div class="card ">
                                <div class="card-content ">
                                    <p class="float-left">Total No. Participants: '.$_SESSION['participants']['total'].'</p><p class="text-right">Total: £'.$_SESSION['cost']['total_cost'].'</p>
                                </div>
                                <div class="card-action ">
                                    <p>Breakdown</p>
                                    <table class="rwc_table confirmation">
                                        <tr><th syle="text-right">Session Total:</th><td><span id="session_total">£ '.number_format($_SESSION['cost']['total_cost'] - $_SESSION['cost']['booking'], 2).'</span></td></tr>
                                        <tr><th>Booking Charge:</th><td><span id="booking_charge">£'.number_format($_SESSION['cost']['booking'], 2).'</span></td></tr>
                                        <tr><th>Total:</th><td class="double_top_border"><span id="total_charge">£'.number_format($_SESSION['cost']['total_cost'], 2).'</span></td></tr>
                                        </table>
                                    <p class="alert alert-default ">We use paypal to complete all transactions.  RampWorld Cardiff does not save any banking details. For more information on PayPal <a href="https://www.paypal.com/us/selfhelp/home/">visit here</a></p>
                                    <p class="alert alert-warning ">Please ensure all information is correct before continuing.</p>
                                    <hr />
                   
                              
                                    
                                </div>
                                <div class="alert alert-danger reg '. ((!empty($formErrors) & isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                                if(isset($formErrors)){
                                
                                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').'</span>)</b>';
                                    foreach ($formErrors as $key =>$err) {
                                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                                    }	
                                }
                            $content .= '</div>';
                                    if(isset($_GET['approved']) && $_GET['approved'] == true){
                                    $content .= '<a href="/pay" id="pay-btn" class="btn btn-primary btn-lg btn-block click-loading">Buy</a><a href="//www.rampworldcardiff.co.uk/online-booking/book/" class="btn btn-default">No, Go back</a>';
                                    } else {
                                    $content .= ' <div class="alert alert-default "><div class="options">
                                        <label class="control control--check required">I agree to the RampWorld Cardiff Terms and Conditions <input type="checkbox" name="tandc" value="true" id="tandc" tabindex="1" '. ((isset($_SESSION['tandc']) && $_SESSION['tandc'] == true) ? 'checked': '').'/>
                                            <div class="control__indicator"></div>
                                        </label>
                                    </div></div><a href="//www.rampworldcardiff.co.uk/online-booking/book/payment" id="checkout-btn"><img src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/blue-rect-paypalcheckout-60px.png" alt="PayPal Checkout" style="margin: 8px auto;"></a><img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/cc-badges-ppppcmcvdam.png" alt="Credit Card Badges" style="width: 80%; margin: 10px auto">';
                                    }

              
                                
                                $content .= '</div>
                            </div>
                            </div>  
                            
                        </div>
                    </div>
                </section>';
                if(isset($_GET['failure']) && !isset($_SESSION['error_explained']) && $_SESSION['error_explained'] == false){
                    $content .= '<div id="errorDialog" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Transaction could not be completed.</h4>
                            </div>
                            <div class="modal-body">';
                            if($_GET['failure'] == 'already_purchased') {
                                $content .= '<p>The transaction has already been processed. You will receive an email confirming all ypour booking details.  All members will have their session associate with their account.  If you do not receive an email, please contact us at <a href="mailto:rampworldcardiff@gmail.com">rampworldcardiff@gmail.com</a></p>';
                            } else {
                                $content .= '<p>Unforuniatly, the transaction was not successful. Please try again. Sorry for any incovenience. If this conintue to occur, please contact us at <a href="mailto:rampworldcardiff@gmail.com">rampworldcardiff@gmail.com</a></p>';
                            }
                    $content .= '</div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </div>

                        </div>
                        </div><script>$(function () { $("#errorDialog").modal("toggle") });</script>';
                    $_SESSION['error_explained'] = true;
                }
                
                return $content;
                break;
            
            case 5:
                $content =$this->_getHeader('Thank you ',''). '
                        <div class="alert alert-success">
                            <span>Success! Your purchase number is: '.$completeDetails['booking_id'].'</span>
                        </div>';
                        if(isset($_GET['issue'])){
                        $content .='<div class="alert alert-warning visible block">
                            <span>We attempted to send you an email, however, this was not successfully. We suggest you screenshot or print this page for your records.</span>
                        </div>';
                        }
                        $content .='<div class="row">
                                        <div class="col-md-8 col-sm-6" id="form_content">
                                            <p class="lead">Session Information</p>';
                        if($_SESSION['session']['session_type'] != 'weekpass'){
                            $content .= '<div class="hidden-ms"><div class="table-responsive"><table class="table ">
                                                <thead><tr><th>Name</th><th>Start Date</th><th>End Date</th></tr></thead>
                                                <tbody><tr><td>'.$_SESSION['session']['session_name'].'</td><td>'.$_SESSION['session']['session_date'].'</td><td>'.$_SESSION['session']['session_start_time'].' - '.$_SESSION['session']['session_end_time'].'</td></tr></tbody>
                                            </table></div></div>
                                        <div class="visible-ms">
                                            <table class="table ">
                                        <tbody><tr><th>Name</th><td>'.$_SESSION['session']['session_name'].'</td></tr><tr><th>Start Date</th><td>'.$_SESSION['session']['session_date'].'</td></tr><tr><th>End Date</th><td>'.$_SESSION['session']['session_start_time'].' - '.$_SESSION['session']['session_end_time'].'</td></tr></tbody></table></div>';
                        } else {
                            $end = \DateTime::createFromFormat('Y-m-d', date('Y-m-d',strtotime($_SESSION['session']['session_end_time'])))->format('l jS F Y');
                            $content .= '<div class="hidden-ms"><div class="table-responsive"><table class="table ">
                                                <thead><tr><th>Name</th><th>Start Date</th><th>End Date</th></tr></thead>
                                                <tbody><tr><td>'.$_SESSION['session']['session_name'].'</td><td>'.\DateTime::createFromFormat('Y-m-d', date('Y-m-d',strtotime($_SESSION['session']['session_date'])))->format('l jS F Y').'</td><td>'. $end .'</td></tr></tbody>
                                            </table>
                                        </div></div>
                                        <div class="visible-ms">
                                            <table class="table ">
                                            <tbody><tr><th>Name</th><td>'.$_SESSION['session']['session_name'].'</td></tr><tr><th>Start Date</th><td>'.\DateTime::createFromFormat('Y-m-d', date('Y-m-d',strtotime($_SESSION['session']['session_date'])))->format('l jS F Y').'</td></tr><tr><th>End Date</th><td>'. $end .'</td></tr></tbody></table></div>
                                            <div class="alert alert-danger">
                                                The week only runs until the last day of the week. This pass will end on the <strong>' . $end . '</strong>. This pass <strong>does not</strong> entitle non-beginners to attend beginner sessions
                                            </div>';
                        }
                        $content .= '<p class="lead">Booking Information</p>
                                    <div class="hidden-ms">
                                        <div class="table-responsive">
                                            <table class="table ">
                                                <thead><tr><th>Booking Name</th><th>Booking Number</th><tr></tr></thead>
                                                <tbody><tr><td>'.$_SESSION['booking']['name'].'</td><td>'.$_SESSION['booking']['number'].'</td></tr><tr><th colspan="2">Booking Email</th></tr><tr><td>'.$_SESSION['booking']['email'].'</td></tr></tbody>
                                            </table>
                                        
                                        </div>
                                    </div>
                                    <div class="visible-ms">
                                        <table class="table ">
                                            <tbody><tr><th>Booking Name</th><td>'.$_SESSION['booking']['name'].'</td></tr><tr><th>Booking Number</th><td>'.$_SESSION['booking']['number'].'</td></tr><tr><th>Booking Email</th><td>'.$_SESSION['booking']['email'].'</td></tr></tbody>
                                        </table>
                                    </div> 
                                    <p class="lead">Participants</p>
                                    <div class="hidden-ms">
                                        <div class="table-responsive">
                                            <table class="table ">
                                                <thead><tr><th>Member Number</th><th>Member Name</th><tr></tr></thead>
                                                <tbody>';
                                                    foreach($_SESSION['participants']['members'] as $member){
                                                        $content .= '<tr><td>'.$member['member_id'].'</td><td>'.ucwords($member['name']).'</td></tr>';
                                                    }
                                $content .= '   </tbdoy>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="visible-ms">
                                        <table class="table">
                                            <thead><tr><th>Number</th><th>Member Name</th><tr></tr></thead>
                                            <tbody>';
                                                foreach($_SESSION['participants']['members'] as $member){
                                                    $content .= '<tr><td>'.$member['member_id'].'</td><td>'.ucwords($member['name']).'</td></tr>';
                                                }
                            $content .= '   </tbody>
                                        </table>
                                    </div>
                                    
                                 </div>
                        
                        <div class="col-md-4 col-sm-6" >
                           <p class="lead">Breakdown</p>
                            <div class="card ">
                                <div class="card-content ">
                                    <p class="float-left">Total No. Participants: '.$completeDetails['participants']['total'].'</p><p class="text-right">Total: £'.$completeDetails['cost']['total_cost'].'</p>
                                </div>
                                <div class="card-action ">
                                    <p>Breakdown</p>
                                    <table class="rwc_table confirmation">
                                        <tr><th syle="text-right">Session Total:</th><td><span id="session_total">£ '.number_format($completeDetails['cost']['total_cost'] - $completeDetails['cost']['booking'], 2).'</span></td></tr>
                                        <tr><th>Booking Charge:</th><td><span id="booking_charge">£'.number_format($completeDetails['cost']['booking'], 2).'</span></td></tr>
                                        <tr><th>Total:</th><td class="double_top_border"><span id="total_charge">£'.number_format($completeDetails['cost']['total_cost'], 2).'</span></td></tr>
                                    </table>
                                </div>
                            </div>
                            <div class="card ">
                                <div class="card-content ">
                                    <p>What to do next?</p>
                                </div>
                                <div class="card-action ">
                                    <p>On the day of the session, we suggest arriving 10 minutes before the start of your session.  We have sent an email to your booking email address and we recommend you bring a screenshot of the email as confirmation.</p>
                                    
                                </div>
                            </div>
                        </div>  
                </section>';
                        return $content;
                    break;
          
                    
        }


    }
    public function sideBar() {
        $content = '<div class="panel-group" id="sections">';
        if(isset($_SESSION['stages']['1_complete']) && $_SESSION['stages']['1_complete'] == '1'){
            $content .= '<div class="panel ">
            <div class="panel-heading">
                <div class="panel-title">
                    <p  style="display: inline-block"><a data-toggle="collapse" data-parent="sections" href="#sessions_info"><span class="profile-image"><i class="glyphicon glyphicon-calendar"></i></span>Session Info</a></p>
                    <a href="?stage=1" class="btn btn-default btn-sm float-right">Change</a>
                </div>
                <div class="panel-collapse collapse in" id="sessions_info">
                    <div class="panel-body">
                        <table class="table table-responsive table-condensed"><tbody>
                        <tr><th>Session name</th><td>'.$_SESSION['session']['session_name'].'</td></tr><tr><th>Session date</th><td>'.$_SESSION['session']['session_date'].'</td></tr><tr><th>Session time</th><td>'.$_SESSION['session']['session_start_time'].' - '.$_SESSION['session']['session_end_time'].'</td></tr><th>Session Restrictions</th><td>'.(($_SESSION['session']['session_beginner'] != 'null')? '<span class="badge badge-danger">Beginners <stromg>Only</strong></span>': '<span class="badge badge-success">No Restrictions</div>').'
                        <small></small></td></tr></tbody></table>
                    </div>
                </div>
            </div></div>';

        }
        if(isset($_SESSION['stages']['2_complete']) && $_SESSION['stages']['2_complete'] == '1'){
            $content .= '<div class="panel ">
            <div class="panel-heading">
                <div class="panel-title">
                    <p  style="display: inline-block"><a data-toggle="collapse" data-parent="sections" href="#booking_info"><span class="profile-image"><i class="glyphicon glyphicon-list-alt"></i></span>Booking Info</a></p>
                    <a href="?stage=2" class="btn btn-default btn-sm float-right">Change</a>
                </div>
                <div class="panel-collapse collapse in" id="booking_info">
                    <div class="panel-body">
                        <table class="table table-responsive table-condensed"><tbody>
                        <tr><th>Booking name</th><td>'.$_SESSION['booking']['name'].'</td></tr><tr><th>Booking phone number</th><td>'.$_SESSION['booking']['number'].'</td></tr><tr><th colspan="2">Booking email</th></tr><tr><td colspan="2">'.$_SESSION['booking']['email'].'</tr></tbody></table>
                    </div>
                </div>
            </div></div>';

            $content .= '<div class="panel ">
            <div class="panel-heading">
                <div class="panel-title" id="pricing">
                    <p  style="display: inline-block"><a data-toggle="collapse" data-parent="sections" href="#total_info"><span class="profile-image"><i class="glyphicon glyphicon-shopping-cart"></i></span>Cart <span id="total">'.((isset($_SESSION['participants']['members'])) ? '£'.$_SESSION['cost']['total_cost']: '').'</span></a></p>';
            if(isset($_SESSION['stages']['3_complete']))
                $content .= '<a href="?stage=2" class="btn btn-default btn-sm float-right">Change</a>';

                    
            $content .= '</div>
                <div class="panel-collapse collapse in" id="total_info">
                    <div class="panel-body">
                        <table class="table table-responsive table-condensed" id="participants_sidebar"><tbody>
                        ';
                        if(isset($_SESSION['participants'])){
                            $content .= '<tr><th>Name</th><th>Price</th></tr>';
                            foreach($_SESSION['participants']['members'] as $member){
                                $content .= '<tr><td>'.ucwords($member['name']).'</td><td>£'.number_format($_SESSION['cost']['session_cost'], 2).'</tr>';
                            }
                        }
                    $content .='</tbody></table><table class="table table-responsive table-condensed"><tbody><tr><th>Booking Fee</th><td>£0.50</td></tr><tr><th>Total</th><td id="total_row">£'.$_SESSION['cost']['total_cost'].'</td></tr></tbody></table>
                    </div>
                </div>
            </div></div>';

        }
        $content .= '</div>';
        return $content;

    }
    public function getHtml($input_type, $col_span, $label, $required, $name, $id, $error_id, $tabindex ,$style = null, $section, $session_name) {

        return '<div class="form-group col-md-'.$col_span.' col-sm-'.$col_span.' col-xs-12 col-ms-12"><label class="col-md-4 col-sm-12 control-label reg '.(($required == true) ? 'required':'').'" for="'.$id.'">'.$label.' </label><div class="col-md-8 col-sm-12"><input type="'.$input_type.'" class="rwc_new col-sm-12 '.(($style != null)? $style: '').'" name="'.$name.'" id="'.$id.'"  tabindex="'.$tabindex.'" value="'.((isset($_SESSION[$section][$session_name]))? $_SESSION[$section][$session_name]:'').'"><span class="glyphicon glyphicon-remove form-control-feedback reg '.( (isset($formErrors[$name])) ? 'block visible':'').'"  id="'.$name.'_error_icon"></span><span class="glyphicon glyphicon-ok form-control-feedback reg '. ((isset($formErrors)) ? 'block visible':'').'"id="'.$name.'_success_icon"></span><span class="error"   id="'.$error_id.'"></span> </div></div>';
   
        
        
    }
}
