<?php
namespace Rampworld\Form;

use Rampworld\Terms\Fetch as TermsAndConditions;
use Rampworld\Registration\Captcha as Captcha;
class Registration
{

    public function getSection($stage, $formErrors, $completeDetails = null)
    {

        switch ($stage) {
            case 1:
                $content =  $this->_getHeader('Participant Personal Details', ''). '
                    <div class="col-xs-12">
                        <div class="col-md-8 col-sm-6" id="form_content">
                            <div class="row">'.
                            
                                $this->getHtml('text', '12', 'Forename', true, 'forename', 'form_forename', 'forename_error', 1, 'capitalize', 'form_forname').
                                $this->getHtml('text', '12', 'Surname', true, 'form_surname', 'form_surname', 'surname_error', 2, 'capitalize').
                            '</div>
                            <div class="row">'.
                                $this->getGender(12).
                                $this->getDOB(12).
                            '</div>
                            <div class="row">'.
                                $this->getHtml('email', '12', 'Email', true, 'form_email_address', 'form_email_address', 'email_address_error', 6, 'lowercase').
                                $this->getHtml('number', '12', 'Phone Number', true, 'form_number', 'form_number', 'number_error', 7).
                            '</div>
                        </div>
                        <div class="col-md-4 col-sm-6" id="side-bar">
                            <div class="well well-sm" style="margin-top:20px;">To become a member, please take your time and complete the form below and following the instructions carefully. If you make a mistake, please contact a member of staff.</div>
                            <p class="alert alert-warning alert-dismissable block visible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All fields marked with a <span class="required"></span> are required.</p>
                            <div class="alert alert-info" id="form_help"><b>This section is information about the participant, not the parent or guardian.</b></div>
                            <div class="alert alert-warning reg col-xs-12" id="under_6_notice">NOTE: We recommend participants under the age of 6 attend the beginner sessions only.
                            </div>
                            <div class="alert alert-danger reg '. ((!empty($formErrors) & isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                if (isset($formErrors)) {
                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').')</b>';
                    foreach ($formErrors as $key => $err) {
                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                    }
                }
                            $content .= '</div>

                        </div>
                    </div>
                    <div class="col-xs-12 text-right ">
                        <input type="hidden" name="stage" value="1">
                        <a data-current="1" data-next="2" class="btn btn-primary" id="form-next" >Next</a>
                    </div>
                </div>
                </section>';
                return $content;
                break;
            case 2:
                $content =$this->_getHeader('Participant Address', ''). '
                    <div class="col-xs-12">
                        <div class="col-md-8 col-sm-6" id="form_content">
                            <div class="row">'.
                                $this->getAddress().
                            '</div>
                        </div>
                        <div class="col-md-4 col-sm-6" id="side-bar">
                            <p class="alert alert-warning alert-dismissable block visible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All fields marked with a <span class="required"></span> are required.</p>
                            <div class="alert alert-info" id="form_help"><b>This section is the participant\'s address. Only First address line and postcode is required.</b></div>
                           
                            <div class="alert alert-danger reg '. ((!empty($formErrors) & isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                if (isset($formErrors)) {
                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').')</b>';
                    foreach ($formErrors as $key => $err) {
                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                    }
                }
                            $content .= '</div>

                        </div>
                    </div>
                    <div class="col-xs-12 text-right ">
                        <input type="hidden" name="stage" value="2">
                        <a data-current="2" data-next="1" class="btn btn-default" id="form-prev">Back</a>
                        <a data-current="2" data-next="3" class="btn btn-primary" id="form-next" >Next</a>
                    </div>
                </div>
                </section>';
                return $content;
                break;
            case 3:

                $terms = new TermsAndConditions();
                $tandcContent = $terms->get();
                $content =$this->_getHeader('Parental/ Guardian Details', ''). '
                    <div class="col-xs-12">
                        <div class="col-md-8 col-sm-6" id="form_content">
                         <a id="copyParticipantNumber" data-number="'.$_SESSION['form_number'].'" class="btn btn-default">Copy Participant\'s Number</a>
                         <a id="copyParticipantEmail" data-email="'.$_SESSION['form_email_address'].'" class="btn btn-default">Copy Participant\'s Email</a>
                         <a id="copyParticipantAddress" data-address-line-one="'.$_SESSION['address_line_one'].'" data-address-postcode="'.$_SESSION['address_postcode'].'" class="btn btn-default">Copy Participant\'s Address</a>
                          
                          
                            <div class="row">'.
                                $this->getHtml('text', 12, 'Parent/ Guardian Full Name', 1, 'form_consent_name', 'form_consent_name', 'consent_name_error', 1, 'capitalize' ).
                                $this->getHtml('number', 12, 'Contact Phone Number', 1, 'form_consent_number', 'form_consent_number', 'consent_number_error', 1 ).
                                $this->getHtml('email', 12, 'Email Address', 1, 'form_consent_email', 'form_consent_email', 'consent_email_error', 1).
                                $this->getAddress(true).
                            

                            '<div class="form-group col-md-12 col-sm-12 col-xs-12 col-ms-12">
                                <div class="terms-conditions-container">
                                    <div class="terms-conditions">
                                        '.$tandcContent['content'].'
                                    </div>
                                    <div id="versionNumber">Version Number: '.$tandcContent['version_number'].'</div>
                                </div>
                                <label class="col-sm-4 control-label reg required">Terms and Conditions </label>
                                <div class="col-sm-8">
                                    <div class="well well-sm">
                                        <div class="options">
                                            <label class="control control--check">I give my permission <span id="participant_name">for the participant</span> to use the facilities and I accept the terms and conditions 
                                                <input type="checkbox" name="consentAccept" id="consentAccept" value="true">
                                                <div class="control__indicator"></div>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6" id="side-bar">
                            <p class="alert alert-warning alert-dismissable block visible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All fields marked with a <span class="required"></span> are required.</p>
                            <div class="alert alert-info" id="form_help"><b>You are under 16 years old, therefore, your must have parent/ guardian permission.</b></div>
                           
                            <div class="alert alert-danger reg '. ((!empty($formErrors) & isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                if (isset($formErrors)) {
                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').')</b>';
                    foreach ($formErrors as $key => $err) {
                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                    }
                }
                            $content .= '</div>

                        </div>
                    </div>
                    <div class="col-xs-12 text-right ">
                        <input type="hidden" name="stage" value="3">
                        <a data-current="3" data-next="2" class="btn btn-default" id="form-prev">Back</a>
                        <a data-current="3" data-next="4" class="btn btn-primary" id="form-next" >Next</a>
                    </div>
                </div>
                </section>';
                return $content;
                break;
            case 4:
                $content =$this->_getHeader('About yourself', ''). '
                    <div class="col-xs-12">
                        <div class="col-md-8 col-sm-6" id="form_content">
                            <div class="row">
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 col-ms-12"><label class="col-md-4 col-sm-12 control-label reg required" >Sports </label><div class="col-md-8 col-sm-12 text-left"><div class="options">
                                    <label class="control control--check">BMX<input type="checkbox" name="form_sport[]" value="BMX" id="form_sport" tabindex="14" '. ((isset($_SESSION['form_sport'])) ? ((preg_match('/BMX/', $_SESSION['form_sport']))? 'checked': ''): '').'/>
                                        <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--check">Scooter<input type="checkbox" name="form_sport[]" value="SMX" id="form_sport" tabindex="15"  '. ((isset($_SESSION['form_sport'])) ? ((preg_match('/SMX/', $_SESSION['form_sport']))? 'checked': ''): '').'/>
                                        <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--check">MTB<input type="checkbox" name="form_sport[]" value="MTB" id="form_sport" tabindex="16"  '. ((isset($_SESSION['form_sport'])) ? ((preg_match('/MTB/', $_SESSION['form_sport']))? 'checked': ''): '').'/>
                                        <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--check">Skateboard<input type="checkbox" name="form_sport[]" value="skateboard" id="form_sport" tabindex="17" '. ((isset($_SESSION['form_sport'])) ? ((preg_match('/skateboard/', $_SESSION['form_sport']))? 'checked': ''): '').'/>
                                        <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--check">Inline<input type="checkbox" name="form_sport[]" value="inline" id="form_sport" tabindex="18" '. ((isset($_SESSION['form_sport'])) ? ((preg_match('/inline/', $_SESSION['form_sport']))? 'checked': ''): '').'/>
                                        <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--check">Spectator<input type="checkbox" name="form_sport[]" value="spectator" id="form_sport" tabindex="18" '. ((isset($_SESSION['form_sport'])) ? ((preg_match('/spectator/', $_SESSION['form_sport']))? 'checked': ''): '').'/>
                                        <div class="control__indicator"></div>
                                    </label>
                                    <label class="control control--check" id="form_other">Other<input type="checkbox" name="form_sport[]" value="other" id="form_sport" tabindex="19" '. ((isset($_SESSION['form_sport'])) ? ((preg_match('/other/', $_SESSION['form_sport']))? 'checked': ''):'').'/>
                                        <div class="control__indicator"></div>
                                    </label>

                                </div></div></div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12 col-ms-12 '.((isset($_SESSION['form_sport'])) ? ((preg_match('/other/', $_SESSION['form_sport']))? '': 'hidden'):'hidden').'" id="form_sport_container"><label class="col-md-4 col-sm-12 control-label reg required" for="form_other_value">Other </label><div class="col-md-8 col-sm-12"><input type="text" class="rwc_new col-sm-12 capitalize" name="sport_other_val" id="form_other_value"  tabindex="" value="'.((isset($_SESSION['sport_other_val']))? $_SESSION['sport_other_val']:'').'"><span class="glyphicon glyphicon-remove form-control-feedback reg '.( (isset($formErrors['sport_other_val'])) ? 'block visible':'').'"  id="form_sport_other_val_error_icon></span><span class="glyphicon glyphicon-ok form-control-feedback reg '. ((isset($formErrors)) ? 'block visible':'').'"id="form_sport_other_val_success_icon"></span><span class="error"  id="sport_error"></span> </div></div>
                                <label class="col-md-12 col-sm-12 control-label reg required">Expertise</label>
						        <div class="form-group col-md-12 col-sm-12 col-xs-12 col-ms-12">
                                    <div id="double-label-slider"></div>
						
        						    <input class="rwc_new" type="hidden" id="expertise" name="expertise" value="'. ((isset($_SESSION['expertise']))? $_SESSION['expertise']:'') .'">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6" id="side-bar">
                            <p class="alert alert-warning alert-dismissable block visible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All fields marked with a <span class="required"></span> are required.</p>
                            <div class="alert alert-info  text-left" id="form_help"><b style="margn-bottom:20px; display:block;">This section is the participant\'s sports and their expertise.</b><br><b>Beginner</b>: Unable to go up and down ramps unaided or without taking their feet off.
                            <br><b>Novice</b>: Able to go up and down without taking their feet off but cannot do any tricks</b>
                            <br><b>Experienced</b>: Able do basic tricks</b>
                            <br><b>Advanced</b>: Able do some difficult tricks</b>
                            <br><b>Expert</b>: Able do some professional tricks</b>
                            <br><b>Professional</b>: Able do many professional tricks and/ or has a sponsorship</b></div>
                           
                            <div class="alert alert-danger reg '. ((!empty($formErrors) && isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                if (isset($formErrors)) {
                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').')</b>';
                    foreach ($formErrors as $key => $err) {
                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                    }
                }
                            $content .= '</div>

                        </div>
                    </div>
                    <div class="col-xs-12 text-right ">
                        <input type="hidden" name="stage" value="5">

                        <a data-current="4" data-next="5" class="btn btn-primary" id="form-next" >Next</a>
                    </div>
                </div>
                </section>';
                return $content;
                break;
            case 5:
                $content = $this->_getHeader('Emergency Contact Details', ''). '<div class="col-xs-12">
                        <div class="col-md-8 col-sm-6" id="form_content">
                            <div class="row">'.((isset($_SESSION['form_consent_name'])) ? '<a id="copyParticipant" data-name="'.$_SESSION['form_consent_name'].'" data-number="'.$_SESSION['form_consent_number'].'" class="btn btn-default">Copy Parental/ Guardian Details</a>':'').
                                $this->getHtml('text', 12, 'Emergency Contact Name 1', 1, 'form_emergency1_name', 'form_emergency1_name', 'emergency1_name_error', 1, 'capitalize').
                                $this->getHtml('number', 12, 'Emergency Phone Number 1', 1, 'form_emergency1_number', 'form_emergency1_number', 'emergency1_number_error', 1 ).
                                $this->getHtml('text', 12, 'Emergency Contact Name 2', 0, 'form_emergency2_name', 'form_emergency2_name', 'emergency1_name_error', 1, 'capitalize').
                                $this->getHtml('number', 12, 'Emergency Phone Number 2', 0, 'form_emergency2_number', 'form_emergency2_number', 'emergency1_number_error', 1 ).
                            '</div>
                            <div class="row">
                                <label class="col-md-4 col-sm-12 control-label reg" >Medical Notes </label>
                                <div class="col-md-8 col-sm-12">
                                <textarea name="form_medical_notes" id="form_medical_notes" class=" '.((isset($_SESSION['form_medical_notes']))? 'has-value': '').' rwc_new" cols="30" rows="10" tabindex="25">'.((isset($_SESSION['form_medical_notes']))? $_SESSION['form_medical_notes']: '').'</textarea></div>
                            </div>
                        </div>
                         <div class="col-md-4 col-sm-6" id="side-bar">
                            <p class="alert alert-warning alert-dismissable block visible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All fields marked with a <span class="required"></span> are required.</p>
                            <div class="alert alert-info  text-left" id="form_help"><b style="margn-bottom:20px; display:block;">This will only be used in case of an accident at RampWorld Cardiff.</b></div>
                           
                            <div class="alert alert-danger reg '. ((!empty($formErrors) && isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                if (isset($formErrors)) {
                    $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').')</b>';
                    foreach ($formErrors as $key => $err) {
                        $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                    }
                }
                            $content .= '</div>

                        </div>

                        </div>
                    </div>
                    <div class="col-xs-12 text-right ">
                        <input type="hidden" name="stage" value="5">
                        <a data-current="5" data-next="3" class="btn btn-default" id="form-prev">Back</a>
                        <a data-current="5" data-next="6" class="btn btn-primary" id="form-next" >Next</a></div>
                </div>
                </section>';
                return $content;
                    break;

            case 6:
                $terms = new TermsAndConditions();
                $tandcContent = $terms->get();
     

                $content = $this->_getHeader('Terms and Conditions', ''). '  
                    <div class="col-xs-12">
                      <div class="col-md-8 col-sm-6" id="form_content">
                        <div class="terms-conditions-container">
                          <div class="terms-conditions">
                            '.$tandcContent['content'].'
                          </div>
                          <div id="versionNumber">Version Number: '.$tandcContent['version_number'].'</div>
                        </div>
                        <div class="row">
                          <div class="float-right">
                            <div class="options">
                              <label class="control control--check required">You, '.$_SESSION['forename']. ' '. $_SESSION['form_surname'].', agree to the Terms and Conditions<input type="checkbox" name="tandc" value="true" id="tandc" tabindex="1" '. ((isset($_SESSION['tandc']) && $_SESSION['tandc'] == true) ? 'checked': '').'/>
                              <div class="control__indicator"></div>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6">
                        <p class="alert alert-warning alert-dismissable block visible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>All fields marked with a <span class="required"></span> are required.</p>
                        <div class="row">
                          <img src="//www.rampworldcardiff.co.uk/wp-content/themes/rampworld/includes/captcha.php" style="margin: 20px 0; border: 1px solid #21a1e1;">
                          <label class="col-md-412 col-sm-12 control-label reg required" >Please type the letters in the box below. </label>
                          <div class="form-group col-md-12 col-sm-12 col-xs-12 col-ms-12 '.(!empty($formErrors) && (isset($formErrors["captcha"]) ) ? 'has-error has-feedback': (isset($formErrors) && (!isset($formErrors['captcha']) && !empty($formErrors)) ? 'has-success has-feedback' : '')).'">
                            <input type="text" name="captcha" id="form_captcha" class="text-center rwc_new">
                            <span class="glyphicon glyphicon-remove form-control-feedback reg '.( (isset($formErrors['captcha'])) ? 'block visible':'').'"  id="form_captcha_error_icon"></span><span class="glyphicon glyphicon-ok form-control-feedback reg '. ((isset($formErrors) && !empty($formErrors) && !isset($formErrors['captcha']) ) ? 'block visible':'').'"id="form_captcha_success_icon"></span>
                          </div>
                      </div>
                      <div class="alert alert-danger reg '. ((!empty($formErrors) && isset($formErrors)) ? 'visible block': '').'" id="main_page_errors">';
                        if (isset($formErrors)) {
                            $content .= '<b>Errors (<span id="totalErrorNo">'. ((isset($formErrors))? count($formErrors) : '').')</b>';
                            foreach ($formErrors as $key => $err) {
                                $content .= '<span style="display:block;margin-top:10px">'.$err.'</span>';
                            }
                        }
                        $content .= '</div>

                      </div>

                    </div>
                  </div>
                  <div class="col-xs-12 text-right ">
                    <input type="hidden" name="stage" value="'.$stage.'">
                    <a data-current="6" data-next="3" class="btn btn-default" id="form-prev">Back</a>
                    <a data-current="6" data-next="7" class="btn btn-primary" id="form-next" >Finish</a>
                  </div>
                </section>';
                return $content;
                    break;
            case 7:
                if (isset($completeDetails['errorID']) && $completeDetails['errorID'] == 1) {
                    return '<section ><div class="content" >
                      <div class="col-xs-12">
                        <div class="col-md-8 col-sm-6" id="form_content">
                            <h2>Something went wrong :(</h2>
                            <p class="lead">A error has occured. Please use the button at the top of the screen to navigate through the errors.  If this contunie to happy, please contact a member of the team or email us.</p>
                        </div>
                        <div class="col-md-4 col-sm-6">
                          <p class="alert alert-info alert-dismissable block visible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>We are always looking to improve our service, below a quick survey to help us understand our customer\'s opinion.</p>
                          <a href="https://www.surveymonkey.co.uk/r/LSS9Q6F" target="_blank" class="btn btn-default">Complete Survey</a>
                        </div>
                      </div>
                    </div>';
                } else {
                    return '<section ><div class="content" >
                        <div class="col-xs-12">
                          <div class="col-md-8 col-sm-6" id="form_content">
                              <h2>Complete!</h2>
                              <p class="lead">'.$completeDetails['name'].', welcome to RampWorld Cardiff! Below is your membership number. When you arrive, please let a member of staff aware of your number.</p>
                              <p class="lead"><b>Hint:</b> Take a screenshot of this number incase you forget!</p>
                              <div class="membershipNumber">'.$completeDetails['mid'].'</div>
                          </div>
                          <div class="col-md-4 col-sm-6">
                            '.((isset($completeDetails['errorID']) && $completeDetails['errorID'] == 2)? '<p class="alert alert-warning">We was not able to send you an email address.  Please screen shot this for future use.<br><b>We attempted to send emails too:</b><br>'.$completeDetails['emails']['participant']. ((!empty($completeDetails['emails']['consent'])) ? '<br>'. $completeDetails['emails']['consent']:'').'</p>' :
                            '<p class="alert alert-warning">We have sent an email to participant, however, under peak times please allow up to 24 hours for the email to arrive.  Some email clients may send the email into your junk folder.  If your email does not arrive, next time your visiting, please mention to one of our staff.<br><b>We sent emails too:</b><br>'.$completeDetails['emails']['participant']. ((!empty($completeDetails['emails']['consent'])) ? '<br>'. $completeDetails['emails']['consent']:'').'</p>').'
                            <a href="http://www.rampworldcardiff.co.uk/membership/registration/" class="btn btn-primary">Start Over</a>

                          </div>

                        </div>
                      </div>';
                }
                  
                break;
        }
    }
    private function _getHeader($title, $info = null)
    {
      return '<section ><div class="content" ><h2>'.ucwords($title).'</h2>';
    }


    public function getHtml($input_type, $col_span, $label, $required, $name, $id, $error_id, $tabindex, $style = null, $prefix = null, $container_id = null)
    {

      return '<div class="form-group col-md-'.$col_span.' col-sm-'.$col_span.' col-xs-12 col-ms-12" '.(($container_id != null) ? 'id="'.$container_id.'"': '').'><label class="col-md-4 col-sm-12 control-label reg '.(($required == true) ? 'required':'').'" for="'.$id.'">'.$label.' </label><div class="col-md-8 col-sm-12"><input type="'.$input_type.'" class="rwc_new col-sm-12 '.(($style != null)? $style: '').'" name="'.$name.'" id="'.$id.'"  tabindex="'.$tabindex.'" value="'.((isset($_SESSION[$name]))? $_SESSION[$name]:'').'"><span class="glyphicon glyphicon-remove form-control-feedback reg '.( (isset($formErrors[$name])) ? 'block visible':'').'"  id="'.(($prefix == null) ?$name.'_error_icon': 'form_'.$name.'_error_icon').'"></span><span class="glyphicon glyphicon-ok form-control-feedback reg '. ((isset($formErrors)) ? 'block visible':'').'"id="'.(($prefix == null) ?$name.'_success_icon': 'form_'.$name.'_success_icon').'"></span><span class="error"   id="'.$error_id.'"></span> </div></div>';
    }
    function getDOB($col_span)
    {
      return '<div class="form-group col-md-'.$col_span.' col-sm-'.$col_span.' col-xs-12 col-ms-12 '.((isset($formErrors["dob"]) ) ? 'has-error has-feedback': (isset($formErrors) && !isset($formErrors['dob']) ? 'has-success has-feedback' : '')).'">
                  <label class="col-md-4 col-sm-12 control-label reg required">Date of Birth </label>
                  <div class="col-md-8 col-sm-12 text-left">
                    <div class="dob-item day">
                      <input type="number" name="form_dob_day" placeholder="dd" id="form_dob_day" max-value="31" min-value="1" tabindex="3" class="rwc_new col-xs-12 " placeholder="Day" min="1" max="32" value="'.((isset($_SESSION['form_dob_day']))? $_SESSION['form_dob_day']:'') .'" value="'.((isset($_SESSION['form_dob_day'])) ? $_SESSION['form_dob_day'] : '').'" >
                      <span class="glyphicon glyphicon-remove form-control-feedback reg form_dob_error_icon '.( (isset($formErrors['dob'])) ? 'block visible':'').'"  ></span><span class="glyphicon glyphicon-ok form-control-feedback reg form_dob_success_icon '. ((isset($formErrors)) ? 'block visible':'').'"></span>
                    </div>
                    <div class="dob-item dropdown">
                      <select class="selectpicker text-center" tabindex="4" name="form_dob_month" id="form_dob_month">
                        <option value="" disabled selected>Month</option>
                        <option value="01" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '01')? 'selected': '').'>January</option>
                        <option value="02" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '02')? 'selected': '').'>Febuary</option>
                        <option value="03" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '03')? 'selected': '').'>March</option>
                        <option value="04" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '04')? 'selected': '').'>April</option>
                        <option value="05" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '05')? 'selected': '').'>May</option>
                        <option value="06" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '06')? 'selected': '').'>June</option>
                        <option value="07" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '07')? 'selected': '').'>July</option>
                        <option value="08" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '08')? 'selected': '').'>August</option>
                        <option value="09" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '09')? 'selected': '').'>September</option>
                        <option value="10" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '10')? 'selected': '').'>October</option>
                        <option value="11" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '11')? 'selected': '').'>November</option>
                        <option value="12" '.  ((isset($_SESSION['form_dob_month']) && $_SESSION['form_dob_month'] == '12')? 'selected': '').'>December</option>

                      </select>
                    </div>
                    <div class="dob-item year">
                      <input type="number" name="form_dob_year" tabindex="6" id="form_dob_year" min-value="1916" class="rwc_new col-xs-12" placeholder="YYYY" value="'. ((isset($_SESSION['form_dob_year']))? $_SESSION['form_dob_year']:'' ).'" tab-index="5">
                      <span class="glyphicon glyphicon-remove form-control-feedback reg form_dob_error_icon  '.( (isset($formErrors['dob'])) ? 'block visible':'').'"></span><span class="glyphicon glyphicon-ok form-control-feedback reg form_dob_success_icon '. ((isset($formErrors)) ? 'block visible':'').'"></span>
                    </div>
                  </div>
                </div>';
    }
    function getGender($col_span)
    {
      return '<div class="form-group col-md-'.$col_span.' col-sm-'.$col_span.' col-xs-12 col-ms-12">
                <label class="col-md-4 col-sm-12 control-label reg required">Gender </label>
                <div class="col-md-8 col-sm-12">
                  <div class="well well-sm">
                    <div class="options">
                      <label class="control control--radio">Male
                        <input type="radio" name="form_gender" id="form_gender" value="m" checked  tabindex="3">
                        <div class="control__indicator"></div>
                      </label>
                      <label class="control control--radio">Female
                        <input type="radio" name="form_gender" id="form_gender" value="f"  tabindex="3">
                        <div class="control__indicator"></div>
                      </label>
                      <label class="control control--radio">Other
                        <input type="radio" name="form_gender" id="form_gender" value="o"  tabindex="3">
                        <div class="control__indicator"></div>
                      </label>
                    </div>
                  </div>
                </div>
              </div>';
    }
    public function getAddress($parental = false)
    {
      if ($parental) {
        return '<div class="row">'.
          $this->getHtml('text', 12, 'Address Line 1', 1, 'consent_address_line_one', 'form_consent_address_line_one', 'form_consent_address_line_one_error', 1, 'capitalize', 'form_consent_address_line_one').
          $this->getHtml('text', 12, 'Postcode', 1, 'consent_address_postcode', 'form_consent_address_postcode', 'consent_address_postcode_error', 1, 'uppercase', 'form_consent_address_postcode').'</div>';
      } else {
        return '<div class="row">'.
          $this->getHtml('text', 12, 'Address Line 1', 1, 'address_line_one', 'form_address_line_one', 'address_line_one_error', 1, 'capitalize' ).
          $this->getHtml('text', 12, 'Address Line 2', 0, 'address_line_two', 'form_address_line_two', 'address_line_two_error', 1, 'capitalize').
          $this->getHtml('text', 12, 'City', 0, 'address_city', 'form_address_city', 'address_city_error', 1, 'capitalize').
          $this->getHtml('text', 12, 'County', 0, 'address_county', 'form_address_county', 'address_county_error', 1, 'capitalize' ).
          $this->getHtml('text', 12, 'Postcode', 1, 'address_postcode', 'form_address_postcode', 'address_postcode_error', 1, 'uppercase' ).'</div>';
      }
    }
    public function getExpertise()
    {
    }
    public function getParental()
    {

      $div =  '<div class="row"><div class="col-xs-12"><h3>Shortcuts</h3><p class="lead ">These will copy the values already provided</p><a href="#" class="btn btn-default " id="copyAddress">Copy Address </a><a href="#" class="btn btn-default " id="copyEmail">Copy Email Address</a><a href="#" class="btn btn-default" id="copyNumber">Copy Number</a></div>';
      $div .= $this->getHtml('text', 8, 'Parent/ Guardian Full Name', 1, 'form_consent_name', 'form_consent_name', 'consent_name_error', 1, 'capitalize' );
      $div .= $this->getHtml('number', 4, 'Contact Phone Number', 1, 'form_consent_number', 'form_consent_number', 'consent_number_error', 1 );
      $div .= $this->getAddress(true);
      $div .= $this->getHtml('email', 8, 'Email Address', 1, 'form_consent_email', 'form_consent_email', 'consent_email_error', 1);
      $div .= '<div class="form-group col-md-12 col-sm-12 col-xs-12 col-ms-12">
                <label class="col-sm-4 control-label reg required">Terms and Conditions </label>
                <div class="col-sm-8">
                  <div class="well well-sm">
                    <div class="options">
                      <label class="control control--check">I give my permission <span id="participant_name">for the participant</span> to use the facilities and I accept the terms and conditions 
                        <input type="checkbox" name="consentAccept" id="consentAccept" value="true">
                        <div class="control__indicator"></div>
                      </label>
                    </div>
                  </div>
                  <p class="btn btn-link"><a href="#terms-and-conditions">Terms and conditions can be found here</a></p>
                </div>
                
                <div class="alert alert-danger reg col-xs-12 '. ((isset($formErrors['consentAccept'])) ? 'visible block': '').'" id="surname_error">
                  '.((isset($formErrors['consentAccept'])) ? $formErrors['gender']: '').'
                </div>
            </div>
          </div>';
      return $div;
  }
}
