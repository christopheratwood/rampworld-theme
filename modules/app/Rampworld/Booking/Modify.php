<?php namespace Rampworld\Booking;

use Rampworld\DB\Connection as Connection;
class Modify {
  private $_db;
  
  public function __construct() {

    $this->_db = Connection::connect();
    $this->started = (isset($_SESSION['progress'])) ? true: false;
  }

  public function create($data) {

    if($this->_db->insert(
      'rwc_transactions_paypal',
      $data,
      array('%s','%s','%s','%s','%s','%s','%s')
    ) == false) {
      return false;
    } else {
      return true;
    }  
  }

  public function update($payment_id, $data) {
    if($this->_db->update(
      'rwc_transactions_paypal',
      $data,
      array(
        'payment_id' => $payment_id
      )
    ) !== false ) {
      return true;
    } else {
      return false;
    }

  }

}