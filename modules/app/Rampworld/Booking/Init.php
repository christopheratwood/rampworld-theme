<?php namespace Rampworld\Booking;

use Rampworld\Session\Fetch as FetchSession;
use Rampworld\DB\Connection as Connection;
class Init {
	public 	$nonOperationalDates,
			$date,
			$dayNumber,
			$holiday_id,
			$format,
			$html = '',
			$started,
			$data = array();


	private $_db;

	public function __construct() {

		$this->_db = Connection::connect();
		$this->started = (isset($_SESSION['progress'])) ? true: false;
	}
	public function getSessionData() {
		$this->data = $_SESSION;
		$this->data['session']['date'] = $this->_reformat($this->data['session']['date']);
	}
	private function _reformat($date) {
		$parts = explode('/', $date);
		return $parts[1]. '-'.$parts[2].'-'.$parts[0];
	}

	public function getNonOperationalDates() {

		$this->nonOperationalDates = $this->_db->get_results('SELECT date_format(`date`, "%m/%d/%Y") as date FROM rwc_closed_dates WHERE `date` >= CURDATE()', ARRAY_A);
		$str = '';
		for($i = 0; $i < count($this->nonOperationalDates); $i++) {
			$str .= $this->nonOperationalDates[$i]['date'] . ',';
		}
		return substr($str, 0, -1);
	}

	public function getSessions($date) {

		//
		if($this->_isValid($date)) {
			//check if open
			if($this->_isOpen($date)) {

				$sesh = new FetchSession();

				$pricing = $sesh->get_prices();
				$this->sessions = $sesh->get_sessions($date);


				if(empty($this->sessions)) {
					return 'No sessinons';
				} else {
					for ($i = 0; $i < count($this->sessions);$i++) {
							$price = (($this->sessions[$i]['beginner'] == "1" || ($this->sessions[$i]['end_time'].split(':')[0] - $this->sessions[$i]['start_time'].split(':')[0]) <= 2) ? $pricing['b'] : $pricing['1']);

							if ( isset( $this->sessions[$i]['session_id'] ) ) {

								$this->html .= '<a href="#" class="selectSession" data-session-start="' . $this->sessions[$i]['start_time'] . '" data-session-end="' . $this->sessions[$i]['end_time'] . '" data-sessin-id="' . $this->sessions[$i]['session_id'] . '"><div class="sessionPreview"><div class="name"><p class="medium">' . $this->sessions[$i]['display_name'] . '</p></div><div class="times"><span class="sessionTitle">Session Times</span><span>' . $this->sessions[$i]['start_time'] . ' - ' . $this->sessions[$i]['end_time'] . '</span></div>';
								$this->html .= '<div class="restriction"><span class="sessionTitle">Restriction</span><span>' . ($this->sessions[$i]['beginner'] == "1") ? 'Beginners Only' : 'None' . '</span></div>';
								$this->html .= '<div class="popularity"><span class="sessionTitle">Popularity</span>' . (150 - intval($this->sessions[$i]['totalBooked'])) . ' Free Spaces</div><span class="cost">£' . $price . '</span></div></a>';
							} else {
								$this->html .= '<a href="#" class="selectSession" data-session-start="' + $this->sessions[$i].start_time + '" data-session-end="' + $this->sessions[$i].end_time + '" data-holiday-session-id="' + $this->sessions[$i].holiday_session_id + '"><div class="sessionPreview"><div class="name"><p class="medium">' + $this->sessions[$i].display_name + '</p></div><div class="times"><span class="sessionTitle">Session Times</span><span>' + $this->sessions[$i].start_time + ' - ' + $this->sessions[$i].end_time + '</span></div><div class="restriction"><span class="sessionTitle">Restriction</span><span>' + (($this->sessions[$i].beginner == "1") ? 'Beginners Only' : 'None') + '</span></div><div class="popularity"><span class="sessionTitle">Availability</span>' + (150 - parseInt($this->sessions[$i].totalBooked)) + ' Free Spaces</div><span class="cost">£' + price + '</span></div></a>';
							}

					}
					$this->html .= "<h2>Consecutive Session(s)</h2>";

					foreach ($this->sessions as $x ) {

						if ($sessions[$x]['consecutive'] == "1") {
							$start_time = sessions[x].start_time;

							$j = 1;
							$this->html .= '<div>';
							foreach ($this->sessions as $i) {
								$total = $sessions[$x]['totalBooked'];
								if ($this->sessions[$i]['start_time'] > $start_time) {
									$j++;
									$total += $this->sessions[$i]['totalBooked'];
									$this->html .= '<a href="#" class="selectSession" data-session-start="' + $start_time + '" data-session-end="' + $this->sessions[$i]['end_time'] + '"><div class="sessionPreview"><div class="name"><p class="medium">' + $this->sessions[$i].display_name + '</p></div><div class="times"><span class="sessionTitle">Session Times</span><span>' +$start_time + ' - ' + $this->sessions[$i]['end_time'] + '</span></div><div class="restriction"><span class="sessionTitle">Restriction</span><span>' + (($this->sessions[$i]['beginners'] == "1") ? 'Beginners Only' : 'None') + '</span></div><div class="popularity"><span class="sessionTitle">Availability</span>' + (150 - parseInt($total)) + ' Free Spaces</div><span class="cost">£' + $pricing[j] + '</span> </div></a>';

								}


							}
							$this->html .= '</div>';


						}
					}

				}

			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	public function checkAvailable($date, $start_time, $end_time ) {
	
		$date = \DateTime::createFromFormat('d-m-Y', date('d-m-Y', strtotime($date)));

		$minimun =  new \DateTime('tomorrow');
		

		if($date->format('Y-m-d') >= $minimun->format('Y-m-d')) {
			$session = new FetchSession();
			$information = json_decode($session->get_sessions($date), true);
			foreach($information as $session) {
					if($session[1][2][1] == $start_time && $session[1][3][1] == $end_time) {
						if(($session[1][0][0] == 'event_id' && $session[1][7][1] >= 150 ) || ($session[1][0][0] != 'event_id' && $session[1][7][1] >= 120) ) {
							session_destroy();
							wp_redirect('//www.rampworldcardiff.co.uk/online-booking/book?no-places-available');
						} 
					}
			}
		
			
		} else {
			//session_destory();
			//wp_redirect('//www.rampworldcardiff.co.uk/online-booking/book?out-of-date');
		}
	}
	private function _isValid($date) {

		$today = new \Date();
		$date = \DateTime::createFromFormat('d/m/d', $date);
		$errors = \DateTime::getLastErrors();


		if (!empty($errors['warning_count']) || is_bool($start_date)) {
			return false;
		} else{
			if($date > $today) {
				$this->dayNumber = $date->format('w');
				return true;
			} else {
				return false;
			}
		}
	}
	private function _isOpen($date) {
		return !in_array($date, $this->nonOperationalDates);
	}
	private function _isHoliday($date) {

		$holiday = $this->_db->get_results('SELECT `holiday_id` FROM `rwc_holidays_sessions` WHERE start_date >= \''.$date.'\' AND end_date <= \''.$date.'\' LIMIT 1', ARRAY_A);

		if(!empty($holiday)) {
			$this->holiday_id = $holiday[0]['holiday_id'];
			return true;
		} else {
			return false;
		}
		
	}

}