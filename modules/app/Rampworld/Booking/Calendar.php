<?php

namespace Rampworld\Theme\Booking;

use Rampworld\Theme\Booking\Session as Session;
use Rampworld\Shared\DB\Connection as Connection;
date_default_timezone_set('Europe/London');

class Calendar extends Session{

	public $sessions = array();
	protected $_db,
			$_session;

	public function __construct() {
		$this->_db  = Connection::connect();
		$this->_session = new Session();
	}
	public function get_private_hires($date) {
		if($date->format('w') == 0 || $date->format('w') == 1 || $date->format('w') == 6) {
			$this->sessions = $this->_db->get_results( $this->_db->prepare("SELECT `display_name` as title, concat('".$date->format('Y-m-d')." ', end_time) as end, concat('".$date->format('Y-m-d')." ', start_time) as start, private FROM rwc_sessions WHERE day = %d AND private = %d ORDER BY start_time ASC",array($date->format('w'), 1)), ARRAY_A);
		

				
			for($i = 0; $i < count($this->sessions); $i++) {

				if($this->sessions[$i]['private'] == 1) {
					if(!empty($this->_db->get_results($this->_db->prepare('SELECT count(1) from rwc_private_hires WHERE `session_date` = %s AND start_time <= %s AND end_time >= %s', array($date->format('Y-m-d'), explode(" ", $this->sessions[$i]['start'])[1], explode(" ", $this->sessions[$i]['end'])[1])) ,ARRAY_A))){
						$this->sessions[$i]['booked'] = 0;
					
						$this->sessions[$i]['className'] = 'session-green';
					}else{
							$this->sessions[$i]['booked'] = 1;
						$this->sessions[$i]['className'] = 'session-red';
					}
				}
			
			}
			return $this->sessions;
		} else {
			return array();
		}
	}
	public function get_sessions($date) {

	

		$date = $date;
		$day = $date->format("w");
		$not_string = '';
		$events = $this->_session->has_event($date);

		for($i = 0; $i < count($events); $i++) {
			
			if($events[$i]['online_exclusive']) {
				$not_string = 'AND (`start_time` < \''. explode(" ", $events[$i]['start'])[1].'\' OR `start_time` >= \''. explode(" ", $events[$i]['end'])[1].'\')' ;
			}

			if($date >=date('Y-m-d')) {
				$events[$i]['totalBooked'] = intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_prepaid_sessions WHERE `session_date` = %s AND session_start_time <= %s AND session_end_time >= %s', array($date->format('Y-m-d'),  explode(" ", $events[$i]['start'])[1], explode(" ", $events[$i]['end'])[1])) ,ARRAY_A)[0]['total']);
			
				if($date == date('Y-m-d')) {
						$events[$i]['totalBooked'] += intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_members_entries WHERE `entry_date` = %d AND exit_time  >= %s', array( explode(" ", $events[$i]['start'])[1], explode(" ", $events[$i]['end'])[1])), ARRAY_A)[0]['total']);
				}
			}
			if(	$events[$i]['totalBooked'] < 30)
				$events[$i]['className'] = 'session-bright-green';
			elseif($events[$i]['totalBooked'] > 30 && $events[$i]['totalBooked'] < 70)
				$events[$i]['className'] = 'session-amber';
			elseif($events[$i]['totalBooked'] > 70 && $events[$i]['totalBooked'] < 120)
				$events[$i]['className'] = 'session-dark-amber';
			elseif(($events[$i]['online_exclusive'] && $events[$i]['totalBooked'] >= 150) ||  $events[$i]['totalBooked'] >= 120)
				$events[$i]['className'] = 'session-red';

		}
		$isHoliday = $this->_db->get_results($this->_db->prepare("SELECT holiday_id FROM rwc_holidays WHERE start_date <= %s AND end_date >= %s AND visible = 1 LIMIT 1",array($date->format('Y-m-d'), $date->format('Y-m-d'))), ARRAY_A);

		if(count($isHoliday) > 0) {
			$this->sessions = $this->_db->get_results($this->_db->prepare("SELECT `display_name` as title, concat('".$date->format('Y-m-d')." ', end_time) as end, concat('".$date->format('Y-m-d')." ', start_time) as start, private, beginner FROM rwc_holidays_sessions WHERE holiday_id = %d AND day = %d ".$not_string." ORDER BY start_time ASC",array($isHoliday[0]['holiday_id'], $day, )), ARRAY_A);
		} else {
			$this->sessions = $this->_db->get_results( $this->_db->prepare("SELECT `display_name` as title, concat('".$date->format('Y-m-d')." ', end_time) as end, concat('".$date->format('Y-m-d')." ', start_time) as start, private, beginner FROM rwc_sessions WHERE day = %d ".$not_string."  ORDER BY start_time ASC",array($day)), ARRAY_A);
		}
	

		for($i = 0; $i < count($this->sessions); $i++) {

			if($this->sessions[$i]['private'] == 1) {
				if(!empty($this->_db->get_results($this->_db->prepare('SELECT count(1) from rwc_private_hires WHERE `session_date` = %s AND start_time <= %s AND end_time >= %s', array($date->format('Y-m-d'), explode(" ", $this->sessions[$i]['start'])[1], explode(" ", $this->sessions[$i]['end'])[1])) ,ARRAY_A)))
					$this->sessions[$i]['className'] = 'session-green';
				else
					$this->sessions[$i]['className'] = 'session-red';
				
			} else {
				
				if($date >=date('Y-m-d')) {
					$this->sessions[$i]['totalBooked'] = intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_prepaid_sessions WHERE `session_date` = %s AND session_start_time <= %s AND session_end_time >= %s', array($date->format('Y-m-d'), explode(" ", $this->sessions[$i]['start'])[1], explode(" ", $this->sessions[$i]['end'])[1])) ,ARRAY_A)[0]['total']);
				
					if($date == date('Y-m-d')) {
							$this->sessions[$i]['totalBooked'] += intval($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as total from rwc_members_entries WHERE `entry_date` = %d AND exit_time  >= %s', array( explode(" ", $this->sessions[$i]['start'])[1], explode(" ", $this->sessions[$i]['end'])[1])), ARRAY_A)[0]['total']);
					}
				}
				if(	$this->sessions[$i]['totalBooked'] < 30)
					$this->sessions[$i]['className'] = 'session-green';
				elseif($this->sessions[$i]['totalBooked'] > 30 && $this->sessions[$i]['totalBooked'] < 70)
					$this->sessions[$i]['className'] = 'session-amber';
				elseif($this->sessions[$i]['totalBooked'] > 70 && $this->sessions[$i]['totalBooked'] < 120)
					$this->sessions[$i]['className'] = 'session-dark-amber';
				elseif($this->sessions[$i]['totalBooked'] > 120 )
					$this->sessions[$i]['className'] = 'session-red';
			} 
			
		}
	
	
		return array_merge($events, $this->sessions);
	

			
	

	}
}