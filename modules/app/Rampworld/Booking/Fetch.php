<?php namespace Rampworld\Booking;

use Rampworld\DB\Connection as Connection;
Class Fetch {
  private $_db;
  
  public function __construct() {

    $this->_db = Connection::connect();
    $this->started = (isset($_SESSION['progress'])) ? true: false;
  }
  public function getIDs($pph) {
    $ids = $this->_db->get_results($this->_db->prepare('SELECT id, payment_id FROM rwc_transactions_paypal WHERE hash = %s LIMIT 1', array($pph)), ARRAY_A);

    if(count($ids) != 0) {
      return $ids[0];
    } else  {
      return false;
    }
  }
  public function getAll($search_field = null, $search_value = null, $start_no = null, $limit = null) {
    
    if($search_field == null) {
      $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete is NOT null AND ps.session_date IS NOT NULL GROUP BY b.id) UNION 
        (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND pm.start_date IS NOT NULL GROUP BY b.id)  ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $start_no, $limit)), ARRAY_A);

      $this->count =  count($this->_db->get_results('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete is NOT null AND ps.session_date IS NOT NULL GROUP BY b.id) UNION 
        (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.start_date IS NOT NULL GROUP BY b.id)', ARRAY_A));
    } else {
      $search_field = str_replace('+', ' ',$search_field);
      if($search_field == 'booking reference'){
        $this->count =  count($this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND  b.id = %d GROUP BY b.id) UNION 
          (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.complete IS NOT NULL AND b.id = pm.transaction_id WHERE b.id = %d GROUP BY b.id)', array( intval($search_value), intval($search_value))), ARRAY_A));

        $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND b.id = %d GROUP BY b.id) UNION 
          (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND b.id = %d GROUP BY b.id)  ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value, $search_value, $start_no, $limit)), ARRAY_A);
      }
      if($search_field == 'booking email'){
        $this->count =  count($this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND email = %s GROUP BY b.id) UNION 
          (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND email = %s GROUP BY b.id)', array( $search_value, $search_value)), ARRAY_A));

        $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND email = %s GROUP BY b.id) UNION 
          (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND email = %s GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value, $search_value, $start_no, $limit)), ARRAY_A);
      } else if($search_field == 'booking name') {
        $this->count =  count($this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND name LIKE \'%%%s%%\' GROUP BY b.id) UNION 
          (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND name LIKE \'%%%s%%\' GROUP BY b.id)', array( $search_value, $search_value)), ARRAY_A));

        $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id WHERE b.complete IS NOT NULL AND name LIKE \'%%%s%%\' GROUP BY b.id) UNION 
          (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND name LIKE \'%%%s%%\' GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value, $search_value, $start_no, $limit)), ARRAY_A);
      } else if($search_field == 'member name') {
            $this->count =  count($this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE  CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id) UNION 
            (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id)', array( $search_value, $search_value)), ARRAY_A));
            $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE b.complete IS NOT NULL AND CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id) UNION 
            (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value, $search_value, $start_no, $limit)), ARRAY_A);
        } else if($search_field == 'member id') {
            $this->count =  count($this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE m.member_id = %d GROUP BY b.id) UNION 
            (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND CONCAT(m.forename, " ", m.surname) LIKE \'%%%s%%\' GROUP BY b.id)', array( intval($search_value), intval($search_value))), ARRAY_A));
            $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE b.complete IS NOT NULL AND m.member_id = %d GROUP BY b.id) UNION 
            (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete IS NOT NULL AND pm.date IS NOT NULL AND m.member_id = %d GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( intval($search_value), intval($search_value), $start_no, $limit)), ARRAY_A);


        } else if ($search_field == 'booking date') {

          $search_value = DateTime::createFromFormat('d/m/Y', date('d/m/Y', strtotime($search_value)));

          $this->count =  count($this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE b.purchased BETWEEN DATE_SUB(%s, INTERVAL 2 DAY) AND DATE_ADD(%s, INTERVAL 2 DAY) GROUP BY b.id) UNION 
            (SELECT b.id FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE  b.purchased BETWEEN DATE_SUB(%s, INTERVAL 2 DAY) AND DATE_ADD(%s, INTERVAL 2 DAY) GROUP BY b.id)', array( $search_value->format('Y-m-d'), $search_value->format('Y-m-d'), $search_value->format('Y-m-d'), $search_value->format('Y-m-d'))), ARRAY_A));
          $this->bookings = $this->_db->get_results($this->_db->prepare('(SELECT b.id, complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, b.purchased, ps.session_date , CONCAT(ps.session_start_time, " - ", ps.session_end_time) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_prepaid_sessions as ps ON b.id = ps.transaction_id LEFT JOIN rwc_members as m ON ps.member_id = m.member_id WHERE b.complete is NOT null AND ps.session_date IS NOT NULL AND b.purchased BETWEEN DATE_SUB(%s, INTERVAL 2 DAY) AND DATE_ADD(%s, INTERVAL 2 DAY) GROUP BY b.id) UNION 
            (SELECT b.id, b.complete, b.email, b.name, b.number, b.session_cost, b.booking_cost, b.refunded, purchased, DATE_FORMAT(pm.date , "%%Y-%%m-%%d") as session_date , CONCAT(pm.start_date, " - ", pm.end_date) as times FROM rwc_transactions_paypal as b LEFT JOIN rwc_paid_membership as pm ON b.id = pm.transaction_id LEFT JOIN rwc_members as m ON pm.member_id = m.member_id WHERE b.complete is NOT null AND pm.date IS NOT NULL AND pm.start_date IS NOT NULL AND b.purchased BETWEEN DATE_SUB(%s, INTERVAL 2 DAY) AND DATE_ADD(%s, INTERVAL 2 DAY) GROUP BY b.id) ORDER BY purchased DESC, name ASC LIMIT %d, %d', array( $search_value->format('Y-m-d'), $search_value->format('Y-m-d') ,$search_value->format('Y-m-d'), $search_value->format('Y-m-d'), $start_no, $limit)), ARRAY_A);

        }
      
    }
  }
}