<?php 
namespace Rampworld\Event\Category;

use Rampworld\DB\Connection as Connection;

class CategoryFetch {
	protected $_db;
	public $categories = array();
	public $count = 0;
	public function __construct() {

		$this->_db  = Connection::connect();
	}

	public function get() {

    $this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_events_categories'), ARRAY_A)[0]['count'];
    $this->categories = $this->_db->get_results('SELECT category_id, display_name 
    FROM rwc_events_categories
    ORDER BY display_name ASC', ARRAY_A);
	}	

	
}
