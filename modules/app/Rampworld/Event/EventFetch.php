<?php 
namespace Rampworld\Event;

use Rampworld\DB\Connection as Connection;

class EventFetch {
	protected $_db;
	public $events = array();
	public $details = array();
	public $count = 0;
	public $statistics = array(
		'booked' 		=> 0,
		'attended'	=> 0
	);
	public $validEvent = false;
	public function __construct() {

		$this->_db  = Connection::connect();
	}

	public function get($eid = null, $cid = null, $name = null, $start_dt = null, $end_dt = null, $start_no = 0, $limit = 16) {

			$bindings = array();
			$query = '';
			$has_query = false;
			$elements = array(array('event.event_id', $eid, '%d'), array('category.category_id', $cid, '%d'), array('event.display_name', $name, '%s'), array('TIMESTAMP(event.date, event.start_time)', $start_dt, '%s'), array('TIMESTAMP(event.date, event.end_time)', $end_dt, '%s'));

			for ($i = 0; $i < count($elements); $i++) {
				if (!empty($elements[$i][1])) {
					if (strlen($query) > 0) {
						$query .= ' AND (';
					} else {
						$query = ' WHERE (';
					}
					if ($elements[$i][2] == '%s') {
						$query .= ' '.$elements[$i][0].' LIKE \'%%%s%%\'';
	
						$bindings[] = trim($elements[$i][1]);
					} else {
						$query .= ' '.$elements[$i][0].' = ' . $elements[$i][2];
						$bindings[] = intval(trim($elements[$i][1]));
					}
		
					$query .= ')';
				}
			}
			$this->count = $this->_db->get_results($this->_db->prepare('SELECT count(*) as count FROM rwc_events as event
			'.$query, $bindings), ARRAY_A)[0]['count'];

			$bindings[] = $start_no;
			$bindings[] = $limit;

			$this->events = $this->_db->get_results($this->_db->prepare('SELECT event.event_id, event.display_name, DATE_FORMAT(event.date, "%%d-%%m-%%Y") as date, DATE_FORMAT(event.start_time, "%%l:%%i%%p") as start_time, DATE_FORMAT(event.end_time, "%%l:%%i%%p") as end_time, event.online_exclusive
			FROM rwc_events as event
			'.$query.' ORDER BY event.date ASC, event.start_time ASC, event.end_time ASC LIMIT %d, %d', $bindings), ARRAY_A);
	}	

	public function getEvent($eid) {
		$this->details = $this->_db->get_results($this->_db->prepare(
			'SELECT event.*,
				count(pre.`prepaid_id`) as booked
			FROM rwc_events as event
			LEFT JOIN rwc_prepaid_sessions as pre ON
				event.date = pre.session_date
				AND event.start_time = pre.session_start_time
				AND event.end_time = pre.session_end_time
			WHERE event.event_id = %d
			LIMIT 1', array(intval($eid))), ARRAY_A);
		$this->statistics['attended'] = $this->_db->get_results(
			$this->_db->prepare(
				'SELECT count(ent.`entry_id`) as attended
				
				FROM rwc_events as event
				LEFT JOIN rwc_members_entries as ent ON
					event.date = ent.entry_date
					AND event.start_time >= DATE_SUB(ent.entry_time, INTERVAL 30 MINUTE)
					AND event.end_time = ent.exit_time
				WHERE event.event_id = %d'
			, array( intval($eid)))
		, ARRAY_A)[0]['attended'];
		$this->statistics['booked'] = $this->details[0]['booked'];


		if(strlen($this->details[0]['event_id']) != 0){
			$this->validEvent = true;
		}
	}

	public function getAllEvents() {
		$this->details = $this->_db->get_results($this->_db->prepare(
			'SELECT event.*,
				count(pre.`prepaid_id`) as booked
			FROM rwc_events as event
			LEFT JOIN rwc_prepaid_sessions as pre ON
				event.date = pre.session_date
				AND event.start_time = pre.session_start_time
				AND event.end_time = pre.session_end_time
			WHERE TIMESTAMP(event.date, event.end_time) > NOW()
			ORDER BY TIMESTAMP(event.date, event.end_time) DESC', array(intval($eid))), ARRAY_A);

		if(count($this->details) != 0){
			$this->validEvent = true;
		}
	}


	public function has_event($date) {
		return $this->_db->get_results($this->_db->prepare("SELECT event_id, display_name, DATE_FORMAT(start_time, '%%H:%%i') as start_time, DATE_FORMAT(end_time, '%%H:%%i') as end_time, discipline_exclusive, beginner, pass_exclusion,'totalBooked' as totalBooked, cost, null as selected, null as isConsecutive, online_exclusive, description FROM rwc_events WHERE date = %s", array($date->format('Y-m-d').' ',$date->format('Y-m-d').' ',  $date->format('Y-m-d'))), ARRAY_A);
	}

	public function format() {
		if( empty($this->details)) return false;
		for( $i = 0; $i < count($this->details); $i++) {
			$this->details[$i]['raw_date'] = $this->details[$i]['date'];
			$date = \DateTime::createFromFormat('Y-m-d', $this->details[$i]['date']);
			$start = \DateTime::createFromFormat('H:i:s', $this->details[$i]['start_time']);
			$end = \DateTime::createFromFormat('H:i:s', $this->details[$i]['end_time']);

			$this->details[$i]['date'] = $date->format('l \t\h\e jS \o\f F Y');
			$this->details[$i]['start_time'] = $start->format('g:i a');
			$this->details[$i]['end_time'] = $end->format('g:i a');
			$this->details[$i]['future-event'] = ($date > Date('Y-m-d'));

			$this->details[$i]['discipline_exclusive'] = $this->formatDisciplines( $this->details[$i]['discipline_exclusive'] );
		}

	}

	public function formatDisciplines( $code ) {
		$disciplines = array();	
		if( substr($code, 0, 1)) {
			$disciplines[] = 'scooter';
		}
		if( substr($code, 1, 1)) {
			$disciplines[] = 'BMX';
		}
		if( substr($code, 2, 1)) {
			$disciplines[] = 'MTB';
		}
		if( substr($code, 3, 1)) {
			$disciplines[] = 'skateboard';
		}
		if( substr($code, 4, 1)) {
			$disciplines[] = 'inline';
		}
		if( substr($code, 5, 1)) {
			$disciplines[] = 'spectator';
		}
		sort($disciplines);
		$code = '';
		for($i = 0; $i < count($disciplines); $i++) {
			$code .= $disciplines[$i];

			if($i == count($disciplines) -2 ) {
				$code .= ', and ';
			} else if($i != count($disciplines) -1 ) {
				$code .= ', ';
			}
		}
		
		return $code;
	}
}
