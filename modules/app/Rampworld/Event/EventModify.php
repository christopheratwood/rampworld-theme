<?php 
namespace Rampworld\Event;

use Rampworld\DB\Connection as Connection;

class EventModify {
	protected $_db;
	public $categories = array();
	public $count = 0;
	public function __construct() {

		$this->_db  = Connection::connect();
	}
  public function create( $data ) {
    $discipline = ((in_array('SMX', $data['discipline'])) ? 1 : 0).
                  ((in_array('BMX', $data['discipline'])) ? 1 : 0).
                  ((in_array('MTB', $data['discipline'])) ? 1 : 0).
                  ((in_array('skateboard', $data['discipline'])) ? 1 : 0).
                  ((in_array('inline', $data['discipline'])) ? 1 : 0).
                  ((in_array('spectator', $data['discipline'])) ? 1 : 0);
    $this->_db->insert(
      'rwc_events',
      array(
        'display_name'          => strval($data['display_name']),
        'date'                  => strval($data['date']),
        'start_time'            => strval($data['start_time']),
        'end_time'              => strval($data['end_time']),
        'cost'                  => floatval($data['price']),
        'discipline_exclusive'  => $discipline,
        'online_exclusive'      => intval($data['online']),
        'pass_exclusion'        => intval($data['pass']),
        'description'           => strval($data['description']),
        'beginner'              => intval($data['beginner']),
        'image_portrait'        => strval($data['image_portrait']),
        'image_landscape'       => strval($data['image_landscape'])
      ),
      array(
        '%s','%s','%s','%s','%d','%s', '%d','%d','%s','%d','%s','%s'
      )
    );
    return $this->_db->insert_id;
  }


  public function update( $eid, $data ) {
    $current_user = wp_get_current_user();

    $discipline = ((in_array('SMX', $data['discipline'])) ? 1 : 0).
                  ((in_array('BMX', $data['discipline'])) ? 1 : 0).
                  ((in_array('MTB', $data['discipline'])) ? 1 : 0).
                  ((in_array('skateboard', $data['discipline'])) ? 1 : 0).
                  ((in_array('inline', $data['discipline'])) ? 1 : 0).
                  ((in_array('spectator', $data['discipline'])) ? 1 : 0);
    $this->_db->update(
      'rwc_events',
      array(
        'display_name'          => strval($data['display_name']),
        'date'                  => strval($data['date']),
        'start_time'            => strval($data['start_time']),
        'end_time'              => strval($data['end_time']),
        'cost'                  => floatval($data['price']),
        'discipline_exclusive'  => $discipline,
        'online_exclusive'      => intval($data['online']),
        'pass_exclusion'        => intval($data['pass']),
        'description'           => strval($data['description']),
        'beginner'              => intval($data['beginner']),
        'image_portrait'        => strval($data['image_portrait']),
        'image_landscape'       => strval($data['image_landscape']),
        'last_updated_user'		  => $current_user->user_firstname.' '. $current_user->user_lastname,
        'updated_amount'			  => intval($data['update_counter']) + 1
      ),
      array(
        'event_id'              => intval( $eid )
      ),
      array(
        '%s','%s','%s','%s','%d','%s', '%d','%d','%s','%d','%s','%s', '%s', '%d'
      ),
      array(
        '%d'
      )
    );
    return true;
  }
	public function delete( $eid ) {
    if(!current_user_can('administrator')){
			wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fsingle&eid='.$eid.'&error=incorrect+permissions');
		} else {
			$this->_db->delete('rwc_events', array('event_id' => $eid));
			wp_redirect(host.'wp-admin/admin.php?page=rampworld-membership%2Fmembership.php%2Fevents%2Fview%2Fall&success=deleted+event&eid='.$eid);


		}
  } 	

	
}
