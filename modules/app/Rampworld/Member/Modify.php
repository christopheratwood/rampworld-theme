<?php
namespace Rampworld\Member;

use Rampworld\DB\Connection as Connection;

class Modify {
	private $_db;
	function __construct( ) {

		$this->_db  = Connection::connect();
	}

	public function create( array $membership_details ) { 


		if( !is_array( $membership_details ) ){
			return false;
		
		} else {
	
			//Change

			$newMemberErrors = [];
			$consentID = null;
			

			if( !empty( $membership_details['consent_name'] ) && !empty( $membership_details['consent_number'] ) && !empty( $membership_details['consent_address_line_one'] ) && !empty( $membership_details['consent_address_postcode'] ) && !empty( $membership_details['consent_email'] ) ) {	
				if( $this->_db->insert( 'rwc_members_consent', array(

					'name' 				=> $membership_details['consent_name'],
					'number' 			=> $membership_details['consent_number'],
					'address_line_one'	=> $membership_details['consent_address_line_one'],
					'address_postcode' 	=> $membership_details['consent_address_postcode'],
					'email' 			=> $membership_details['consent_email'] ) ) == false) {
					
					$newMemberErrors['101'] = $this->_db->last_error;
				
				} else {
					$consentID = $this->_db->insert_id;
				}
			}
			
			//2. insert Member data into rwc_members
			if( $this->_db->insert( 'rwc_members', ['forename' 			=> $membership_details['forename'],
				'surname' 			=> $membership_details['surname'],
				'dob' 				=> date( 'Y-m-d', strtotime( $membership_details['dob'] ) ),
				'gender'			=> $membership_details['gender'],
				'email' 			=> $membership_details['email'],
				'number' 			=> $membership_details['number'],
				'address_line_one'	=> $membership_details['address_line_one'],
				'address_line_two'	=> $membership_details['address_line_two'],
				'address_city' 		=> $membership_details['address_city'],
				'address_county' 	=> $membership_details['address_county'],
				'address_postcode' 	=> $membership_details['address_postcode'],
				'consent_id' 		=> $consentID,
				'discipline' 		=> $membership_details['discipline'],
				'expertise' 		=> $membership_details['expertise'],
				'medical_notes' 	=> $membership_details['medical_notes'] ] ) == false )   {
				$newMemberErrors['102'] = $this->_db->last_error;
				
			} else {
				

				//memberID
				$memberID = $this->_db->insert_id; 
				

				if( $this->_db->insert( 'rwc_members_emergency', [
					'member_id' => $memberID,
					'name' 		=> $membership_details['emergency_name_1'],
					'emergency_number' 	=> $membership_details['emergency_number_1']
					] , [ '%d', '%s', '%s' ] ) == false ) {

					if(strlen($this->_db->last_error) > 0)
						$newMemberErrors['103'] = $this->_db->last_error;
				
				} 

				

				//4. insert emergency details into rwc_members_emergency
				if( !empty( $membership_details['emergency_name_2'] ) && !empty( $membership_details['emergency_number_2'] ) ) {
					if($this->_db->insert('rwc_members_emergency', [
						'member_id' => $memberID,
						'name'		=> $membership_details['emergency_name_2'],
						'emergency_number' 	=> $membership_details['emergency_number_2']
						]  ) == false ) {
						if(strlen($this->_db->last_error) > 0)
							$newMemberErrors['104'] = $this->_db->last_error;

					}
				
				} 

			}

			if( empty( $newMemberErrors ) )
				return $memberID;
			else 
				return $newMemberErrors;
		}

		
		
	}

	public function update ($id, $data) {
		

		$em2Pas = false;
		$additonal = [];
		
		$consent_req = (!empty( $data['consent_name'] ) && !empty( $data['consent_number']) && !empty( $data['consent_address_line_one'] ) && !empty( $data['consent_address_postcode'] ) && !empty( $data['consent_email'] )) ? true : false;



		if( $this->_db->update( 'rwc_members', [
				'additional_verification' => true,
				'forename' 			=> $data['forename'],
				'surname' 			=> $data['surname'],
				'dob' 				=> date( 'Y-m-d', strtotime( $data['dob'] ) ),
				'gender'			=> $data['gender'],
				'email' 			=> $data['email'],
				'number' 			=> $data['number'],
				'address_line_one'	=> $data['address_line_one'],
				'address_line_two'	=> $data['address_line_two'],
				'address_city' 		=> $data['address_city'],
				'address_county' 	=> $data['address_county'],
				'address_postcode' 	=> $data['address_postcode'],
				'discipline' 		=> $data['discipline'],
				'expertise' 		=> $data['expertise'],
				'medical_notes' 	=> $data['medical_notes'] ], ['member_id' => $id] ) === false ) {
			return $this->_db->last_error;
		} else {
			if( $data['consent_id'] != '') {

				$today = new DateTime(date('d-m-Y H:i:s'));
				$adob = new DateTime($data['dob']);
				$intval = $today->diff($adob);

				if(intval($intval->y) > 15) {
					//delete
					if( $this->_db->update( 'rwc_members', [
						'consent_id' => null], ['member_id' => $id] ) === false ) {
						return $this->_db->last_error;
					} else {
						if( $this->_db->delete( 'rwc_members_consent', [ 'consent_id' => $data['consent_id'] ] ) === false) {
							return $this->_db->last_query;
						} 
					}

				} else {
					if( $this->_db->update( 'rwc_members_consent', [
						'name' 				=> $data['consent_name'],
						'number' 			=> $data['consent_number'],
						'address_line_one'	=> $data['consent_address_line_one'],
						'address_postcode' 	=> $data['consent_address_postcode'],
						'email' 			=> $data['consent_email'] ], ['consent_id' => $data['consent_id']])  === false )  {
						return $this->_db->last_error;
					
					}
				}
			} else {
				//insert
				if( $this->_db->insert( 'rwc_members_consent', array(

					'name' 				=> $data['consent_name'],
					'number' 			=> $data['consent_number'],
					'address_line_one'	=> $data['consent_address_line_one'],
					'address_postcode' 	=> $data['consent_address_postcode'],
					'email' 			=> $data['consent_email'] ) ) == false) {

						return $this->_db->last_error;
					} else {
						$consentID = $this->_db->insert_id;
						if( $this->_db->update( 'rwc_members', [
						'consent_id' => $consentID], ['member_id' => $id] ) === false ) {
							return $this->_db->last_error;
						}
					}
			}

			
				
			if( $this->_db->update( 'rwc_members_emergency', [
				'name' 		=> $data['emergency_name_1'],
				'emergency_number' 	=> $data['emergency_number_1']
				] , ['emergency_id' => $data['emergency_id_1']] ) === false ) {
				return $this->_db->last_query;
			} else {
				
				if( !empty( $data['emergency_2_id'] ) ) {
					
					if( empty( $data['emergency_name_2'] ) && empty( $data['emergency_number_2'] ) ) {
						//delete record
						if( $this->_db->delete( 'rwc_members_emergency', [ 'emergency_id' => $data['emergency_id_2'] ] ) === false) {
							return $this->_db->last_query;
						} else {
							$em2Pas = true;
						}
						
					} else {
						if( $this->_db->update( 'rwc_members_emergency', [
							'name' 		=> $data['emergency_name_2'],
							'emergency_number' 	=> $data['emergency_number_2']
							] , ['emergency_id' => $data['emergency_id_2'] ] ) === false ) {
							return $this->_db->last_query;
						} else {
							$em2Pas = true;
						}
					}
				} else { 
				
					if( !empty( $data['emergency_name_2'] ) && !empty( $data['emergency_number_2'] ) ) {

						if($this->_db->insert('rwc_members_emergency', [
							'member_id' => $id,
							'name' 		=> $data['emergency_name_2'],
							'emergency_number' 	=> $data['emergency_number_2']
							] , [ '%d', '%s', '%s' ] ) === false ) {
							
							return $this->_db->last_query;

						} else {
							$em2Pas = true;
						}
					} else {
						$em2Pas = true;
					}

				}
				
			} if( $em2Pas == true) {

				require_once 'RandomGenerator.php';
				require_once 'Mailer.php';
				

				$randomGen = new RandomGenerator();
				$cus_ref = $randomGen->generate();
				$consent_ref = null;


				while( $this->_db->get_var( 'SELECT COUNT(`member_action_id`) FROM `rwc_members_actions` WHERE member_verification = \''. $cus_ref .'\' LIMIT 1') > 0  ) {
					$cus_ref = $randomGen->generate();
				}
				$today = new DateTime(date('d-m-Y H:i:s'));
				$adob = new DateTime($data['dob']);
				$intval = $today->diff($adob);

				if(intval($intval->y) < 16) {
					$consent_ref = $randomGen->generate();
					while( $this->_db->get_var( 'SELECT COUNT(`member_action_id`) FROM `rwc_members_actions` WHERE consent_verification = \''. $consent_ref .'\' LIMIT 1') > 0  ) {
						$consent_ref = $randomGen->generate();
					}
				}
				//check if member already has action
			
				if( $this->_db->insert(
					'rwc_members_actions',
					[
						'action_type' 			=> 1,
						'member_id' 			=> $id,
						'member_verification' 	=> $cus_ref,
						'consent_verification'	=> $consent_ref
					] , [ '%d','%d', '%s', '%s' ] ) === false ) {

				} else {
					
					$additonal['memberID'] = $id;
					$additonal['member_verification'] = $cus_ref;
					$additonal['consent_verification'] = $consent_ref;
					//mail
					$mail = new Mailer( array_merge($data , $additonal));
					
					if( $mail->create_account_change(  ) == true ) {
						if( $mail->send() == true ) {
							if(intval($intval->y) < 16) {
								$mail2 = new Mailer( array_merge($data , $additonal));

								if( $mail2->create_account_change_consent(  ) == true ) {
									if( $mail2->send() == true ) {
										return true;
									} else {
										return $mail2->errorMessages();
									}

								//done 
									//return true;
								} else {
									
									return $mail->errorMessages();
								
								}
							} else {
								return true;
							}
							
						} else {
							return $mail->errorMessages();
						}
					} else {
						return $mail->errorMessages();
					}
				}


			} else {
				return false;
			}
			
		}
  }
  
  public function delete () {

  }
}