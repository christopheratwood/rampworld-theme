<?php

namespace Rampworld\Calendar;

use Rampworld\DB\Connection as Connection;
date_default_timezone_set('Europe/London');

class ClosedDates{

	public $sessions = array();
	protected $_db,
			$_session;

	public function __construct() {
		$this->_db  = Connection::connect();

  }
  
	public function get_alert() {
    $non_operational = $this->_db->get_results('SELECT date_format(date, "%d/%m/%Y") as d FROM rwc_closed_dates WHERE date >= CURDATE() AND visible_date <= CURDATE() ORDER BY date aSC', ARRAY_A);
    if(count($non_operational) != 0){
      $non_operational_html = '<div class="alert alert-danger" style="margin-top: 20px"><p class="lead"><strong>Important notice</strong></p><div><p>RampWorld Cardiff is closed on the following dates: <strong>';
      for($i = 0; $i< count($non_operational); $i++){
          if($i > 0) {
            if($i == count($non_operational) - 1 ) {
              $non_operational_html .= ', and '.$non_operational[$i]['d'];
            } else {
              $non_operational_html .= ', '.$non_operational[$i]['d'];
            }
          } else {
            $non_operational_html .= $non_operational[$i]['d'];
          }
        }
        $non_operational_html .='.</strong></p></div>';
    } else {
      $non_operational_html = '';
    }
    return $non_operational_html;
	
	}
}