<?php namespace Rampworld\AssetFinder;

class SVG {
	private static $location = __DIR__ . '/../../../../assets/dist/svgs/';

  public static function get($name) {
    if( file_exists( self::$location . str_replace(' ', '-', strtolower($name)) . '.svg' ) ) {
      return file_get_contents( self::$location . str_replace(' ', '-', strtolower($name)) . '.svg' );
    } else {
      return file_get_contents( self::$location . 'missing.svg' );
    }
    
  }
}