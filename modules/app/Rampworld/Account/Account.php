<?php namespace Rampworld\Account;



require_once 'Logging.php';
session_start();
class Account extends Logging{
	private $_db;
  public $account = array();
  public function __construct() {
    parent::__construct();
    global $wpdb;

    if(!isset($wpdb))
    {
        //the '../' is the number of folders to go up from the current file to the root-map.
        require_once('../../../../../wp-config.php');
        require_once('../../../../../wp-includes/wp-db.php');
    }
    $this->_db = new wpdb(DB_MEMBER_USER, DB_MEMBER_PASSWORD, DB_MEMBER_NAME, DB_MEMBER_HOST);
    $this->started = (isset($_SESSION['progress'])) ? true: false;
  }
  public function create($member_id, $username, $password) {
    if($this->_db->get_results($this->_db->prepare('SELECT count(member_id) as exists FROM rwc_members WHERE member_id = %d', array($member_id)), ARRAY_A)[0]['exists'] != null) {
      if($this->_db->get_results($this->_db->prepare('SELECT count(account_id) as exists FROM rwc_accounts WHERE username = %s', array($username)), ARRAY_A)[0]['exists'] != null) {
        require_once 'Password.php';
        $hash = password_hash($password, PASSWORD_BCRYPT, array("cost" => 13));

        $this->_db->insert(
          'rwc_accounts',
          array(
            'member_id'   => $member_id,
            'username'    => $username,
            'password'    => $hash
          ),
          array(
            '%d',
            '%s'
          )
        );
        return $this->_db->insert_id;
      } else {
        return array(
          'This username is taken'
        );
      }
    } else {
      return array(
        'This member does not exist'
      );
    }
    
  }
  public function check($username, $password) {
    
    $hash = $this->_db->get_results($this->_db->prepare('SELECT account_id, forename, surname, number, email, password, locked, verified FROM rwc_accounts as acc LEFT JOIN rwc_members as mem on acc.member_id = mem.member_id WHERE acc.username = %s OR mem.email = %s LIMIT 1', array($username, $username)), ARRAY_A);
    if(isset($hash[0]) && $hash[0]['password'] != null) {
      if($hash[0]['locked'] == false) {
        require_once 'Password.php';
        
        if (password_verify($password, $hash[0]['password'])) {
          $this->addAccountLog($hash[0]['account_id'], 1);

          $this->account = array(
            'name'    => ucwords($hash[0]['forename'] . ' ' . $hash[0]['surname']),
            'email'   => strtolower($hash[0]['email']),
            'number'  => strtolower($hash[0]['number']),
            'account_id'  => $hash[0]['account_id']
          );
          $_SESSION['account']['name'] = ucwords($hash[0]['forename'] . ' ' . $hash[0]['surname']);
          $_SESSION['account']['email'] = strtolower($hash[0]['email']);
          $_SESSION['account']['number'] = $hash[0]['number'];
          $_SESSION['account']['account_id'] = $hash[0]['account_id'];
          
          return true;
        } else {
          $this->addAccountLog($hash[0]['account_id'], 3);
          return false;
        }
      } else {
        $this->addAccountLog($hash[0]['account_id'], 4);
        return array(
          "errorCode" => 2,
          "errorMessage" => 'This account has been locked.'
        );
      }
    } else {
      return false;
   }
  }

  
  public function logout($account_id) {
    $this->addAccountLog($account_id, 2);
    unset($_SESSION['account']);
    return true;
    

  }

}