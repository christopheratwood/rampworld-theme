<?php

require __DIR__ .'/modules/vendor/autoload.php';
use Rampworld\Session\Fetch as Fetch;

/**
 * The template for displaying pages
 *
 * @package WordPress
 * @subpackage BirdFILED
 * @si
 */
get_header();

$tmpSession = [];
$name = '';

//get all sessions
$sessions = FetchSesion();
$today = $sessions->getTodays();
global $wpdb;

$sessions = $wpdb->get_results('Select * FROM `rwc_sessions`order By day ASC, start_time ASC', ARRAY_A);
if( empty( $sessions ) ) {
	$sessionsHTML = '<h1>Sorry, There are no Sessions :( </h1>';
} else {
	$sessionsHTML = '';
	for ($i=0; $i < count($sessions); $i++) {
		if( strlen($sessions[$i]['display_name']) > 0)
			$name = ' (' . ucwords($sessions[$i]['display_name']) . ');';
		else
			$name = '';
		

		$tmpSession[$sessions[$i]['day']][] = $sessions[$i]['start_time']. ' - ' . $sessions[$i]['end_time']. $name ;
	}
	//This fixes the issue ofr having sunday be first on timetable
	
	//create temp sunday array holding times
	$sunday = $tmpSession[0];

	//Remove orignal sunday times
	unset($tmpSession[0]);

	//Re add sunday to end of array
	$tmpSession[0] = $sunday;

	//END if fix
	
	

	//Loop through each day
	foreach ( $tmpSession as $day => $value ) {
		$dayName = '';

		switch ( $day ) {
		 	case 1:
		 		$dayName = 'Monday';
		 		break;
		 	case 2:
		 		$dayName = 'Tuesday';
		 		break;
		 	case 3:
		 		$dayName = 'Wednesday';
		 		break;
		 	case 4:
		 		$dayName = 'Thursday';
		 		break;
		 	case 5:
		 		$dayName = 'Friday';
		 		break;
		 	case 6:
		 		$dayName = 'Saturday';
		 		break;
		 	case 0:
		 		$dayName = 'Sunday';
		 		break;
		 	default:
		 		# code...
		 		break;
		}


		
		$sessionsHTML .= '<tr><td data-label="Day">'.$dayName.'</td>';
		
		$last = end( $value );

	 	foreach ( $value as $key => $val ) {

	 		switch ( $key ) {
	 			case 0:
	 				$label = 'First Session';
	 				break;
	 			case 1:
	 				$label = 'Second Session';
	 				break;
	 			case 2:
	 				$label = 'Third Session';
	 				break;
	 			case 3:
	 				$label = 'Fourth Session';
	 				break;
	 			case 4:
	 				$label = 'Fifth Session';
	 				break;
	 			case 5:
	 				$label = 'Sixth Session';
	 				break;
	 			default:
	 				# code...
	 				break;
	 		}
	 		$sessionsHTML .= '<td data-label="' . $label . '">' . $val . '</td>';
	 		
	 		// Renderss the timetable evenly
	 		if( $val == $last ) {
	 			for ($i = $key + 1; $i < 5; $i++) { 
	 				$sessionsHTML .= '<td class="noRender"></td>';
	 			}
	 		}
	 	}

	 	$sessionsHTML .= '</tr>';
	}
}
?>
<div id="pageTitle">
	<p>Sessions Times</p>
</div>
<div id="content">
	<div class="container center">
		<h1>Sessions</h1>
		<p class="medium">Below is a breakdown of the sessions available at RampWorld Cardiff.</p>
		<table class="clean">
        <thead>
            <th>Day</th>
            <th>First Session</th>
            <th>Second Session</th>
            <th>Third Session</th>
            <th>Forth Session</th>
            <th>Fifth Session</th>

        </thead>
        <tbody>
	
			<?php echo $sessionsHTML;?>
		</tbody>
		</table>
	</div>
</div>
<?php
get_footer();
?>